import 'package:epbeira/Controller/Routes.dart';
import 'package:epbeira/Database/database.dart';
import 'package:epbeira/View/Settings.dart';
import 'package:epbeira/View/UsersList.dart';
import 'package:flutter/material.dart';
import 'package:provider/provider.dart';

void main() {
 
  runApp(MyApp());

}

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return Provider(create: (_) => MyDatabase(),
      child:MaterialApp(
        debugShowCheckedModeBanner: false,
        title: 'EpBeira',
        theme: ThemeData(
          primarySwatch: Colors.blue,
          visualDensity: VisualDensity.adaptivePlatformDensity,
        ),
        home: MyHomePage(title: 'EpBeira'),
      ),
    );
  }
}

class MyHomePage extends StatefulWidget {
  
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      title: widget.title,
      theme: ThemeData(
      primarySwatch: Colors.blue,
      visualDensity: VisualDensity.adaptivePlatformDensity,
      ),
      routes:{
        AppRoutes.Home:(_) => HomePage(),
        AppRoutes.SETTINGS:(_) => Settigs(),
      }
    );
  }
}
