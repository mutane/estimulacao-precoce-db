//import 'package:flutter/widgets.dart';
part of 'package:epbeira/View/UsersList.dart';
//'package:epbeira/View/SearchData.dart';

 Route _createRoute(args,object) {
  return PageRouteBuilder(

    pageBuilder: (context, animation, secondaryAnimation) => object,
    settings: RouteSettings(
        arguments: args
    ),
    transitionsBuilder: (context, animation, secondaryAnimation, child) {
      var begin = Offset(2.0, 0.0);
      var end = Offset.zero;
      var curve = Curves.ease;
      var tween = Tween(begin:begin,end:end).chain(CurveTween(curve: curve));
      return SlideTransition(
        position: animation.drive(tween),
        child: child,
      );
    },
  );
}
