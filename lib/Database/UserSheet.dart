import 'package:epbeira/Database/database.dart';
import 'package:moor/moor.dart';

class UserSheet extends User{
   User fromJsonSheet(Map<String, dynamic> json,GeneratedDatabase db,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    //final stringType = db.typeSystem.forDartType<String>();
    //final dateTimeType = db.typeSystem.forDartType<DateTime>();
  //  final intType = db.typeSystem.forDartType<int>();
    //final doubleType = db.typeSystem.forDartType<double>();

    return User(
      uuid: serializer.fromJson<String>(json['uuid']),
      nomeDoPesquisador: serializer.fromJson<String>(json['nomeDoPesquisador']),
      numeroDeInquerito: serializer.fromJson<String>(json['numeroDeInquerito']),
      data: DateTime.parse(json['data']),
      municipio: serializer.fromJson<String>(json['municipio']),
      bairro: serializer.fromJson<String>(json['bairro']),
      pontoDeReferenciaDaCasa:
          serializer.fromJson<String>(json['pontoDeReferenciaDaCasa']),
      nome: serializer.fromJson<String>(json['nome']),
      idade: serializer.fromJson<String>(json['idade']),
      contacto: serializer.fromJson<String>(json['contacto']),
      eChefeDaFamilia: serializer.fromJson<String>(json['eChefeDaFamilia']),
      caoNaoVinculoDeParantesco:
          serializer.fromJson<String>(json['caoNaoVinculoDeParantesco']),
      idadeDoChefeDaFamilia:
          serializer.fromJson<String>(json['idadeDoChefeDaFamilia']),
      estudou: serializer.fromJson<String>(json['estudou']),
      eAlfabetizado: serializer.fromJson<String>(json['eAlfabetizado']),
      completouEnsinoPrimario:
          serializer.fromJson<String>(json['completouEnsinoPrimario']),
      completouEnsinoSecundario:
          serializer.fromJson<String>(json['completouEnsinoSecundario']),
      fezCursoSuperior: serializer.fromJson<String>(json['fezCursoSuperior']),
      naoSabe: serializer.fromJson<String>(json['naoSabe']),
      chefeDeFamiliaTemSalario:
          serializer.fromJson<String>(json['chefeDeFamiliaTemSalario']),
      quantoGastaParaSustentarFamiliaPorMes:serializer.fromJson<String>(json['quantoGastaParaSustentarFamiliaPorMes']),
      quantasFamiliasResidemNacasa:
          serializer.fromJson<String>(json['quantasFamiliasResidemNacasa']),
      quantosAdultos: serializer.fromJson<String>(json['quantosAdultos']),
      quantosAdultosMaioresDe18anos:
          serializer.fromJson<String>(json['quantosAdultosMaioresDe18anos']),
      quantosAdultosMaioresDe18anosHomens: serializer
          .fromJson<String>(json['quantosAdultosMaioresDe18anosHomens']),
      quantosAdultosMaioresDe18anosHomensTrabalham: serializer.fromJson<String>(
          json['quantosAdultosMaioresDe18anosHomensTrabalham']),
      quantosAdultosMaioresDe18anosHomensAlfabetizados:
          serializer.fromJson<String>(
              json['quantosAdultosMaioresDe18anosHomensAlfabetizados']),
      quantosAdultosM18Mulheres:
          serializer.fromJson<String>(json['quantosAdultosM18Mulheres']),
      quantosAdultosM18MulheresTrabalham: serializer
          .fromJson<String>(json['quantosAdultosM18MulheresTrabalham']),
      quantosAdultosM18MulheresAlfabetizados: serializer
          .fromJson<String>(json['quantosAdultosM18MulheresAlfabetizados']),
      relatosDeDeficiencia:
          serializer.fromJson<String>(json['relatosDeDeficiencia']),
      totalcriancasAte1anoemeio:
          serializer.fromJson<String>(json['totalcriancasAte1anoemeio']),
      totalcriancasAte1anoemeioMeninos:
          serializer.fromJson<String>(json['totalcriancasAte1anoemeioMeninos']),
      totalcriancasAte1anoemeioMeninas:
          serializer.fromJson<String>(json['totalcriancasAte1anoemeioMeninas']),
      algumasCriancasAte1anoemeioNaodesenvolvem: serializer
          .fromJson<String>(json['algumasCriancasAte1anoemeioNaodesenvolvem']),
      algumasCriancasAte1anoemeioNaodesenvolvemQuantas:
          serializer.fromJson<String>(
              json['algumasCriancasAte1anoemeioNaodesenvolvemQuantas']),
      algumaCriancasAte1anoemeioQueTipodeProblema: serializer.fromJson<String>(
          json['algumaCriancasAte1anoemeioQueTipodeProblema']),
      algumaCriancasAte1anoemeioTemProblemaMotor: serializer
          .fromJson<String>(json['algumaCriancasAte1anoemeioTemProblemaMotor']),
      algumaCriancasAte1anoemeioTemProblemaMotorQuantas:
          serializer.fromJson<String>(
              json['algumaCriancasAte1anoemeioTemProblemaMotorQuantas']),
      algumaCriancasAte1anoemeioTemMalFormacao: serializer
          .fromJson<String>(json['algumaCriancasAte1anoemeioTemMalFormacao']),
      algumaCriancasAte1anoemeioTemMalFormacaoQuantas:
          serializer.fromJson<String>(
              json['algumaCriancasAte1anoemeioTemMalFormacaoQuantas']),
      algumaCriancasAte1anoemeioNaoReagemAIncentivo:
          serializer.fromJson<String>(
              json['algumaCriancasAte1anoemeioNaoReagemAIncentivo']),
      algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas:
          serializer.fromJson<String>(
              json['algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas']),
      algumaCriancasAte1anoemeioTemReacaoAgressiva: serializer.fromJson<String>(
          json['algumaCriancasAte1anoemeioTemReacaoAgressiva']),
      algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas:
          serializer.fromJson<String>(
              json['algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas']),
      totalcriancasAte1anoemeioA3anos:
          serializer.fromJson<String>(json['totalcriancasAte1anoemeioA3anos']),
      totalcriancasAte1anoemeioA3anosMeninos: serializer
          .fromJson<String>(json['totalcriancasAte1anoemeioA3anosMeninos']),
      totalcriancasAte1anoemeioA3anosMeninas: serializer
          .fromJson<String>(json['totalcriancasAte1anoemeioA3anosMeninas']),
      algumasCriancasAte1anoemeioA3anosNaodesenvolvem:
          serializer.fromJson<String>(
              json['algumasCriancasAte1anoemeioA3anosNaodesenvolvem']),
      algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas:
          serializer.fromJson<String>(
              json['algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas']),
      algumaCriancasAte1anoemeioA3anosQueTipodeProblema:
          serializer.fromJson<String>(
              json['algumaCriancasAte1anoemeioA3anosQueTipodeProblema']),
      algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas:
          serializer.fromJson<String>(
              json['algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas']),
      algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas:
          serializer.fromJson<String>(json[
              'algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas']),
      algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo:
          serializer.fromJson<String>(json[
              'algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo']),
      algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas:
          serializer.fromJson<String>(json[
              'algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas']),
      algumasCriancasAte1anoemeioA3anosAlgunsAgridem:
          serializer.fromJson<String>(
              json['algumasCriancasAte1anoemeioA3anosAlgunsAgridem']),
      algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas:
          serializer.fromJson<String>(
              json['algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas']),
      algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal:
          serializer.fromJson<String>(json[
              'algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal']),
      algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas:
          serializer.fromJson<String>(json[
              'algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas']),
      algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao:
          serializer.fromJson<String>(
              json['algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao']),
      algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas:
          serializer.fromJson<String>(json[
              'algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas']),
      algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas:
          serializer.fromJson<String>(
              json['algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas']),
      algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas:
          serializer.fromJson<String>(json[
              'algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas']),
      algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente:
          serializer.fromJson<String>(json[
              'algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente']),
      algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas:
          serializer.fromJson<String>(json[
              'algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas']),
      algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos:
          serializer.fromJson<String>(json[
              'algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos']),
      algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas:
          serializer.fromJson<String>(json[
              'algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas']),
      algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown:
          serializer.fromJson<String>(json[
              'algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown']),
      algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas:
          serializer.fromJson<String>(json[
              'algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas']),
      totalcriancasde3A6anos:
          serializer.fromJson<String>(json['totalcriancasde3A6anos']),
      totalcriancasde3A6anosMeninos:
          serializer.fromJson<String>(json['totalcriancasde3A6anosMeninos']),
      totalcriancasde3A6anosMeninas:
          serializer.fromJson<String>(json['totalcriancasde3A6anosMeninas']),
      algumasCriancasde3A6anosNaodesenvolvem: serializer
          .fromJson<String>(json['algumasCriancasde3A6anosNaodesenvolvem']),
      algumasCriancasde3A6anosNaodesenvolvemQuantas:
          serializer.fromJson<String>(
              json['algumasCriancasde3A6anosNaodesenvolvemQuantas']),
      algumaCriancasde3A6anosQueTipodeProblema: serializer
          .fromJson<String>(json['algumaCriancasde3A6anosQueTipodeProblema']),
      algumasCriancasde3A6anosNaoInteragemComPessoas:
          serializer.fromJson<String>(
              json['algumasCriancasde3A6anosNaoInteragemComPessoas']),
      algumasCriancasde3A6anosNaoInteragemComPessoasQuantas:
          serializer.fromJson<String>(
              json['algumasCriancasde3A6anosNaoInteragemComPessoasQuantas']),
      algumasCriancasde3A6anosTemComportamentoAgressivo:
          serializer.fromJson<String>(
              json['algumasCriancasde3A6anosTemComportamentoAgressivo']),
      algumasCriancasde3A6anosTemComportamentoAgressivoQuantas:
          serializer.fromJson<String>(
              json['algumasCriancasde3A6anosTemComportamentoAgressivoQuantas']),
      algumasCriancasde3A6anosAlgunsAgridem: serializer
          .fromJson<String>(json['algumasCriancasde3A6anosAlgunsAgridem']),
      algumasCriancasde3A6anosAlgunsAgridemQuantas: serializer.fromJson<String>(
          json['algumasCriancasde3A6anosAlgunsAgridemQuantas']),
      algumasCriancasde3A6anosAlgumasAgitadasQueONormal:
          serializer.fromJson<String>(
              json['algumasCriancasde3A6anosAlgumasAgitadasQueONormal']),
      algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas:
          serializer.fromJson<String>(
              json['algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas']),
      algumasCriancasde3A6anosAlgumasTemMalFormacao:
          serializer.fromJson<String>(
              json['algumasCriancasde3A6anosAlgumasTemMalFormacao']),
      algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas:
          serializer.fromJson<String>(
              json['algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas']),
      algumasCriancasde3A6anosAlgumasNaoAndamSozinhas:
          serializer.fromJson<String>(
              json['algumasCriancasde3A6anosAlgumasNaoAndamSozinhas']),
      algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas:
          serializer.fromJson<String>(
              json['algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas']),
      algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente:
          serializer.fromJson<String>(
              json['algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente']),
      algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas:
          serializer.fromJson<String>(json[
              'algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas']),
      algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos:
          serializer.fromJson<String>(
              json['algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos']),
      algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas:
          serializer.fromJson<String>(json[
              'algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas']),
      algumasCriancasde3A6anosAlgumasTemSindromeDeDown:
          serializer.fromJson<String>(
              json['algumasCriancasde3A6anosAlgumasTemSindromeDeDown']),
      algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas:
          serializer.fromJson<String>(
              json['algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas']),
      algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas:
          serializer.fromJson<String>(json[
              'algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas']),
      algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas:
          serializer.fromJson<String>(json[
              'algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas']),
      algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas:
          serializer.fromJson<String>(json[
              'algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas']),
      algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas:
          serializer.fromJson<String>(json[
              'algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas']),
      algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas:
          serializer.fromJson<String>(json[
              'algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas']),
      algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas:
          serializer.fromJson<String>(json[
              'algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas']),
      algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas:
          serializer.fromJson<String>(json[
              'algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas']),
      algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas:
          serializer.fromJson<String>(json[
              'algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas']),
      quantasCriancasde3A6anosEstaoNaEscolinha: serializer
          .fromJson<String>(json['quantasCriancasde3A6anosEstaoNaEscolinha']),
      criancasDe3A6anosNaoEstaoNaEscolinhaPorque: serializer
          .fromJson<String>(json['criancasDe3A6anosNaoEstaoNaEscolinhaPorque']),
      quantasCriancasde3A6anosTrabalham: serializer
          .fromJson<String>(json['quantasCriancasde3A6anosTrabalham']),
      criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos:
          serializer.fromJson<String>(
              json['criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos']),
      totalcriancasde7A9anos:
          serializer.fromJson<String>(json['totalcriancasde7A9anos']),
      totalcriancasde7A9anosMeninos:
          serializer.fromJson<String>(json['totalcriancasde7A9anosMeninos']),
      totalcriancasde7A9anosMeninas:
          serializer.fromJson<String>(json['totalcriancasde7A9anosMeninas']),
      quantasCriancasde7A9anosEstaoNaEscola: serializer
          .fromJson<String>(json['quantasCriancasde7A9anosEstaoNaEscola']),
      quantasCriancasde7A9anosTrabalham: serializer
          .fromJson<String>(json['quantasCriancasde7A9anosTrabalham']),
      nosUltimos5AnosQuantasGravidezesTiveNaFamilia:
          serializer.fromJson<String>(
              json['nosUltimos5AnosQuantasGravidezesTiveNaFamilia']),
      partosOcoridosNoHospital:
          serializer.fromJson<String>(json['partosOcoridosNoHospital']),
      partosOcoridosEmCasa:
          serializer.fromJson<String>(json['partosOcoridosEmCasa']),
      partosOcoridosEmOutroLocal:
          serializer.fromJson<String>(json['partosOcoridosEmOutroLocal']),
      outroLocalDoPartoQualLocal:
          serializer.fromJson<String>(json['outroLocalDoPartoQualLocal']),
      quantasCriancasNasceramVivas:
          serializer.fromJson<String>(json['quantasCriancasNasceramVivas']),
      quantasContinuamVivas:
          serializer.fromJson<String>(json['quantasContinuamVivas']),
      dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram:
          serializer.fromJson<String>(json[
              'dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram']),
      descreverOProblema:
          serializer.fromJson<String>(json['descreverOProblema']),
      aFamiliaProcurouAjudaParaSuperarEsseProblema: serializer.fromJson<String>(
          json['aFamiliaProcurouAjudaParaSuperarEsseProblema']),
      seSimQueTipoDeAjudaEOnde:
          serializer.fromJson<String>(json['seSimQueTipoDeAjudaEOnde']),
      osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia:
          serializer.fromJson<String>(json[
              'osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia']),
      dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida:
          serializer.fromJson<String>(json[
              'dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida']),
      sabeDizerOMotivoDosObitos:
          serializer.fromJson<String>(json['sabeDizerOMotivoDosObitos']),
      seSimQualFoiPrincipalRazaodoObito: serializer
          .fromJson<String>(json['seSimQualFoiPrincipalRazaodoObito']),
      voceTemMedoDeTerUmFilhoOuNetoComProblemas: serializer
          .fromJson<String>(json['voceTemMedoDeTerUmFilhoOuNetoComProblemas']),
      expliquePorQue: serializer.fromJson<String>(json['expliquePorQue']),
      porqueVoceAchaQueUmaCriancaPodeNascerComProblema:
          serializer.fromJson<String>(
              json['porqueVoceAchaQueUmaCriancaPodeNascerComProblema']),
      oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema:
          serializer.fromJson<String>(
              json['oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema']),
      achaQueUmaCriancaQueNasceComProblemaPodeSobreviver:
          serializer.fromJson<String>(
              json['achaQueUmaCriancaQueNasceComProblemaPodeSobreviver']),
      expliquePorque: serializer.fromJson<String>(json['expliquePorque']),
      quemPodeAjudarQuandoUmaCriancaNasceComProblema:
          serializer.fromJson<String>(
              json['quemPodeAjudarQuandoUmaCriancaNasceComProblema']),
      emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao:
          serializer.fromJson<String>(
              json['emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao']),
      emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene:
          serializer.fromJson<String>(json[
              'emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene']),
      emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos:
          serializer.fromJson<String>(
              json['emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos']),
      aPessoaEntrevistadaPareceuSinceraNasSuasRespostas:
          serializer.fromJson<String>(
              json['aPessoaEntrevistadaPareceuSinceraNasSuasRespostas']),
      algumasPerguntasIncomodaramAPessoaEntrevistada:
          serializer.fromJson<String>(
              json['algumasPerguntasIncomodaramAPessoaEntrevistada']),
      seSimQuais3PrincipaisPerguntas:
          serializer.fromJson<String>(json['seSimQuais3PrincipaisPerguntas']),
      todasAsPerguntasIncomodaram:
          serializer.fromJson<String>(json['todasAsPerguntasIncomodaram']),
      aResidenciaEraDe: serializer.fromJson<String>(json['aResidenciaEraDe']),
      comoAvaliaHigieneDaCasa:
          serializer.fromJson<String>(json['comoAvaliaHigieneDaCasa']),
      oQueEraMal: serializer.fromJson<String>(json['oQueEraMal']),
      tinhaCriancasNaCasaDuranteInquerito: serializer
          .fromJson<String>(json['tinhaCriancasNaCasaDuranteInquerito']),
      tinhaCriancasNaCasaDuranteInqueritoQuantas: serializer
          .fromJson<String>(json['tinhaCriancasNaCasaDuranteInqueritoQuantas']),
      tentouIncentivar: serializer.fromJson<String>(json['tentouIncentivar']),
      tentouIncentivarQuantas:
          serializer.fromJson<String>(json['tentouIncentivarQuantas']),
      seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos:
          serializer.fromJson<String>(json[
              'seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos']),
      observouCriancasDeAte1Ano:
          serializer.fromJson<String>(json['observouCriancasDeAte1Ano']),
      observouCriancasDeAte1AnoQuantos:
          serializer.fromJson<String>(json['observouCriancasDeAte1AnoQuantos']),
      algumasApresentavamProblemas:
          serializer.fromJson<String>(json['algumasApresentavamProblemas']),
      algumasApresentavamProblemasQuantos: serializer
          .fromJson<String>(json['algumasApresentavamProblemasQuantos']),
      descreveOProblemaDessasCriancas:
          serializer.fromJson<String>(json['descreveOProblemaDessasCriancas']),
      observouCriancasDe1AnoEmeioAte3Anos: serializer
          .fromJson<String>(json['observouCriancasDe1AnoEmeioAte3Anos']),
      observouCriancasDe1AnoEmeioAte3AnosQuantas: serializer
          .fromJson<String>(json['observouCriancasDe1AnoEmeioAte3AnosQuantas']),
      algumasCriancasDe1anoEmeioObservadasApresentavamProblemas:
          serializer.fromJson<String>(json[
              'algumasCriancasDe1anoEmeioObservadasApresentavamProblemas']),
      algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas:
          serializer.fromJson<String>(json[
              'algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas']),
      algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve:
          serializer.fromJson<String>(json[
              'algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve']),
      observouCriancasDe3AnosAte6Anos:
          serializer.fromJson<String>(json['observouCriancasDe3AnosAte6Anos']),
      observouCriancasDe3AnosAte6AnosQuantas: serializer
          .fromJson<String>(json['observouCriancasDe3AnosAte6AnosQuantas']),
      observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas:
          serializer.fromJson<String>(json[
              'observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas']),
      observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas:
          serializer.fromJson<String>(json[
              'observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas']),
      observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve:
          serializer.fromJson<String>(json[
              'observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve']),
      observouSituacoesQueLheChamaramAtencao: serializer
          .fromJson<String>(json['observouSituacoesQueLheChamaramAtencao']),
      emRelacaoAPessoaEntrevistada:
          serializer.fromJson<String>(json['emRelacaoAPessoaEntrevistada']),
      emRelacaoAPessoaEntrevistadaDescreve: serializer
          .fromJson<String>(json['emRelacaoAPessoaEntrevistadaDescreve']),
      emRelacaoAOutrosAdultosPresente:
          serializer.fromJson<String>(json['emRelacaoAOutrosAdultosPresente']),
      emRelacaoAOutrosAdultosPresenteDescreve: serializer
          .fromJson<String>(json['emRelacaoAOutrosAdultosPresenteDescreve']),
      emRelacaoAsCriancasPresentes:
          serializer.fromJson<String>(json['emRelacaoAsCriancasPresentes']),
      emRelacaoAsCriancasPresentesDescreve: serializer
          .fromJson<String>(json['emRelacaoAsCriancasPresentesDescreve']),
      emRelacaoAVizinhanca:
          serializer.fromJson<String>(json['emRelacaoAVizinhanca']),
      emRelacaoAVizinhancaDescreve:
          serializer.fromJson<String>(json['emRelacaoAVizinhancaDescreve']),
      outras: serializer.fromJson<String>(json['outras']),
      outrasDescreve: serializer.fromJson<String>(json['outrasDescreve']),
      createdAt: (DateTime.parse(json['createdAt'])),
      updatedAt: DateTime.parse(json['updatedAt']),
    );
  }

  Map<String, dynamic> toJsonSheet({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'uuid': serializer.toJson<String>(uuid),
      'nomeDoPesquisador': serializer.toJson<String>(nomeDoPesquisador),
      'numeroDeInquerito': serializer.toJson<String>(numeroDeInquerito),
      'data': "$data",
      'municipio': serializer.toJson<String>(municipio),
      'bairro': serializer.toJson<String>(bairro),
      'pontoDeReferenciaDaCasa':
          serializer.toJson<String>(pontoDeReferenciaDaCasa),
      'nome': serializer.toJson<String>(nome),
      'idade': serializer.toJson<String>(idade),
      'contacto': serializer.toJson<String>(contacto),
      'eChefeDaFamilia': serializer.toJson<String>(eChefeDaFamilia),
      'caoNaoVinculoDeParantesco':
          serializer.toJson<String>(caoNaoVinculoDeParantesco),
      'idadeDoChefeDaFamilia': serializer.toJson<String>(idadeDoChefeDaFamilia),
      'estudou': serializer.toJson<String>(estudou),
      'eAlfabetizado': serializer.toJson<String>(eAlfabetizado),
      'completouEnsinoPrimario':
          serializer.toJson<String>(completouEnsinoPrimario),
      'completouEnsinoSecundario':
          serializer.toJson<String>(completouEnsinoSecundario),
      'fezCursoSuperior': serializer.toJson<String>(fezCursoSuperior),
      'naoSabe': serializer.toJson<String>(naoSabe),
      'chefeDeFamiliaTemSalario':
          serializer.toJson<String>(chefeDeFamiliaTemSalario),
      'quantoGastaParaSustentarFamiliaPorMes':
          serializer.toJson<String>(quantoGastaParaSustentarFamiliaPorMes),
      'quantasFamiliasResidemNacasa':
          serializer.toJson<String>(quantasFamiliasResidemNacasa),
      'quantosAdultos': serializer.toJson<String>(quantosAdultos),
      'quantosAdultosMaioresDe18anos':
          serializer.toJson<String>(quantosAdultosMaioresDe18anos),
      'quantosAdultosMaioresDe18anosHomens':
          serializer.toJson<String>(quantosAdultosMaioresDe18anosHomens),
      'quantosAdultosMaioresDe18anosHomensTrabalham': serializer
          .toJson<String>(quantosAdultosMaioresDe18anosHomensTrabalham),
      'quantosAdultosMaioresDe18anosHomensAlfabetizados': serializer
          .toJson<String>(quantosAdultosMaioresDe18anosHomensAlfabetizados),
      'quantosAdultosM18Mulheres':
          serializer.toJson<String>(quantosAdultosM18Mulheres),
      'quantosAdultosM18MulheresTrabalham':
          serializer.toJson<String>(quantosAdultosM18MulheresTrabalham),
      'quantosAdultosM18MulheresAlfabetizados':
          serializer.toJson<String>(quantosAdultosM18MulheresAlfabetizados),
      'relatosDeDeficiencia': serializer.toJson<String>(relatosDeDeficiencia),
      'totalcriancasAte1anoemeio':
          serializer.toJson<String>(totalcriancasAte1anoemeio),
      'totalcriancasAte1anoemeioMeninos':
          serializer.toJson<String>(totalcriancasAte1anoemeioMeninos),
      'totalcriancasAte1anoemeioMeninas':
          serializer.toJson<String>(totalcriancasAte1anoemeioMeninas),
      'algumasCriancasAte1anoemeioNaodesenvolvem':
          serializer.toJson<String>(algumasCriancasAte1anoemeioNaodesenvolvem),
      'algumasCriancasAte1anoemeioNaodesenvolvemQuantas': serializer
          .toJson<String>(algumasCriancasAte1anoemeioNaodesenvolvemQuantas),
      'algumaCriancasAte1anoemeioQueTipodeProblema': serializer
          .toJson<String>(algumaCriancasAte1anoemeioQueTipodeProblema),
      'algumaCriancasAte1anoemeioTemProblemaMotor':
          serializer.toJson<String>(algumaCriancasAte1anoemeioTemProblemaMotor),
      'algumaCriancasAte1anoemeioTemProblemaMotorQuantas': serializer
          .toJson<String>(algumaCriancasAte1anoemeioTemProblemaMotorQuantas),
      'algumaCriancasAte1anoemeioTemMalFormacao':
          serializer.toJson<String>(algumaCriancasAte1anoemeioTemMalFormacao),
      'algumaCriancasAte1anoemeioTemMalFormacaoQuantas': serializer
          .toJson<String>(algumaCriancasAte1anoemeioTemMalFormacaoQuantas),
      'algumaCriancasAte1anoemeioNaoReagemAIncentivo': serializer
          .toJson<String>(algumaCriancasAte1anoemeioNaoReagemAIncentivo),
      'algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas': serializer
          .toJson<String>(algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas),
      'algumaCriancasAte1anoemeioTemReacaoAgressiva': serializer
          .toJson<String>(algumaCriancasAte1anoemeioTemReacaoAgressiva),
      'algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas': serializer
          .toJson<String>(algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas),
      'totalcriancasAte1anoemeioA3anos':
          serializer.toJson<String>(totalcriancasAte1anoemeioA3anos),
      'totalcriancasAte1anoemeioA3anosMeninos':
          serializer.toJson<String>(totalcriancasAte1anoemeioA3anosMeninos),
      'totalcriancasAte1anoemeioA3anosMeninas':
          serializer.toJson<String>(totalcriancasAte1anoemeioA3anosMeninas),
      'algumasCriancasAte1anoemeioA3anosNaodesenvolvem': serializer
          .toJson<String>(algumasCriancasAte1anoemeioA3anosNaodesenvolvem),
      'algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas),
      'algumaCriancasAte1anoemeioA3anosQueTipodeProblema': serializer
          .toJson<String>(algumaCriancasAte1anoemeioA3anosQueTipodeProblema),
      'algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas),
      'algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas),
      'algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo),
      'algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas),
      'algumasCriancasAte1anoemeioA3anosAlgunsAgridem': serializer
          .toJson<String>(algumasCriancasAte1anoemeioA3anosAlgunsAgridem),
      'algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas),
      'algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal),
      'algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas),
      'algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao),
      'algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas),
      'algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas),
      'algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas),
      'algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente),
      'algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas),
      'algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos),
      'algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas),
      'algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown),
      'algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas),
      'totalcriancasde3A6anos':
          serializer.toJson<String>(totalcriancasde3A6anos),
      'totalcriancasde3A6anosMeninos':
          serializer.toJson<String>(totalcriancasde3A6anosMeninos),
      'totalcriancasde3A6anosMeninas':
          serializer.toJson<String>(totalcriancasde3A6anosMeninas),
      'algumasCriancasde3A6anosNaodesenvolvem':
          serializer.toJson<String>(algumasCriancasde3A6anosNaodesenvolvem),
      'algumasCriancasde3A6anosNaodesenvolvemQuantas': serializer
          .toJson<String>(algumasCriancasde3A6anosNaodesenvolvemQuantas),
      'algumaCriancasde3A6anosQueTipodeProblema':
          serializer.toJson<String>(algumaCriancasde3A6anosQueTipodeProblema),
      'algumasCriancasde3A6anosNaoInteragemComPessoas': serializer
          .toJson<String>(algumasCriancasde3A6anosNaoInteragemComPessoas),
      'algumasCriancasde3A6anosNaoInteragemComPessoasQuantas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosNaoInteragemComPessoasQuantas),
      'algumasCriancasde3A6anosTemComportamentoAgressivo': serializer
          .toJson<String>(algumasCriancasde3A6anosTemComportamentoAgressivo),
      'algumasCriancasde3A6anosTemComportamentoAgressivoQuantas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosTemComportamentoAgressivoQuantas),
      'algumasCriancasde3A6anosAlgunsAgridem':
          serializer.toJson<String>(algumasCriancasde3A6anosAlgunsAgridem),
      'algumasCriancasde3A6anosAlgunsAgridemQuantas': serializer
          .toJson<String>(algumasCriancasde3A6anosAlgunsAgridemQuantas),
      'algumasCriancasde3A6anosAlgumasAgitadasQueONormal': serializer
          .toJson<String>(algumasCriancasde3A6anosAlgumasAgitadasQueONormal),
      'algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas),
      'algumasCriancasde3A6anosAlgumasTemMalFormacao': serializer
          .toJson<String>(algumasCriancasde3A6anosAlgumasTemMalFormacao),
      'algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas': serializer
          .toJson<String>(algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas),
      'algumasCriancasde3A6anosAlgumasNaoAndamSozinhas': serializer
          .toJson<String>(algumasCriancasde3A6anosAlgumasNaoAndamSozinhas),
      'algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas),
      'algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente),
      'algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas),
      'algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos),
      'algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas),
      'algumasCriancasde3A6anosAlgumasTemSindromeDeDown': serializer
          .toJson<String>(algumasCriancasde3A6anosAlgumasTemSindromeDeDown),
      'algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas),
      'algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas),
      'algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas),
      'algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas),
      'algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas),
      'algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas),
      'algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas),
      'algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas),
      'algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas),
      'quantasCriancasde3A6anosEstaoNaEscolinha':
          serializer.toJson<String>(quantasCriancasde3A6anosEstaoNaEscolinha),
      'criancasDe3A6anosNaoEstaoNaEscolinhaPorque':
          serializer.toJson<String>(criancasDe3A6anosNaoEstaoNaEscolinhaPorque),
      'quantasCriancasde3A6anosTrabalham':
          serializer.toJson<String>(quantasCriancasde3A6anosTrabalham),
      'criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos': serializer
          .toJson<String>(criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos),
      'totalcriancasde7A9anos':
          serializer.toJson<String>(totalcriancasde7A9anos),
      'totalcriancasde7A9anosMeninos':
          serializer.toJson<String>(totalcriancasde7A9anosMeninos),
      'totalcriancasde7A9anosMeninas':
          serializer.toJson<String>(totalcriancasde7A9anosMeninas),
      'quantasCriancasde7A9anosEstaoNaEscola':
          serializer.toJson<String>(quantasCriancasde7A9anosEstaoNaEscola),
      'quantasCriancasde7A9anosTrabalham':
          serializer.toJson<String>(quantasCriancasde7A9anosTrabalham),
      'nosUltimos5AnosQuantasGravidezesTiveNaFamilia': serializer
          .toJson<String>(nosUltimos5AnosQuantasGravidezesTiveNaFamilia),
      'partosOcoridosNoHospital':
          serializer.toJson<String>(partosOcoridosNoHospital),
      'partosOcoridosEmCasa': serializer.toJson<String>(partosOcoridosEmCasa),
      'partosOcoridosEmOutroLocal':
          serializer.toJson<String>(partosOcoridosEmOutroLocal),
      'outroLocalDoPartoQualLocal':
          serializer.toJson<String>(outroLocalDoPartoQualLocal),
      'quantasCriancasNasceramVivas':
          serializer.toJson<String>(quantasCriancasNasceramVivas),
      'quantasContinuamVivas': serializer.toJson<String>(quantasContinuamVivas),
      'dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram':
          serializer.toJson<String>(
              dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram),
      'descreverOProblema': serializer.toJson<String>(descreverOProblema),
      'aFamiliaProcurouAjudaParaSuperarEsseProblema': serializer
          .toJson<String>(aFamiliaProcurouAjudaParaSuperarEsseProblema),
      'seSimQueTipoDeAjudaEOnde':
          serializer.toJson<String>(seSimQueTipoDeAjudaEOnde),
      'osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia':
          serializer.toJson<String>(
              osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia),
      'dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida':
          serializer.toJson<String>(
              dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida),
      'sabeDizerOMotivoDosObitos':
          serializer.toJson<String>(sabeDizerOMotivoDosObitos),
      'seSimQualFoiPrincipalRazaodoObito':
          serializer.toJson<String>(seSimQualFoiPrincipalRazaodoObito),
      'voceTemMedoDeTerUmFilhoOuNetoComProblemas':
          serializer.toJson<String>(voceTemMedoDeTerUmFilhoOuNetoComProblemas),
      'expliquePorQue': serializer.toJson<String>(expliquePorQue),
      'porqueVoceAchaQueUmaCriancaPodeNascerComProblema': serializer
          .toJson<String>(porqueVoceAchaQueUmaCriancaPodeNascerComProblema),
      'oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema':
          serializer.toJson<String>(
              oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema),
      'achaQueUmaCriancaQueNasceComProblemaPodeSobreviver': serializer
          .toJson<String>(achaQueUmaCriancaQueNasceComProblemaPodeSobreviver),
      'expliquePorque': serializer.toJson<String>(expliquePorque),
      'quemPodeAjudarQuandoUmaCriancaNasceComProblema': serializer
          .toJson<String>(quemPodeAjudarQuandoUmaCriancaNasceComProblema),
      'emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao':
          serializer.toJson<String>(
              emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao),
      'emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene':
          serializer.toJson<String>(
              emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene),
      'emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos': serializer
          .toJson<String>(emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos),
      'aPessoaEntrevistadaPareceuSinceraNasSuasRespostas': serializer
          .toJson<String>(aPessoaEntrevistadaPareceuSinceraNasSuasRespostas),
      'algumasPerguntasIncomodaramAPessoaEntrevistada': serializer
          .toJson<String>(algumasPerguntasIncomodaramAPessoaEntrevistada),
      'seSimQuais3PrincipaisPerguntas':
          serializer.toJson<String>(seSimQuais3PrincipaisPerguntas),
      'todasAsPerguntasIncomodaram':
          serializer.toJson<String>(todasAsPerguntasIncomodaram),
      'aResidenciaEraDe': serializer.toJson<String>(aResidenciaEraDe),
      'comoAvaliaHigieneDaCasa':
          serializer.toJson<String>(comoAvaliaHigieneDaCasa),
      'oQueEraMal': serializer.toJson<String>(oQueEraMal),
      'tinhaCriancasNaCasaDuranteInquerito':
          serializer.toJson<String>(tinhaCriancasNaCasaDuranteInquerito),
      'tinhaCriancasNaCasaDuranteInqueritoQuantas':
          serializer.toJson<String>(tinhaCriancasNaCasaDuranteInqueritoQuantas),
      'tentouIncentivar': serializer.toJson<String>(tentouIncentivar),
      'tentouIncentivarQuantas':
          serializer.toJson<String>(tentouIncentivarQuantas),
      'seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos':
          serializer.toJson<String>(
              seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos),
      'observouCriancasDeAte1Ano':
          serializer.toJson<String>(observouCriancasDeAte1Ano),
      'observouCriancasDeAte1AnoQuantos':
          serializer.toJson<String>(observouCriancasDeAte1AnoQuantos),
      'algumasApresentavamProblemas':
          serializer.toJson<String>(algumasApresentavamProblemas),
      'algumasApresentavamProblemasQuantos':
          serializer.toJson<String>(algumasApresentavamProblemasQuantos),
      'descreveOProblemaDessasCriancas':
          serializer.toJson<String>(descreveOProblemaDessasCriancas),
      'observouCriancasDe1AnoEmeioAte3Anos':
          serializer.toJson<String>(observouCriancasDe1AnoEmeioAte3Anos),
      'observouCriancasDe1AnoEmeioAte3AnosQuantas':
          serializer.toJson<String>(observouCriancasDe1AnoEmeioAte3AnosQuantas),
      'algumasCriancasDe1anoEmeioObservadasApresentavamProblemas':
          serializer.toJson<String>(
              algumasCriancasDe1anoEmeioObservadasApresentavamProblemas),
      'algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas':
          serializer.toJson<String>(
              algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas),
      'algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve':
          serializer.toJson<String>(
              algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve),
      'observouCriancasDe3AnosAte6Anos':
          serializer.toJson<String>(observouCriancasDe3AnosAte6Anos),
      'observouCriancasDe3AnosAte6AnosQuantas':
          serializer.toJson<String>(observouCriancasDe3AnosAte6AnosQuantas),
      'observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas':
          serializer.toJson<String>(
              observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas),
      'observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas':
          serializer.toJson<String>(
              observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas),
      'observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve':
          serializer.toJson<String>(
              observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve),
      'observouSituacoesQueLheChamaramAtencao':
          serializer.toJson<String>(observouSituacoesQueLheChamaramAtencao),
      'emRelacaoAPessoaEntrevistada':
          serializer.toJson<String>(emRelacaoAPessoaEntrevistada),
      'emRelacaoAPessoaEntrevistadaDescreve':
          serializer.toJson<String>(emRelacaoAPessoaEntrevistadaDescreve),
      'emRelacaoAOutrosAdultosPresente':
          serializer.toJson<String>(emRelacaoAOutrosAdultosPresente),
      'emRelacaoAOutrosAdultosPresenteDescreve':
          serializer.toJson<String>(emRelacaoAOutrosAdultosPresenteDescreve),
      'emRelacaoAsCriancasPresentes':
          serializer.toJson<String>(emRelacaoAsCriancasPresentes),
      'emRelacaoAsCriancasPresentesDescreve':
          serializer.toJson<String>(emRelacaoAsCriancasPresentesDescreve),
      'emRelacaoAVizinhanca': serializer.toJson<String>(emRelacaoAVizinhanca),
      'emRelacaoAVizinhancaDescreve':
          serializer.toJson<String>(emRelacaoAVizinhancaDescreve),
      'outras': serializer.toJson<String>(outras),
      'outrasDescreve': serializer.toJson<String>(outrasDescreve),
      'createdAt': "$createdAt",
      'updatedAt': "$updatedAt",
    };
  }

   
}