// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'database.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class User extends DataClass implements Insertable<User> {
  final String uuid;
  final String nomeDoPesquisador;
  final String numeroDeInquerito;
  final DateTime data;
  final String municipio;
  final String bairro;
  final String pontoDeReferenciaDaCasa;
  final String nome;
  final String idade;
  final String contacto;
  final String eChefeDaFamilia;
  final String caoNaoVinculoDeParantesco;
  final String idadeDoChefeDaFamilia;
  final String estudou;
  final String eAlfabetizado;
  final String completouEnsinoPrimario;
  final String completouEnsinoSecundario;
  final String fezCursoSuperior;
  final String naoSabe;
  final String chefeDeFamiliaTemSalario;
  final String quantoGastaParaSustentarFamiliaPorMes;
  final String quantasFamiliasResidemNacasa;
  final String quantosAdultos;
  final String quantosAdultosMaioresDe18anos;
  final String quantosAdultosMaioresDe18anosHomens;
  final String quantosAdultosMaioresDe18anosHomensTrabalham;
  final String quantosAdultosMaioresDe18anosHomensAlfabetizados;
  final String quantosAdultosM18Mulheres;
  final String quantosAdultosM18MulheresTrabalham;
  final String quantosAdultosM18MulheresAlfabetizados;
  final String relatosDeDeficiencia;
  final String totalcriancasAte1anoemeio;
  final String totalcriancasAte1anoemeioMeninos;
  final String totalcriancasAte1anoemeioMeninas;
  final String algumasCriancasAte1anoemeioNaodesenvolvem;
  final String algumasCriancasAte1anoemeioNaodesenvolvemQuantas;
  final String algumaCriancasAte1anoemeioQueTipodeProblema;
  final String algumaCriancasAte1anoemeioTemProblemaMotor;
  final String algumaCriancasAte1anoemeioTemProblemaMotorQuantas;
  final String algumaCriancasAte1anoemeioTemMalFormacao;
  final String algumaCriancasAte1anoemeioTemMalFormacaoQuantas;
  final String algumaCriancasAte1anoemeioNaoReagemAIncentivo;
  final String algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas;
  final String algumaCriancasAte1anoemeioTemReacaoAgressiva;
  final String algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas;
  final String totalcriancasAte1anoemeioA3anos;
  final String totalcriancasAte1anoemeioA3anosMeninos;
  final String totalcriancasAte1anoemeioA3anosMeninas;
  final String algumasCriancasAte1anoemeioA3anosNaodesenvolvem;
  final String algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas;
  final String algumaCriancasAte1anoemeioA3anosQueTipodeProblema;
  final String algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas;
  final String algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas;
  final String algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo;
  final String
      algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas;
  final String algumasCriancasAte1anoemeioA3anosAlgunsAgridem;
  final String algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas;
  final String algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal;
  final String
      algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas;
  final String algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao;
  final String algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas;
  final String algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas;
  final String algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas;
  final String algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente;
  final String
      algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas;
  final String algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos;
  final String
      algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas;
  final String algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown;
  final String algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas;
  final String totalcriancasde3A6anos;
  final String totalcriancasde3A6anosMeninos;
  final String totalcriancasde3A6anosMeninas;
  final String algumasCriancasde3A6anosNaodesenvolvem;
  final String algumasCriancasde3A6anosNaodesenvolvemQuantas;
  final String algumaCriancasde3A6anosQueTipodeProblema;
  final String algumasCriancasde3A6anosNaoInteragemComPessoas;
  final String algumasCriancasde3A6anosNaoInteragemComPessoasQuantas;
  final String algumasCriancasde3A6anosTemComportamentoAgressivo;
  final String algumasCriancasde3A6anosTemComportamentoAgressivoQuantas;
  final String algumasCriancasde3A6anosAlgunsAgridem;
  final String algumasCriancasde3A6anosAlgunsAgridemQuantas;
  final String algumasCriancasde3A6anosAlgumasAgitadasQueONormal;
  final String algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas;
  final String algumasCriancasde3A6anosAlgumasTemMalFormacao;
  final String algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas;
  final String algumasCriancasde3A6anosAlgumasNaoAndamSozinhas;
  final String algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas;
  final String algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente;
  final String algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas;
  final String algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos;
  final String algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas;
  final String algumasCriancasde3A6anosAlgumasTemSindromeDeDown;
  final String algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas;
  final String algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas;
  final String algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas;
  final String algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas;
  final String
      algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas;
  final String
      algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas;
  final String
      algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas;
  final String algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas;
  final String
      algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas;
  final String quantasCriancasde3A6anosEstaoNaEscolinha;
  final String criancasDe3A6anosNaoEstaoNaEscolinhaPorque;
  final String quantasCriancasde3A6anosTrabalham;
  final String criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos;
  final String totalcriancasde7A9anos;
  final String totalcriancasde7A9anosMeninos;
  final String totalcriancasde7A9anosMeninas;
  final String quantasCriancasde7A9anosEstaoNaEscola;
  final String quantasCriancasde7A9anosTrabalham;
  final String nosUltimos5AnosQuantasGravidezesTiveNaFamilia;
  final String partosOcoridosNoHospital;
  final String partosOcoridosEmCasa;
  final String partosOcoridosEmOutroLocal;
  final String outroLocalDoPartoQualLocal;
  final String quantasCriancasNasceramVivas;
  final String quantasContinuamVivas;
  final String
      dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram;
  final String descreverOProblema;
  final String aFamiliaProcurouAjudaParaSuperarEsseProblema;
  final String seSimQueTipoDeAjudaEOnde;
  final String
      osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia;
  final String
      dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida;
  final String sabeDizerOMotivoDosObitos;
  final String seSimQualFoiPrincipalRazaodoObito;
  final String voceTemMedoDeTerUmFilhoOuNetoComProblemas;
  final String expliquePorQue;
  final String porqueVoceAchaQueUmaCriancaPodeNascerComProblema;
  final String oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema;
  final String achaQueUmaCriancaQueNasceComProblemaPodeSobreviver;
  final String expliquePorque;
  final String quemPodeAjudarQuandoUmaCriancaNasceComProblema;
  final String emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao;
  final String emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene;
  final String emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos;
  final String aPessoaEntrevistadaPareceuSinceraNasSuasRespostas;
  final String algumasPerguntasIncomodaramAPessoaEntrevistada;
  final String seSimQuais3PrincipaisPerguntas;
  final String todasAsPerguntasIncomodaram;
  final String aResidenciaEraDe;
  final String comoAvaliaHigieneDaCasa;
  final String oQueEraMal;
  final String tinhaCriancasNaCasaDuranteInquerito;
  final String tinhaCriancasNaCasaDuranteInqueritoQuantas;
  final String tentouIncentivar;
  final String tentouIncentivarQuantas;
  final String seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos;
  final String observouCriancasDeAte1Ano;
  final String observouCriancasDeAte1AnoQuantos;
  final String algumasApresentavamProblemas;
  final String algumasApresentavamProblemasQuantos;
  final String descreveOProblemaDessasCriancas;
  final String observouCriancasDe1AnoEmeioAte3Anos;
  final String observouCriancasDe1AnoEmeioAte3AnosQuantas;
  final String algumasCriancasDe1anoEmeioObservadasApresentavamProblemas;
  final String algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas;
  final String
      algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve;
  final String observouCriancasDe3AnosAte6Anos;
  final String observouCriancasDe3AnosAte6AnosQuantas;
  final String observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas;
  final String
      observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas;
  final String
      observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve;
  final String observouSituacoesQueLheChamaramAtencao;
  final String emRelacaoAPessoaEntrevistada;
  final String emRelacaoAPessoaEntrevistadaDescreve;
  final String emRelacaoAOutrosAdultosPresente;
  final String emRelacaoAOutrosAdultosPresenteDescreve;
  final String emRelacaoAsCriancasPresentes;
  final String emRelacaoAsCriancasPresentesDescreve;
  final String emRelacaoAVizinhanca;
  final String emRelacaoAVizinhancaDescreve;
  final String outras;
  final String outrasDescreve;
  final DateTime createdAt;
  final DateTime updatedAt;
  User(
      {this.uuid,
      this.nomeDoPesquisador,
      this.numeroDeInquerito,
      this.data,
      this.municipio,
      this.bairro,
      this.pontoDeReferenciaDaCasa,
      this.nome,
      this.idade,
      this.contacto,
      this.eChefeDaFamilia,
      this.caoNaoVinculoDeParantesco,
      this.idadeDoChefeDaFamilia,
      this.estudou,
      this.eAlfabetizado,
      this.completouEnsinoPrimario,
      this.completouEnsinoSecundario,
      this.fezCursoSuperior,
      this.naoSabe,
      this.chefeDeFamiliaTemSalario,
      this.quantoGastaParaSustentarFamiliaPorMes,
      this.quantasFamiliasResidemNacasa,
      this.quantosAdultos,
      this.quantosAdultosMaioresDe18anos,
      this.quantosAdultosMaioresDe18anosHomens,
      this.quantosAdultosMaioresDe18anosHomensTrabalham,
      this.quantosAdultosMaioresDe18anosHomensAlfabetizados,
      this.quantosAdultosM18Mulheres,
      this.quantosAdultosM18MulheresTrabalham,
      this.quantosAdultosM18MulheresAlfabetizados,
      this.relatosDeDeficiencia,
      this.totalcriancasAte1anoemeio,
      this.totalcriancasAte1anoemeioMeninos,
      this.totalcriancasAte1anoemeioMeninas,
      this.algumasCriancasAte1anoemeioNaodesenvolvem,
      this.algumasCriancasAte1anoemeioNaodesenvolvemQuantas,
      this.algumaCriancasAte1anoemeioQueTipodeProblema,
      this.algumaCriancasAte1anoemeioTemProblemaMotor,
      this.algumaCriancasAte1anoemeioTemProblemaMotorQuantas,
      this.algumaCriancasAte1anoemeioTemMalFormacao,
      this.algumaCriancasAte1anoemeioTemMalFormacaoQuantas,
      this.algumaCriancasAte1anoemeioNaoReagemAIncentivo,
      this.algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas,
      this.algumaCriancasAte1anoemeioTemReacaoAgressiva,
      this.algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas,
      this.totalcriancasAte1anoemeioA3anos,
      this.totalcriancasAte1anoemeioA3anosMeninos,
      this.totalcriancasAte1anoemeioA3anosMeninas,
      this.algumasCriancasAte1anoemeioA3anosNaodesenvolvem,
      this.algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas,
      this.algumaCriancasAte1anoemeioA3anosQueTipodeProblema,
      this.algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas,
      this.algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas,
      this.algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo,
      this.algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas,
      this.algumasCriancasAte1anoemeioA3anosAlgunsAgridem,
      this.algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas,
      this.algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal,
      this.algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas,
      this.algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao,
      this.algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas,
      this.algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas,
      this.algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas,
      this.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente,
      this.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas,
      this.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos,
      this.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas,
      this.algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown,
      this.algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas,
      this.totalcriancasde3A6anos,
      this.totalcriancasde3A6anosMeninos,
      this.totalcriancasde3A6anosMeninas,
      this.algumasCriancasde3A6anosNaodesenvolvem,
      this.algumasCriancasde3A6anosNaodesenvolvemQuantas,
      this.algumaCriancasde3A6anosQueTipodeProblema,
      this.algumasCriancasde3A6anosNaoInteragemComPessoas,
      this.algumasCriancasde3A6anosNaoInteragemComPessoasQuantas,
      this.algumasCriancasde3A6anosTemComportamentoAgressivo,
      this.algumasCriancasde3A6anosTemComportamentoAgressivoQuantas,
      this.algumasCriancasde3A6anosAlgunsAgridem,
      this.algumasCriancasde3A6anosAlgunsAgridemQuantas,
      this.algumasCriancasde3A6anosAlgumasAgitadasQueONormal,
      this.algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas,
      this.algumasCriancasde3A6anosAlgumasTemMalFormacao,
      this.algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas,
      this.algumasCriancasde3A6anosAlgumasNaoAndamSozinhas,
      this.algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas,
      this.algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente,
      this.algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas,
      this.algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos,
      this.algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas,
      this.algumasCriancasde3A6anosAlgumasTemSindromeDeDown,
      this.algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas,
      this.algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas,
      this.algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas,
      this.algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas,
      this.algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas,
      this.algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas,
      this.algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas,
      this.algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas,
      this.algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas,
      this.quantasCriancasde3A6anosEstaoNaEscolinha,
      this.criancasDe3A6anosNaoEstaoNaEscolinhaPorque,
      this.quantasCriancasde3A6anosTrabalham,
      this.criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos,
      this.totalcriancasde7A9anos,
      this.totalcriancasde7A9anosMeninos,
      this.totalcriancasde7A9anosMeninas,
      this.quantasCriancasde7A9anosEstaoNaEscola,
      this.quantasCriancasde7A9anosTrabalham,
      this.nosUltimos5AnosQuantasGravidezesTiveNaFamilia,
      this.partosOcoridosNoHospital,
      this.partosOcoridosEmCasa,
      this.partosOcoridosEmOutroLocal,
      this.outroLocalDoPartoQualLocal,
      this.quantasCriancasNasceramVivas,
      this.quantasContinuamVivas,
      this.dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram,
      this.descreverOProblema,
      this.aFamiliaProcurouAjudaParaSuperarEsseProblema,
      this.seSimQueTipoDeAjudaEOnde,
      this.osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia,
      this.dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida,
      this.sabeDizerOMotivoDosObitos,
      this.seSimQualFoiPrincipalRazaodoObito,
      this.voceTemMedoDeTerUmFilhoOuNetoComProblemas,
      this.expliquePorQue,
      this.porqueVoceAchaQueUmaCriancaPodeNascerComProblema,
      this.oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema,
      this.achaQueUmaCriancaQueNasceComProblemaPodeSobreviver,
      this.expliquePorque,
      this.quemPodeAjudarQuandoUmaCriancaNasceComProblema,
      this.emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao,
      this.emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene,
      this.emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos,
      this.aPessoaEntrevistadaPareceuSinceraNasSuasRespostas,
      this.algumasPerguntasIncomodaramAPessoaEntrevistada,
      this.seSimQuais3PrincipaisPerguntas,
      this.todasAsPerguntasIncomodaram,
      this.aResidenciaEraDe,
      this.comoAvaliaHigieneDaCasa,
      this.oQueEraMal,
      this.tinhaCriancasNaCasaDuranteInquerito,
      this.tinhaCriancasNaCasaDuranteInqueritoQuantas,
      this.tentouIncentivar,
      this.tentouIncentivarQuantas,
      this.seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos,
      this.observouCriancasDeAte1Ano,
      this.observouCriancasDeAte1AnoQuantos,
      this.algumasApresentavamProblemas,
      this.algumasApresentavamProblemasQuantos,
      this.descreveOProblemaDessasCriancas,
      this.observouCriancasDe1AnoEmeioAte3Anos,
      this.observouCriancasDe1AnoEmeioAte3AnosQuantas,
      this.algumasCriancasDe1anoEmeioObservadasApresentavamProblemas,
      this.algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas,
      this.algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve,
      this.observouCriancasDe3AnosAte6Anos,
      this.observouCriancasDe3AnosAte6AnosQuantas,
      this.observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas,
      this.observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas,
      this.observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve,
      this.observouSituacoesQueLheChamaramAtencao,
      this.emRelacaoAPessoaEntrevistada,
      this.emRelacaoAPessoaEntrevistadaDescreve,
      this.emRelacaoAOutrosAdultosPresente,
      this.emRelacaoAOutrosAdultosPresenteDescreve,
      this.emRelacaoAsCriancasPresentes,
      this.emRelacaoAsCriancasPresentesDescreve,
      this.emRelacaoAVizinhanca,
      this.emRelacaoAVizinhancaDescreve,
      this.outras,
      this.outrasDescreve,
      this.createdAt,
      this.updatedAt});
  factory User.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return User(
      uuid: stringType.mapFromDatabaseResponse(data['${effectivePrefix}uuid']),
      nomeDoPesquisador: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}nome_do_pesquisador']),
      numeroDeInquerito: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}numero_de_inquerito']),
      data:
          dateTimeType.mapFromDatabaseResponse(data['${effectivePrefix}data']),
      municipio: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}municipio']),
      bairro:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}bairro']),
      pontoDeReferenciaDaCasa: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}ponto_de_referencia_da_casa']),
      nome: stringType.mapFromDatabaseResponse(data['${effectivePrefix}nome']),
      idade:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}idade']),
      contacto: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}contacto']),
      eChefeDaFamilia: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}e_chefe_da_familia']),
      caoNaoVinculoDeParantesco: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}cao_nao_vinculo_de_parantesco']),
      idadeDoChefeDaFamilia: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}idade_do_chefe_da_familia']),
      estudou:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}estudou']),
      eAlfabetizado: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}e_alfabetizado']),
      completouEnsinoPrimario: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}completou_ensino_primario']),
      completouEnsinoSecundario: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}completou_ensino_secundario']),
      fezCursoSuperior: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}fez_curso_superior']),
      naoSabe: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}nao_sabe']),
      chefeDeFamiliaTemSalario: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}chefe_de_familia_tem_salario']),
      quantoGastaParaSustentarFamiliaPorMes: stringType.mapFromDatabaseResponse(
          data[
              '${effectivePrefix}quanto_gasta_para_sustentar_familia_por_mes']),
      quantasFamiliasResidemNacasa: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}quantas_familias_residem_nacasa']),
      quantosAdultos: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}quantos_adultos']),
      quantosAdultosMaioresDe18anos: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}quantos_adultos_maiores_de18anos']),
      quantosAdultosMaioresDe18anosHomens: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}quantos_adultos_maiores_de18anos_homens']),
      quantosAdultosMaioresDe18anosHomensTrabalham:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}quantos_adultos_maiores_de18anos_homens_trabalham']),
      quantosAdultosMaioresDe18anosHomensAlfabetizados:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}quantos_adultos_maiores_de18anos_homens_alfabetizados']),
      quantosAdultosM18Mulheres: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}quantos_adultos_m18_mulheres']),
      quantosAdultosM18MulheresTrabalham: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}quantos_adultos_m18_mulheres_trabalham']),
      quantosAdultosM18MulheresAlfabetizados:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}quantos_adultos_m18_mulheres_alfabetizados']),
      relatosDeDeficiencia: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}relatos_de_deficiencia']),
      totalcriancasAte1anoemeio: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}totalcriancas_ate1anoemeio']),
      totalcriancasAte1anoemeioMeninos: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}totalcriancas_ate1anoemeio_meninos']),
      totalcriancasAte1anoemeioMeninas: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}totalcriancas_ate1anoemeio_meninas']),
      algumasCriancasAte1anoemeioNaodesenvolvem:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancas_ate1anoemeio_naodesenvolvem']),
      algumasCriancasAte1anoemeioNaodesenvolvemQuantas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancas_ate1anoemeio_naodesenvolvem_quantas']),
      algumaCriancasAte1anoemeioQueTipodeProblema:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}alguma_criancas_ate1anoemeio_que_tipode_problema']),
      algumaCriancasAte1anoemeioTemProblemaMotor:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}alguma_criancas_ate1anoemeio_tem_problema_motor']),
      algumaCriancasAte1anoemeioTemProblemaMotorQuantas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}alguma_criancas_ate1anoemeio_tem_problema_motor_quantas']),
      algumaCriancasAte1anoemeioTemMalFormacao:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}alguma_criancas_ate1anoemeio_tem_mal_formacao']),
      algumaCriancasAte1anoemeioTemMalFormacaoQuantas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}alguma_criancas_ate1anoemeio_tem_mal_formacao_quantas']),
      algumaCriancasAte1anoemeioNaoReagemAIncentivo:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}alguma_criancas_ate1anoemeio_nao_reagem_a_incentivo']),
      algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}alguma_criancas_ate1anoemeio_nao_reagem_a_incentivo_quantas']),
      algumaCriancasAte1anoemeioTemReacaoAgressiva:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}alguma_criancas_ate1anoemeio_tem_reacao_agressiva']),
      algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}alguma_criancas_ate1anoemeio_tem_reacao_agressiva_quantas']),
      totalcriancasAte1anoemeioA3anos: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}totalcriancas_ate1anoemeio_a3anos']),
      totalcriancasAte1anoemeioA3anosMeninos:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}totalcriancas_ate1anoemeio_a3anos_meninos']),
      totalcriancasAte1anoemeioA3anosMeninas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}totalcriancas_ate1anoemeio_a3anos_meninas']),
      algumasCriancasAte1anoemeioA3anosNaodesenvolvem:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancas_ate1anoemeio_a3anos_naodesenvolvem']),
      algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancas_ate1anoemeio_a3anos_naodesenvolvem_quantas']),
      algumaCriancasAte1anoemeioA3anosQueTipodeProblema:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}alguma_criancas_ate1anoemeio_a3anos_que_tipode_problema']),
      algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancas_ate1anoemeio_a3anos_nao_interagem_com_pessoas']),
      algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancas_ate1anoemeio_a3anos_nao_interagem_com_pessoas_quantas']),
      algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancas_ate1anoemeio_a3anos_tem_comportamento_agressivo']),
      algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancas_ate1anoemeio_a3anos_tem_comportamento_agressivo_quantas']),
      algumasCriancasAte1anoemeioA3anosAlgunsAgridem:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancas_ate1anoemeio_a3anos_alguns_agridem']),
      algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancas_ate1anoemeio_a3anos_alguns_agridem_quantas']),
      algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancas_ate1anoemeio_a3anos_algumas_agitadas_que_o_normal']),
      algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancas_ate1anoemeio_a3anos_algumas_agitadas_que_o_normal_quantas']),
      algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancas_ate1anoemeio_a3anos_algumas_tem_mal_formacao']),
      algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancas_ate1anoemeio_a3anos_algumas_tem_mal_formacao_quantas']),
      algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancas_ate1anoemeio_a3anos_algumas_nao_andam_sozinhas']),
      algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancas_ate1anoemeio_a3anos_algumas_nao_andam_sozinhas_quantas']),
      algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancas_ate1anoemeio_a3anos_algumas_nao_expressam_verbalmente']),
      algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancas_ate1anoemeio_a3anos_algumas_nao_expressam_verbalmente_quantas']),
      algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancas_ate1anoemeio_a3anos_algumas_nao_expressam_sentimentos']),
      algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancas_ate1anoemeio_a3anos_algumas_nao_expressam_sentimentos_quantas']),
      algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancas_ate1anoemeio_a3anos_algumas_tem_sindrome_de_down']),
      algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancas_ate1anoemeio_a3anos_algumas_tem_sindrome_de_down_quantas']),
      totalcriancasde3A6anos: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}totalcriancasde3_a6anos']),
      totalcriancasde3A6anosMeninos: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}totalcriancasde3_a6anos_meninos']),
      totalcriancasde3A6anosMeninas: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}totalcriancasde3_a6anos_meninas']),
      algumasCriancasde3A6anosNaodesenvolvem:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancasde3_a6anos_naodesenvolvem']),
      algumasCriancasde3A6anosNaodesenvolvemQuantas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancasde3_a6anos_naodesenvolvem_quantas']),
      algumaCriancasde3A6anosQueTipodeProblema:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}alguma_criancasde3_a6anos_que_tipode_problema']),
      algumasCriancasde3A6anosNaoInteragemComPessoas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancasde3_a6anos_nao_interagem_com_pessoas']),
      algumasCriancasde3A6anosNaoInteragemComPessoasQuantas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancasde3_a6anos_nao_interagem_com_pessoas_quantas']),
      algumasCriancasde3A6anosTemComportamentoAgressivo:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancasde3_a6anos_tem_comportamento_agressivo']),
      algumasCriancasde3A6anosTemComportamentoAgressivoQuantas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancasde3_a6anos_tem_comportamento_agressivo_quantas']),
      algumasCriancasde3A6anosAlgunsAgridem: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}algumas_criancasde3_a6anos_alguns_agridem']),
      algumasCriancasde3A6anosAlgunsAgridemQuantas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancasde3_a6anos_alguns_agridem_quantas']),
      algumasCriancasde3A6anosAlgumasAgitadasQueONormal:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancasde3_a6anos_algumas_agitadas_que_o_normal']),
      algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancasde3_a6anos_algumas_agitadas_que_o_normal_quantas']),
      algumasCriancasde3A6anosAlgumasTemMalFormacao:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancasde3_a6anos_algumas_tem_mal_formacao']),
      algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancasde3_a6anos_algumas_tem_mal_formacao_quantas']),
      algumasCriancasde3A6anosAlgumasNaoAndamSozinhas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancasde3_a6anos_algumas_nao_andam_sozinhas']),
      algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancasde3_a6anos_algumas_nao_andam_sozinhas_quantas']),
      algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancasde3_a6anos_algumas_nao_expressam_verbalmente']),
      algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancasde3_a6anos_algumas_nao_expressam_verbalmente_quantas']),
      algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancasde3_a6anos_algumas_nao_expressam_sentimentos']),
      algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancasde3_a6anos_algumas_nao_expressam_sentimentos_quantas']),
      algumasCriancasde3A6anosAlgumasTemSindromeDeDown:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancasde3_a6anos_algumas_tem_sindrome_de_down']),
      algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancasde3_a6anos_algumas_tem_sindrome_de_down_quantas']),
      algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancasde3_a6anos_algumas_nao_sabem_se_alimentar_sozinhas']),
      algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancasde3_a6anos_algumas_nao_sabe_se_alimentar_sozinhas_quantas']),
      algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancasde3_a6anos_algumas_nao_conseguem_tomar_banho_sozinhas']),
      algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancasde3_a6anos_algumas_nao_conseguem_tomar_banho_sozinhas_quantas']),
      algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancasde3_a6anos_algumas_nao_conseguem_ir_a_casa_de_banho_sozinhas']),
      algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancasde3_a6anos_algumas_nao_conseguem_ir_a_casa_de_banho_sozinhas_quantas']),
      algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancasde3_a6anos_algumas_nao_conseguem_se_vestir_sozinhas']),
      algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancasde3_a6anos_algumas_nao_conseguem_se_vestir_sozinhas_quantas']),
      quantasCriancasde3A6anosEstaoNaEscolinha:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}quantas_criancasde3_a6anos_estao_na_escolinha']),
      criancasDe3A6anosNaoEstaoNaEscolinhaPorque:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}criancas_de3_a6anos_nao_estao_na_escolinha_porque']),
      quantasCriancasde3A6anosTrabalham: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}quantas_criancasde3_a6anos_trabalham']),
      criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}criancas_de3_a6anos_nao_estao_na_escolinha_por_outos_motivos']),
      totalcriancasde7A9anos: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}totalcriancasde7_a9anos']),
      totalcriancasde7A9anosMeninos: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}totalcriancasde7_a9anos_meninos']),
      totalcriancasde7A9anosMeninas: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}totalcriancasde7_a9anos_meninas']),
      quantasCriancasde7A9anosEstaoNaEscola: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}quantas_criancasde7_a9anos_estao_na_escola']),
      quantasCriancasde7A9anosTrabalham: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}quantas_criancasde7_a9anos_trabalham']),
      nosUltimos5AnosQuantasGravidezesTiveNaFamilia:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}nos_ultimos5_anos_quantas_gravidezes_tive_na_familia']),
      partosOcoridosNoHospital: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}partos_ocoridos_no_hospital']),
      partosOcoridosEmCasa: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}partos_ocoridos_em_casa']),
      partosOcoridosEmOutroLocal: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}partos_ocoridos_em_outro_local']),
      outroLocalDoPartoQualLocal: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}outro_local_do_parto_qual_local']),
      quantasCriancasNasceramVivas: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}quantas_criancas_nasceram_vivas']),
      quantasContinuamVivas: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}quantas_continuam_vivas']),
      dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}das_vivas_algumas_apresentam_problemas_de_desenvolvimento_que_outros_nao_tiveram']),
      descreverOProblema: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}descrever_o_problema']),
      aFamiliaProcurouAjudaParaSuperarEsseProblema:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}a_familia_procurou_ajuda_para_superar_esse_problema']),
      seSimQueTipoDeAjudaEOnde: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}se_sim_que_tipo_de_ajuda_e_onde']),
      osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}os_vizinhos_se_preocuparam_quando_nasceu_uma_crianca_com_problema_na_familia']),
      dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}das_criancas_que_nasceram_vivas_quantas_nao_sobreviveram_nos2_primeiros_anos_de_vida']),
      sabeDizerOMotivoDosObitos: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}sabe_dizer_o_motivo_dos_obitos']),
      seSimQualFoiPrincipalRazaodoObito: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}se_sim_qual_foi_principal_razaodo_obito']),
      voceTemMedoDeTerUmFilhoOuNetoComProblemas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}voce_tem_medo_de_ter_um_filho_ou_neto_com_problemas']),
      expliquePorQue: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}explique_por_que']),
      porqueVoceAchaQueUmaCriancaPodeNascerComProblema:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}porque_voce_acha_que_uma_crianca_pode_nascer_com_problema']),
      oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}o_que_acontece_no_seu_bairro_quando_nasce_uma_crianca_com_problema']),
      achaQueUmaCriancaQueNasceComProblemaPodeSobreviver:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}acha_que_uma_crianca_que_nasce_com_problema_pode_sobreviver']),
      expliquePorque: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}explique_porque']),
      quemPodeAjudarQuandoUmaCriancaNasceComProblema:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}quem_pode_ajudar_quando_uma_crianca_nasce_com_problema']),
      emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}em_casa_quem_cuida_mais_das_criancas_ate6_anos_para_alimentacao']),
      emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}em_casa_quem_cuida_mais_das_criancas_ate6_anos_para_os_cuidados_de_higiene']),
      emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}em_casa_quem_cuida_mais_das_criancas_ate6_anos_para_os_estudos']),
      aPessoaEntrevistadaPareceuSinceraNasSuasRespostas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}a_pessoa_entrevistada_pareceu_sincera_nas_suas_respostas']),
      algumasPerguntasIncomodaramAPessoaEntrevistada:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_perguntas_incomodaram_a_pessoa_entrevistada']),
      seSimQuais3PrincipaisPerguntas: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}se_sim_quais3_principais_perguntas']),
      todasAsPerguntasIncomodaram: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}todas_as_perguntas_incomodaram']),
      aResidenciaEraDe: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}a_residencia_era_de']),
      comoAvaliaHigieneDaCasa: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}como_avalia_higiene_da_casa']),
      oQueEraMal: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}o_que_era_mal']),
      tinhaCriancasNaCasaDuranteInquerito: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}tinha_criancas_na_casa_durante_inquerito']),
      tinhaCriancasNaCasaDuranteInqueritoQuantas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}tinha_criancas_na_casa_durante_inquerito_quantas']),
      tentouIncentivar: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}tentou_incentivar']),
      tentouIncentivarQuantas: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}tentou_incentivar_quantas']),
      seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}se_incentivou_quantas_reagiram_positivamente_aos_seus_incentivos']),
      observouCriancasDeAte1Ano: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}observou_criancas_de_ate1_ano']),
      observouCriancasDeAte1AnoQuantos: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}observou_criancas_de_ate1_ano_quantos']),
      algumasApresentavamProblemas: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}algumas_apresentavam_problemas']),
      algumasApresentavamProblemasQuantos: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}algumas_apresentavam_problemas_quantos']),
      descreveOProblemaDessasCriancas: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}descreve_o_problema_dessas_criancas']),
      observouCriancasDe1AnoEmeioAte3Anos: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}observou_criancas_de1_ano_emeio_ate3_anos']),
      observouCriancasDe1AnoEmeioAte3AnosQuantas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}observou_criancas_de1_ano_emeio_ate3_anos_quantas']),
      algumasCriancasDe1anoEmeioObservadasApresentavamProblemas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancas_de1ano_emeio_observadas_apresentavam_problemas']),
      algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancas_de1ano_emeio_observadas_apresentavam_problemas_quantas']),
      algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}algumas_criancas_de1ano_emeio_observadas_apresentavam_problemas_descreve']),
      observouCriancasDe3AnosAte6Anos: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}observou_criancas_de3_anos_ate6_anos']),
      observouCriancasDe3AnosAte6AnosQuantas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}observou_criancas_de3_anos_ate6_anos_quantas']),
      observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}observou_criancas_de3_anos_ate6_anos_que_algumas_apresentavam_problemas']),
      observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}observou_criancas_de3_anos_ate6_anos_que_algumas_apresentavam_problemas_quantas']),
      observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}observou_criancas_de3_anos_ate6_anos_que_algumas_apresentavam_problemas_descreve']),
      observouSituacoesQueLheChamaramAtencao:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}observou_situacoes_que_lhe_chamaram_atencao']),
      emRelacaoAPessoaEntrevistada: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}em_relacao_a_pessoa_entrevistada']),
      emRelacaoAPessoaEntrevistadaDescreve: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}em_relacao_a_pessoa_entrevistada_descreve']),
      emRelacaoAOutrosAdultosPresente: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}em_relacao_a_outros_adultos_presente']),
      emRelacaoAOutrosAdultosPresenteDescreve:
          stringType.mapFromDatabaseResponse(data[
              '${effectivePrefix}em_relacao_a_outros_adultos_presente_descreve']),
      emRelacaoAsCriancasPresentes: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}em_relacao_as_criancas_presentes']),
      emRelacaoAsCriancasPresentesDescreve: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}em_relacao_as_criancas_presentes_descreve']),
      emRelacaoAVizinhanca: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}em_relacao_a_vizinhanca']),
      emRelacaoAVizinhancaDescreve: stringType.mapFromDatabaseResponse(
          data['${effectivePrefix}em_relacao_a_vizinhanca_descreve']),
      outras:
          stringType.mapFromDatabaseResponse(data['${effectivePrefix}outras']),
      outrasDescreve: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}outras_descreve']),
      createdAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
      updatedAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}updated_at']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || uuid != null) {
      map['uuid'] = Variable<String>(uuid);
    }
    if (!nullToAbsent || nomeDoPesquisador != null) {
      map['nome_do_pesquisador'] = Variable<String>(nomeDoPesquisador);
    }
    if (!nullToAbsent || numeroDeInquerito != null) {
      map['numero_de_inquerito'] = Variable<String>(numeroDeInquerito);
    }
    if (!nullToAbsent || data != null) {
      map['data'] = Variable<DateTime>(data);
    }
    if (!nullToAbsent || municipio != null) {
      map['municipio'] = Variable<String>(municipio);
    }
    if (!nullToAbsent || bairro != null) {
      map['bairro'] = Variable<String>(bairro);
    }
    if (!nullToAbsent || pontoDeReferenciaDaCasa != null) {
      map['ponto_de_referencia_da_casa'] =
          Variable<String>(pontoDeReferenciaDaCasa);
    }
    if (!nullToAbsent || nome != null) {
      map['nome'] = Variable<String>(nome);
    }
    if (!nullToAbsent || idade != null) {
      map['idade'] = Variable<String>(idade);
    }
    if (!nullToAbsent || contacto != null) {
      map['contacto'] = Variable<String>(contacto);
    }
    if (!nullToAbsent || eChefeDaFamilia != null) {
      map['e_chefe_da_familia'] = Variable<String>(eChefeDaFamilia);
    }
    if (!nullToAbsent || caoNaoVinculoDeParantesco != null) {
      map['cao_nao_vinculo_de_parantesco'] =
          Variable<String>(caoNaoVinculoDeParantesco);
    }
    if (!nullToAbsent || idadeDoChefeDaFamilia != null) {
      map['idade_do_chefe_da_familia'] =
          Variable<String>(idadeDoChefeDaFamilia);
    }
    if (!nullToAbsent || estudou != null) {
      map['estudou'] = Variable<String>(estudou);
    }
    if (!nullToAbsent || eAlfabetizado != null) {
      map['e_alfabetizado'] = Variable<String>(eAlfabetizado);
    }
    if (!nullToAbsent || completouEnsinoPrimario != null) {
      map['completou_ensino_primario'] =
          Variable<String>(completouEnsinoPrimario);
    }
    if (!nullToAbsent || completouEnsinoSecundario != null) {
      map['completou_ensino_secundario'] =
          Variable<String>(completouEnsinoSecundario);
    }
    if (!nullToAbsent || fezCursoSuperior != null) {
      map['fez_curso_superior'] = Variable<String>(fezCursoSuperior);
    }
    if (!nullToAbsent || naoSabe != null) {
      map['nao_sabe'] = Variable<String>(naoSabe);
    }
    if (!nullToAbsent || chefeDeFamiliaTemSalario != null) {
      map['chefe_de_familia_tem_salario'] =
          Variable<String>(chefeDeFamiliaTemSalario);
    }
    if (!nullToAbsent || quantoGastaParaSustentarFamiliaPorMes != null) {
      map['quanto_gasta_para_sustentar_familia_por_mes'] =
          Variable<String>(quantoGastaParaSustentarFamiliaPorMes);
    }
    if (!nullToAbsent || quantasFamiliasResidemNacasa != null) {
      map['quantas_familias_residem_nacasa'] =
          Variable<String>(quantasFamiliasResidemNacasa);
    }
    if (!nullToAbsent || quantosAdultos != null) {
      map['quantos_adultos'] = Variable<String>(quantosAdultos);
    }
    if (!nullToAbsent || quantosAdultosMaioresDe18anos != null) {
      map['quantos_adultos_maiores_de18anos'] =
          Variable<String>(quantosAdultosMaioresDe18anos);
    }
    if (!nullToAbsent || quantosAdultosMaioresDe18anosHomens != null) {
      map['quantos_adultos_maiores_de18anos_homens'] =
          Variable<String>(quantosAdultosMaioresDe18anosHomens);
    }
    if (!nullToAbsent || quantosAdultosMaioresDe18anosHomensTrabalham != null) {
      map['quantos_adultos_maiores_de18anos_homens_trabalham'] =
          Variable<String>(quantosAdultosMaioresDe18anosHomensTrabalham);
    }
    if (!nullToAbsent ||
        quantosAdultosMaioresDe18anosHomensAlfabetizados != null) {
      map['quantos_adultos_maiores_de18anos_homens_alfabetizados'] =
          Variable<String>(quantosAdultosMaioresDe18anosHomensAlfabetizados);
    }
    if (!nullToAbsent || quantosAdultosM18Mulheres != null) {
      map['quantos_adultos_m18_mulheres'] =
          Variable<String>(quantosAdultosM18Mulheres);
    }
    if (!nullToAbsent || quantosAdultosM18MulheresTrabalham != null) {
      map['quantos_adultos_m18_mulheres_trabalham'] =
          Variable<String>(quantosAdultosM18MulheresTrabalham);
    }
    if (!nullToAbsent || quantosAdultosM18MulheresAlfabetizados != null) {
      map['quantos_adultos_m18_mulheres_alfabetizados'] =
          Variable<String>(quantosAdultosM18MulheresAlfabetizados);
    }
    if (!nullToAbsent || relatosDeDeficiencia != null) {
      map['relatos_de_deficiencia'] = Variable<String>(relatosDeDeficiencia);
    }
    if (!nullToAbsent || totalcriancasAte1anoemeio != null) {
      map['totalcriancas_ate1anoemeio'] =
          Variable<String>(totalcriancasAte1anoemeio);
    }
    if (!nullToAbsent || totalcriancasAte1anoemeioMeninos != null) {
      map['totalcriancas_ate1anoemeio_meninos'] =
          Variable<String>(totalcriancasAte1anoemeioMeninos);
    }
    if (!nullToAbsent || totalcriancasAte1anoemeioMeninas != null) {
      map['totalcriancas_ate1anoemeio_meninas'] =
          Variable<String>(totalcriancasAte1anoemeioMeninas);
    }
    if (!nullToAbsent || algumasCriancasAte1anoemeioNaodesenvolvem != null) {
      map['algumas_criancas_ate1anoemeio_naodesenvolvem'] =
          Variable<String>(algumasCriancasAte1anoemeioNaodesenvolvem);
    }
    if (!nullToAbsent ||
        algumasCriancasAte1anoemeioNaodesenvolvemQuantas != null) {
      map['algumas_criancas_ate1anoemeio_naodesenvolvem_quantas'] =
          Variable<String>(algumasCriancasAte1anoemeioNaodesenvolvemQuantas);
    }
    if (!nullToAbsent || algumaCriancasAte1anoemeioQueTipodeProblema != null) {
      map['alguma_criancas_ate1anoemeio_que_tipode_problema'] =
          Variable<String>(algumaCriancasAte1anoemeioQueTipodeProblema);
    }
    if (!nullToAbsent || algumaCriancasAte1anoemeioTemProblemaMotor != null) {
      map['alguma_criancas_ate1anoemeio_tem_problema_motor'] =
          Variable<String>(algumaCriancasAte1anoemeioTemProblemaMotor);
    }
    if (!nullToAbsent ||
        algumaCriancasAte1anoemeioTemProblemaMotorQuantas != null) {
      map['alguma_criancas_ate1anoemeio_tem_problema_motor_quantas'] =
          Variable<String>(algumaCriancasAte1anoemeioTemProblemaMotorQuantas);
    }
    if (!nullToAbsent || algumaCriancasAte1anoemeioTemMalFormacao != null) {
      map['alguma_criancas_ate1anoemeio_tem_mal_formacao'] =
          Variable<String>(algumaCriancasAte1anoemeioTemMalFormacao);
    }
    if (!nullToAbsent ||
        algumaCriancasAte1anoemeioTemMalFormacaoQuantas != null) {
      map['alguma_criancas_ate1anoemeio_tem_mal_formacao_quantas'] =
          Variable<String>(algumaCriancasAte1anoemeioTemMalFormacaoQuantas);
    }
    if (!nullToAbsent ||
        algumaCriancasAte1anoemeioNaoReagemAIncentivo != null) {
      map['alguma_criancas_ate1anoemeio_nao_reagem_a_incentivo'] =
          Variable<String>(algumaCriancasAte1anoemeioNaoReagemAIncentivo);
    }
    if (!nullToAbsent ||
        algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas != null) {
      map['alguma_criancas_ate1anoemeio_nao_reagem_a_incentivo_quantas'] =
          Variable<String>(
              algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas);
    }
    if (!nullToAbsent || algumaCriancasAte1anoemeioTemReacaoAgressiva != null) {
      map['alguma_criancas_ate1anoemeio_tem_reacao_agressiva'] =
          Variable<String>(algumaCriancasAte1anoemeioTemReacaoAgressiva);
    }
    if (!nullToAbsent ||
        algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas != null) {
      map['alguma_criancas_ate1anoemeio_tem_reacao_agressiva_quantas'] =
          Variable<String>(algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas);
    }
    if (!nullToAbsent || totalcriancasAte1anoemeioA3anos != null) {
      map['totalcriancas_ate1anoemeio_a3anos'] =
          Variable<String>(totalcriancasAte1anoemeioA3anos);
    }
    if (!nullToAbsent || totalcriancasAte1anoemeioA3anosMeninos != null) {
      map['totalcriancas_ate1anoemeio_a3anos_meninos'] =
          Variable<String>(totalcriancasAte1anoemeioA3anosMeninos);
    }
    if (!nullToAbsent || totalcriancasAte1anoemeioA3anosMeninas != null) {
      map['totalcriancas_ate1anoemeio_a3anos_meninas'] =
          Variable<String>(totalcriancasAte1anoemeioA3anosMeninas);
    }
    if (!nullToAbsent ||
        algumasCriancasAte1anoemeioA3anosNaodesenvolvem != null) {
      map['algumas_criancas_ate1anoemeio_a3anos_naodesenvolvem'] =
          Variable<String>(algumasCriancasAte1anoemeioA3anosNaodesenvolvem);
    }
    if (!nullToAbsent ||
        algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas != null) {
      map['algumas_criancas_ate1anoemeio_a3anos_naodesenvolvem_quantas'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas);
    }
    if (!nullToAbsent ||
        algumaCriancasAte1anoemeioA3anosQueTipodeProblema != null) {
      map['alguma_criancas_ate1anoemeio_a3anos_que_tipode_problema'] =
          Variable<String>(algumaCriancasAte1anoemeioA3anosQueTipodeProblema);
    }
    if (!nullToAbsent ||
        algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas != null) {
      map['algumas_criancas_ate1anoemeio_a3anos_nao_interagem_com_pessoas'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas);
    }
    if (!nullToAbsent ||
        algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas !=
            null) {
      map['algumas_criancas_ate1anoemeio_a3anos_nao_interagem_com_pessoas_quantas'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas);
    }
    if (!nullToAbsent ||
        algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo != null) {
      map['algumas_criancas_ate1anoemeio_a3anos_tem_comportamento_agressivo'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo);
    }
    if (!nullToAbsent ||
        algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas !=
            null) {
      map['algumas_criancas_ate1anoemeio_a3anos_tem_comportamento_agressivo_quantas'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas);
    }
    if (!nullToAbsent ||
        algumasCriancasAte1anoemeioA3anosAlgunsAgridem != null) {
      map['algumas_criancas_ate1anoemeio_a3anos_alguns_agridem'] =
          Variable<String>(algumasCriancasAte1anoemeioA3anosAlgunsAgridem);
    }
    if (!nullToAbsent ||
        algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas != null) {
      map['algumas_criancas_ate1anoemeio_a3anos_alguns_agridem_quantas'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas);
    }
    if (!nullToAbsent ||
        algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal != null) {
      map['algumas_criancas_ate1anoemeio_a3anos_algumas_agitadas_que_o_normal'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal);
    }
    if (!nullToAbsent ||
        algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas !=
            null) {
      map['algumas_criancas_ate1anoemeio_a3anos_algumas_agitadas_que_o_normal_quantas'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas);
    }
    if (!nullToAbsent ||
        algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao != null) {
      map['algumas_criancas_ate1anoemeio_a3anos_algumas_tem_mal_formacao'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao);
    }
    if (!nullToAbsent ||
        algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas != null) {
      map['algumas_criancas_ate1anoemeio_a3anos_algumas_tem_mal_formacao_quantas'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas);
    }
    if (!nullToAbsent ||
        algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas != null) {
      map['algumas_criancas_ate1anoemeio_a3anos_algumas_nao_andam_sozinhas'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas);
    }
    if (!nullToAbsent ||
        algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas !=
            null) {
      map['algumas_criancas_ate1anoemeio_a3anos_algumas_nao_andam_sozinhas_quantas'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas);
    }
    if (!nullToAbsent ||
        algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente !=
            null) {
      map['algumas_criancas_ate1anoemeio_a3anos_algumas_nao_expressam_verbalmente'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente);
    }
    if (!nullToAbsent ||
        algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas !=
            null) {
      map['algumas_criancas_ate1anoemeio_a3anos_algumas_nao_expressam_verbalmente_quantas'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas);
    }
    if (!nullToAbsent ||
        algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos !=
            null) {
      map['algumas_criancas_ate1anoemeio_a3anos_algumas_nao_expressam_sentimentos'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos);
    }
    if (!nullToAbsent ||
        algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas !=
            null) {
      map['algumas_criancas_ate1anoemeio_a3anos_algumas_nao_expressam_sentimentos_quantas'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas);
    }
    if (!nullToAbsent ||
        algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown != null) {
      map['algumas_criancas_ate1anoemeio_a3anos_algumas_tem_sindrome_de_down'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown);
    }
    if (!nullToAbsent ||
        algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas !=
            null) {
      map['algumas_criancas_ate1anoemeio_a3anos_algumas_tem_sindrome_de_down_quantas'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas);
    }
    if (!nullToAbsent || totalcriancasde3A6anos != null) {
      map['totalcriancasde3_a6anos'] = Variable<String>(totalcriancasde3A6anos);
    }
    if (!nullToAbsent || totalcriancasde3A6anosMeninos != null) {
      map['totalcriancasde3_a6anos_meninos'] =
          Variable<String>(totalcriancasde3A6anosMeninos);
    }
    if (!nullToAbsent || totalcriancasde3A6anosMeninas != null) {
      map['totalcriancasde3_a6anos_meninas'] =
          Variable<String>(totalcriancasde3A6anosMeninas);
    }
    if (!nullToAbsent || algumasCriancasde3A6anosNaodesenvolvem != null) {
      map['algumas_criancasde3_a6anos_naodesenvolvem'] =
          Variable<String>(algumasCriancasde3A6anosNaodesenvolvem);
    }
    if (!nullToAbsent ||
        algumasCriancasde3A6anosNaodesenvolvemQuantas != null) {
      map['algumas_criancasde3_a6anos_naodesenvolvem_quantas'] =
          Variable<String>(algumasCriancasde3A6anosNaodesenvolvemQuantas);
    }
    if (!nullToAbsent || algumaCriancasde3A6anosQueTipodeProblema != null) {
      map['alguma_criancasde3_a6anos_que_tipode_problema'] =
          Variable<String>(algumaCriancasde3A6anosQueTipodeProblema);
    }
    if (!nullToAbsent ||
        algumasCriancasde3A6anosNaoInteragemComPessoas != null) {
      map['algumas_criancasde3_a6anos_nao_interagem_com_pessoas'] =
          Variable<String>(algumasCriancasde3A6anosNaoInteragemComPessoas);
    }
    if (!nullToAbsent ||
        algumasCriancasde3A6anosNaoInteragemComPessoasQuantas != null) {
      map['algumas_criancasde3_a6anos_nao_interagem_com_pessoas_quantas'] =
          Variable<String>(
              algumasCriancasde3A6anosNaoInteragemComPessoasQuantas);
    }
    if (!nullToAbsent ||
        algumasCriancasde3A6anosTemComportamentoAgressivo != null) {
      map['algumas_criancasde3_a6anos_tem_comportamento_agressivo'] =
          Variable<String>(algumasCriancasde3A6anosTemComportamentoAgressivo);
    }
    if (!nullToAbsent ||
        algumasCriancasde3A6anosTemComportamentoAgressivoQuantas != null) {
      map['algumas_criancasde3_a6anos_tem_comportamento_agressivo_quantas'] =
          Variable<String>(
              algumasCriancasde3A6anosTemComportamentoAgressivoQuantas);
    }
    if (!nullToAbsent || algumasCriancasde3A6anosAlgunsAgridem != null) {
      map['algumas_criancasde3_a6anos_alguns_agridem'] =
          Variable<String>(algumasCriancasde3A6anosAlgunsAgridem);
    }
    if (!nullToAbsent || algumasCriancasde3A6anosAlgunsAgridemQuantas != null) {
      map['algumas_criancasde3_a6anos_alguns_agridem_quantas'] =
          Variable<String>(algumasCriancasde3A6anosAlgunsAgridemQuantas);
    }
    if (!nullToAbsent ||
        algumasCriancasde3A6anosAlgumasAgitadasQueONormal != null) {
      map['algumas_criancasde3_a6anos_algumas_agitadas_que_o_normal'] =
          Variable<String>(algumasCriancasde3A6anosAlgumasAgitadasQueONormal);
    }
    if (!nullToAbsent ||
        algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas != null) {
      map['algumas_criancasde3_a6anos_algumas_agitadas_que_o_normal_quantas'] =
          Variable<String>(
              algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas);
    }
    if (!nullToAbsent ||
        algumasCriancasde3A6anosAlgumasTemMalFormacao != null) {
      map['algumas_criancasde3_a6anos_algumas_tem_mal_formacao'] =
          Variable<String>(algumasCriancasde3A6anosAlgumasTemMalFormacao);
    }
    if (!nullToAbsent ||
        algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas != null) {
      map['algumas_criancasde3_a6anos_algumas_tem_mal_formacao_quantas'] =
          Variable<String>(
              algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas);
    }
    if (!nullToAbsent ||
        algumasCriancasde3A6anosAlgumasNaoAndamSozinhas != null) {
      map['algumas_criancasde3_a6anos_algumas_nao_andam_sozinhas'] =
          Variable<String>(algumasCriancasde3A6anosAlgumasNaoAndamSozinhas);
    }
    if (!nullToAbsent ||
        algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas != null) {
      map['algumas_criancasde3_a6anos_algumas_nao_andam_sozinhas_quantas'] =
          Variable<String>(
              algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas);
    }
    if (!nullToAbsent ||
        algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente != null) {
      map['algumas_criancasde3_a6anos_algumas_nao_expressam_verbalmente'] =
          Variable<String>(
              algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente);
    }
    if (!nullToAbsent ||
        algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas != null) {
      map['algumas_criancasde3_a6anos_algumas_nao_expressam_verbalmente_quantas'] =
          Variable<String>(
              algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas);
    }
    if (!nullToAbsent ||
        algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos != null) {
      map['algumas_criancasde3_a6anos_algumas_nao_expressam_sentimentos'] =
          Variable<String>(
              algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos);
    }
    if (!nullToAbsent ||
        algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas != null) {
      map['algumas_criancasde3_a6anos_algumas_nao_expressam_sentimentos_quantas'] =
          Variable<String>(
              algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas);
    }
    if (!nullToAbsent ||
        algumasCriancasde3A6anosAlgumasTemSindromeDeDown != null) {
      map['algumas_criancasde3_a6anos_algumas_tem_sindrome_de_down'] =
          Variable<String>(algumasCriancasde3A6anosAlgumasTemSindromeDeDown);
    }
    if (!nullToAbsent ||
        algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas != null) {
      map['algumas_criancasde3_a6anos_algumas_tem_sindrome_de_down_quantas'] =
          Variable<String>(
              algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas);
    }
    if (!nullToAbsent ||
        algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas != null) {
      map['algumas_criancasde3_a6anos_algumas_nao_sabem_se_alimentar_sozinhas'] =
          Variable<String>(
              algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas);
    }
    if (!nullToAbsent ||
        algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas !=
            null) {
      map['algumas_criancasde3_a6anos_algumas_nao_sabe_se_alimentar_sozinhas_quantas'] =
          Variable<String>(
              algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas);
    }
    if (!nullToAbsent ||
        algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas != null) {
      map['algumas_criancasde3_a6anos_algumas_nao_conseguem_tomar_banho_sozinhas'] =
          Variable<String>(
              algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas);
    }
    if (!nullToAbsent ||
        algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas !=
            null) {
      map['algumas_criancasde3_a6anos_algumas_nao_conseguem_tomar_banho_sozinhas_quantas'] =
          Variable<String>(
              algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas);
    }
    if (!nullToAbsent ||
        algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas !=
            null) {
      map['algumas_criancasde3_a6anos_algumas_nao_conseguem_ir_a_casa_de_banho_sozinhas'] =
          Variable<String>(
              algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas);
    }
    if (!nullToAbsent ||
        algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas !=
            null) {
      map['algumas_criancasde3_a6anos_algumas_nao_conseguem_ir_a_casa_de_banho_sozinhas_quantas'] =
          Variable<String>(
              algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas);
    }
    if (!nullToAbsent ||
        algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas != null) {
      map['algumas_criancasde3_a6anos_algumas_nao_conseguem_se_vestir_sozinhas'] =
          Variable<String>(
              algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas);
    }
    if (!nullToAbsent ||
        algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas !=
            null) {
      map['algumas_criancasde3_a6anos_algumas_nao_conseguem_se_vestir_sozinhas_quantas'] =
          Variable<String>(
              algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas);
    }
    if (!nullToAbsent || quantasCriancasde3A6anosEstaoNaEscolinha != null) {
      map['quantas_criancasde3_a6anos_estao_na_escolinha'] =
          Variable<String>(quantasCriancasde3A6anosEstaoNaEscolinha);
    }
    if (!nullToAbsent || criancasDe3A6anosNaoEstaoNaEscolinhaPorque != null) {
      map['criancas_de3_a6anos_nao_estao_na_escolinha_porque'] =
          Variable<String>(criancasDe3A6anosNaoEstaoNaEscolinhaPorque);
    }
    if (!nullToAbsent || quantasCriancasde3A6anosTrabalham != null) {
      map['quantas_criancasde3_a6anos_trabalham'] =
          Variable<String>(quantasCriancasde3A6anosTrabalham);
    }
    if (!nullToAbsent ||
        criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos != null) {
      map['criancas_de3_a6anos_nao_estao_na_escolinha_por_outos_motivos'] =
          Variable<String>(criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos);
    }
    if (!nullToAbsent || totalcriancasde7A9anos != null) {
      map['totalcriancasde7_a9anos'] = Variable<String>(totalcriancasde7A9anos);
    }
    if (!nullToAbsent || totalcriancasde7A9anosMeninos != null) {
      map['totalcriancasde7_a9anos_meninos'] =
          Variable<String>(totalcriancasde7A9anosMeninos);
    }
    if (!nullToAbsent || totalcriancasde7A9anosMeninas != null) {
      map['totalcriancasde7_a9anos_meninas'] =
          Variable<String>(totalcriancasde7A9anosMeninas);
    }
    if (!nullToAbsent || quantasCriancasde7A9anosEstaoNaEscola != null) {
      map['quantas_criancasde7_a9anos_estao_na_escola'] =
          Variable<String>(quantasCriancasde7A9anosEstaoNaEscola);
    }
    if (!nullToAbsent || quantasCriancasde7A9anosTrabalham != null) {
      map['quantas_criancasde7_a9anos_trabalham'] =
          Variable<String>(quantasCriancasde7A9anosTrabalham);
    }
    if (!nullToAbsent ||
        nosUltimos5AnosQuantasGravidezesTiveNaFamilia != null) {
      map['nos_ultimos5_anos_quantas_gravidezes_tive_na_familia'] =
          Variable<String>(nosUltimos5AnosQuantasGravidezesTiveNaFamilia);
    }
    if (!nullToAbsent || partosOcoridosNoHospital != null) {
      map['partos_ocoridos_no_hospital'] =
          Variable<String>(partosOcoridosNoHospital);
    }
    if (!nullToAbsent || partosOcoridosEmCasa != null) {
      map['partos_ocoridos_em_casa'] = Variable<String>(partosOcoridosEmCasa);
    }
    if (!nullToAbsent || partosOcoridosEmOutroLocal != null) {
      map['partos_ocoridos_em_outro_local'] =
          Variable<String>(partosOcoridosEmOutroLocal);
    }
    if (!nullToAbsent || outroLocalDoPartoQualLocal != null) {
      map['outro_local_do_parto_qual_local'] =
          Variable<String>(outroLocalDoPartoQualLocal);
    }
    if (!nullToAbsent || quantasCriancasNasceramVivas != null) {
      map['quantas_criancas_nasceram_vivas'] =
          Variable<String>(quantasCriancasNasceramVivas);
    }
    if (!nullToAbsent || quantasContinuamVivas != null) {
      map['quantas_continuam_vivas'] = Variable<String>(quantasContinuamVivas);
    }
    if (!nullToAbsent ||
        dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram !=
            null) {
      map['das_vivas_algumas_apresentam_problemas_de_desenvolvimento_que_outros_nao_tiveram'] =
          Variable<String>(
              dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram);
    }
    if (!nullToAbsent || descreverOProblema != null) {
      map['descrever_o_problema'] = Variable<String>(descreverOProblema);
    }
    if (!nullToAbsent || aFamiliaProcurouAjudaParaSuperarEsseProblema != null) {
      map['a_familia_procurou_ajuda_para_superar_esse_problema'] =
          Variable<String>(aFamiliaProcurouAjudaParaSuperarEsseProblema);
    }
    if (!nullToAbsent || seSimQueTipoDeAjudaEOnde != null) {
      map['se_sim_que_tipo_de_ajuda_e_onde'] =
          Variable<String>(seSimQueTipoDeAjudaEOnde);
    }
    if (!nullToAbsent ||
        osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia !=
            null) {
      map['os_vizinhos_se_preocuparam_quando_nasceu_uma_crianca_com_problema_na_familia'] =
          Variable<String>(
              osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia);
    }
    if (!nullToAbsent ||
        dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida !=
            null) {
      map['das_criancas_que_nasceram_vivas_quantas_nao_sobreviveram_nos2_primeiros_anos_de_vida'] =
          Variable<String>(
              dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida);
    }
    if (!nullToAbsent || sabeDizerOMotivoDosObitos != null) {
      map['sabe_dizer_o_motivo_dos_obitos'] =
          Variable<String>(sabeDizerOMotivoDosObitos);
    }
    if (!nullToAbsent || seSimQualFoiPrincipalRazaodoObito != null) {
      map['se_sim_qual_foi_principal_razaodo_obito'] =
          Variable<String>(seSimQualFoiPrincipalRazaodoObito);
    }
    if (!nullToAbsent || voceTemMedoDeTerUmFilhoOuNetoComProblemas != null) {
      map['voce_tem_medo_de_ter_um_filho_ou_neto_com_problemas'] =
          Variable<String>(voceTemMedoDeTerUmFilhoOuNetoComProblemas);
    }
    if (!nullToAbsent || expliquePorQue != null) {
      map['explique_por_que'] = Variable<String>(expliquePorQue);
    }
    if (!nullToAbsent ||
        porqueVoceAchaQueUmaCriancaPodeNascerComProblema != null) {
      map['porque_voce_acha_que_uma_crianca_pode_nascer_com_problema'] =
          Variable<String>(porqueVoceAchaQueUmaCriancaPodeNascerComProblema);
    }
    if (!nullToAbsent ||
        oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema != null) {
      map['o_que_acontece_no_seu_bairro_quando_nasce_uma_crianca_com_problema'] =
          Variable<String>(
              oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema);
    }
    if (!nullToAbsent ||
        achaQueUmaCriancaQueNasceComProblemaPodeSobreviver != null) {
      map['acha_que_uma_crianca_que_nasce_com_problema_pode_sobreviver'] =
          Variable<String>(achaQueUmaCriancaQueNasceComProblemaPodeSobreviver);
    }
    if (!nullToAbsent || expliquePorque != null) {
      map['explique_porque'] = Variable<String>(expliquePorque);
    }
    if (!nullToAbsent ||
        quemPodeAjudarQuandoUmaCriancaNasceComProblema != null) {
      map['quem_pode_ajudar_quando_uma_crianca_nasce_com_problema'] =
          Variable<String>(quemPodeAjudarQuandoUmaCriancaNasceComProblema);
    }
    if (!nullToAbsent ||
        emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao != null) {
      map['em_casa_quem_cuida_mais_das_criancas_ate6_anos_para_alimentacao'] =
          Variable<String>(
              emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao);
    }
    if (!nullToAbsent ||
        emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene != null) {
      map['em_casa_quem_cuida_mais_das_criancas_ate6_anos_para_os_cuidados_de_higiene'] =
          Variable<String>(
              emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene);
    }
    if (!nullToAbsent ||
        emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos != null) {
      map['em_casa_quem_cuida_mais_das_criancas_ate6_anos_para_os_estudos'] =
          Variable<String>(emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos);
    }
    if (!nullToAbsent ||
        aPessoaEntrevistadaPareceuSinceraNasSuasRespostas != null) {
      map['a_pessoa_entrevistada_pareceu_sincera_nas_suas_respostas'] =
          Variable<String>(aPessoaEntrevistadaPareceuSinceraNasSuasRespostas);
    }
    if (!nullToAbsent ||
        algumasPerguntasIncomodaramAPessoaEntrevistada != null) {
      map['algumas_perguntas_incomodaram_a_pessoa_entrevistada'] =
          Variable<String>(algumasPerguntasIncomodaramAPessoaEntrevistada);
    }
    if (!nullToAbsent || seSimQuais3PrincipaisPerguntas != null) {
      map['se_sim_quais3_principais_perguntas'] =
          Variable<String>(seSimQuais3PrincipaisPerguntas);
    }
    if (!nullToAbsent || todasAsPerguntasIncomodaram != null) {
      map['todas_as_perguntas_incomodaram'] =
          Variable<String>(todasAsPerguntasIncomodaram);
    }
    if (!nullToAbsent || aResidenciaEraDe != null) {
      map['a_residencia_era_de'] = Variable<String>(aResidenciaEraDe);
    }
    if (!nullToAbsent || comoAvaliaHigieneDaCasa != null) {
      map['como_avalia_higiene_da_casa'] =
          Variable<String>(comoAvaliaHigieneDaCasa);
    }
    if (!nullToAbsent || oQueEraMal != null) {
      map['o_que_era_mal'] = Variable<String>(oQueEraMal);
    }
    if (!nullToAbsent || tinhaCriancasNaCasaDuranteInquerito != null) {
      map['tinha_criancas_na_casa_durante_inquerito'] =
          Variable<String>(tinhaCriancasNaCasaDuranteInquerito);
    }
    if (!nullToAbsent || tinhaCriancasNaCasaDuranteInqueritoQuantas != null) {
      map['tinha_criancas_na_casa_durante_inquerito_quantas'] =
          Variable<String>(tinhaCriancasNaCasaDuranteInqueritoQuantas);
    }
    if (!nullToAbsent || tentouIncentivar != null) {
      map['tentou_incentivar'] = Variable<String>(tentouIncentivar);
    }
    if (!nullToAbsent || tentouIncentivarQuantas != null) {
      map['tentou_incentivar_quantas'] =
          Variable<String>(tentouIncentivarQuantas);
    }
    if (!nullToAbsent ||
        seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos != null) {
      map['se_incentivou_quantas_reagiram_positivamente_aos_seus_incentivos'] =
          Variable<String>(
              seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos);
    }
    if (!nullToAbsent || observouCriancasDeAte1Ano != null) {
      map['observou_criancas_de_ate1_ano'] =
          Variable<String>(observouCriancasDeAte1Ano);
    }
    if (!nullToAbsent || observouCriancasDeAte1AnoQuantos != null) {
      map['observou_criancas_de_ate1_ano_quantos'] =
          Variable<String>(observouCriancasDeAte1AnoQuantos);
    }
    if (!nullToAbsent || algumasApresentavamProblemas != null) {
      map['algumas_apresentavam_problemas'] =
          Variable<String>(algumasApresentavamProblemas);
    }
    if (!nullToAbsent || algumasApresentavamProblemasQuantos != null) {
      map['algumas_apresentavam_problemas_quantos'] =
          Variable<String>(algumasApresentavamProblemasQuantos);
    }
    if (!nullToAbsent || descreveOProblemaDessasCriancas != null) {
      map['descreve_o_problema_dessas_criancas'] =
          Variable<String>(descreveOProblemaDessasCriancas);
    }
    if (!nullToAbsent || observouCriancasDe1AnoEmeioAte3Anos != null) {
      map['observou_criancas_de1_ano_emeio_ate3_anos'] =
          Variable<String>(observouCriancasDe1AnoEmeioAte3Anos);
    }
    if (!nullToAbsent || observouCriancasDe1AnoEmeioAte3AnosQuantas != null) {
      map['observou_criancas_de1_ano_emeio_ate3_anos_quantas'] =
          Variable<String>(observouCriancasDe1AnoEmeioAte3AnosQuantas);
    }
    if (!nullToAbsent ||
        algumasCriancasDe1anoEmeioObservadasApresentavamProblemas != null) {
      map['algumas_criancas_de1ano_emeio_observadas_apresentavam_problemas'] =
          Variable<String>(
              algumasCriancasDe1anoEmeioObservadasApresentavamProblemas);
    }
    if (!nullToAbsent ||
        algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas !=
            null) {
      map['algumas_criancas_de1ano_emeio_observadas_apresentavam_problemas_quantas'] =
          Variable<String>(
              algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas);
    }
    if (!nullToAbsent ||
        algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve !=
            null) {
      map['algumas_criancas_de1ano_emeio_observadas_apresentavam_problemas_descreve'] =
          Variable<String>(
              algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve);
    }
    if (!nullToAbsent || observouCriancasDe3AnosAte6Anos != null) {
      map['observou_criancas_de3_anos_ate6_anos'] =
          Variable<String>(observouCriancasDe3AnosAte6Anos);
    }
    if (!nullToAbsent || observouCriancasDe3AnosAte6AnosQuantas != null) {
      map['observou_criancas_de3_anos_ate6_anos_quantas'] =
          Variable<String>(observouCriancasDe3AnosAte6AnosQuantas);
    }
    if (!nullToAbsent ||
        observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas !=
            null) {
      map['observou_criancas_de3_anos_ate6_anos_que_algumas_apresentavam_problemas'] =
          Variable<String>(
              observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas);
    }
    if (!nullToAbsent ||
        observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas !=
            null) {
      map['observou_criancas_de3_anos_ate6_anos_que_algumas_apresentavam_problemas_quantas'] =
          Variable<String>(
              observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas);
    }
    if (!nullToAbsent ||
        observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve !=
            null) {
      map['observou_criancas_de3_anos_ate6_anos_que_algumas_apresentavam_problemas_descreve'] =
          Variable<String>(
              observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve);
    }
    if (!nullToAbsent || observouSituacoesQueLheChamaramAtencao != null) {
      map['observou_situacoes_que_lhe_chamaram_atencao'] =
          Variable<String>(observouSituacoesQueLheChamaramAtencao);
    }
    if (!nullToAbsent || emRelacaoAPessoaEntrevistada != null) {
      map['em_relacao_a_pessoa_entrevistada'] =
          Variable<String>(emRelacaoAPessoaEntrevistada);
    }
    if (!nullToAbsent || emRelacaoAPessoaEntrevistadaDescreve != null) {
      map['em_relacao_a_pessoa_entrevistada_descreve'] =
          Variable<String>(emRelacaoAPessoaEntrevistadaDescreve);
    }
    if (!nullToAbsent || emRelacaoAOutrosAdultosPresente != null) {
      map['em_relacao_a_outros_adultos_presente'] =
          Variable<String>(emRelacaoAOutrosAdultosPresente);
    }
    if (!nullToAbsent || emRelacaoAOutrosAdultosPresenteDescreve != null) {
      map['em_relacao_a_outros_adultos_presente_descreve'] =
          Variable<String>(emRelacaoAOutrosAdultosPresenteDescreve);
    }
    if (!nullToAbsent || emRelacaoAsCriancasPresentes != null) {
      map['em_relacao_as_criancas_presentes'] =
          Variable<String>(emRelacaoAsCriancasPresentes);
    }
    if (!nullToAbsent || emRelacaoAsCriancasPresentesDescreve != null) {
      map['em_relacao_as_criancas_presentes_descreve'] =
          Variable<String>(emRelacaoAsCriancasPresentesDescreve);
    }
    if (!nullToAbsent || emRelacaoAVizinhanca != null) {
      map['em_relacao_a_vizinhanca'] = Variable<String>(emRelacaoAVizinhanca);
    }
    if (!nullToAbsent || emRelacaoAVizinhancaDescreve != null) {
      map['em_relacao_a_vizinhanca_descreve'] =
          Variable<String>(emRelacaoAVizinhancaDescreve);
    }
    if (!nullToAbsent || outras != null) {
      map['outras'] = Variable<String>(outras);
    }
    if (!nullToAbsent || outrasDescreve != null) {
      map['outras_descreve'] = Variable<String>(outrasDescreve);
    }
    if (!nullToAbsent || createdAt != null) {
      map['created_at'] = Variable<DateTime>(createdAt);
    }
    if (!nullToAbsent || updatedAt != null) {
      map['updated_at'] = Variable<DateTime>(updatedAt);
    }
    return map;
  }

  UsersCompanion toCompanion(bool nullToAbsent) {
    return UsersCompanion(
      uuid: uuid == null && nullToAbsent ? const Value.absent() : Value(uuid),
      nomeDoPesquisador: nomeDoPesquisador == null && nullToAbsent
          ? const Value.absent()
          : Value(nomeDoPesquisador),
      numeroDeInquerito: numeroDeInquerito == null && nullToAbsent
          ? const Value.absent()
          : Value(numeroDeInquerito),
      data: data == null && nullToAbsent ? const Value.absent() : Value(data),
      municipio: municipio == null && nullToAbsent
          ? const Value.absent()
          : Value(municipio),
      bairro:
          bairro == null && nullToAbsent ? const Value.absent() : Value(bairro),
      pontoDeReferenciaDaCasa: pontoDeReferenciaDaCasa == null && nullToAbsent
          ? const Value.absent()
          : Value(pontoDeReferenciaDaCasa),
      nome: nome == null && nullToAbsent ? const Value.absent() : Value(nome),
      idade:
          idade == null && nullToAbsent ? const Value.absent() : Value(idade),
      contacto: contacto == null && nullToAbsent
          ? const Value.absent()
          : Value(contacto),
      eChefeDaFamilia: eChefeDaFamilia == null && nullToAbsent
          ? const Value.absent()
          : Value(eChefeDaFamilia),
      caoNaoVinculoDeParantesco:
          caoNaoVinculoDeParantesco == null && nullToAbsent
              ? const Value.absent()
              : Value(caoNaoVinculoDeParantesco),
      idadeDoChefeDaFamilia: idadeDoChefeDaFamilia == null && nullToAbsent
          ? const Value.absent()
          : Value(idadeDoChefeDaFamilia),
      estudou: estudou == null && nullToAbsent
          ? const Value.absent()
          : Value(estudou),
      eAlfabetizado: eAlfabetizado == null && nullToAbsent
          ? const Value.absent()
          : Value(eAlfabetizado),
      completouEnsinoPrimario: completouEnsinoPrimario == null && nullToAbsent
          ? const Value.absent()
          : Value(completouEnsinoPrimario),
      completouEnsinoSecundario:
          completouEnsinoSecundario == null && nullToAbsent
              ? const Value.absent()
              : Value(completouEnsinoSecundario),
      fezCursoSuperior: fezCursoSuperior == null && nullToAbsent
          ? const Value.absent()
          : Value(fezCursoSuperior),
      naoSabe: naoSabe == null && nullToAbsent
          ? const Value.absent()
          : Value(naoSabe),
      chefeDeFamiliaTemSalario: chefeDeFamiliaTemSalario == null && nullToAbsent
          ? const Value.absent()
          : Value(chefeDeFamiliaTemSalario),
      quantoGastaParaSustentarFamiliaPorMes:
          quantoGastaParaSustentarFamiliaPorMes == null && nullToAbsent
              ? const Value.absent()
              : Value(quantoGastaParaSustentarFamiliaPorMes),
      quantasFamiliasResidemNacasa:
          quantasFamiliasResidemNacasa == null && nullToAbsent
              ? const Value.absent()
              : Value(quantasFamiliasResidemNacasa),
      quantosAdultos: quantosAdultos == null && nullToAbsent
          ? const Value.absent()
          : Value(quantosAdultos),
      quantosAdultosMaioresDe18anos:
          quantosAdultosMaioresDe18anos == null && nullToAbsent
              ? const Value.absent()
              : Value(quantosAdultosMaioresDe18anos),
      quantosAdultosMaioresDe18anosHomens:
          quantosAdultosMaioresDe18anosHomens == null && nullToAbsent
              ? const Value.absent()
              : Value(quantosAdultosMaioresDe18anosHomens),
      quantosAdultosMaioresDe18anosHomensTrabalham:
          quantosAdultosMaioresDe18anosHomensTrabalham == null && nullToAbsent
              ? const Value.absent()
              : Value(quantosAdultosMaioresDe18anosHomensTrabalham),
      quantosAdultosMaioresDe18anosHomensAlfabetizados:
          quantosAdultosMaioresDe18anosHomensAlfabetizados == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(quantosAdultosMaioresDe18anosHomensAlfabetizados),
      quantosAdultosM18Mulheres:
          quantosAdultosM18Mulheres == null && nullToAbsent
              ? const Value.absent()
              : Value(quantosAdultosM18Mulheres),
      quantosAdultosM18MulheresTrabalham:
          quantosAdultosM18MulheresTrabalham == null && nullToAbsent
              ? const Value.absent()
              : Value(quantosAdultosM18MulheresTrabalham),
      quantosAdultosM18MulheresAlfabetizados:
          quantosAdultosM18MulheresAlfabetizados == null && nullToAbsent
              ? const Value.absent()
              : Value(quantosAdultosM18MulheresAlfabetizados),
      relatosDeDeficiencia: relatosDeDeficiencia == null && nullToAbsent
          ? const Value.absent()
          : Value(relatosDeDeficiencia),
      totalcriancasAte1anoemeio:
          totalcriancasAte1anoemeio == null && nullToAbsent
              ? const Value.absent()
              : Value(totalcriancasAte1anoemeio),
      totalcriancasAte1anoemeioMeninos:
          totalcriancasAte1anoemeioMeninos == null && nullToAbsent
              ? const Value.absent()
              : Value(totalcriancasAte1anoemeioMeninos),
      totalcriancasAte1anoemeioMeninas:
          totalcriancasAte1anoemeioMeninas == null && nullToAbsent
              ? const Value.absent()
              : Value(totalcriancasAte1anoemeioMeninas),
      algumasCriancasAte1anoemeioNaodesenvolvem:
          algumasCriancasAte1anoemeioNaodesenvolvem == null && nullToAbsent
              ? const Value.absent()
              : Value(algumasCriancasAte1anoemeioNaodesenvolvem),
      algumasCriancasAte1anoemeioNaodesenvolvemQuantas:
          algumasCriancasAte1anoemeioNaodesenvolvemQuantas == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(algumasCriancasAte1anoemeioNaodesenvolvemQuantas),
      algumaCriancasAte1anoemeioQueTipodeProblema:
          algumaCriancasAte1anoemeioQueTipodeProblema == null && nullToAbsent
              ? const Value.absent()
              : Value(algumaCriancasAte1anoemeioQueTipodeProblema),
      algumaCriancasAte1anoemeioTemProblemaMotor:
          algumaCriancasAte1anoemeioTemProblemaMotor == null && nullToAbsent
              ? const Value.absent()
              : Value(algumaCriancasAte1anoemeioTemProblemaMotor),
      algumaCriancasAte1anoemeioTemProblemaMotorQuantas:
          algumaCriancasAte1anoemeioTemProblemaMotorQuantas == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(algumaCriancasAte1anoemeioTemProblemaMotorQuantas),
      algumaCriancasAte1anoemeioTemMalFormacao:
          algumaCriancasAte1anoemeioTemMalFormacao == null && nullToAbsent
              ? const Value.absent()
              : Value(algumaCriancasAte1anoemeioTemMalFormacao),
      algumaCriancasAte1anoemeioTemMalFormacaoQuantas:
          algumaCriancasAte1anoemeioTemMalFormacaoQuantas == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(algumaCriancasAte1anoemeioTemMalFormacaoQuantas),
      algumaCriancasAte1anoemeioNaoReagemAIncentivo:
          algumaCriancasAte1anoemeioNaoReagemAIncentivo == null && nullToAbsent
              ? const Value.absent()
              : Value(algumaCriancasAte1anoemeioNaoReagemAIncentivo),
      algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas:
          algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas),
      algumaCriancasAte1anoemeioTemReacaoAgressiva:
          algumaCriancasAte1anoemeioTemReacaoAgressiva == null && nullToAbsent
              ? const Value.absent()
              : Value(algumaCriancasAte1anoemeioTemReacaoAgressiva),
      algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas:
          algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas),
      totalcriancasAte1anoemeioA3anos:
          totalcriancasAte1anoemeioA3anos == null && nullToAbsent
              ? const Value.absent()
              : Value(totalcriancasAte1anoemeioA3anos),
      totalcriancasAte1anoemeioA3anosMeninos:
          totalcriancasAte1anoemeioA3anosMeninos == null && nullToAbsent
              ? const Value.absent()
              : Value(totalcriancasAte1anoemeioA3anosMeninos),
      totalcriancasAte1anoemeioA3anosMeninas:
          totalcriancasAte1anoemeioA3anosMeninas == null && nullToAbsent
              ? const Value.absent()
              : Value(totalcriancasAte1anoemeioA3anosMeninas),
      algumasCriancasAte1anoemeioA3anosNaodesenvolvem:
          algumasCriancasAte1anoemeioA3anosNaodesenvolvem == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(algumasCriancasAte1anoemeioA3anosNaodesenvolvem),
      algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas:
          algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas),
      algumaCriancasAte1anoemeioA3anosQueTipodeProblema:
          algumaCriancasAte1anoemeioA3anosQueTipodeProblema == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(algumaCriancasAte1anoemeioA3anosQueTipodeProblema),
      algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas:
          algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas),
      algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas:
          algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas ==
                      null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(
                  algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas),
      algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo:
          algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(
                  algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo),
      algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas:
          algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas ==
                      null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(
                  algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas),
      algumasCriancasAte1anoemeioA3anosAlgunsAgridem:
          algumasCriancasAte1anoemeioA3anosAlgunsAgridem == null && nullToAbsent
              ? const Value.absent()
              : Value(algumasCriancasAte1anoemeioA3anosAlgunsAgridem),
      algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas:
          algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas),
      algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal:
          algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(
                  algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal),
      algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas:
          algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas ==
                      null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(
                  algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas),
      algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao:
          algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao),
      algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas:
          algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas ==
                      null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(
                  algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas),
      algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas:
          algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas),
      algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas:
          algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas ==
                      null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(
                  algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas),
      algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente:
          algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente ==
                      null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(
                  algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente),
      algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas:
          algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas ==
                      null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(
                  algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas),
      algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos:
          algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos ==
                      null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(
                  algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos),
      algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas:
          algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas ==
                      null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(
                  algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas),
      algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown:
          algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(
                  algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown),
      algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas:
          algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas ==
                      null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(
                  algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas),
      totalcriancasde3A6anos: totalcriancasde3A6anos == null && nullToAbsent
          ? const Value.absent()
          : Value(totalcriancasde3A6anos),
      totalcriancasde3A6anosMeninos:
          totalcriancasde3A6anosMeninos == null && nullToAbsent
              ? const Value.absent()
              : Value(totalcriancasde3A6anosMeninos),
      totalcriancasde3A6anosMeninas:
          totalcriancasde3A6anosMeninas == null && nullToAbsent
              ? const Value.absent()
              : Value(totalcriancasde3A6anosMeninas),
      algumasCriancasde3A6anosNaodesenvolvem:
          algumasCriancasde3A6anosNaodesenvolvem == null && nullToAbsent
              ? const Value.absent()
              : Value(algumasCriancasde3A6anosNaodesenvolvem),
      algumasCriancasde3A6anosNaodesenvolvemQuantas:
          algumasCriancasde3A6anosNaodesenvolvemQuantas == null && nullToAbsent
              ? const Value.absent()
              : Value(algumasCriancasde3A6anosNaodesenvolvemQuantas),
      algumaCriancasde3A6anosQueTipodeProblema:
          algumaCriancasde3A6anosQueTipodeProblema == null && nullToAbsent
              ? const Value.absent()
              : Value(algumaCriancasde3A6anosQueTipodeProblema),
      algumasCriancasde3A6anosNaoInteragemComPessoas:
          algumasCriancasde3A6anosNaoInteragemComPessoas == null && nullToAbsent
              ? const Value.absent()
              : Value(algumasCriancasde3A6anosNaoInteragemComPessoas),
      algumasCriancasde3A6anosNaoInteragemComPessoasQuantas:
          algumasCriancasde3A6anosNaoInteragemComPessoasQuantas == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(algumasCriancasde3A6anosNaoInteragemComPessoasQuantas),
      algumasCriancasde3A6anosTemComportamentoAgressivo:
          algumasCriancasde3A6anosTemComportamentoAgressivo == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(algumasCriancasde3A6anosTemComportamentoAgressivo),
      algumasCriancasde3A6anosTemComportamentoAgressivoQuantas:
          algumasCriancasde3A6anosTemComportamentoAgressivoQuantas == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(algumasCriancasde3A6anosTemComportamentoAgressivoQuantas),
      algumasCriancasde3A6anosAlgunsAgridem:
          algumasCriancasde3A6anosAlgunsAgridem == null && nullToAbsent
              ? const Value.absent()
              : Value(algumasCriancasde3A6anosAlgunsAgridem),
      algumasCriancasde3A6anosAlgunsAgridemQuantas:
          algumasCriancasde3A6anosAlgunsAgridemQuantas == null && nullToAbsent
              ? const Value.absent()
              : Value(algumasCriancasde3A6anosAlgunsAgridemQuantas),
      algumasCriancasde3A6anosAlgumasAgitadasQueONormal:
          algumasCriancasde3A6anosAlgumasAgitadasQueONormal == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(algumasCriancasde3A6anosAlgumasAgitadasQueONormal),
      algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas:
          algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas),
      algumasCriancasde3A6anosAlgumasTemMalFormacao:
          algumasCriancasde3A6anosAlgumasTemMalFormacao == null && nullToAbsent
              ? const Value.absent()
              : Value(algumasCriancasde3A6anosAlgumasTemMalFormacao),
      algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas:
          algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas),
      algumasCriancasde3A6anosAlgumasNaoAndamSozinhas:
          algumasCriancasde3A6anosAlgumasNaoAndamSozinhas == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(algumasCriancasde3A6anosAlgumasNaoAndamSozinhas),
      algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas:
          algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas),
      algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente:
          algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente),
      algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas:
          algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas ==
                      null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(
                  algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas),
      algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos:
          algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos),
      algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas:
          algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas ==
                      null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(
                  algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas),
      algumasCriancasde3A6anosAlgumasTemSindromeDeDown:
          algumasCriancasde3A6anosAlgumasTemSindromeDeDown == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(algumasCriancasde3A6anosAlgumasTemSindromeDeDown),
      algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas:
          algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas),
      algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas:
          algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(
                  algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas),
      algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas:
          algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas ==
                      null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(
                  algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas),
      algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas:
          algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas ==
                      null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(
                  algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas),
      algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas:
          algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas ==
                      null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(
                  algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas),
      algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas:
          algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas ==
                      null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(
                  algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas),
      algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas:
          algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas ==
                      null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(
                  algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas),
      algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas:
          algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(
                  algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas),
      algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas:
          algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas ==
                      null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(
                  algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas),
      quantasCriancasde3A6anosEstaoNaEscolinha:
          quantasCriancasde3A6anosEstaoNaEscolinha == null && nullToAbsent
              ? const Value.absent()
              : Value(quantasCriancasde3A6anosEstaoNaEscolinha),
      criancasDe3A6anosNaoEstaoNaEscolinhaPorque:
          criancasDe3A6anosNaoEstaoNaEscolinhaPorque == null && nullToAbsent
              ? const Value.absent()
              : Value(criancasDe3A6anosNaoEstaoNaEscolinhaPorque),
      quantasCriancasde3A6anosTrabalham:
          quantasCriancasde3A6anosTrabalham == null && nullToAbsent
              ? const Value.absent()
              : Value(quantasCriancasde3A6anosTrabalham),
      criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos:
          criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos),
      totalcriancasde7A9anos: totalcriancasde7A9anos == null && nullToAbsent
          ? const Value.absent()
          : Value(totalcriancasde7A9anos),
      totalcriancasde7A9anosMeninos:
          totalcriancasde7A9anosMeninos == null && nullToAbsent
              ? const Value.absent()
              : Value(totalcriancasde7A9anosMeninos),
      totalcriancasde7A9anosMeninas:
          totalcriancasde7A9anosMeninas == null && nullToAbsent
              ? const Value.absent()
              : Value(totalcriancasde7A9anosMeninas),
      quantasCriancasde7A9anosEstaoNaEscola:
          quantasCriancasde7A9anosEstaoNaEscola == null && nullToAbsent
              ? const Value.absent()
              : Value(quantasCriancasde7A9anosEstaoNaEscola),
      quantasCriancasde7A9anosTrabalham:
          quantasCriancasde7A9anosTrabalham == null && nullToAbsent
              ? const Value.absent()
              : Value(quantasCriancasde7A9anosTrabalham),
      nosUltimos5AnosQuantasGravidezesTiveNaFamilia:
          nosUltimos5AnosQuantasGravidezesTiveNaFamilia == null && nullToAbsent
              ? const Value.absent()
              : Value(nosUltimos5AnosQuantasGravidezesTiveNaFamilia),
      partosOcoridosNoHospital: partosOcoridosNoHospital == null && nullToAbsent
          ? const Value.absent()
          : Value(partosOcoridosNoHospital),
      partosOcoridosEmCasa: partosOcoridosEmCasa == null && nullToAbsent
          ? const Value.absent()
          : Value(partosOcoridosEmCasa),
      partosOcoridosEmOutroLocal:
          partosOcoridosEmOutroLocal == null && nullToAbsent
              ? const Value.absent()
              : Value(partosOcoridosEmOutroLocal),
      outroLocalDoPartoQualLocal:
          outroLocalDoPartoQualLocal == null && nullToAbsent
              ? const Value.absent()
              : Value(outroLocalDoPartoQualLocal),
      quantasCriancasNasceramVivas:
          quantasCriancasNasceramVivas == null && nullToAbsent
              ? const Value.absent()
              : Value(quantasCriancasNasceramVivas),
      quantasContinuamVivas: quantasContinuamVivas == null && nullToAbsent
          ? const Value.absent()
          : Value(quantasContinuamVivas),
      dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram:
          dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram ==
                      null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(
                  dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram),
      descreverOProblema: descreverOProblema == null && nullToAbsent
          ? const Value.absent()
          : Value(descreverOProblema),
      aFamiliaProcurouAjudaParaSuperarEsseProblema:
          aFamiliaProcurouAjudaParaSuperarEsseProblema == null && nullToAbsent
              ? const Value.absent()
              : Value(aFamiliaProcurouAjudaParaSuperarEsseProblema),
      seSimQueTipoDeAjudaEOnde: seSimQueTipoDeAjudaEOnde == null && nullToAbsent
          ? const Value.absent()
          : Value(seSimQueTipoDeAjudaEOnde),
      osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia:
          osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia ==
                      null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(
                  osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia),
      dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida:
          dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida ==
                      null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(
                  dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida),
      sabeDizerOMotivoDosObitos:
          sabeDizerOMotivoDosObitos == null && nullToAbsent
              ? const Value.absent()
              : Value(sabeDizerOMotivoDosObitos),
      seSimQualFoiPrincipalRazaodoObito:
          seSimQualFoiPrincipalRazaodoObito == null && nullToAbsent
              ? const Value.absent()
              : Value(seSimQualFoiPrincipalRazaodoObito),
      voceTemMedoDeTerUmFilhoOuNetoComProblemas:
          voceTemMedoDeTerUmFilhoOuNetoComProblemas == null && nullToAbsent
              ? const Value.absent()
              : Value(voceTemMedoDeTerUmFilhoOuNetoComProblemas),
      expliquePorQue: expliquePorQue == null && nullToAbsent
          ? const Value.absent()
          : Value(expliquePorQue),
      porqueVoceAchaQueUmaCriancaPodeNascerComProblema:
          porqueVoceAchaQueUmaCriancaPodeNascerComProblema == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(porqueVoceAchaQueUmaCriancaPodeNascerComProblema),
      oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema:
          oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema),
      achaQueUmaCriancaQueNasceComProblemaPodeSobreviver:
          achaQueUmaCriancaQueNasceComProblemaPodeSobreviver == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(achaQueUmaCriancaQueNasceComProblemaPodeSobreviver),
      expliquePorque: expliquePorque == null && nullToAbsent
          ? const Value.absent()
          : Value(expliquePorque),
      quemPodeAjudarQuandoUmaCriancaNasceComProblema:
          quemPodeAjudarQuandoUmaCriancaNasceComProblema == null && nullToAbsent
              ? const Value.absent()
              : Value(quemPodeAjudarQuandoUmaCriancaNasceComProblema),
      emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao:
          emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao),
      emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene:
          emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene ==
                      null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(
                  emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene),
      emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos:
          emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos),
      aPessoaEntrevistadaPareceuSinceraNasSuasRespostas:
          aPessoaEntrevistadaPareceuSinceraNasSuasRespostas == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(aPessoaEntrevistadaPareceuSinceraNasSuasRespostas),
      algumasPerguntasIncomodaramAPessoaEntrevistada:
          algumasPerguntasIncomodaramAPessoaEntrevistada == null && nullToAbsent
              ? const Value.absent()
              : Value(algumasPerguntasIncomodaramAPessoaEntrevistada),
      seSimQuais3PrincipaisPerguntas:
          seSimQuais3PrincipaisPerguntas == null && nullToAbsent
              ? const Value.absent()
              : Value(seSimQuais3PrincipaisPerguntas),
      todasAsPerguntasIncomodaram:
          todasAsPerguntasIncomodaram == null && nullToAbsent
              ? const Value.absent()
              : Value(todasAsPerguntasIncomodaram),
      aResidenciaEraDe: aResidenciaEraDe == null && nullToAbsent
          ? const Value.absent()
          : Value(aResidenciaEraDe),
      comoAvaliaHigieneDaCasa: comoAvaliaHigieneDaCasa == null && nullToAbsent
          ? const Value.absent()
          : Value(comoAvaliaHigieneDaCasa),
      oQueEraMal: oQueEraMal == null && nullToAbsent
          ? const Value.absent()
          : Value(oQueEraMal),
      tinhaCriancasNaCasaDuranteInquerito:
          tinhaCriancasNaCasaDuranteInquerito == null && nullToAbsent
              ? const Value.absent()
              : Value(tinhaCriancasNaCasaDuranteInquerito),
      tinhaCriancasNaCasaDuranteInqueritoQuantas:
          tinhaCriancasNaCasaDuranteInqueritoQuantas == null && nullToAbsent
              ? const Value.absent()
              : Value(tinhaCriancasNaCasaDuranteInqueritoQuantas),
      tentouIncentivar: tentouIncentivar == null && nullToAbsent
          ? const Value.absent()
          : Value(tentouIncentivar),
      tentouIncentivarQuantas: tentouIncentivarQuantas == null && nullToAbsent
          ? const Value.absent()
          : Value(tentouIncentivarQuantas),
      seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos:
          seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(
                  seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos),
      observouCriancasDeAte1Ano:
          observouCriancasDeAte1Ano == null && nullToAbsent
              ? const Value.absent()
              : Value(observouCriancasDeAte1Ano),
      observouCriancasDeAte1AnoQuantos:
          observouCriancasDeAte1AnoQuantos == null && nullToAbsent
              ? const Value.absent()
              : Value(observouCriancasDeAte1AnoQuantos),
      algumasApresentavamProblemas:
          algumasApresentavamProblemas == null && nullToAbsent
              ? const Value.absent()
              : Value(algumasApresentavamProblemas),
      algumasApresentavamProblemasQuantos:
          algumasApresentavamProblemasQuantos == null && nullToAbsent
              ? const Value.absent()
              : Value(algumasApresentavamProblemasQuantos),
      descreveOProblemaDessasCriancas:
          descreveOProblemaDessasCriancas == null && nullToAbsent
              ? const Value.absent()
              : Value(descreveOProblemaDessasCriancas),
      observouCriancasDe1AnoEmeioAte3Anos:
          observouCriancasDe1AnoEmeioAte3Anos == null && nullToAbsent
              ? const Value.absent()
              : Value(observouCriancasDe1AnoEmeioAte3Anos),
      observouCriancasDe1AnoEmeioAte3AnosQuantas:
          observouCriancasDe1AnoEmeioAte3AnosQuantas == null && nullToAbsent
              ? const Value.absent()
              : Value(observouCriancasDe1AnoEmeioAte3AnosQuantas),
      algumasCriancasDe1anoEmeioObservadasApresentavamProblemas:
          algumasCriancasDe1anoEmeioObservadasApresentavamProblemas == null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(
                  algumasCriancasDe1anoEmeioObservadasApresentavamProblemas),
      algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas:
          algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas ==
                      null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(
                  algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas),
      algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve:
          algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve ==
                      null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(
                  algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve),
      observouCriancasDe3AnosAte6Anos:
          observouCriancasDe3AnosAte6Anos == null && nullToAbsent
              ? const Value.absent()
              : Value(observouCriancasDe3AnosAte6Anos),
      observouCriancasDe3AnosAte6AnosQuantas:
          observouCriancasDe3AnosAte6AnosQuantas == null && nullToAbsent
              ? const Value.absent()
              : Value(observouCriancasDe3AnosAte6AnosQuantas),
      observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas:
          observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas ==
                      null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(
                  observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas),
      observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas:
          observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas ==
                      null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(
                  observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas),
      observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve:
          observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve ==
                      null &&
                  nullToAbsent
              ? const Value.absent()
              : Value(
                  observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve),
      observouSituacoesQueLheChamaramAtencao:
          observouSituacoesQueLheChamaramAtencao == null && nullToAbsent
              ? const Value.absent()
              : Value(observouSituacoesQueLheChamaramAtencao),
      emRelacaoAPessoaEntrevistada:
          emRelacaoAPessoaEntrevistada == null && nullToAbsent
              ? const Value.absent()
              : Value(emRelacaoAPessoaEntrevistada),
      emRelacaoAPessoaEntrevistadaDescreve:
          emRelacaoAPessoaEntrevistadaDescreve == null && nullToAbsent
              ? const Value.absent()
              : Value(emRelacaoAPessoaEntrevistadaDescreve),
      emRelacaoAOutrosAdultosPresente:
          emRelacaoAOutrosAdultosPresente == null && nullToAbsent
              ? const Value.absent()
              : Value(emRelacaoAOutrosAdultosPresente),
      emRelacaoAOutrosAdultosPresenteDescreve:
          emRelacaoAOutrosAdultosPresenteDescreve == null && nullToAbsent
              ? const Value.absent()
              : Value(emRelacaoAOutrosAdultosPresenteDescreve),
      emRelacaoAsCriancasPresentes:
          emRelacaoAsCriancasPresentes == null && nullToAbsent
              ? const Value.absent()
              : Value(emRelacaoAsCriancasPresentes),
      emRelacaoAsCriancasPresentesDescreve:
          emRelacaoAsCriancasPresentesDescreve == null && nullToAbsent
              ? const Value.absent()
              : Value(emRelacaoAsCriancasPresentesDescreve),
      emRelacaoAVizinhanca: emRelacaoAVizinhanca == null && nullToAbsent
          ? const Value.absent()
          : Value(emRelacaoAVizinhanca),
      emRelacaoAVizinhancaDescreve:
          emRelacaoAVizinhancaDescreve == null && nullToAbsent
              ? const Value.absent()
              : Value(emRelacaoAVizinhancaDescreve),
      outras:
          outras == null && nullToAbsent ? const Value.absent() : Value(outras),
      outrasDescreve: outrasDescreve == null && nullToAbsent
          ? const Value.absent()
          : Value(outrasDescreve),
      createdAt: createdAt == null && nullToAbsent
          ? const Value.absent()
          : Value(createdAt),
      updatedAt: updatedAt == null && nullToAbsent
          ? const Value.absent()
          : Value(updatedAt),
    );
  }

  factory User.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return User(
      uuid: serializer.fromJson<String>(json['uuid']),
      nomeDoPesquisador: serializer.fromJson<String>(json['nomeDoPesquisador']),
      numeroDeInquerito: serializer.fromJson<String>(json['numeroDeInquerito']),
      data: serializer.fromJson<DateTime>(json['data']),
      municipio: serializer.fromJson<String>(json['municipio']),
      bairro: serializer.fromJson<String>(json['bairro']),
      pontoDeReferenciaDaCasa:
          serializer.fromJson<String>(json['pontoDeReferenciaDaCasa']),
      nome: serializer.fromJson<String>(json['nome']),
      idade: serializer.fromJson<String>(json['idade']),
      contacto: serializer.fromJson<String>(json['contacto']),
      eChefeDaFamilia: serializer.fromJson<String>(json['eChefeDaFamilia']),
      caoNaoVinculoDeParantesco:
          serializer.fromJson<String>(json['caoNaoVinculoDeParantesco']),
      idadeDoChefeDaFamilia:
          serializer.fromJson<String>(json['idadeDoChefeDaFamilia']),
      estudou: serializer.fromJson<String>(json['estudou']),
      eAlfabetizado: serializer.fromJson<String>(json['eAlfabetizado']),
      completouEnsinoPrimario:
          serializer.fromJson<String>(json['completouEnsinoPrimario']),
      completouEnsinoSecundario:
          serializer.fromJson<String>(json['completouEnsinoSecundario']),
      fezCursoSuperior: serializer.fromJson<String>(json['fezCursoSuperior']),
      naoSabe: serializer.fromJson<String>(json['naoSabe']),
      chefeDeFamiliaTemSalario:
          serializer.fromJson<String>(json['chefeDeFamiliaTemSalario']),
      quantoGastaParaSustentarFamiliaPorMes: serializer
          .fromJson<String>(json['quantoGastaParaSustentarFamiliaPorMes']),
      quantasFamiliasResidemNacasa:
          serializer.fromJson<String>(json['quantasFamiliasResidemNacasa']),
      quantosAdultos: serializer.fromJson<String>(json['quantosAdultos']),
      quantosAdultosMaioresDe18anos:
          serializer.fromJson<String>(json['quantosAdultosMaioresDe18anos']),
      quantosAdultosMaioresDe18anosHomens: serializer
          .fromJson<String>(json['quantosAdultosMaioresDe18anosHomens']),
      quantosAdultosMaioresDe18anosHomensTrabalham: serializer.fromJson<String>(
          json['quantosAdultosMaioresDe18anosHomensTrabalham']),
      quantosAdultosMaioresDe18anosHomensAlfabetizados:
          serializer.fromJson<String>(
              json['quantosAdultosMaioresDe18anosHomensAlfabetizados']),
      quantosAdultosM18Mulheres:
          serializer.fromJson<String>(json['quantosAdultosM18Mulheres']),
      quantosAdultosM18MulheresTrabalham: serializer
          .fromJson<String>(json['quantosAdultosM18MulheresTrabalham']),
      quantosAdultosM18MulheresAlfabetizados: serializer
          .fromJson<String>(json['quantosAdultosM18MulheresAlfabetizados']),
      relatosDeDeficiencia:
          serializer.fromJson<String>(json['relatosDeDeficiencia']),
      totalcriancasAte1anoemeio:
          serializer.fromJson<String>(json['totalcriancasAte1anoemeio']),
      totalcriancasAte1anoemeioMeninos:
          serializer.fromJson<String>(json['totalcriancasAte1anoemeioMeninos']),
      totalcriancasAte1anoemeioMeninas:
          serializer.fromJson<String>(json['totalcriancasAte1anoemeioMeninas']),
      algumasCriancasAte1anoemeioNaodesenvolvem: serializer
          .fromJson<String>(json['algumasCriancasAte1anoemeioNaodesenvolvem']),
      algumasCriancasAte1anoemeioNaodesenvolvemQuantas:
          serializer.fromJson<String>(
              json['algumasCriancasAte1anoemeioNaodesenvolvemQuantas']),
      algumaCriancasAte1anoemeioQueTipodeProblema: serializer.fromJson<String>(
          json['algumaCriancasAte1anoemeioQueTipodeProblema']),
      algumaCriancasAte1anoemeioTemProblemaMotor: serializer
          .fromJson<String>(json['algumaCriancasAte1anoemeioTemProblemaMotor']),
      algumaCriancasAte1anoemeioTemProblemaMotorQuantas:
          serializer.fromJson<String>(
              json['algumaCriancasAte1anoemeioTemProblemaMotorQuantas']),
      algumaCriancasAte1anoemeioTemMalFormacao: serializer
          .fromJson<String>(json['algumaCriancasAte1anoemeioTemMalFormacao']),
      algumaCriancasAte1anoemeioTemMalFormacaoQuantas:
          serializer.fromJson<String>(
              json['algumaCriancasAte1anoemeioTemMalFormacaoQuantas']),
      algumaCriancasAte1anoemeioNaoReagemAIncentivo:
          serializer.fromJson<String>(
              json['algumaCriancasAte1anoemeioNaoReagemAIncentivo']),
      algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas:
          serializer.fromJson<String>(
              json['algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas']),
      algumaCriancasAte1anoemeioTemReacaoAgressiva: serializer.fromJson<String>(
          json['algumaCriancasAte1anoemeioTemReacaoAgressiva']),
      algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas:
          serializer.fromJson<String>(
              json['algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas']),
      totalcriancasAte1anoemeioA3anos:
          serializer.fromJson<String>(json['totalcriancasAte1anoemeioA3anos']),
      totalcriancasAte1anoemeioA3anosMeninos: serializer
          .fromJson<String>(json['totalcriancasAte1anoemeioA3anosMeninos']),
      totalcriancasAte1anoemeioA3anosMeninas: serializer
          .fromJson<String>(json['totalcriancasAte1anoemeioA3anosMeninas']),
      algumasCriancasAte1anoemeioA3anosNaodesenvolvem:
          serializer.fromJson<String>(
              json['algumasCriancasAte1anoemeioA3anosNaodesenvolvem']),
      algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas:
          serializer.fromJson<String>(
              json['algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas']),
      algumaCriancasAte1anoemeioA3anosQueTipodeProblema:
          serializer.fromJson<String>(
              json['algumaCriancasAte1anoemeioA3anosQueTipodeProblema']),
      algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas:
          serializer.fromJson<String>(
              json['algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas']),
      algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas:
          serializer.fromJson<String>(json[
              'algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas']),
      algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo:
          serializer.fromJson<String>(json[
              'algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo']),
      algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas:
          serializer.fromJson<String>(json[
              'algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas']),
      algumasCriancasAte1anoemeioA3anosAlgunsAgridem:
          serializer.fromJson<String>(
              json['algumasCriancasAte1anoemeioA3anosAlgunsAgridem']),
      algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas:
          serializer.fromJson<String>(
              json['algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas']),
      algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal:
          serializer.fromJson<String>(json[
              'algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal']),
      algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas:
          serializer.fromJson<String>(json[
              'algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas']),
      algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao:
          serializer.fromJson<String>(
              json['algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao']),
      algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas:
          serializer.fromJson<String>(json[
              'algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas']),
      algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas:
          serializer.fromJson<String>(
              json['algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas']),
      algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas:
          serializer.fromJson<String>(json[
              'algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas']),
      algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente:
          serializer.fromJson<String>(json[
              'algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente']),
      algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas:
          serializer.fromJson<String>(json[
              'algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas']),
      algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos:
          serializer.fromJson<String>(json[
              'algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos']),
      algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas:
          serializer.fromJson<String>(json[
              'algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas']),
      algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown:
          serializer.fromJson<String>(json[
              'algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown']),
      algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas:
          serializer.fromJson<String>(json[
              'algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas']),
      totalcriancasde3A6anos:
          serializer.fromJson<String>(json['totalcriancasde3A6anos']),
      totalcriancasde3A6anosMeninos:
          serializer.fromJson<String>(json['totalcriancasde3A6anosMeninos']),
      totalcriancasde3A6anosMeninas:
          serializer.fromJson<String>(json['totalcriancasde3A6anosMeninas']),
      algumasCriancasde3A6anosNaodesenvolvem: serializer
          .fromJson<String>(json['algumasCriancasde3A6anosNaodesenvolvem']),
      algumasCriancasde3A6anosNaodesenvolvemQuantas:
          serializer.fromJson<String>(
              json['algumasCriancasde3A6anosNaodesenvolvemQuantas']),
      algumaCriancasde3A6anosQueTipodeProblema: serializer
          .fromJson<String>(json['algumaCriancasde3A6anosQueTipodeProblema']),
      algumasCriancasde3A6anosNaoInteragemComPessoas:
          serializer.fromJson<String>(
              json['algumasCriancasde3A6anosNaoInteragemComPessoas']),
      algumasCriancasde3A6anosNaoInteragemComPessoasQuantas:
          serializer.fromJson<String>(
              json['algumasCriancasde3A6anosNaoInteragemComPessoasQuantas']),
      algumasCriancasde3A6anosTemComportamentoAgressivo:
          serializer.fromJson<String>(
              json['algumasCriancasde3A6anosTemComportamentoAgressivo']),
      algumasCriancasde3A6anosTemComportamentoAgressivoQuantas:
          serializer.fromJson<String>(
              json['algumasCriancasde3A6anosTemComportamentoAgressivoQuantas']),
      algumasCriancasde3A6anosAlgunsAgridem: serializer
          .fromJson<String>(json['algumasCriancasde3A6anosAlgunsAgridem']),
      algumasCriancasde3A6anosAlgunsAgridemQuantas: serializer.fromJson<String>(
          json['algumasCriancasde3A6anosAlgunsAgridemQuantas']),
      algumasCriancasde3A6anosAlgumasAgitadasQueONormal:
          serializer.fromJson<String>(
              json['algumasCriancasde3A6anosAlgumasAgitadasQueONormal']),
      algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas:
          serializer.fromJson<String>(
              json['algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas']),
      algumasCriancasde3A6anosAlgumasTemMalFormacao:
          serializer.fromJson<String>(
              json['algumasCriancasde3A6anosAlgumasTemMalFormacao']),
      algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas:
          serializer.fromJson<String>(
              json['algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas']),
      algumasCriancasde3A6anosAlgumasNaoAndamSozinhas:
          serializer.fromJson<String>(
              json['algumasCriancasde3A6anosAlgumasNaoAndamSozinhas']),
      algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas:
          serializer.fromJson<String>(
              json['algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas']),
      algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente:
          serializer.fromJson<String>(
              json['algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente']),
      algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas:
          serializer.fromJson<String>(json[
              'algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas']),
      algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos:
          serializer.fromJson<String>(
              json['algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos']),
      algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas:
          serializer.fromJson<String>(json[
              'algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas']),
      algumasCriancasde3A6anosAlgumasTemSindromeDeDown:
          serializer.fromJson<String>(
              json['algumasCriancasde3A6anosAlgumasTemSindromeDeDown']),
      algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas:
          serializer.fromJson<String>(
              json['algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas']),
      algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas:
          serializer.fromJson<String>(json[
              'algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas']),
      algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas:
          serializer.fromJson<String>(json[
              'algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas']),
      algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas:
          serializer.fromJson<String>(json[
              'algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas']),
      algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas:
          serializer.fromJson<String>(json[
              'algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas']),
      algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas:
          serializer.fromJson<String>(json[
              'algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas']),
      algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas:
          serializer.fromJson<String>(json[
              'algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas']),
      algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas:
          serializer.fromJson<String>(json[
              'algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas']),
      algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas:
          serializer.fromJson<String>(json[
              'algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas']),
      quantasCriancasde3A6anosEstaoNaEscolinha: serializer
          .fromJson<String>(json['quantasCriancasde3A6anosEstaoNaEscolinha']),
      criancasDe3A6anosNaoEstaoNaEscolinhaPorque: serializer
          .fromJson<String>(json['criancasDe3A6anosNaoEstaoNaEscolinhaPorque']),
      quantasCriancasde3A6anosTrabalham: serializer
          .fromJson<String>(json['quantasCriancasde3A6anosTrabalham']),
      criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos:
          serializer.fromJson<String>(
              json['criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos']),
      totalcriancasde7A9anos:
          serializer.fromJson<String>(json['totalcriancasde7A9anos']),
      totalcriancasde7A9anosMeninos:
          serializer.fromJson<String>(json['totalcriancasde7A9anosMeninos']),
      totalcriancasde7A9anosMeninas:
          serializer.fromJson<String>(json['totalcriancasde7A9anosMeninas']),
      quantasCriancasde7A9anosEstaoNaEscola: serializer
          .fromJson<String>(json['quantasCriancasde7A9anosEstaoNaEscola']),
      quantasCriancasde7A9anosTrabalham: serializer
          .fromJson<String>(json['quantasCriancasde7A9anosTrabalham']),
      nosUltimos5AnosQuantasGravidezesTiveNaFamilia:
          serializer.fromJson<String>(
              json['nosUltimos5AnosQuantasGravidezesTiveNaFamilia']),
      partosOcoridosNoHospital:
          serializer.fromJson<String>(json['partosOcoridosNoHospital']),
      partosOcoridosEmCasa:
          serializer.fromJson<String>(json['partosOcoridosEmCasa']),
      partosOcoridosEmOutroLocal:
          serializer.fromJson<String>(json['partosOcoridosEmOutroLocal']),
      outroLocalDoPartoQualLocal:
          serializer.fromJson<String>(json['outroLocalDoPartoQualLocal']),
      quantasCriancasNasceramVivas:
          serializer.fromJson<String>(json['quantasCriancasNasceramVivas']),
      quantasContinuamVivas:
          serializer.fromJson<String>(json['quantasContinuamVivas']),
      dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram:
          serializer.fromJson<String>(json[
              'dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram']),
      descreverOProblema:
          serializer.fromJson<String>(json['descreverOProblema']),
      aFamiliaProcurouAjudaParaSuperarEsseProblema: serializer.fromJson<String>(
          json['aFamiliaProcurouAjudaParaSuperarEsseProblema']),
      seSimQueTipoDeAjudaEOnde:
          serializer.fromJson<String>(json['seSimQueTipoDeAjudaEOnde']),
      osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia:
          serializer.fromJson<String>(json[
              'osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia']),
      dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida:
          serializer.fromJson<String>(json[
              'dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida']),
      sabeDizerOMotivoDosObitos:
          serializer.fromJson<String>(json['sabeDizerOMotivoDosObitos']),
      seSimQualFoiPrincipalRazaodoObito: serializer
          .fromJson<String>(json['seSimQualFoiPrincipalRazaodoObito']),
      voceTemMedoDeTerUmFilhoOuNetoComProblemas: serializer
          .fromJson<String>(json['voceTemMedoDeTerUmFilhoOuNetoComProblemas']),
      expliquePorQue: serializer.fromJson<String>(json['expliquePorQue']),
      porqueVoceAchaQueUmaCriancaPodeNascerComProblema:
          serializer.fromJson<String>(
              json['porqueVoceAchaQueUmaCriancaPodeNascerComProblema']),
      oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema:
          serializer.fromJson<String>(
              json['oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema']),
      achaQueUmaCriancaQueNasceComProblemaPodeSobreviver:
          serializer.fromJson<String>(
              json['achaQueUmaCriancaQueNasceComProblemaPodeSobreviver']),
      expliquePorque: serializer.fromJson<String>(json['expliquePorque']),
      quemPodeAjudarQuandoUmaCriancaNasceComProblema:
          serializer.fromJson<String>(
              json['quemPodeAjudarQuandoUmaCriancaNasceComProblema']),
      emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao:
          serializer.fromJson<String>(
              json['emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao']),
      emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene:
          serializer.fromJson<String>(json[
              'emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene']),
      emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos:
          serializer.fromJson<String>(
              json['emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos']),
      aPessoaEntrevistadaPareceuSinceraNasSuasRespostas:
          serializer.fromJson<String>(
              json['aPessoaEntrevistadaPareceuSinceraNasSuasRespostas']),
      algumasPerguntasIncomodaramAPessoaEntrevistada:
          serializer.fromJson<String>(
              json['algumasPerguntasIncomodaramAPessoaEntrevistada']),
      seSimQuais3PrincipaisPerguntas:
          serializer.fromJson<String>(json['seSimQuais3PrincipaisPerguntas']),
      todasAsPerguntasIncomodaram:
          serializer.fromJson<String>(json['todasAsPerguntasIncomodaram']),
      aResidenciaEraDe: serializer.fromJson<String>(json['aResidenciaEraDe']),
      comoAvaliaHigieneDaCasa:
          serializer.fromJson<String>(json['comoAvaliaHigieneDaCasa']),
      oQueEraMal: serializer.fromJson<String>(json['oQueEraMal']),
      tinhaCriancasNaCasaDuranteInquerito: serializer
          .fromJson<String>(json['tinhaCriancasNaCasaDuranteInquerito']),
      tinhaCriancasNaCasaDuranteInqueritoQuantas: serializer
          .fromJson<String>(json['tinhaCriancasNaCasaDuranteInqueritoQuantas']),
      tentouIncentivar: serializer.fromJson<String>(json['tentouIncentivar']),
      tentouIncentivarQuantas:
          serializer.fromJson<String>(json['tentouIncentivarQuantas']),
      seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos:
          serializer.fromJson<String>(json[
              'seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos']),
      observouCriancasDeAte1Ano:
          serializer.fromJson<String>(json['observouCriancasDeAte1Ano']),
      observouCriancasDeAte1AnoQuantos:
          serializer.fromJson<String>(json['observouCriancasDeAte1AnoQuantos']),
      algumasApresentavamProblemas:
          serializer.fromJson<String>(json['algumasApresentavamProblemas']),
      algumasApresentavamProblemasQuantos: serializer
          .fromJson<String>(json['algumasApresentavamProblemasQuantos']),
      descreveOProblemaDessasCriancas:
          serializer.fromJson<String>(json['descreveOProblemaDessasCriancas']),
      observouCriancasDe1AnoEmeioAte3Anos: serializer
          .fromJson<String>(json['observouCriancasDe1AnoEmeioAte3Anos']),
      observouCriancasDe1AnoEmeioAte3AnosQuantas: serializer
          .fromJson<String>(json['observouCriancasDe1AnoEmeioAte3AnosQuantas']),
      algumasCriancasDe1anoEmeioObservadasApresentavamProblemas:
          serializer.fromJson<String>(json[
              'algumasCriancasDe1anoEmeioObservadasApresentavamProblemas']),
      algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas:
          serializer.fromJson<String>(json[
              'algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas']),
      algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve:
          serializer.fromJson<String>(json[
              'algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve']),
      observouCriancasDe3AnosAte6Anos:
          serializer.fromJson<String>(json['observouCriancasDe3AnosAte6Anos']),
      observouCriancasDe3AnosAte6AnosQuantas: serializer
          .fromJson<String>(json['observouCriancasDe3AnosAte6AnosQuantas']),
      observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas:
          serializer.fromJson<String>(json[
              'observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas']),
      observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas:
          serializer.fromJson<String>(json[
              'observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas']),
      observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve:
          serializer.fromJson<String>(json[
              'observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve']),
      observouSituacoesQueLheChamaramAtencao: serializer
          .fromJson<String>(json['observouSituacoesQueLheChamaramAtencao']),
      emRelacaoAPessoaEntrevistada:
          serializer.fromJson<String>(json['emRelacaoAPessoaEntrevistada']),
      emRelacaoAPessoaEntrevistadaDescreve: serializer
          .fromJson<String>(json['emRelacaoAPessoaEntrevistadaDescreve']),
      emRelacaoAOutrosAdultosPresente:
          serializer.fromJson<String>(json['emRelacaoAOutrosAdultosPresente']),
      emRelacaoAOutrosAdultosPresenteDescreve: serializer
          .fromJson<String>(json['emRelacaoAOutrosAdultosPresenteDescreve']),
      emRelacaoAsCriancasPresentes:
          serializer.fromJson<String>(json['emRelacaoAsCriancasPresentes']),
      emRelacaoAsCriancasPresentesDescreve: serializer
          .fromJson<String>(json['emRelacaoAsCriancasPresentesDescreve']),
      emRelacaoAVizinhanca:
          serializer.fromJson<String>(json['emRelacaoAVizinhanca']),
      emRelacaoAVizinhancaDescreve:
          serializer.fromJson<String>(json['emRelacaoAVizinhancaDescreve']),
      outras: serializer.fromJson<String>(json['outras']),
      outrasDescreve: serializer.fromJson<String>(json['outrasDescreve']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
      updatedAt: serializer.fromJson<DateTime>(json['updatedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'uuid': serializer.toJson<String>(uuid),
      'nomeDoPesquisador': serializer.toJson<String>(nomeDoPesquisador),
      'numeroDeInquerito': serializer.toJson<String>(numeroDeInquerito),
      'data': serializer.toJson<DateTime>(data),
      'municipio': serializer.toJson<String>(municipio),
      'bairro': serializer.toJson<String>(bairro),
      'pontoDeReferenciaDaCasa':
          serializer.toJson<String>(pontoDeReferenciaDaCasa),
      'nome': serializer.toJson<String>(nome),
      'idade': serializer.toJson<String>(idade),
      'contacto': serializer.toJson<String>(contacto),
      'eChefeDaFamilia': serializer.toJson<String>(eChefeDaFamilia),
      'caoNaoVinculoDeParantesco':
          serializer.toJson<String>(caoNaoVinculoDeParantesco),
      'idadeDoChefeDaFamilia': serializer.toJson<String>(idadeDoChefeDaFamilia),
      'estudou': serializer.toJson<String>(estudou),
      'eAlfabetizado': serializer.toJson<String>(eAlfabetizado),
      'completouEnsinoPrimario':
          serializer.toJson<String>(completouEnsinoPrimario),
      'completouEnsinoSecundario':
          serializer.toJson<String>(completouEnsinoSecundario),
      'fezCursoSuperior': serializer.toJson<String>(fezCursoSuperior),
      'naoSabe': serializer.toJson<String>(naoSabe),
      'chefeDeFamiliaTemSalario':
          serializer.toJson<String>(chefeDeFamiliaTemSalario),
      'quantoGastaParaSustentarFamiliaPorMes':
          serializer.toJson<String>(quantoGastaParaSustentarFamiliaPorMes),
      'quantasFamiliasResidemNacasa':
          serializer.toJson<String>(quantasFamiliasResidemNacasa),
      'quantosAdultos': serializer.toJson<String>(quantosAdultos),
      'quantosAdultosMaioresDe18anos':
          serializer.toJson<String>(quantosAdultosMaioresDe18anos),
      'quantosAdultosMaioresDe18anosHomens':
          serializer.toJson<String>(quantosAdultosMaioresDe18anosHomens),
      'quantosAdultosMaioresDe18anosHomensTrabalham': serializer
          .toJson<String>(quantosAdultosMaioresDe18anosHomensTrabalham),
      'quantosAdultosMaioresDe18anosHomensAlfabetizados': serializer
          .toJson<String>(quantosAdultosMaioresDe18anosHomensAlfabetizados),
      'quantosAdultosM18Mulheres':
          serializer.toJson<String>(quantosAdultosM18Mulheres),
      'quantosAdultosM18MulheresTrabalham':
          serializer.toJson<String>(quantosAdultosM18MulheresTrabalham),
      'quantosAdultosM18MulheresAlfabetizados':
          serializer.toJson<String>(quantosAdultosM18MulheresAlfabetizados),
      'relatosDeDeficiencia': serializer.toJson<String>(relatosDeDeficiencia),
      'totalcriancasAte1anoemeio':
          serializer.toJson<String>(totalcriancasAte1anoemeio),
      'totalcriancasAte1anoemeioMeninos':
          serializer.toJson<String>(totalcriancasAte1anoemeioMeninos),
      'totalcriancasAte1anoemeioMeninas':
          serializer.toJson<String>(totalcriancasAte1anoemeioMeninas),
      'algumasCriancasAte1anoemeioNaodesenvolvem':
          serializer.toJson<String>(algumasCriancasAte1anoemeioNaodesenvolvem),
      'algumasCriancasAte1anoemeioNaodesenvolvemQuantas': serializer
          .toJson<String>(algumasCriancasAte1anoemeioNaodesenvolvemQuantas),
      'algumaCriancasAte1anoemeioQueTipodeProblema': serializer
          .toJson<String>(algumaCriancasAte1anoemeioQueTipodeProblema),
      'algumaCriancasAte1anoemeioTemProblemaMotor':
          serializer.toJson<String>(algumaCriancasAte1anoemeioTemProblemaMotor),
      'algumaCriancasAte1anoemeioTemProblemaMotorQuantas': serializer
          .toJson<String>(algumaCriancasAte1anoemeioTemProblemaMotorQuantas),
      'algumaCriancasAte1anoemeioTemMalFormacao':
          serializer.toJson<String>(algumaCriancasAte1anoemeioTemMalFormacao),
      'algumaCriancasAte1anoemeioTemMalFormacaoQuantas': serializer
          .toJson<String>(algumaCriancasAte1anoemeioTemMalFormacaoQuantas),
      'algumaCriancasAte1anoemeioNaoReagemAIncentivo': serializer
          .toJson<String>(algumaCriancasAte1anoemeioNaoReagemAIncentivo),
      'algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas': serializer
          .toJson<String>(algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas),
      'algumaCriancasAte1anoemeioTemReacaoAgressiva': serializer
          .toJson<String>(algumaCriancasAte1anoemeioTemReacaoAgressiva),
      'algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas': serializer
          .toJson<String>(algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas),
      'totalcriancasAte1anoemeioA3anos':
          serializer.toJson<String>(totalcriancasAte1anoemeioA3anos),
      'totalcriancasAte1anoemeioA3anosMeninos':
          serializer.toJson<String>(totalcriancasAte1anoemeioA3anosMeninos),
      'totalcriancasAte1anoemeioA3anosMeninas':
          serializer.toJson<String>(totalcriancasAte1anoemeioA3anosMeninas),
      'algumasCriancasAte1anoemeioA3anosNaodesenvolvem': serializer
          .toJson<String>(algumasCriancasAte1anoemeioA3anosNaodesenvolvem),
      'algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas),
      'algumaCriancasAte1anoemeioA3anosQueTipodeProblema': serializer
          .toJson<String>(algumaCriancasAte1anoemeioA3anosQueTipodeProblema),
      'algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas),
      'algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas),
      'algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo),
      'algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas),
      'algumasCriancasAte1anoemeioA3anosAlgunsAgridem': serializer
          .toJson<String>(algumasCriancasAte1anoemeioA3anosAlgunsAgridem),
      'algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas),
      'algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal),
      'algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas),
      'algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao),
      'algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas),
      'algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas),
      'algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas),
      'algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente),
      'algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas),
      'algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos),
      'algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas),
      'algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown),
      'algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas),
      'totalcriancasde3A6anos':
          serializer.toJson<String>(totalcriancasde3A6anos),
      'totalcriancasde3A6anosMeninos':
          serializer.toJson<String>(totalcriancasde3A6anosMeninos),
      'totalcriancasde3A6anosMeninas':
          serializer.toJson<String>(totalcriancasde3A6anosMeninas),
      'algumasCriancasde3A6anosNaodesenvolvem':
          serializer.toJson<String>(algumasCriancasde3A6anosNaodesenvolvem),
      'algumasCriancasde3A6anosNaodesenvolvemQuantas': serializer
          .toJson<String>(algumasCriancasde3A6anosNaodesenvolvemQuantas),
      'algumaCriancasde3A6anosQueTipodeProblema':
          serializer.toJson<String>(algumaCriancasde3A6anosQueTipodeProblema),
      'algumasCriancasde3A6anosNaoInteragemComPessoas': serializer
          .toJson<String>(algumasCriancasde3A6anosNaoInteragemComPessoas),
      'algumasCriancasde3A6anosNaoInteragemComPessoasQuantas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosNaoInteragemComPessoasQuantas),
      'algumasCriancasde3A6anosTemComportamentoAgressivo': serializer
          .toJson<String>(algumasCriancasde3A6anosTemComportamentoAgressivo),
      'algumasCriancasde3A6anosTemComportamentoAgressivoQuantas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosTemComportamentoAgressivoQuantas),
      'algumasCriancasde3A6anosAlgunsAgridem':
          serializer.toJson<String>(algumasCriancasde3A6anosAlgunsAgridem),
      'algumasCriancasde3A6anosAlgunsAgridemQuantas': serializer
          .toJson<String>(algumasCriancasde3A6anosAlgunsAgridemQuantas),
      'algumasCriancasde3A6anosAlgumasAgitadasQueONormal': serializer
          .toJson<String>(algumasCriancasde3A6anosAlgumasAgitadasQueONormal),
      'algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas),
      'algumasCriancasde3A6anosAlgumasTemMalFormacao': serializer
          .toJson<String>(algumasCriancasde3A6anosAlgumasTemMalFormacao),
      'algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas': serializer
          .toJson<String>(algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas),
      'algumasCriancasde3A6anosAlgumasNaoAndamSozinhas': serializer
          .toJson<String>(algumasCriancasde3A6anosAlgumasNaoAndamSozinhas),
      'algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas),
      'algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente),
      'algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas),
      'algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos),
      'algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas),
      'algumasCriancasde3A6anosAlgumasTemSindromeDeDown': serializer
          .toJson<String>(algumasCriancasde3A6anosAlgumasTemSindromeDeDown),
      'algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas),
      'algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas),
      'algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas),
      'algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas),
      'algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas),
      'algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas),
      'algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas),
      'algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas),
      'algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas),
      'quantasCriancasde3A6anosEstaoNaEscolinha':
          serializer.toJson<String>(quantasCriancasde3A6anosEstaoNaEscolinha),
      'criancasDe3A6anosNaoEstaoNaEscolinhaPorque':
          serializer.toJson<String>(criancasDe3A6anosNaoEstaoNaEscolinhaPorque),
      'quantasCriancasde3A6anosTrabalham':
          serializer.toJson<String>(quantasCriancasde3A6anosTrabalham),
      'criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos': serializer
          .toJson<String>(criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos),
      'totalcriancasde7A9anos':
          serializer.toJson<String>(totalcriancasde7A9anos),
      'totalcriancasde7A9anosMeninos':
          serializer.toJson<String>(totalcriancasde7A9anosMeninos),
      'totalcriancasde7A9anosMeninas':
          serializer.toJson<String>(totalcriancasde7A9anosMeninas),
      'quantasCriancasde7A9anosEstaoNaEscola':
          serializer.toJson<String>(quantasCriancasde7A9anosEstaoNaEscola),
      'quantasCriancasde7A9anosTrabalham':
          serializer.toJson<String>(quantasCriancasde7A9anosTrabalham),
      'nosUltimos5AnosQuantasGravidezesTiveNaFamilia': serializer
          .toJson<String>(nosUltimos5AnosQuantasGravidezesTiveNaFamilia),
      'partosOcoridosNoHospital':
          serializer.toJson<String>(partosOcoridosNoHospital),
      'partosOcoridosEmCasa': serializer.toJson<String>(partosOcoridosEmCasa),
      'partosOcoridosEmOutroLocal':
          serializer.toJson<String>(partosOcoridosEmOutroLocal),
      'outroLocalDoPartoQualLocal':
          serializer.toJson<String>(outroLocalDoPartoQualLocal),
      'quantasCriancasNasceramVivas':
          serializer.toJson<String>(quantasCriancasNasceramVivas),
      'quantasContinuamVivas': serializer.toJson<String>(quantasContinuamVivas),
      'dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram':
          serializer.toJson<String>(
              dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram),
      'descreverOProblema': serializer.toJson<String>(descreverOProblema),
      'aFamiliaProcurouAjudaParaSuperarEsseProblema': serializer
          .toJson<String>(aFamiliaProcurouAjudaParaSuperarEsseProblema),
      'seSimQueTipoDeAjudaEOnde':
          serializer.toJson<String>(seSimQueTipoDeAjudaEOnde),
      'osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia':
          serializer.toJson<String>(
              osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia),
      'dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida':
          serializer.toJson<String>(
              dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida),
      'sabeDizerOMotivoDosObitos':
          serializer.toJson<String>(sabeDizerOMotivoDosObitos),
      'seSimQualFoiPrincipalRazaodoObito':
          serializer.toJson<String>(seSimQualFoiPrincipalRazaodoObito),
      'voceTemMedoDeTerUmFilhoOuNetoComProblemas':
          serializer.toJson<String>(voceTemMedoDeTerUmFilhoOuNetoComProblemas),
      'expliquePorQue': serializer.toJson<String>(expliquePorQue),
      'porqueVoceAchaQueUmaCriancaPodeNascerComProblema': serializer
          .toJson<String>(porqueVoceAchaQueUmaCriancaPodeNascerComProblema),
      'oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema':
          serializer.toJson<String>(
              oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema),
      'achaQueUmaCriancaQueNasceComProblemaPodeSobreviver': serializer
          .toJson<String>(achaQueUmaCriancaQueNasceComProblemaPodeSobreviver),
      'expliquePorque': serializer.toJson<String>(expliquePorque),
      'quemPodeAjudarQuandoUmaCriancaNasceComProblema': serializer
          .toJson<String>(quemPodeAjudarQuandoUmaCriancaNasceComProblema),
      'emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao':
          serializer.toJson<String>(
              emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao),
      'emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene':
          serializer.toJson<String>(
              emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene),
      'emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos': serializer
          .toJson<String>(emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos),
      'aPessoaEntrevistadaPareceuSinceraNasSuasRespostas': serializer
          .toJson<String>(aPessoaEntrevistadaPareceuSinceraNasSuasRespostas),
      'algumasPerguntasIncomodaramAPessoaEntrevistada': serializer
          .toJson<String>(algumasPerguntasIncomodaramAPessoaEntrevistada),
      'seSimQuais3PrincipaisPerguntas':
          serializer.toJson<String>(seSimQuais3PrincipaisPerguntas),
      'todasAsPerguntasIncomodaram':
          serializer.toJson<String>(todasAsPerguntasIncomodaram),
      'aResidenciaEraDe': serializer.toJson<String>(aResidenciaEraDe),
      'comoAvaliaHigieneDaCasa':
          serializer.toJson<String>(comoAvaliaHigieneDaCasa),
      'oQueEraMal': serializer.toJson<String>(oQueEraMal),
      'tinhaCriancasNaCasaDuranteInquerito':
          serializer.toJson<String>(tinhaCriancasNaCasaDuranteInquerito),
      'tinhaCriancasNaCasaDuranteInqueritoQuantas':
          serializer.toJson<String>(tinhaCriancasNaCasaDuranteInqueritoQuantas),
      'tentouIncentivar': serializer.toJson<String>(tentouIncentivar),
      'tentouIncentivarQuantas':
          serializer.toJson<String>(tentouIncentivarQuantas),
      'seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos':
          serializer.toJson<String>(
              seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos),
      'observouCriancasDeAte1Ano':
          serializer.toJson<String>(observouCriancasDeAte1Ano),
      'observouCriancasDeAte1AnoQuantos':
          serializer.toJson<String>(observouCriancasDeAte1AnoQuantos),
      'algumasApresentavamProblemas':
          serializer.toJson<String>(algumasApresentavamProblemas),
      'algumasApresentavamProblemasQuantos':
          serializer.toJson<String>(algumasApresentavamProblemasQuantos),
      'descreveOProblemaDessasCriancas':
          serializer.toJson<String>(descreveOProblemaDessasCriancas),
      'observouCriancasDe1AnoEmeioAte3Anos':
          serializer.toJson<String>(observouCriancasDe1AnoEmeioAte3Anos),
      'observouCriancasDe1AnoEmeioAte3AnosQuantas':
          serializer.toJson<String>(observouCriancasDe1AnoEmeioAte3AnosQuantas),
      'algumasCriancasDe1anoEmeioObservadasApresentavamProblemas':
          serializer.toJson<String>(
              algumasCriancasDe1anoEmeioObservadasApresentavamProblemas),
      'algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas':
          serializer.toJson<String>(
              algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas),
      'algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve':
          serializer.toJson<String>(
              algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve),
      'observouCriancasDe3AnosAte6Anos':
          serializer.toJson<String>(observouCriancasDe3AnosAte6Anos),
      'observouCriancasDe3AnosAte6AnosQuantas':
          serializer.toJson<String>(observouCriancasDe3AnosAte6AnosQuantas),
      'observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas':
          serializer.toJson<String>(
              observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas),
      'observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas':
          serializer.toJson<String>(
              observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas),
      'observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve':
          serializer.toJson<String>(
              observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve),
      'observouSituacoesQueLheChamaramAtencao':
          serializer.toJson<String>(observouSituacoesQueLheChamaramAtencao),
      'emRelacaoAPessoaEntrevistada':
          serializer.toJson<String>(emRelacaoAPessoaEntrevistada),
      'emRelacaoAPessoaEntrevistadaDescreve':
          serializer.toJson<String>(emRelacaoAPessoaEntrevistadaDescreve),
      'emRelacaoAOutrosAdultosPresente':
          serializer.toJson<String>(emRelacaoAOutrosAdultosPresente),
      'emRelacaoAOutrosAdultosPresenteDescreve':
          serializer.toJson<String>(emRelacaoAOutrosAdultosPresenteDescreve),
      'emRelacaoAsCriancasPresentes':
          serializer.toJson<String>(emRelacaoAsCriancasPresentes),
      'emRelacaoAsCriancasPresentesDescreve':
          serializer.toJson<String>(emRelacaoAsCriancasPresentesDescreve),
      'emRelacaoAVizinhanca': serializer.toJson<String>(emRelacaoAVizinhanca),
      'emRelacaoAVizinhancaDescreve':
          serializer.toJson<String>(emRelacaoAVizinhancaDescreve),
      'outras': serializer.toJson<String>(outras),
      'outrasDescreve': serializer.toJson<String>(outrasDescreve),
      'createdAt': serializer.toJson<DateTime>(createdAt),
      'updatedAt': serializer.toJson<DateTime>(updatedAt),
    };
  }

 Map<String, dynamic> toJsonSheet({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'uuid': serializer.toJson<String>(uuid),
      'nomeDoPesquisador': serializer.toJson<String>(nomeDoPesquisador),
      'numeroDeInquerito': serializer.toJson<String>(numeroDeInquerito),
      'data': "$data",
      'municipio': serializer.toJson<String>(municipio),
      'bairro': serializer.toJson<String>(bairro),
      'pontoDeReferenciaDaCasa':
          serializer.toJson<String>(pontoDeReferenciaDaCasa),
      'nome': serializer.toJson<String>(nome),
      'idade': serializer.toJson<String>(idade),
      'contacto': serializer.toJson<String>(contacto),
      'eChefeDaFamilia': serializer.toJson<String>(eChefeDaFamilia),
      'caoNaoVinculoDeParantesco':
          serializer.toJson<String>(caoNaoVinculoDeParantesco),
      'idadeDoChefeDaFamilia': serializer.toJson<String>(idadeDoChefeDaFamilia),
      'estudou': serializer.toJson<String>(estudou),
      'eAlfabetizado': serializer.toJson<String>(eAlfabetizado),
      'completouEnsinoPrimario':
          serializer.toJson<String>(completouEnsinoPrimario),
      'completouEnsinoSecundario':
          serializer.toJson<String>(completouEnsinoSecundario),
      'fezCursoSuperior': serializer.toJson<String>(fezCursoSuperior),
      'naoSabe': serializer.toJson<String>(naoSabe),
      'chefeDeFamiliaTemSalario':
          serializer.toJson<String>(chefeDeFamiliaTemSalario),
      'quantoGastaParaSustentarFamiliaPorMes':
          serializer.toJson<String>(quantoGastaParaSustentarFamiliaPorMes),
      'quantasFamiliasResidemNacasa':
          serializer.toJson<String>(quantasFamiliasResidemNacasa),
      'quantosAdultos': serializer.toJson<String>(quantosAdultos),
      'quantosAdultosMaioresDe18anos':
          serializer.toJson<String>(quantosAdultosMaioresDe18anos),
      'quantosAdultosMaioresDe18anosHomens':
          serializer.toJson<String>(quantosAdultosMaioresDe18anosHomens),
      'quantosAdultosMaioresDe18anosHomensTrabalham': serializer
          .toJson<String>(quantosAdultosMaioresDe18anosHomensTrabalham),
      'quantosAdultosMaioresDe18anosHomensAlfabetizados': serializer
          .toJson<String>(quantosAdultosMaioresDe18anosHomensAlfabetizados),
      'quantosAdultosM18Mulheres':
          serializer.toJson<String>(quantosAdultosM18Mulheres),
      'quantosAdultosM18MulheresTrabalham':
          serializer.toJson<String>(quantosAdultosM18MulheresTrabalham),
      'quantosAdultosM18MulheresAlfabetizados':
          serializer.toJson<String>(quantosAdultosM18MulheresAlfabetizados),
      'relatosDeDeficiencia': serializer.toJson<String>(relatosDeDeficiencia),
      'totalcriancasAte1anoemeio':
          serializer.toJson<String>(totalcriancasAte1anoemeio),
      'totalcriancasAte1anoemeioMeninos':
          serializer.toJson<String>(totalcriancasAte1anoemeioMeninos),
      'totalcriancasAte1anoemeioMeninas':
          serializer.toJson<String>(totalcriancasAte1anoemeioMeninas),
      'algumasCriancasAte1anoemeioNaodesenvolvem':
          serializer.toJson<String>(algumasCriancasAte1anoemeioNaodesenvolvem),
      'algumasCriancasAte1anoemeioNaodesenvolvemQuantas': serializer
          .toJson<String>(algumasCriancasAte1anoemeioNaodesenvolvemQuantas),
      'algumaCriancasAte1anoemeioQueTipodeProblema': serializer
          .toJson<String>(algumaCriancasAte1anoemeioQueTipodeProblema),
      'algumaCriancasAte1anoemeioTemProblemaMotor':
          serializer.toJson<String>(algumaCriancasAte1anoemeioTemProblemaMotor),
      'algumaCriancasAte1anoemeioTemProblemaMotorQuantas': serializer
          .toJson<String>(algumaCriancasAte1anoemeioTemProblemaMotorQuantas),
      'algumaCriancasAte1anoemeioTemMalFormacao':
          serializer.toJson<String>(algumaCriancasAte1anoemeioTemMalFormacao),
      'algumaCriancasAte1anoemeioTemMalFormacaoQuantas': serializer
          .toJson<String>(algumaCriancasAte1anoemeioTemMalFormacaoQuantas),
      'algumaCriancasAte1anoemeioNaoReagemAIncentivo': serializer
          .toJson<String>(algumaCriancasAte1anoemeioNaoReagemAIncentivo),
      'algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas': serializer
          .toJson<String>(algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas),
      'algumaCriancasAte1anoemeioTemReacaoAgressiva': serializer
          .toJson<String>(algumaCriancasAte1anoemeioTemReacaoAgressiva),
      'algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas': serializer
          .toJson<String>(algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas),
      'totalcriancasAte1anoemeioA3anos':
          serializer.toJson<String>(totalcriancasAte1anoemeioA3anos),
      'totalcriancasAte1anoemeioA3anosMeninos':
          serializer.toJson<String>(totalcriancasAte1anoemeioA3anosMeninos),
      'totalcriancasAte1anoemeioA3anosMeninas':
          serializer.toJson<String>(totalcriancasAte1anoemeioA3anosMeninas),
      'algumasCriancasAte1anoemeioA3anosNaodesenvolvem': serializer
          .toJson<String>(algumasCriancasAte1anoemeioA3anosNaodesenvolvem),
      'algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas),
      'algumaCriancasAte1anoemeioA3anosQueTipodeProblema': serializer
          .toJson<String>(algumaCriancasAte1anoemeioA3anosQueTipodeProblema),
      'algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas),
      'algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas),
      'algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo),
      'algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas),
      'algumasCriancasAte1anoemeioA3anosAlgunsAgridem': serializer
          .toJson<String>(algumasCriancasAte1anoemeioA3anosAlgunsAgridem),
      'algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas),
      'algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal),
      'algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas),
      'algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao),
      'algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas),
      'algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas),
      'algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas),
      'algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente),
      'algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas),
      'algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos),
      'algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas),
      'algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown),
      'algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas':
          serializer.toJson<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas),
      'totalcriancasde3A6anos':
          serializer.toJson<String>(totalcriancasde3A6anos),
      'totalcriancasde3A6anosMeninos':
          serializer.toJson<String>(totalcriancasde3A6anosMeninos),
      'totalcriancasde3A6anosMeninas':
          serializer.toJson<String>(totalcriancasde3A6anosMeninas),
      'algumasCriancasde3A6anosNaodesenvolvem':
          serializer.toJson<String>(algumasCriancasde3A6anosNaodesenvolvem),
      'algumasCriancasde3A6anosNaodesenvolvemQuantas': serializer
          .toJson<String>(algumasCriancasde3A6anosNaodesenvolvemQuantas),
      'algumaCriancasde3A6anosQueTipodeProblema':
          serializer.toJson<String>(algumaCriancasde3A6anosQueTipodeProblema),
      'algumasCriancasde3A6anosNaoInteragemComPessoas': serializer
          .toJson<String>(algumasCriancasde3A6anosNaoInteragemComPessoas),
      'algumasCriancasde3A6anosNaoInteragemComPessoasQuantas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosNaoInteragemComPessoasQuantas),
      'algumasCriancasde3A6anosTemComportamentoAgressivo': serializer
          .toJson<String>(algumasCriancasde3A6anosTemComportamentoAgressivo),
      'algumasCriancasde3A6anosTemComportamentoAgressivoQuantas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosTemComportamentoAgressivoQuantas),
      'algumasCriancasde3A6anosAlgunsAgridem':
          serializer.toJson<String>(algumasCriancasde3A6anosAlgunsAgridem),
      'algumasCriancasde3A6anosAlgunsAgridemQuantas': serializer
          .toJson<String>(algumasCriancasde3A6anosAlgunsAgridemQuantas),
      'algumasCriancasde3A6anosAlgumasAgitadasQueONormal': serializer
          .toJson<String>(algumasCriancasde3A6anosAlgumasAgitadasQueONormal),
      'algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas),
      'algumasCriancasde3A6anosAlgumasTemMalFormacao': serializer
          .toJson<String>(algumasCriancasde3A6anosAlgumasTemMalFormacao),
      'algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas': serializer
          .toJson<String>(algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas),
      'algumasCriancasde3A6anosAlgumasNaoAndamSozinhas': serializer
          .toJson<String>(algumasCriancasde3A6anosAlgumasNaoAndamSozinhas),
      'algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas),
      'algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente),
      'algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas),
      'algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos),
      'algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas),
      'algumasCriancasde3A6anosAlgumasTemSindromeDeDown': serializer
          .toJson<String>(algumasCriancasde3A6anosAlgumasTemSindromeDeDown),
      'algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas),
      'algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas),
      'algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas),
      'algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas),
      'algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas),
      'algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas),
      'algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas),
      'algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas),
      'algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas':
          serializer.toJson<String>(
              algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas),
      'quantasCriancasde3A6anosEstaoNaEscolinha':
          serializer.toJson<String>(quantasCriancasde3A6anosEstaoNaEscolinha),
      'criancasDe3A6anosNaoEstaoNaEscolinhaPorque':
          serializer.toJson<String>(criancasDe3A6anosNaoEstaoNaEscolinhaPorque),
      'quantasCriancasde3A6anosTrabalham':
          serializer.toJson<String>(quantasCriancasde3A6anosTrabalham),
      'criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos': serializer
          .toJson<String>(criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos),
      'totalcriancasde7A9anos':
          serializer.toJson<String>(totalcriancasde7A9anos),
      'totalcriancasde7A9anosMeninos':
          serializer.toJson<String>(totalcriancasde7A9anosMeninos),
      'totalcriancasde7A9anosMeninas':
          serializer.toJson<String>(totalcriancasde7A9anosMeninas),
      'quantasCriancasde7A9anosEstaoNaEscola':
          serializer.toJson<String>(quantasCriancasde7A9anosEstaoNaEscola),
      'quantasCriancasde7A9anosTrabalham':
          serializer.toJson<String>(quantasCriancasde7A9anosTrabalham),
      'nosUltimos5AnosQuantasGravidezesTiveNaFamilia': serializer
          .toJson<String>(nosUltimos5AnosQuantasGravidezesTiveNaFamilia),
      'partosOcoridosNoHospital':
          serializer.toJson<String>(partosOcoridosNoHospital),
      'partosOcoridosEmCasa': serializer.toJson<String>(partosOcoridosEmCasa),
      'partosOcoridosEmOutroLocal':
          serializer.toJson<String>(partosOcoridosEmOutroLocal),
      'outroLocalDoPartoQualLocal':
          serializer.toJson<String>(outroLocalDoPartoQualLocal),
      'quantasCriancasNasceramVivas':
          serializer.toJson<String>(quantasCriancasNasceramVivas),
      'quantasContinuamVivas': serializer.toJson<String>(quantasContinuamVivas),
      'dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram':
          serializer.toJson<String>(
              dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram),
      'descreverOProblema': serializer.toJson<String>(descreverOProblema),
      'aFamiliaProcurouAjudaParaSuperarEsseProblema': serializer
          .toJson<String>(aFamiliaProcurouAjudaParaSuperarEsseProblema),
      'seSimQueTipoDeAjudaEOnde':
          serializer.toJson<String>(seSimQueTipoDeAjudaEOnde),
      'osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia':
          serializer.toJson<String>(
              osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia),
      'dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida':
          serializer.toJson<String>(
              dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida),
      'sabeDizerOMotivoDosObitos':
          serializer.toJson<String>(sabeDizerOMotivoDosObitos),
      'seSimQualFoiPrincipalRazaodoObito':
          serializer.toJson<String>(seSimQualFoiPrincipalRazaodoObito),
      'voceTemMedoDeTerUmFilhoOuNetoComProblemas':
          serializer.toJson<String>(voceTemMedoDeTerUmFilhoOuNetoComProblemas),
      'expliquePorQue': serializer.toJson<String>(expliquePorQue),
      'porqueVoceAchaQueUmaCriancaPodeNascerComProblema': serializer
          .toJson<String>(porqueVoceAchaQueUmaCriancaPodeNascerComProblema),
      'oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema':
          serializer.toJson<String>(
              oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema),
      'achaQueUmaCriancaQueNasceComProblemaPodeSobreviver': serializer
          .toJson<String>(achaQueUmaCriancaQueNasceComProblemaPodeSobreviver),
      'expliquePorque': serializer.toJson<String>(expliquePorque),
      'quemPodeAjudarQuandoUmaCriancaNasceComProblema': serializer
          .toJson<String>(quemPodeAjudarQuandoUmaCriancaNasceComProblema),
      'emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao':
          serializer.toJson<String>(
              emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao),
      'emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene':
          serializer.toJson<String>(
              emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene),
      'emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos': serializer
          .toJson<String>(emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos),
      'aPessoaEntrevistadaPareceuSinceraNasSuasRespostas': serializer
          .toJson<String>(aPessoaEntrevistadaPareceuSinceraNasSuasRespostas),
      'algumasPerguntasIncomodaramAPessoaEntrevistada': serializer
          .toJson<String>(algumasPerguntasIncomodaramAPessoaEntrevistada),
      'seSimQuais3PrincipaisPerguntas':
          serializer.toJson<String>(seSimQuais3PrincipaisPerguntas),
      'todasAsPerguntasIncomodaram':
          serializer.toJson<String>(todasAsPerguntasIncomodaram),
      'aResidenciaEraDe': serializer.toJson<String>(aResidenciaEraDe),
      'comoAvaliaHigieneDaCasa':
          serializer.toJson<String>(comoAvaliaHigieneDaCasa),
      'oQueEraMal': serializer.toJson<String>(oQueEraMal),
      'tinhaCriancasNaCasaDuranteInquerito':
          serializer.toJson<String>(tinhaCriancasNaCasaDuranteInquerito),
      'tinhaCriancasNaCasaDuranteInqueritoQuantas':
          serializer.toJson<String>(tinhaCriancasNaCasaDuranteInqueritoQuantas),
      'tentouIncentivar': serializer.toJson<String>(tentouIncentivar),
      'tentouIncentivarQuantas':
          serializer.toJson<String>(tentouIncentivarQuantas),
      'seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos':
          serializer.toJson<String>(
              seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos),
      'observouCriancasDeAte1Ano':
          serializer.toJson<String>(observouCriancasDeAte1Ano),
      'observouCriancasDeAte1AnoQuantos':
          serializer.toJson<String>(observouCriancasDeAte1AnoQuantos),
      'algumasApresentavamProblemas':
          serializer.toJson<String>(algumasApresentavamProblemas),
      'algumasApresentavamProblemasQuantos':
          serializer.toJson<String>(algumasApresentavamProblemasQuantos),
      'descreveOProblemaDessasCriancas':
          serializer.toJson<String>(descreveOProblemaDessasCriancas),
      'observouCriancasDe1AnoEmeioAte3Anos':
          serializer.toJson<String>(observouCriancasDe1AnoEmeioAte3Anos),
      'observouCriancasDe1AnoEmeioAte3AnosQuantas':
          serializer.toJson<String>(observouCriancasDe1AnoEmeioAte3AnosQuantas),
      'algumasCriancasDe1anoEmeioObservadasApresentavamProblemas':
          serializer.toJson<String>(
              algumasCriancasDe1anoEmeioObservadasApresentavamProblemas),
      'algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas':
          serializer.toJson<String>(
              algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas),
      'algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve':
          serializer.toJson<String>(
              algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve),
      'observouCriancasDe3AnosAte6Anos':
          serializer.toJson<String>(observouCriancasDe3AnosAte6Anos),
      'observouCriancasDe3AnosAte6AnosQuantas':
          serializer.toJson<String>(observouCriancasDe3AnosAte6AnosQuantas),
      'observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas':
          serializer.toJson<String>(
              observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas),
      'observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas':
          serializer.toJson<String>(
              observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas),
      'observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve':
          serializer.toJson<String>(
              observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve),
      'observouSituacoesQueLheChamaramAtencao':
          serializer.toJson<String>(observouSituacoesQueLheChamaramAtencao),
      'emRelacaoAPessoaEntrevistada':
          serializer.toJson<String>(emRelacaoAPessoaEntrevistada),
      'emRelacaoAPessoaEntrevistadaDescreve':
          serializer.toJson<String>(emRelacaoAPessoaEntrevistadaDescreve),
      'emRelacaoAOutrosAdultosPresente':
          serializer.toJson<String>(emRelacaoAOutrosAdultosPresente),
      'emRelacaoAOutrosAdultosPresenteDescreve':
          serializer.toJson<String>(emRelacaoAOutrosAdultosPresenteDescreve),
      'emRelacaoAsCriancasPresentes':
          serializer.toJson<String>(emRelacaoAsCriancasPresentes),
      'emRelacaoAsCriancasPresentesDescreve':
          serializer.toJson<String>(emRelacaoAsCriancasPresentesDescreve),
      'emRelacaoAVizinhanca': serializer.toJson<String>(emRelacaoAVizinhanca),
      'emRelacaoAVizinhancaDescreve':
          serializer.toJson<String>(emRelacaoAVizinhancaDescreve),
      'outras': serializer.toJson<String>(outras),
      'outrasDescreve': serializer.toJson<String>(outrasDescreve),
      'createdAt': "$createdAt",
      'updatedAt': "$updatedAt",
    };
  }

  User copyWith(
          {String uuid,
          String nomeDoPesquisador,
          String numeroDeInquerito,
          DateTime data,
          String municipio,
          String bairro,
          String pontoDeReferenciaDaCasa,
          String nome,
          String idade,
          String contacto,
          String eChefeDaFamilia,
          String caoNaoVinculoDeParantesco,
          String idadeDoChefeDaFamilia,
          String estudou,
          String eAlfabetizado,
          String completouEnsinoPrimario,
          String completouEnsinoSecundario,
          String fezCursoSuperior,
          String naoSabe,
          String chefeDeFamiliaTemSalario,
          String quantoGastaParaSustentarFamiliaPorMes,
          String quantasFamiliasResidemNacasa,
          String quantosAdultos,
          String quantosAdultosMaioresDe18anos,
          String quantosAdultosMaioresDe18anosHomens,
          String quantosAdultosMaioresDe18anosHomensTrabalham,
          String quantosAdultosMaioresDe18anosHomensAlfabetizados,
          String quantosAdultosM18Mulheres,
          String quantosAdultosM18MulheresTrabalham,
          String quantosAdultosM18MulheresAlfabetizados,
          String relatosDeDeficiencia,
          String totalcriancasAte1anoemeio,
          String totalcriancasAte1anoemeioMeninos,
          String totalcriancasAte1anoemeioMeninas,
          String algumasCriancasAte1anoemeioNaodesenvolvem,
          String algumasCriancasAte1anoemeioNaodesenvolvemQuantas,
          String algumaCriancasAte1anoemeioQueTipodeProblema,
          String algumaCriancasAte1anoemeioTemProblemaMotor,
          String algumaCriancasAte1anoemeioTemProblemaMotorQuantas,
          String algumaCriancasAte1anoemeioTemMalFormacao,
          String algumaCriancasAte1anoemeioTemMalFormacaoQuantas,
          String algumaCriancasAte1anoemeioNaoReagemAIncentivo,
          String algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas,
          String algumaCriancasAte1anoemeioTemReacaoAgressiva,
          String algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas,
          String totalcriancasAte1anoemeioA3anos,
          String totalcriancasAte1anoemeioA3anosMeninos,
          String totalcriancasAte1anoemeioA3anosMeninas,
          String algumasCriancasAte1anoemeioA3anosNaodesenvolvem,
          String algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas,
          String algumaCriancasAte1anoemeioA3anosQueTipodeProblema,
          String algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas,
          String algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas,
          String algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo,
          String
              algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas,
          String algumasCriancasAte1anoemeioA3anosAlgunsAgridem,
          String algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas,
          String algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal,
          String
              algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas,
          String algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao,
          String algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas,
          String algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas,
          String
              algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas,
          String
              algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente,
          String
              algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas,
          String
              algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos,
          String
              algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas,
          String algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown,
          String
              algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas,
          String totalcriancasde3A6anos,
          String totalcriancasde3A6anosMeninos,
          String totalcriancasde3A6anosMeninas,
          String algumasCriancasde3A6anosNaodesenvolvem,
          String algumasCriancasde3A6anosNaodesenvolvemQuantas,
          String algumaCriancasde3A6anosQueTipodeProblema,
          String algumasCriancasde3A6anosNaoInteragemComPessoas,
          String algumasCriancasde3A6anosNaoInteragemComPessoasQuantas,
          String algumasCriancasde3A6anosTemComportamentoAgressivo,
          String algumasCriancasde3A6anosTemComportamentoAgressivoQuantas,
          String algumasCriancasde3A6anosAlgunsAgridem,
          String algumasCriancasde3A6anosAlgunsAgridemQuantas,
          String algumasCriancasde3A6anosAlgumasAgitadasQueONormal,
          String algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas,
          String algumasCriancasde3A6anosAlgumasTemMalFormacao,
          String algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas,
          String algumasCriancasde3A6anosAlgumasNaoAndamSozinhas,
          String algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas,
          String algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente,
          String algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas,
          String algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos,
          String algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas,
          String algumasCriancasde3A6anosAlgumasTemSindromeDeDown,
          String algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas,
          String algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas,
          String
              algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas,
          String algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas,
          String
              algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas,
          String
              algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas,
          String
              algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas,
          String algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas,
          String
              algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas,
          String quantasCriancasde3A6anosEstaoNaEscolinha,
          String criancasDe3A6anosNaoEstaoNaEscolinhaPorque,
          String quantasCriancasde3A6anosTrabalham,
          String criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos,
          String totalcriancasde7A9anos,
          String totalcriancasde7A9anosMeninos,
          String totalcriancasde7A9anosMeninas,
          String quantasCriancasde7A9anosEstaoNaEscola,
          String quantasCriancasde7A9anosTrabalham,
          String nosUltimos5AnosQuantasGravidezesTiveNaFamilia,
          String partosOcoridosNoHospital,
          String partosOcoridosEmCasa,
          String partosOcoridosEmOutroLocal,
          String outroLocalDoPartoQualLocal,
          String quantasCriancasNasceramVivas,
          String quantasContinuamVivas,
          String
              dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram,
          String descreverOProblema,
          String aFamiliaProcurouAjudaParaSuperarEsseProblema,
          String seSimQueTipoDeAjudaEOnde,
          String
              osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia,
          String
              dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida,
          String sabeDizerOMotivoDosObitos,
          String seSimQualFoiPrincipalRazaodoObito,
          String voceTemMedoDeTerUmFilhoOuNetoComProblemas,
          String expliquePorQue,
          String porqueVoceAchaQueUmaCriancaPodeNascerComProblema,
          String oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema,
          String achaQueUmaCriancaQueNasceComProblemaPodeSobreviver,
          String expliquePorque,
          String quemPodeAjudarQuandoUmaCriancaNasceComProblema,
          String emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao,
          String emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene,
          String emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos,
          String aPessoaEntrevistadaPareceuSinceraNasSuasRespostas,
          String algumasPerguntasIncomodaramAPessoaEntrevistada,
          String seSimQuais3PrincipaisPerguntas,
          String todasAsPerguntasIncomodaram,
          String aResidenciaEraDe,
          String comoAvaliaHigieneDaCasa,
          String oQueEraMal,
          String tinhaCriancasNaCasaDuranteInquerito,
          String tinhaCriancasNaCasaDuranteInqueritoQuantas,
          String tentouIncentivar,
          String tentouIncentivarQuantas,
          String seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos,
          String observouCriancasDeAte1Ano,
          String observouCriancasDeAte1AnoQuantos,
          String algumasApresentavamProblemas,
          String algumasApresentavamProblemasQuantos,
          String descreveOProblemaDessasCriancas,
          String observouCriancasDe1AnoEmeioAte3Anos,
          String observouCriancasDe1AnoEmeioAte3AnosQuantas,
          String algumasCriancasDe1anoEmeioObservadasApresentavamProblemas,
          String
              algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas,
          String
              algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve,
          String observouCriancasDe3AnosAte6Anos,
          String observouCriancasDe3AnosAte6AnosQuantas,
          String observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas,
          String
              observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas,
          String
              observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve,
          String observouSituacoesQueLheChamaramAtencao,
          String emRelacaoAPessoaEntrevistada,
          String emRelacaoAPessoaEntrevistadaDescreve,
          String emRelacaoAOutrosAdultosPresente,
          String emRelacaoAOutrosAdultosPresenteDescreve,
          String emRelacaoAsCriancasPresentes,
          String emRelacaoAsCriancasPresentesDescreve,
          String emRelacaoAVizinhanca,
          String emRelacaoAVizinhancaDescreve,
          String outras,
          String outrasDescreve,
          DateTime createdAt,
          DateTime updatedAt}) =>
      User(
        uuid: uuid ?? this.uuid,
        nomeDoPesquisador: nomeDoPesquisador ?? this.nomeDoPesquisador,
        numeroDeInquerito: numeroDeInquerito ?? this.numeroDeInquerito,
        data: data ?? this.data,
        municipio: municipio ?? this.municipio,
        bairro: bairro ?? this.bairro,
        pontoDeReferenciaDaCasa:
            pontoDeReferenciaDaCasa ?? this.pontoDeReferenciaDaCasa,
        nome: nome ?? this.nome,
        idade: idade ?? this.idade,
        contacto: contacto ?? this.contacto,
        eChefeDaFamilia: eChefeDaFamilia ?? this.eChefeDaFamilia,
        caoNaoVinculoDeParantesco:
            caoNaoVinculoDeParantesco ?? this.caoNaoVinculoDeParantesco,
        idadeDoChefeDaFamilia:
            idadeDoChefeDaFamilia ?? this.idadeDoChefeDaFamilia,
        estudou: estudou ?? this.estudou,
        eAlfabetizado: eAlfabetizado ?? this.eAlfabetizado,
        completouEnsinoPrimario:
            completouEnsinoPrimario ?? this.completouEnsinoPrimario,
        completouEnsinoSecundario:
            completouEnsinoSecundario ?? this.completouEnsinoSecundario,
        fezCursoSuperior: fezCursoSuperior ?? this.fezCursoSuperior,
        naoSabe: naoSabe ?? this.naoSabe,
        chefeDeFamiliaTemSalario:
            chefeDeFamiliaTemSalario ?? this.chefeDeFamiliaTemSalario,
        quantoGastaParaSustentarFamiliaPorMes:
            quantoGastaParaSustentarFamiliaPorMes ??
                this.quantoGastaParaSustentarFamiliaPorMes,
        quantasFamiliasResidemNacasa:
            quantasFamiliasResidemNacasa ?? this.quantasFamiliasResidemNacasa,
        quantosAdultos: quantosAdultos ?? this.quantosAdultos,
        quantosAdultosMaioresDe18anos:
            quantosAdultosMaioresDe18anos ?? this.quantosAdultosMaioresDe18anos,
        quantosAdultosMaioresDe18anosHomens:
            quantosAdultosMaioresDe18anosHomens ??
                this.quantosAdultosMaioresDe18anosHomens,
        quantosAdultosMaioresDe18anosHomensTrabalham:
            quantosAdultosMaioresDe18anosHomensTrabalham ??
                this.quantosAdultosMaioresDe18anosHomensTrabalham,
        quantosAdultosMaioresDe18anosHomensAlfabetizados:
            quantosAdultosMaioresDe18anosHomensAlfabetizados ??
                this.quantosAdultosMaioresDe18anosHomensAlfabetizados,
        quantosAdultosM18Mulheres:
            quantosAdultosM18Mulheres ?? this.quantosAdultosM18Mulheres,
        quantosAdultosM18MulheresTrabalham:
            quantosAdultosM18MulheresTrabalham ??
                this.quantosAdultosM18MulheresTrabalham,
        quantosAdultosM18MulheresAlfabetizados:
            quantosAdultosM18MulheresAlfabetizados ??
                this.quantosAdultosM18MulheresAlfabetizados,
        relatosDeDeficiencia: relatosDeDeficiencia ?? this.relatosDeDeficiencia,
        totalcriancasAte1anoemeio:
            totalcriancasAte1anoemeio ?? this.totalcriancasAte1anoemeio,
        totalcriancasAte1anoemeioMeninos: totalcriancasAte1anoemeioMeninos ??
            this.totalcriancasAte1anoemeioMeninos,
        totalcriancasAte1anoemeioMeninas: totalcriancasAte1anoemeioMeninas ??
            this.totalcriancasAte1anoemeioMeninas,
        algumasCriancasAte1anoemeioNaodesenvolvem:
            algumasCriancasAte1anoemeioNaodesenvolvem ??
                this.algumasCriancasAte1anoemeioNaodesenvolvem,
        algumasCriancasAte1anoemeioNaodesenvolvemQuantas:
            algumasCriancasAte1anoemeioNaodesenvolvemQuantas ??
                this.algumasCriancasAte1anoemeioNaodesenvolvemQuantas,
        algumaCriancasAte1anoemeioQueTipodeProblema:
            algumaCriancasAte1anoemeioQueTipodeProblema ??
                this.algumaCriancasAte1anoemeioQueTipodeProblema,
        algumaCriancasAte1anoemeioTemProblemaMotor:
            algumaCriancasAte1anoemeioTemProblemaMotor ??
                this.algumaCriancasAte1anoemeioTemProblemaMotor,
        algumaCriancasAte1anoemeioTemProblemaMotorQuantas:
            algumaCriancasAte1anoemeioTemProblemaMotorQuantas ??
                this.algumaCriancasAte1anoemeioTemProblemaMotorQuantas,
        algumaCriancasAte1anoemeioTemMalFormacao:
            algumaCriancasAte1anoemeioTemMalFormacao ??
                this.algumaCriancasAte1anoemeioTemMalFormacao,
        algumaCriancasAte1anoemeioTemMalFormacaoQuantas:
            algumaCriancasAte1anoemeioTemMalFormacaoQuantas ??
                this.algumaCriancasAte1anoemeioTemMalFormacaoQuantas,
        algumaCriancasAte1anoemeioNaoReagemAIncentivo:
            algumaCriancasAte1anoemeioNaoReagemAIncentivo ??
                this.algumaCriancasAte1anoemeioNaoReagemAIncentivo,
        algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas:
            algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas ??
                this.algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas,
        algumaCriancasAte1anoemeioTemReacaoAgressiva:
            algumaCriancasAte1anoemeioTemReacaoAgressiva ??
                this.algumaCriancasAte1anoemeioTemReacaoAgressiva,
        algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas:
            algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas ??
                this.algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas,
        totalcriancasAte1anoemeioA3anos: totalcriancasAte1anoemeioA3anos ??
            this.totalcriancasAte1anoemeioA3anos,
        totalcriancasAte1anoemeioA3anosMeninos:
            totalcriancasAte1anoemeioA3anosMeninos ??
                this.totalcriancasAte1anoemeioA3anosMeninos,
        totalcriancasAte1anoemeioA3anosMeninas:
            totalcriancasAte1anoemeioA3anosMeninas ??
                this.totalcriancasAte1anoemeioA3anosMeninas,
        algumasCriancasAte1anoemeioA3anosNaodesenvolvem:
            algumasCriancasAte1anoemeioA3anosNaodesenvolvem ??
                this.algumasCriancasAte1anoemeioA3anosNaodesenvolvem,
        algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas:
            algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas ??
                this.algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas,
        algumaCriancasAte1anoemeioA3anosQueTipodeProblema:
            algumaCriancasAte1anoemeioA3anosQueTipodeProblema ??
                this.algumaCriancasAte1anoemeioA3anosQueTipodeProblema,
        algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas:
            algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas ??
                this.algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas,
        algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas:
            algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas ??
                this.algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas,
        algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo:
            algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo ??
                this.algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo,
        algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas:
            algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas ??
                this.algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas,
        algumasCriancasAte1anoemeioA3anosAlgunsAgridem:
            algumasCriancasAte1anoemeioA3anosAlgunsAgridem ??
                this.algumasCriancasAte1anoemeioA3anosAlgunsAgridem,
        algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas:
            algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas ??
                this.algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas,
        algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal:
            algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal ??
                this.algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal,
        algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas:
            algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas ??
                this.algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas,
        algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao:
            algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao ??
                this.algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao,
        algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas:
            algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas ??
                this.algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas,
        algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas:
            algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas ??
                this.algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas,
        algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas:
            algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas ??
                this.algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas,
        algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente:
            algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente ??
                this.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente,
        algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas:
            algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas ??
                this.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas,
        algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos:
            algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos ??
                this.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos,
        algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas:
            algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas ??
                this.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas,
        algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown:
            algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown ??
                this.algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown,
        algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas:
            algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas ??
                this.algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas,
        totalcriancasde3A6anos:
            totalcriancasde3A6anos ?? this.totalcriancasde3A6anos,
        totalcriancasde3A6anosMeninos:
            totalcriancasde3A6anosMeninos ?? this.totalcriancasde3A6anosMeninos,
        totalcriancasde3A6anosMeninas:
            totalcriancasde3A6anosMeninas ?? this.totalcriancasde3A6anosMeninas,
        algumasCriancasde3A6anosNaodesenvolvem:
            algumasCriancasde3A6anosNaodesenvolvem ??
                this.algumasCriancasde3A6anosNaodesenvolvem,
        algumasCriancasde3A6anosNaodesenvolvemQuantas:
            algumasCriancasde3A6anosNaodesenvolvemQuantas ??
                this.algumasCriancasde3A6anosNaodesenvolvemQuantas,
        algumaCriancasde3A6anosQueTipodeProblema:
            algumaCriancasde3A6anosQueTipodeProblema ??
                this.algumaCriancasde3A6anosQueTipodeProblema,
        algumasCriancasde3A6anosNaoInteragemComPessoas:
            algumasCriancasde3A6anosNaoInteragemComPessoas ??
                this.algumasCriancasde3A6anosNaoInteragemComPessoas,
        algumasCriancasde3A6anosNaoInteragemComPessoasQuantas:
            algumasCriancasde3A6anosNaoInteragemComPessoasQuantas ??
                this.algumasCriancasde3A6anosNaoInteragemComPessoasQuantas,
        algumasCriancasde3A6anosTemComportamentoAgressivo:
            algumasCriancasde3A6anosTemComportamentoAgressivo ??
                this.algumasCriancasde3A6anosTemComportamentoAgressivo,
        algumasCriancasde3A6anosTemComportamentoAgressivoQuantas:
            algumasCriancasde3A6anosTemComportamentoAgressivoQuantas ??
                this.algumasCriancasde3A6anosTemComportamentoAgressivoQuantas,
        algumasCriancasde3A6anosAlgunsAgridem:
            algumasCriancasde3A6anosAlgunsAgridem ??
                this.algumasCriancasde3A6anosAlgunsAgridem,
        algumasCriancasde3A6anosAlgunsAgridemQuantas:
            algumasCriancasde3A6anosAlgunsAgridemQuantas ??
                this.algumasCriancasde3A6anosAlgunsAgridemQuantas,
        algumasCriancasde3A6anosAlgumasAgitadasQueONormal:
            algumasCriancasde3A6anosAlgumasAgitadasQueONormal ??
                this.algumasCriancasde3A6anosAlgumasAgitadasQueONormal,
        algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas:
            algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas ??
                this.algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas,
        algumasCriancasde3A6anosAlgumasTemMalFormacao:
            algumasCriancasde3A6anosAlgumasTemMalFormacao ??
                this.algumasCriancasde3A6anosAlgumasTemMalFormacao,
        algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas:
            algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas ??
                this.algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas,
        algumasCriancasde3A6anosAlgumasNaoAndamSozinhas:
            algumasCriancasde3A6anosAlgumasNaoAndamSozinhas ??
                this.algumasCriancasde3A6anosAlgumasNaoAndamSozinhas,
        algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas:
            algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas ??
                this.algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas,
        algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente:
            algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente ??
                this.algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente,
        algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas:
            algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas ??
                this.algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas,
        algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos:
            algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos ??
                this.algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos,
        algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas:
            algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas ??
                this.algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas,
        algumasCriancasde3A6anosAlgumasTemSindromeDeDown:
            algumasCriancasde3A6anosAlgumasTemSindromeDeDown ??
                this.algumasCriancasde3A6anosAlgumasTemSindromeDeDown,
        algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas:
            algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas ??
                this.algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas,
        algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas:
            algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas ??
                this.algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas,
        algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas:
            algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas ??
                this.algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas,
        algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas:
            algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas ??
                this.algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas,
        algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas:
            algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas ??
                this.algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas,
        algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas:
            algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas ??
                this.algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas,
        algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas:
            algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas ??
                this.algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas,
        algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas:
            algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas ??
                this.algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas,
        algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas:
            algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas ??
                this.algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas,
        quantasCriancasde3A6anosEstaoNaEscolinha:
            quantasCriancasde3A6anosEstaoNaEscolinha ??
                this.quantasCriancasde3A6anosEstaoNaEscolinha,
        criancasDe3A6anosNaoEstaoNaEscolinhaPorque:
            criancasDe3A6anosNaoEstaoNaEscolinhaPorque ??
                this.criancasDe3A6anosNaoEstaoNaEscolinhaPorque,
        quantasCriancasde3A6anosTrabalham: quantasCriancasde3A6anosTrabalham ??
            this.quantasCriancasde3A6anosTrabalham,
        criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos:
            criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos ??
                this.criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos,
        totalcriancasde7A9anos:
            totalcriancasde7A9anos ?? this.totalcriancasde7A9anos,
        totalcriancasde7A9anosMeninos:
            totalcriancasde7A9anosMeninos ?? this.totalcriancasde7A9anosMeninos,
        totalcriancasde7A9anosMeninas:
            totalcriancasde7A9anosMeninas ?? this.totalcriancasde7A9anosMeninas,
        quantasCriancasde7A9anosEstaoNaEscola:
            quantasCriancasde7A9anosEstaoNaEscola ??
                this.quantasCriancasde7A9anosEstaoNaEscola,
        quantasCriancasde7A9anosTrabalham: quantasCriancasde7A9anosTrabalham ??
            this.quantasCriancasde7A9anosTrabalham,
        nosUltimos5AnosQuantasGravidezesTiveNaFamilia:
            nosUltimos5AnosQuantasGravidezesTiveNaFamilia ??
                this.nosUltimos5AnosQuantasGravidezesTiveNaFamilia,
        partosOcoridosNoHospital:
            partosOcoridosNoHospital ?? this.partosOcoridosNoHospital,
        partosOcoridosEmCasa: partosOcoridosEmCasa ?? this.partosOcoridosEmCasa,
        partosOcoridosEmOutroLocal:
            partosOcoridosEmOutroLocal ?? this.partosOcoridosEmOutroLocal,
        outroLocalDoPartoQualLocal:
            outroLocalDoPartoQualLocal ?? this.outroLocalDoPartoQualLocal,
        quantasCriancasNasceramVivas:
            quantasCriancasNasceramVivas ?? this.quantasCriancasNasceramVivas,
        quantasContinuamVivas:
            quantasContinuamVivas ?? this.quantasContinuamVivas,
        dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram:
            dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram ??
                this.dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram,
        descreverOProblema: descreverOProblema ?? this.descreverOProblema,
        aFamiliaProcurouAjudaParaSuperarEsseProblema:
            aFamiliaProcurouAjudaParaSuperarEsseProblema ??
                this.aFamiliaProcurouAjudaParaSuperarEsseProblema,
        seSimQueTipoDeAjudaEOnde:
            seSimQueTipoDeAjudaEOnde ?? this.seSimQueTipoDeAjudaEOnde,
        osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia:
            osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia ??
                this.osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia,
        dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida:
            dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida ??
                this.dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida,
        sabeDizerOMotivoDosObitos:
            sabeDizerOMotivoDosObitos ?? this.sabeDizerOMotivoDosObitos,
        seSimQualFoiPrincipalRazaodoObito: seSimQualFoiPrincipalRazaodoObito ??
            this.seSimQualFoiPrincipalRazaodoObito,
        voceTemMedoDeTerUmFilhoOuNetoComProblemas:
            voceTemMedoDeTerUmFilhoOuNetoComProblemas ??
                this.voceTemMedoDeTerUmFilhoOuNetoComProblemas,
        expliquePorQue: expliquePorQue ?? this.expliquePorQue,
        porqueVoceAchaQueUmaCriancaPodeNascerComProblema:
            porqueVoceAchaQueUmaCriancaPodeNascerComProblema ??
                this.porqueVoceAchaQueUmaCriancaPodeNascerComProblema,
        oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema:
            oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema ??
                this.oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema,
        achaQueUmaCriancaQueNasceComProblemaPodeSobreviver:
            achaQueUmaCriancaQueNasceComProblemaPodeSobreviver ??
                this.achaQueUmaCriancaQueNasceComProblemaPodeSobreviver,
        expliquePorque: expliquePorque ?? this.expliquePorque,
        quemPodeAjudarQuandoUmaCriancaNasceComProblema:
            quemPodeAjudarQuandoUmaCriancaNasceComProblema ??
                this.quemPodeAjudarQuandoUmaCriancaNasceComProblema,
        emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao:
            emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao ??
                this.emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao,
        emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene:
            emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene ??
                this.emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene,
        emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos:
            emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos ??
                this.emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos,
        aPessoaEntrevistadaPareceuSinceraNasSuasRespostas:
            aPessoaEntrevistadaPareceuSinceraNasSuasRespostas ??
                this.aPessoaEntrevistadaPareceuSinceraNasSuasRespostas,
        algumasPerguntasIncomodaramAPessoaEntrevistada:
            algumasPerguntasIncomodaramAPessoaEntrevistada ??
                this.algumasPerguntasIncomodaramAPessoaEntrevistada,
        seSimQuais3PrincipaisPerguntas: seSimQuais3PrincipaisPerguntas ??
            this.seSimQuais3PrincipaisPerguntas,
        todasAsPerguntasIncomodaram:
            todasAsPerguntasIncomodaram ?? this.todasAsPerguntasIncomodaram,
        aResidenciaEraDe: aResidenciaEraDe ?? this.aResidenciaEraDe,
        comoAvaliaHigieneDaCasa:
            comoAvaliaHigieneDaCasa ?? this.comoAvaliaHigieneDaCasa,
        oQueEraMal: oQueEraMal ?? this.oQueEraMal,
        tinhaCriancasNaCasaDuranteInquerito:
            tinhaCriancasNaCasaDuranteInquerito ??
                this.tinhaCriancasNaCasaDuranteInquerito,
        tinhaCriancasNaCasaDuranteInqueritoQuantas:
            tinhaCriancasNaCasaDuranteInqueritoQuantas ??
                this.tinhaCriancasNaCasaDuranteInqueritoQuantas,
        tentouIncentivar: tentouIncentivar ?? this.tentouIncentivar,
        tentouIncentivarQuantas:
            tentouIncentivarQuantas ?? this.tentouIncentivarQuantas,
        seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos:
            seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos ??
                this.seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos,
        observouCriancasDeAte1Ano:
            observouCriancasDeAte1Ano ?? this.observouCriancasDeAte1Ano,
        observouCriancasDeAte1AnoQuantos: observouCriancasDeAte1AnoQuantos ??
            this.observouCriancasDeAte1AnoQuantos,
        algumasApresentavamProblemas:
            algumasApresentavamProblemas ?? this.algumasApresentavamProblemas,
        algumasApresentavamProblemasQuantos:
            algumasApresentavamProblemasQuantos ??
                this.algumasApresentavamProblemasQuantos,
        descreveOProblemaDessasCriancas: descreveOProblemaDessasCriancas ??
            this.descreveOProblemaDessasCriancas,
        observouCriancasDe1AnoEmeioAte3Anos:
            observouCriancasDe1AnoEmeioAte3Anos ??
                this.observouCriancasDe1AnoEmeioAte3Anos,
        observouCriancasDe1AnoEmeioAte3AnosQuantas:
            observouCriancasDe1AnoEmeioAte3AnosQuantas ??
                this.observouCriancasDe1AnoEmeioAte3AnosQuantas,
        algumasCriancasDe1anoEmeioObservadasApresentavamProblemas:
            algumasCriancasDe1anoEmeioObservadasApresentavamProblemas ??
                this.algumasCriancasDe1anoEmeioObservadasApresentavamProblemas,
        algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas:
            algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas ??
                this.algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas,
        algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve:
            algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve ??
                this.algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve,
        observouCriancasDe3AnosAte6Anos: observouCriancasDe3AnosAte6Anos ??
            this.observouCriancasDe3AnosAte6Anos,
        observouCriancasDe3AnosAte6AnosQuantas:
            observouCriancasDe3AnosAte6AnosQuantas ??
                this.observouCriancasDe3AnosAte6AnosQuantas,
        observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas:
            observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas ??
                this.observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas,
        observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas:
            observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas ??
                this.observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas,
        observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve:
            observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve ??
                this.observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve,
        observouSituacoesQueLheChamaramAtencao:
            observouSituacoesQueLheChamaramAtencao ??
                this.observouSituacoesQueLheChamaramAtencao,
        emRelacaoAPessoaEntrevistada:
            emRelacaoAPessoaEntrevistada ?? this.emRelacaoAPessoaEntrevistada,
        emRelacaoAPessoaEntrevistadaDescreve:
            emRelacaoAPessoaEntrevistadaDescreve ??
                this.emRelacaoAPessoaEntrevistadaDescreve,
        emRelacaoAOutrosAdultosPresente: emRelacaoAOutrosAdultosPresente ??
            this.emRelacaoAOutrosAdultosPresente,
        emRelacaoAOutrosAdultosPresenteDescreve:
            emRelacaoAOutrosAdultosPresenteDescreve ??
                this.emRelacaoAOutrosAdultosPresenteDescreve,
        emRelacaoAsCriancasPresentes:
            emRelacaoAsCriancasPresentes ?? this.emRelacaoAsCriancasPresentes,
        emRelacaoAsCriancasPresentesDescreve:
            emRelacaoAsCriancasPresentesDescreve ??
                this.emRelacaoAsCriancasPresentesDescreve,
        emRelacaoAVizinhanca: emRelacaoAVizinhanca ?? this.emRelacaoAVizinhanca,
        emRelacaoAVizinhancaDescreve:
            emRelacaoAVizinhancaDescreve ?? this.emRelacaoAVizinhancaDescreve,
        outras: outras ?? this.outras,
        outrasDescreve: outrasDescreve ?? this.outrasDescreve,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
      );
  @override
  String toString() {
    return (StringBuffer('User(')
          ..write('uuid: $uuid, ')
          ..write('nomeDoPesquisador: $nomeDoPesquisador, ')
          ..write('numeroDeInquerito: $numeroDeInquerito, ')
          ..write('data: $data, ')
          ..write('municipio: $municipio, ')
          ..write('bairro: $bairro, ')
          ..write('pontoDeReferenciaDaCasa: $pontoDeReferenciaDaCasa, ')
          ..write('nome: $nome, ')
          ..write('idade: $idade, ')
          ..write('contacto: $contacto, ')
          ..write('eChefeDaFamilia: $eChefeDaFamilia, ')
          ..write('caoNaoVinculoDeParantesco: $caoNaoVinculoDeParantesco, ')
          ..write('idadeDoChefeDaFamilia: $idadeDoChefeDaFamilia, ')
          ..write('estudou: $estudou, ')
          ..write('eAlfabetizado: $eAlfabetizado, ')
          ..write('completouEnsinoPrimario: $completouEnsinoPrimario, ')
          ..write('completouEnsinoSecundario: $completouEnsinoSecundario, ')
          ..write('fezCursoSuperior: $fezCursoSuperior, ')
          ..write('naoSabe: $naoSabe, ')
          ..write('chefeDeFamiliaTemSalario: $chefeDeFamiliaTemSalario, ')
          ..write(
              'quantoGastaParaSustentarFamiliaPorMes: $quantoGastaParaSustentarFamiliaPorMes, ')
          ..write(
              'quantasFamiliasResidemNacasa: $quantasFamiliasResidemNacasa, ')
          ..write('quantosAdultos: $quantosAdultos, ')
          ..write(
              'quantosAdultosMaioresDe18anos: $quantosAdultosMaioresDe18anos, ')
          ..write(
              'quantosAdultosMaioresDe18anosHomens: $quantosAdultosMaioresDe18anosHomens, ')
          ..write(
              'quantosAdultosMaioresDe18anosHomensTrabalham: $quantosAdultosMaioresDe18anosHomensTrabalham, ')
          ..write(
              'quantosAdultosMaioresDe18anosHomensAlfabetizados: $quantosAdultosMaioresDe18anosHomensAlfabetizados, ')
          ..write('quantosAdultosM18Mulheres: $quantosAdultosM18Mulheres, ')
          ..write(
              'quantosAdultosM18MulheresTrabalham: $quantosAdultosM18MulheresTrabalham, ')
          ..write(
              'quantosAdultosM18MulheresAlfabetizados: $quantosAdultosM18MulheresAlfabetizados, ')
          ..write('relatosDeDeficiencia: $relatosDeDeficiencia, ')
          ..write('totalcriancasAte1anoemeio: $totalcriancasAte1anoemeio, ')
          ..write(
              'totalcriancasAte1anoemeioMeninos: $totalcriancasAte1anoemeioMeninos, ')
          ..write(
              'totalcriancasAte1anoemeioMeninas: $totalcriancasAte1anoemeioMeninas, ')
          ..write(
              'algumasCriancasAte1anoemeioNaodesenvolvem: $algumasCriancasAte1anoemeioNaodesenvolvem, ')
          ..write(
              'algumasCriancasAte1anoemeioNaodesenvolvemQuantas: $algumasCriancasAte1anoemeioNaodesenvolvemQuantas, ')
          ..write(
              'algumaCriancasAte1anoemeioQueTipodeProblema: $algumaCriancasAte1anoemeioQueTipodeProblema, ')
          ..write(
              'algumaCriancasAte1anoemeioTemProblemaMotor: $algumaCriancasAte1anoemeioTemProblemaMotor, ')
          ..write(
              'algumaCriancasAte1anoemeioTemProblemaMotorQuantas: $algumaCriancasAte1anoemeioTemProblemaMotorQuantas, ')
          ..write(
              'algumaCriancasAte1anoemeioTemMalFormacao: $algumaCriancasAte1anoemeioTemMalFormacao, ')
          ..write(
              'algumaCriancasAte1anoemeioTemMalFormacaoQuantas: $algumaCriancasAte1anoemeioTemMalFormacaoQuantas, ')
          ..write(
              'algumaCriancasAte1anoemeioNaoReagemAIncentivo: $algumaCriancasAte1anoemeioNaoReagemAIncentivo, ')
          ..write(
              'algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas: $algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas, ')
          ..write(
              'algumaCriancasAte1anoemeioTemReacaoAgressiva: $algumaCriancasAte1anoemeioTemReacaoAgressiva, ')
          ..write(
              'algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas: $algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas, ')
          ..write(
              'totalcriancasAte1anoemeioA3anos: $totalcriancasAte1anoemeioA3anos, ')
          ..write(
              'totalcriancasAte1anoemeioA3anosMeninos: $totalcriancasAte1anoemeioA3anosMeninos, ')
          ..write(
              'totalcriancasAte1anoemeioA3anosMeninas: $totalcriancasAte1anoemeioA3anosMeninas, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosNaodesenvolvem: $algumasCriancasAte1anoemeioA3anosNaodesenvolvem, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas: $algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas, ')
          ..write(
              'algumaCriancasAte1anoemeioA3anosQueTipodeProblema: $algumaCriancasAte1anoemeioA3anosQueTipodeProblema, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas: $algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas: $algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo: $algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas: $algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosAlgunsAgridem: $algumasCriancasAte1anoemeioA3anosAlgunsAgridem, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas: $algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal: $algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas: $algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao: $algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas: $algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas: $algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas: $algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente: $algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas: $algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos: $algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas: $algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown: $algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas: $algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas, ')
          ..write('totalcriancasde3A6anos: $totalcriancasde3A6anos, ')
          ..write(
              'totalcriancasde3A6anosMeninos: $totalcriancasde3A6anosMeninos, ')
          ..write(
              'totalcriancasde3A6anosMeninas: $totalcriancasde3A6anosMeninas, ')
          ..write(
              'algumasCriancasde3A6anosNaodesenvolvem: $algumasCriancasde3A6anosNaodesenvolvem, ')
          ..write(
              'algumasCriancasde3A6anosNaodesenvolvemQuantas: $algumasCriancasde3A6anosNaodesenvolvemQuantas, ')
          ..write(
              'algumaCriancasde3A6anosQueTipodeProblema: $algumaCriancasde3A6anosQueTipodeProblema, ')
          ..write(
              'algumasCriancasde3A6anosNaoInteragemComPessoas: $algumasCriancasde3A6anosNaoInteragemComPessoas, ')
          ..write(
              'algumasCriancasde3A6anosNaoInteragemComPessoasQuantas: $algumasCriancasde3A6anosNaoInteragemComPessoasQuantas, ')
          ..write(
              'algumasCriancasde3A6anosTemComportamentoAgressivo: $algumasCriancasde3A6anosTemComportamentoAgressivo, ')
          ..write(
              'algumasCriancasde3A6anosTemComportamentoAgressivoQuantas: $algumasCriancasde3A6anosTemComportamentoAgressivoQuantas, ')
          ..write(
              'algumasCriancasde3A6anosAlgunsAgridem: $algumasCriancasde3A6anosAlgunsAgridem, ')
          ..write(
              'algumasCriancasde3A6anosAlgunsAgridemQuantas: $algumasCriancasde3A6anosAlgunsAgridemQuantas, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasAgitadasQueONormal: $algumasCriancasde3A6anosAlgumasAgitadasQueONormal, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas: $algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasTemMalFormacao: $algumasCriancasde3A6anosAlgumasTemMalFormacao, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas: $algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasNaoAndamSozinhas: $algumasCriancasde3A6anosAlgumasNaoAndamSozinhas, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas: $algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente: $algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas: $algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos: $algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas: $algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasTemSindromeDeDown: $algumasCriancasde3A6anosAlgumasTemSindromeDeDown, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas: $algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas: $algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas: $algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas: $algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas: $algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas: $algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas: $algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas: $algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas: $algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas, ')
          ..write(
              'quantasCriancasde3A6anosEstaoNaEscolinha: $quantasCriancasde3A6anosEstaoNaEscolinha, ')
          ..write(
              'criancasDe3A6anosNaoEstaoNaEscolinhaPorque: $criancasDe3A6anosNaoEstaoNaEscolinhaPorque, ')
          ..write(
              'quantasCriancasde3A6anosTrabalham: $quantasCriancasde3A6anosTrabalham, ')
          ..write(
              'criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos: $criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos, ')
          ..write('totalcriancasde7A9anos: $totalcriancasde7A9anos, ')
          ..write(
              'totalcriancasde7A9anosMeninos: $totalcriancasde7A9anosMeninos, ')
          ..write(
              'totalcriancasde7A9anosMeninas: $totalcriancasde7A9anosMeninas, ')
          ..write(
              'quantasCriancasde7A9anosEstaoNaEscola: $quantasCriancasde7A9anosEstaoNaEscola, ')
          ..write(
              'quantasCriancasde7A9anosTrabalham: $quantasCriancasde7A9anosTrabalham, ')
          ..write(
              'nosUltimos5AnosQuantasGravidezesTiveNaFamilia: $nosUltimos5AnosQuantasGravidezesTiveNaFamilia, ')
          ..write('partosOcoridosNoHospital: $partosOcoridosNoHospital, ')
          ..write('partosOcoridosEmCasa: $partosOcoridosEmCasa, ')
          ..write('partosOcoridosEmOutroLocal: $partosOcoridosEmOutroLocal, ')
          ..write('outroLocalDoPartoQualLocal: $outroLocalDoPartoQualLocal, ')
          ..write(
              'quantasCriancasNasceramVivas: $quantasCriancasNasceramVivas, ')
          ..write('quantasContinuamVivas: $quantasContinuamVivas, ')
          ..write(
              'dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram: $dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram, ')
          ..write('descreverOProblema: $descreverOProblema, ')
          ..write(
              'aFamiliaProcurouAjudaParaSuperarEsseProblema: $aFamiliaProcurouAjudaParaSuperarEsseProblema, ')
          ..write('seSimQueTipoDeAjudaEOnde: $seSimQueTipoDeAjudaEOnde, ')
          ..write(
              'osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia: $osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia, ')
          ..write(
              'dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida: $dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida, ')
          ..write('sabeDizerOMotivoDosObitos: $sabeDizerOMotivoDosObitos, ')
          ..write(
              'seSimQualFoiPrincipalRazaodoObito: $seSimQualFoiPrincipalRazaodoObito, ')
          ..write(
              'voceTemMedoDeTerUmFilhoOuNetoComProblemas: $voceTemMedoDeTerUmFilhoOuNetoComProblemas, ')
          ..write('expliquePorQue: $expliquePorQue, ')
          ..write(
              'porqueVoceAchaQueUmaCriancaPodeNascerComProblema: $porqueVoceAchaQueUmaCriancaPodeNascerComProblema, ')
          ..write(
              'oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema: $oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema, ')
          ..write(
              'achaQueUmaCriancaQueNasceComProblemaPodeSobreviver: $achaQueUmaCriancaQueNasceComProblemaPodeSobreviver, ')
          ..write('expliquePorque: $expliquePorque, ')
          ..write(
              'quemPodeAjudarQuandoUmaCriancaNasceComProblema: $quemPodeAjudarQuandoUmaCriancaNasceComProblema, ')
          ..write(
              'emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao: $emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao, ')
          ..write(
              'emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene: $emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene, ')
          ..write(
              'emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos: $emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos, ')
          ..write(
              'aPessoaEntrevistadaPareceuSinceraNasSuasRespostas: $aPessoaEntrevistadaPareceuSinceraNasSuasRespostas, ')
          ..write(
              'algumasPerguntasIncomodaramAPessoaEntrevistada: $algumasPerguntasIncomodaramAPessoaEntrevistada, ')
          ..write(
              'seSimQuais3PrincipaisPerguntas: $seSimQuais3PrincipaisPerguntas, ')
          ..write('todasAsPerguntasIncomodaram: $todasAsPerguntasIncomodaram, ')
          ..write('aResidenciaEraDe: $aResidenciaEraDe, ')
          ..write('comoAvaliaHigieneDaCasa: $comoAvaliaHigieneDaCasa, ')
          ..write('oQueEraMal: $oQueEraMal, ')
          ..write(
              'tinhaCriancasNaCasaDuranteInquerito: $tinhaCriancasNaCasaDuranteInquerito, ')
          ..write(
              'tinhaCriancasNaCasaDuranteInqueritoQuantas: $tinhaCriancasNaCasaDuranteInqueritoQuantas, ')
          ..write('tentouIncentivar: $tentouIncentivar, ')
          ..write('tentouIncentivarQuantas: $tentouIncentivarQuantas, ')
          ..write(
              'seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos: $seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos, ')
          ..write('observouCriancasDeAte1Ano: $observouCriancasDeAte1Ano, ')
          ..write(
              'observouCriancasDeAte1AnoQuantos: $observouCriancasDeAte1AnoQuantos, ')
          ..write(
              'algumasApresentavamProblemas: $algumasApresentavamProblemas, ')
          ..write(
              'algumasApresentavamProblemasQuantos: $algumasApresentavamProblemasQuantos, ')
          ..write(
              'descreveOProblemaDessasCriancas: $descreveOProblemaDessasCriancas, ')
          ..write(
              'observouCriancasDe1AnoEmeioAte3Anos: $observouCriancasDe1AnoEmeioAte3Anos, ')
          ..write(
              'observouCriancasDe1AnoEmeioAte3AnosQuantas: $observouCriancasDe1AnoEmeioAte3AnosQuantas, ')
          ..write(
              'algumasCriancasDe1anoEmeioObservadasApresentavamProblemas: $algumasCriancasDe1anoEmeioObservadasApresentavamProblemas, ')
          ..write(
              'algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas: $algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas, ')
          ..write(
              'algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve: $algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve, ')
          ..write(
              'observouCriancasDe3AnosAte6Anos: $observouCriancasDe3AnosAte6Anos, ')
          ..write(
              'observouCriancasDe3AnosAte6AnosQuantas: $observouCriancasDe3AnosAte6AnosQuantas, ')
          ..write(
              'observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas: $observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas, ')
          ..write(
              'observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas: $observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas, ')
          ..write(
              'observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve: $observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve, ')
          ..write(
              'observouSituacoesQueLheChamaramAtencao: $observouSituacoesQueLheChamaramAtencao, ')
          ..write(
              'emRelacaoAPessoaEntrevistada: $emRelacaoAPessoaEntrevistada, ')
          ..write(
              'emRelacaoAPessoaEntrevistadaDescreve: $emRelacaoAPessoaEntrevistadaDescreve, ')
          ..write(
              'emRelacaoAOutrosAdultosPresente: $emRelacaoAOutrosAdultosPresente, ')
          ..write(
              'emRelacaoAOutrosAdultosPresenteDescreve: $emRelacaoAOutrosAdultosPresenteDescreve, ')
          ..write(
              'emRelacaoAsCriancasPresentes: $emRelacaoAsCriancasPresentes, ')
          ..write(
              'emRelacaoAsCriancasPresentesDescreve: $emRelacaoAsCriancasPresentesDescreve, ')
          ..write('emRelacaoAVizinhanca: $emRelacaoAVizinhanca, ')
          ..write(
              'emRelacaoAVizinhancaDescreve: $emRelacaoAVizinhancaDescreve, ')
          ..write('outras: $outras, ')
          ..write('outrasDescreve: $outrasDescreve, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      uuid.hashCode,
      $mrjc(
          nomeDoPesquisador.hashCode,
          $mrjc(
              numeroDeInquerito.hashCode,
              $mrjc(
                  data.hashCode,
                  $mrjc(
                      municipio.hashCode,
                      $mrjc(
                          bairro.hashCode,
                          $mrjc(
                              pontoDeReferenciaDaCasa.hashCode,
                              $mrjc(
                                  nome.hashCode,
                                  $mrjc(
                                      idade.hashCode,
                                      $mrjc(
                                          contacto.hashCode,
                                          $mrjc(
                                              eChefeDaFamilia.hashCode,
                                              $mrjc(
                                                  caoNaoVinculoDeParantesco
                                                      .hashCode,
                                                  $mrjc(
                                                      idadeDoChefeDaFamilia
                                                          .hashCode,
                                                      $mrjc(
                                                          estudou.hashCode,
                                                          $mrjc(
                                                              eAlfabetizado
                                                                  .hashCode,
                                                              $mrjc(
                                                                  completouEnsinoPrimario
                                                                      .hashCode,
                                                                  $mrjc(
                                                                      completouEnsinoSecundario
                                                                          .hashCode,
                                                                      $mrjc(
                                                                          fezCursoSuperior
                                                                              .hashCode,
                                                                          $mrjc(
                                                                              naoSabe.hashCode,
                                                                              $mrjc(chefeDeFamiliaTemSalario.hashCode, $mrjc(quantoGastaParaSustentarFamiliaPorMes.hashCode, $mrjc(quantasFamiliasResidemNacasa.hashCode, $mrjc(quantosAdultos.hashCode, $mrjc(quantosAdultosMaioresDe18anos.hashCode, $mrjc(quantosAdultosMaioresDe18anosHomens.hashCode, $mrjc(quantosAdultosMaioresDe18anosHomensTrabalham.hashCode, $mrjc(quantosAdultosMaioresDe18anosHomensAlfabetizados.hashCode, $mrjc(quantosAdultosM18Mulheres.hashCode, $mrjc(quantosAdultosM18MulheresTrabalham.hashCode, $mrjc(quantosAdultosM18MulheresAlfabetizados.hashCode, $mrjc(relatosDeDeficiencia.hashCode, $mrjc(totalcriancasAte1anoemeio.hashCode, $mrjc(totalcriancasAte1anoemeioMeninos.hashCode, $mrjc(totalcriancasAte1anoemeioMeninas.hashCode, $mrjc(algumasCriancasAte1anoemeioNaodesenvolvem.hashCode, $mrjc(algumasCriancasAte1anoemeioNaodesenvolvemQuantas.hashCode, $mrjc(algumaCriancasAte1anoemeioQueTipodeProblema.hashCode, $mrjc(algumaCriancasAte1anoemeioTemProblemaMotor.hashCode, $mrjc(algumaCriancasAte1anoemeioTemProblemaMotorQuantas.hashCode, $mrjc(algumaCriancasAte1anoemeioTemMalFormacao.hashCode, $mrjc(algumaCriancasAte1anoemeioTemMalFormacaoQuantas.hashCode, $mrjc(algumaCriancasAte1anoemeioNaoReagemAIncentivo.hashCode, $mrjc(algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas.hashCode, $mrjc(algumaCriancasAte1anoemeioTemReacaoAgressiva.hashCode, $mrjc(algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas.hashCode, $mrjc(totalcriancasAte1anoemeioA3anos.hashCode, $mrjc(totalcriancasAte1anoemeioA3anosMeninos.hashCode, $mrjc(totalcriancasAte1anoemeioA3anosMeninas.hashCode, $mrjc(algumasCriancasAte1anoemeioA3anosNaodesenvolvem.hashCode, $mrjc(algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas.hashCode, $mrjc(algumaCriancasAte1anoemeioA3anosQueTipodeProblema.hashCode, $mrjc(algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas.hashCode, $mrjc(algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas.hashCode, $mrjc(algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo.hashCode, $mrjc(algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas.hashCode, $mrjc(algumasCriancasAte1anoemeioA3anosAlgunsAgridem.hashCode, $mrjc(algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas.hashCode, $mrjc(algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal.hashCode, $mrjc(algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas.hashCode, $mrjc(algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao.hashCode, $mrjc(algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas.hashCode, $mrjc(algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas.hashCode, $mrjc(algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas.hashCode, $mrjc(algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente.hashCode, $mrjc(algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas.hashCode, $mrjc(algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos.hashCode, $mrjc(algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas.hashCode, $mrjc(algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown.hashCode, $mrjc(algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas.hashCode, $mrjc(totalcriancasde3A6anos.hashCode, $mrjc(totalcriancasde3A6anosMeninos.hashCode, $mrjc(totalcriancasde3A6anosMeninas.hashCode, $mrjc(algumasCriancasde3A6anosNaodesenvolvem.hashCode, $mrjc(algumasCriancasde3A6anosNaodesenvolvemQuantas.hashCode, $mrjc(algumaCriancasde3A6anosQueTipodeProblema.hashCode, $mrjc(algumasCriancasde3A6anosNaoInteragemComPessoas.hashCode, $mrjc(algumasCriancasde3A6anosNaoInteragemComPessoasQuantas.hashCode, $mrjc(algumasCriancasde3A6anosTemComportamentoAgressivo.hashCode, $mrjc(algumasCriancasde3A6anosTemComportamentoAgressivoQuantas.hashCode, $mrjc(algumasCriancasde3A6anosAlgunsAgridem.hashCode, $mrjc(algumasCriancasde3A6anosAlgunsAgridemQuantas.hashCode, $mrjc(algumasCriancasde3A6anosAlgumasAgitadasQueONormal.hashCode, $mrjc(algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas.hashCode, $mrjc(algumasCriancasde3A6anosAlgumasTemMalFormacao.hashCode, $mrjc(algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas.hashCode, $mrjc(algumasCriancasde3A6anosAlgumasNaoAndamSozinhas.hashCode, $mrjc(algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas.hashCode, $mrjc(algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente.hashCode, $mrjc(algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas.hashCode, $mrjc(algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos.hashCode, $mrjc(algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas.hashCode, $mrjc(algumasCriancasde3A6anosAlgumasTemSindromeDeDown.hashCode, $mrjc(algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas.hashCode, $mrjc(algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas.hashCode, $mrjc(algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas.hashCode, $mrjc(algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas.hashCode, $mrjc(algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas.hashCode, $mrjc(algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas.hashCode, $mrjc(algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas.hashCode, $mrjc(algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas.hashCode, $mrjc(algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas.hashCode, $mrjc(quantasCriancasde3A6anosEstaoNaEscolinha.hashCode, $mrjc(criancasDe3A6anosNaoEstaoNaEscolinhaPorque.hashCode, $mrjc(quantasCriancasde3A6anosTrabalham.hashCode, $mrjc(criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos.hashCode, $mrjc(totalcriancasde7A9anos.hashCode, $mrjc(totalcriancasde7A9anosMeninos.hashCode, $mrjc(totalcriancasde7A9anosMeninas.hashCode, $mrjc(quantasCriancasde7A9anosEstaoNaEscola.hashCode, $mrjc(quantasCriancasde7A9anosTrabalham.hashCode, $mrjc(nosUltimos5AnosQuantasGravidezesTiveNaFamilia.hashCode, $mrjc(partosOcoridosNoHospital.hashCode, $mrjc(partosOcoridosEmCasa.hashCode, $mrjc(partosOcoridosEmOutroLocal.hashCode, $mrjc(outroLocalDoPartoQualLocal.hashCode, $mrjc(quantasCriancasNasceramVivas.hashCode, $mrjc(quantasContinuamVivas.hashCode, $mrjc(dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram.hashCode, $mrjc(descreverOProblema.hashCode, $mrjc(aFamiliaProcurouAjudaParaSuperarEsseProblema.hashCode, $mrjc(seSimQueTipoDeAjudaEOnde.hashCode, $mrjc(osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia.hashCode, $mrjc(dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida.hashCode, $mrjc(sabeDizerOMotivoDosObitos.hashCode, $mrjc(seSimQualFoiPrincipalRazaodoObito.hashCode, $mrjc(voceTemMedoDeTerUmFilhoOuNetoComProblemas.hashCode, $mrjc(expliquePorQue.hashCode, $mrjc(porqueVoceAchaQueUmaCriancaPodeNascerComProblema.hashCode, $mrjc(oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema.hashCode, $mrjc(achaQueUmaCriancaQueNasceComProblemaPodeSobreviver.hashCode, $mrjc(expliquePorque.hashCode, $mrjc(quemPodeAjudarQuandoUmaCriancaNasceComProblema.hashCode, $mrjc(emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao.hashCode, $mrjc(emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene.hashCode, $mrjc(emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos.hashCode, $mrjc(aPessoaEntrevistadaPareceuSinceraNasSuasRespostas.hashCode, $mrjc(algumasPerguntasIncomodaramAPessoaEntrevistada.hashCode, $mrjc(seSimQuais3PrincipaisPerguntas.hashCode, $mrjc(todasAsPerguntasIncomodaram.hashCode, $mrjc(aResidenciaEraDe.hashCode, $mrjc(comoAvaliaHigieneDaCasa.hashCode, $mrjc(oQueEraMal.hashCode, $mrjc(tinhaCriancasNaCasaDuranteInquerito.hashCode, $mrjc(tinhaCriancasNaCasaDuranteInqueritoQuantas.hashCode, $mrjc(tentouIncentivar.hashCode, $mrjc(tentouIncentivarQuantas.hashCode, $mrjc(seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos.hashCode, $mrjc(observouCriancasDeAte1Ano.hashCode, $mrjc(observouCriancasDeAte1AnoQuantos.hashCode, $mrjc(algumasApresentavamProblemas.hashCode, $mrjc(algumasApresentavamProblemasQuantos.hashCode, $mrjc(descreveOProblemaDessasCriancas.hashCode, $mrjc(observouCriancasDe1AnoEmeioAte3Anos.hashCode, $mrjc(observouCriancasDe1AnoEmeioAte3AnosQuantas.hashCode, $mrjc(algumasCriancasDe1anoEmeioObservadasApresentavamProblemas.hashCode, $mrjc(algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas.hashCode, $mrjc(algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve.hashCode, $mrjc(observouCriancasDe3AnosAte6Anos.hashCode, $mrjc(observouCriancasDe3AnosAte6AnosQuantas.hashCode, $mrjc(observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas.hashCode, $mrjc(observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas.hashCode, $mrjc(observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve.hashCode, $mrjc(observouSituacoesQueLheChamaramAtencao.hashCode, $mrjc(emRelacaoAPessoaEntrevistada.hashCode, $mrjc(emRelacaoAPessoaEntrevistadaDescreve.hashCode, $mrjc(emRelacaoAOutrosAdultosPresente.hashCode, $mrjc(emRelacaoAOutrosAdultosPresenteDescreve.hashCode, $mrjc(emRelacaoAsCriancasPresentes.hashCode, $mrjc(emRelacaoAsCriancasPresentesDescreve.hashCode, $mrjc(emRelacaoAVizinhanca.hashCode, $mrjc(emRelacaoAVizinhancaDescreve.hashCode, $mrjc(outras.hashCode, $mrjc(outrasDescreve.hashCode, $mrjc(createdAt.hashCode, updatedAt.hashCode)))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is User &&
          other.uuid == this.uuid &&
          other.nomeDoPesquisador == this.nomeDoPesquisador &&
          other.numeroDeInquerito == this.numeroDeInquerito &&
          other.data == this.data &&
          other.municipio == this.municipio &&
          other.bairro == this.bairro &&
          other.pontoDeReferenciaDaCasa == this.pontoDeReferenciaDaCasa &&
          other.nome == this.nome &&
          other.idade == this.idade &&
          other.contacto == this.contacto &&
          other.eChefeDaFamilia == this.eChefeDaFamilia &&
          other.caoNaoVinculoDeParantesco == this.caoNaoVinculoDeParantesco &&
          other.idadeDoChefeDaFamilia == this.idadeDoChefeDaFamilia &&
          other.estudou == this.estudou &&
          other.eAlfabetizado == this.eAlfabetizado &&
          other.completouEnsinoPrimario == this.completouEnsinoPrimario &&
          other.completouEnsinoSecundario == this.completouEnsinoSecundario &&
          other.fezCursoSuperior == this.fezCursoSuperior &&
          other.naoSabe == this.naoSabe &&
          other.chefeDeFamiliaTemSalario == this.chefeDeFamiliaTemSalario &&
          other.quantoGastaParaSustentarFamiliaPorMes ==
              this.quantoGastaParaSustentarFamiliaPorMes &&
          other.quantasFamiliasResidemNacasa ==
              this.quantasFamiliasResidemNacasa &&
          other.quantosAdultos == this.quantosAdultos &&
          other.quantosAdultosMaioresDe18anos ==
              this.quantosAdultosMaioresDe18anos &&
          other.quantosAdultosMaioresDe18anosHomens ==
              this.quantosAdultosMaioresDe18anosHomens &&
          other.quantosAdultosMaioresDe18anosHomensTrabalham ==
              this.quantosAdultosMaioresDe18anosHomensTrabalham &&
          other.quantosAdultosMaioresDe18anosHomensAlfabetizados ==
              this.quantosAdultosMaioresDe18anosHomensAlfabetizados &&
          other.quantosAdultosM18Mulheres == this.quantosAdultosM18Mulheres &&
          other.quantosAdultosM18MulheresTrabalham ==
              this.quantosAdultosM18MulheresTrabalham &&
          other.quantosAdultosM18MulheresAlfabetizados ==
              this.quantosAdultosM18MulheresAlfabetizados &&
          other.relatosDeDeficiencia == this.relatosDeDeficiencia &&
          other.totalcriancasAte1anoemeio == this.totalcriancasAte1anoemeio &&
          other.totalcriancasAte1anoemeioMeninos ==
              this.totalcriancasAte1anoemeioMeninos &&
          other.totalcriancasAte1anoemeioMeninas ==
              this.totalcriancasAte1anoemeioMeninas &&
          other.algumasCriancasAte1anoemeioNaodesenvolvem ==
              this.algumasCriancasAte1anoemeioNaodesenvolvem &&
          other.algumasCriancasAte1anoemeioNaodesenvolvemQuantas ==
              this.algumasCriancasAte1anoemeioNaodesenvolvemQuantas &&
          other.algumaCriancasAte1anoemeioQueTipodeProblema ==
              this.algumaCriancasAte1anoemeioQueTipodeProblema &&
          other.algumaCriancasAte1anoemeioTemProblemaMotor ==
              this.algumaCriancasAte1anoemeioTemProblemaMotor &&
          other.algumaCriancasAte1anoemeioTemProblemaMotorQuantas ==
              this.algumaCriancasAte1anoemeioTemProblemaMotorQuantas &&
          other.algumaCriancasAte1anoemeioTemMalFormacao ==
              this.algumaCriancasAte1anoemeioTemMalFormacao &&
          other.algumaCriancasAte1anoemeioTemMalFormacaoQuantas ==
              this.algumaCriancasAte1anoemeioTemMalFormacaoQuantas &&
          other.algumaCriancasAte1anoemeioNaoReagemAIncentivo ==
              this.algumaCriancasAte1anoemeioNaoReagemAIncentivo &&
          other.algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas ==
              this.algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas &&
          other.algumaCriancasAte1anoemeioTemReacaoAgressiva ==
              this.algumaCriancasAte1anoemeioTemReacaoAgressiva &&
          other.algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas ==
              this.algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas &&
          other.totalcriancasAte1anoemeioA3anos ==
              this.totalcriancasAte1anoemeioA3anos &&
          other.totalcriancasAte1anoemeioA3anosMeninos ==
              this.totalcriancasAte1anoemeioA3anosMeninos &&
          other.totalcriancasAte1anoemeioA3anosMeninas ==
              this.totalcriancasAte1anoemeioA3anosMeninas &&
          other.algumasCriancasAte1anoemeioA3anosNaodesenvolvem ==
              this.algumasCriancasAte1anoemeioA3anosNaodesenvolvem &&
          other.algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas ==
              this.algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas &&
          other.algumaCriancasAte1anoemeioA3anosQueTipodeProblema ==
              this.algumaCriancasAte1anoemeioA3anosQueTipodeProblema &&
          other.algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas ==
              this.algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas &&
          other.algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas ==
              this
                  .algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas &&
          other.algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo ==
              this.algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo &&
          other.algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas ==
              this
                  .algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas &&
          other.algumasCriancasAte1anoemeioA3anosAlgunsAgridem ==
              this.algumasCriancasAte1anoemeioA3anosAlgunsAgridem &&
          other.algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas ==
              this.algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas &&
          other.algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal ==
              this.algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal &&
          other.algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas ==
              this
                  .algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas &&
          other.algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao ==
              this.algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao &&
          other.algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas ==
              this
                  .algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas &&
          other.algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas ==
              this.algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas &&
          other.algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas ==
              this
                  .algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas &&
          other.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente ==
              this
                  .algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente &&
          other.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas ==
              this
                  .algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas &&
          other.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos ==
              this.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos &&
          other.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas == this.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas &&
          other.algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown == this.algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown &&
          other.algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas == this.algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas &&
          other.totalcriancasde3A6anos == this.totalcriancasde3A6anos &&
          other.totalcriancasde3A6anosMeninos == this.totalcriancasde3A6anosMeninos &&
          other.totalcriancasde3A6anosMeninas == this.totalcriancasde3A6anosMeninas &&
          other.algumasCriancasde3A6anosNaodesenvolvem == this.algumasCriancasde3A6anosNaodesenvolvem &&
          other.algumasCriancasde3A6anosNaodesenvolvemQuantas == this.algumasCriancasde3A6anosNaodesenvolvemQuantas &&
          other.algumaCriancasde3A6anosQueTipodeProblema == this.algumaCriancasde3A6anosQueTipodeProblema &&
          other.algumasCriancasde3A6anosNaoInteragemComPessoas == this.algumasCriancasde3A6anosNaoInteragemComPessoas &&
          other.algumasCriancasde3A6anosNaoInteragemComPessoasQuantas == this.algumasCriancasde3A6anosNaoInteragemComPessoasQuantas &&
          other.algumasCriancasde3A6anosTemComportamentoAgressivo == this.algumasCriancasde3A6anosTemComportamentoAgressivo &&
          other.algumasCriancasde3A6anosTemComportamentoAgressivoQuantas == this.algumasCriancasde3A6anosTemComportamentoAgressivoQuantas &&
          other.algumasCriancasde3A6anosAlgunsAgridem == this.algumasCriancasde3A6anosAlgunsAgridem &&
          other.algumasCriancasde3A6anosAlgunsAgridemQuantas == this.algumasCriancasde3A6anosAlgunsAgridemQuantas &&
          other.algumasCriancasde3A6anosAlgumasAgitadasQueONormal == this.algumasCriancasde3A6anosAlgumasAgitadasQueONormal &&
          other.algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas == this.algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas &&
          other.algumasCriancasde3A6anosAlgumasTemMalFormacao == this.algumasCriancasde3A6anosAlgumasTemMalFormacao &&
          other.algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas == this.algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas &&
          other.algumasCriancasde3A6anosAlgumasNaoAndamSozinhas == this.algumasCriancasde3A6anosAlgumasNaoAndamSozinhas &&
          other.algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas == this.algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas &&
          other.algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente == this.algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente &&
          other.algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas == this.algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas &&
          other.algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos == this.algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos &&
          other.algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas == this.algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas &&
          other.algumasCriancasde3A6anosAlgumasTemSindromeDeDown == this.algumasCriancasde3A6anosAlgumasTemSindromeDeDown &&
          other.algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas == this.algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas &&
          other.algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas == this.algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas &&
          other.algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas == this.algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas &&
          other.algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas == this.algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas &&
          other.algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas == this.algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas &&
          other.algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas == this.algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas &&
          other.algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas == this.algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas &&
          other.algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas == this.algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas &&
          other.algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas == this.algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas &&
          other.quantasCriancasde3A6anosEstaoNaEscolinha == this.quantasCriancasde3A6anosEstaoNaEscolinha &&
          other.criancasDe3A6anosNaoEstaoNaEscolinhaPorque == this.criancasDe3A6anosNaoEstaoNaEscolinhaPorque &&
          other.quantasCriancasde3A6anosTrabalham == this.quantasCriancasde3A6anosTrabalham &&
          other.criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos == this.criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos &&
          other.totalcriancasde7A9anos == this.totalcriancasde7A9anos &&
          other.totalcriancasde7A9anosMeninos == this.totalcriancasde7A9anosMeninos &&
          other.totalcriancasde7A9anosMeninas == this.totalcriancasde7A9anosMeninas &&
          other.quantasCriancasde7A9anosEstaoNaEscola == this.quantasCriancasde7A9anosEstaoNaEscola &&
          other.quantasCriancasde7A9anosTrabalham == this.quantasCriancasde7A9anosTrabalham &&
          other.nosUltimos5AnosQuantasGravidezesTiveNaFamilia == this.nosUltimos5AnosQuantasGravidezesTiveNaFamilia &&
          other.partosOcoridosNoHospital == this.partosOcoridosNoHospital &&
          other.partosOcoridosEmCasa == this.partosOcoridosEmCasa &&
          other.partosOcoridosEmOutroLocal == this.partosOcoridosEmOutroLocal &&
          other.outroLocalDoPartoQualLocal == this.outroLocalDoPartoQualLocal &&
          other.quantasCriancasNasceramVivas == this.quantasCriancasNasceramVivas &&
          other.quantasContinuamVivas == this.quantasContinuamVivas &&
          other.dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram == this.dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram &&
          other.descreverOProblema == this.descreverOProblema &&
          other.aFamiliaProcurouAjudaParaSuperarEsseProblema == this.aFamiliaProcurouAjudaParaSuperarEsseProblema &&
          other.seSimQueTipoDeAjudaEOnde == this.seSimQueTipoDeAjudaEOnde &&
          other.osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia == this.osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia &&
          other.dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida == this.dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida &&
          other.sabeDizerOMotivoDosObitos == this.sabeDizerOMotivoDosObitos &&
          other.seSimQualFoiPrincipalRazaodoObito == this.seSimQualFoiPrincipalRazaodoObito &&
          other.voceTemMedoDeTerUmFilhoOuNetoComProblemas == this.voceTemMedoDeTerUmFilhoOuNetoComProblemas &&
          other.expliquePorQue == this.expliquePorQue &&
          other.porqueVoceAchaQueUmaCriancaPodeNascerComProblema == this.porqueVoceAchaQueUmaCriancaPodeNascerComProblema &&
          other.oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema == this.oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema &&
          other.achaQueUmaCriancaQueNasceComProblemaPodeSobreviver == this.achaQueUmaCriancaQueNasceComProblemaPodeSobreviver &&
          other.expliquePorque == this.expliquePorque &&
          other.quemPodeAjudarQuandoUmaCriancaNasceComProblema == this.quemPodeAjudarQuandoUmaCriancaNasceComProblema &&
          other.emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao == this.emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao &&
          other.emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene == this.emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene &&
          other.emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos == this.emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos &&
          other.aPessoaEntrevistadaPareceuSinceraNasSuasRespostas == this.aPessoaEntrevistadaPareceuSinceraNasSuasRespostas &&
          other.algumasPerguntasIncomodaramAPessoaEntrevistada == this.algumasPerguntasIncomodaramAPessoaEntrevistada &&
          other.seSimQuais3PrincipaisPerguntas == this.seSimQuais3PrincipaisPerguntas &&
          other.todasAsPerguntasIncomodaram == this.todasAsPerguntasIncomodaram &&
          other.aResidenciaEraDe == this.aResidenciaEraDe &&
          other.comoAvaliaHigieneDaCasa == this.comoAvaliaHigieneDaCasa &&
          other.oQueEraMal == this.oQueEraMal &&
          other.tinhaCriancasNaCasaDuranteInquerito == this.tinhaCriancasNaCasaDuranteInquerito &&
          other.tinhaCriancasNaCasaDuranteInqueritoQuantas == this.tinhaCriancasNaCasaDuranteInqueritoQuantas &&
          other.tentouIncentivar == this.tentouIncentivar &&
          other.tentouIncentivarQuantas == this.tentouIncentivarQuantas &&
          other.seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos == this.seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos &&
          other.observouCriancasDeAte1Ano == this.observouCriancasDeAte1Ano &&
          other.observouCriancasDeAte1AnoQuantos == this.observouCriancasDeAte1AnoQuantos &&
          other.algumasApresentavamProblemas == this.algumasApresentavamProblemas &&
          other.algumasApresentavamProblemasQuantos == this.algumasApresentavamProblemasQuantos &&
          other.descreveOProblemaDessasCriancas == this.descreveOProblemaDessasCriancas &&
          other.observouCriancasDe1AnoEmeioAte3Anos == this.observouCriancasDe1AnoEmeioAte3Anos &&
          other.observouCriancasDe1AnoEmeioAte3AnosQuantas == this.observouCriancasDe1AnoEmeioAte3AnosQuantas &&
          other.algumasCriancasDe1anoEmeioObservadasApresentavamProblemas == this.algumasCriancasDe1anoEmeioObservadasApresentavamProblemas &&
          other.algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas == this.algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas &&
          other.algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve == this.algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve &&
          other.observouCriancasDe3AnosAte6Anos == this.observouCriancasDe3AnosAte6Anos &&
          other.observouCriancasDe3AnosAte6AnosQuantas == this.observouCriancasDe3AnosAte6AnosQuantas &&
          other.observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas == this.observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas &&
          other.observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas == this.observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas &&
          other.observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve == this.observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve &&
          other.observouSituacoesQueLheChamaramAtencao == this.observouSituacoesQueLheChamaramAtencao &&
          other.emRelacaoAPessoaEntrevistada == this.emRelacaoAPessoaEntrevistada &&
          other.emRelacaoAPessoaEntrevistadaDescreve == this.emRelacaoAPessoaEntrevistadaDescreve &&
          other.emRelacaoAOutrosAdultosPresente == this.emRelacaoAOutrosAdultosPresente &&
          other.emRelacaoAOutrosAdultosPresenteDescreve == this.emRelacaoAOutrosAdultosPresenteDescreve &&
          other.emRelacaoAsCriancasPresentes == this.emRelacaoAsCriancasPresentes &&
          other.emRelacaoAsCriancasPresentesDescreve == this.emRelacaoAsCriancasPresentesDescreve &&
          other.emRelacaoAVizinhanca == this.emRelacaoAVizinhanca &&
          other.emRelacaoAVizinhancaDescreve == this.emRelacaoAVizinhancaDescreve &&
          other.outras == this.outras &&
          other.outrasDescreve == this.outrasDescreve &&
          other.createdAt == this.createdAt &&
          other.updatedAt == this.updatedAt);
}

class UsersCompanion extends UpdateCompanion<User> {
  final Value<String> uuid;
  final Value<String> nomeDoPesquisador;
  final Value<String> numeroDeInquerito;
  final Value<DateTime> data;
  final Value<String> municipio;
  final Value<String> bairro;
  final Value<String> pontoDeReferenciaDaCasa;
  final Value<String> nome;
  final Value<String> idade;
  final Value<String> contacto;
  final Value<String> eChefeDaFamilia;
  final Value<String> caoNaoVinculoDeParantesco;
  final Value<String> idadeDoChefeDaFamilia;
  final Value<String> estudou;
  final Value<String> eAlfabetizado;
  final Value<String> completouEnsinoPrimario;
  final Value<String> completouEnsinoSecundario;
  final Value<String> fezCursoSuperior;
  final Value<String> naoSabe;
  final Value<String> chefeDeFamiliaTemSalario;
  final Value<String> quantoGastaParaSustentarFamiliaPorMes;
  final Value<String> quantasFamiliasResidemNacasa;
  final Value<String> quantosAdultos;
  final Value<String> quantosAdultosMaioresDe18anos;
  final Value<String> quantosAdultosMaioresDe18anosHomens;
  final Value<String> quantosAdultosMaioresDe18anosHomensTrabalham;
  final Value<String> quantosAdultosMaioresDe18anosHomensAlfabetizados;
  final Value<String> quantosAdultosM18Mulheres;
  final Value<String> quantosAdultosM18MulheresTrabalham;
  final Value<String> quantosAdultosM18MulheresAlfabetizados;
  final Value<String> relatosDeDeficiencia;
  final Value<String> totalcriancasAte1anoemeio;
  final Value<String> totalcriancasAte1anoemeioMeninos;
  final Value<String> totalcriancasAte1anoemeioMeninas;
  final Value<String> algumasCriancasAte1anoemeioNaodesenvolvem;
  final Value<String> algumasCriancasAte1anoemeioNaodesenvolvemQuantas;
  final Value<String> algumaCriancasAte1anoemeioQueTipodeProblema;
  final Value<String> algumaCriancasAte1anoemeioTemProblemaMotor;
  final Value<String> algumaCriancasAte1anoemeioTemProblemaMotorQuantas;
  final Value<String> algumaCriancasAte1anoemeioTemMalFormacao;
  final Value<String> algumaCriancasAte1anoemeioTemMalFormacaoQuantas;
  final Value<String> algumaCriancasAte1anoemeioNaoReagemAIncentivo;
  final Value<String> algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas;
  final Value<String> algumaCriancasAte1anoemeioTemReacaoAgressiva;
  final Value<String> algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas;
  final Value<String> totalcriancasAte1anoemeioA3anos;
  final Value<String> totalcriancasAte1anoemeioA3anosMeninos;
  final Value<String> totalcriancasAte1anoemeioA3anosMeninas;
  final Value<String> algumasCriancasAte1anoemeioA3anosNaodesenvolvem;
  final Value<String> algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas;
  final Value<String> algumaCriancasAte1anoemeioA3anosQueTipodeProblema;
  final Value<String> algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas;
  final Value<String>
      algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas;
  final Value<String>
      algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo;
  final Value<String>
      algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas;
  final Value<String> algumasCriancasAte1anoemeioA3anosAlgunsAgridem;
  final Value<String> algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas;
  final Value<String>
      algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal;
  final Value<String>
      algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas;
  final Value<String> algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao;
  final Value<String>
      algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas;
  final Value<String> algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas;
  final Value<String>
      algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas;
  final Value<String>
      algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente;
  final Value<String>
      algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas;
  final Value<String>
      algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos;
  final Value<String>
      algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas;
  final Value<String> algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown;
  final Value<String>
      algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas;
  final Value<String> totalcriancasde3A6anos;
  final Value<String> totalcriancasde3A6anosMeninos;
  final Value<String> totalcriancasde3A6anosMeninas;
  final Value<String> algumasCriancasde3A6anosNaodesenvolvem;
  final Value<String> algumasCriancasde3A6anosNaodesenvolvemQuantas;
  final Value<String> algumaCriancasde3A6anosQueTipodeProblema;
  final Value<String> algumasCriancasde3A6anosNaoInteragemComPessoas;
  final Value<String> algumasCriancasde3A6anosNaoInteragemComPessoasQuantas;
  final Value<String> algumasCriancasde3A6anosTemComportamentoAgressivo;
  final Value<String> algumasCriancasde3A6anosTemComportamentoAgressivoQuantas;
  final Value<String> algumasCriancasde3A6anosAlgunsAgridem;
  final Value<String> algumasCriancasde3A6anosAlgunsAgridemQuantas;
  final Value<String> algumasCriancasde3A6anosAlgumasAgitadasQueONormal;
  final Value<String> algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas;
  final Value<String> algumasCriancasde3A6anosAlgumasTemMalFormacao;
  final Value<String> algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas;
  final Value<String> algumasCriancasde3A6anosAlgumasNaoAndamSozinhas;
  final Value<String> algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas;
  final Value<String> algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente;
  final Value<String>
      algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas;
  final Value<String> algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos;
  final Value<String>
      algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas;
  final Value<String> algumasCriancasde3A6anosAlgumasTemSindromeDeDown;
  final Value<String> algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas;
  final Value<String>
      algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas;
  final Value<String>
      algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas;
  final Value<String>
      algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas;
  final Value<String>
      algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas;
  final Value<String>
      algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas;
  final Value<String>
      algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas;
  final Value<String>
      algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas;
  final Value<String>
      algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas;
  final Value<String> quantasCriancasde3A6anosEstaoNaEscolinha;
  final Value<String> criancasDe3A6anosNaoEstaoNaEscolinhaPorque;
  final Value<String> quantasCriancasde3A6anosTrabalham;
  final Value<String> criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos;
  final Value<String> totalcriancasde7A9anos;
  final Value<String> totalcriancasde7A9anosMeninos;
  final Value<String> totalcriancasde7A9anosMeninas;
  final Value<String> quantasCriancasde7A9anosEstaoNaEscola;
  final Value<String> quantasCriancasde7A9anosTrabalham;
  final Value<String> nosUltimos5AnosQuantasGravidezesTiveNaFamilia;
  final Value<String> partosOcoridosNoHospital;
  final Value<String> partosOcoridosEmCasa;
  final Value<String> partosOcoridosEmOutroLocal;
  final Value<String> outroLocalDoPartoQualLocal;
  final Value<String> quantasCriancasNasceramVivas;
  final Value<String> quantasContinuamVivas;
  final Value<String>
      dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram;
  final Value<String> descreverOProblema;
  final Value<String> aFamiliaProcurouAjudaParaSuperarEsseProblema;
  final Value<String> seSimQueTipoDeAjudaEOnde;
  final Value<String>
      osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia;
  final Value<String>
      dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida;
  final Value<String> sabeDizerOMotivoDosObitos;
  final Value<String> seSimQualFoiPrincipalRazaodoObito;
  final Value<String> voceTemMedoDeTerUmFilhoOuNetoComProblemas;
  final Value<String> expliquePorQue;
  final Value<String> porqueVoceAchaQueUmaCriancaPodeNascerComProblema;
  final Value<String> oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema;
  final Value<String> achaQueUmaCriancaQueNasceComProblemaPodeSobreviver;
  final Value<String> expliquePorque;
  final Value<String> quemPodeAjudarQuandoUmaCriancaNasceComProblema;
  final Value<String> emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao;
  final Value<String>
      emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene;
  final Value<String> emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos;
  final Value<String> aPessoaEntrevistadaPareceuSinceraNasSuasRespostas;
  final Value<String> algumasPerguntasIncomodaramAPessoaEntrevistada;
  final Value<String> seSimQuais3PrincipaisPerguntas;
  final Value<String> todasAsPerguntasIncomodaram;
  final Value<String> aResidenciaEraDe;
  final Value<String> comoAvaliaHigieneDaCasa;
  final Value<String> oQueEraMal;
  final Value<String> tinhaCriancasNaCasaDuranteInquerito;
  final Value<String> tinhaCriancasNaCasaDuranteInqueritoQuantas;
  final Value<String> tentouIncentivar;
  final Value<String> tentouIncentivarQuantas;
  final Value<String> seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos;
  final Value<String> observouCriancasDeAte1Ano;
  final Value<String> observouCriancasDeAte1AnoQuantos;
  final Value<String> algumasApresentavamProblemas;
  final Value<String> algumasApresentavamProblemasQuantos;
  final Value<String> descreveOProblemaDessasCriancas;
  final Value<String> observouCriancasDe1AnoEmeioAte3Anos;
  final Value<String> observouCriancasDe1AnoEmeioAte3AnosQuantas;
  final Value<String> algumasCriancasDe1anoEmeioObservadasApresentavamProblemas;
  final Value<String>
      algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas;
  final Value<String>
      algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve;
  final Value<String> observouCriancasDe3AnosAte6Anos;
  final Value<String> observouCriancasDe3AnosAte6AnosQuantas;
  final Value<String>
      observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas;
  final Value<String>
      observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas;
  final Value<String>
      observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve;
  final Value<String> observouSituacoesQueLheChamaramAtencao;
  final Value<String> emRelacaoAPessoaEntrevistada;
  final Value<String> emRelacaoAPessoaEntrevistadaDescreve;
  final Value<String> emRelacaoAOutrosAdultosPresente;
  final Value<String> emRelacaoAOutrosAdultosPresenteDescreve;
  final Value<String> emRelacaoAsCriancasPresentes;
  final Value<String> emRelacaoAsCriancasPresentesDescreve;
  final Value<String> emRelacaoAVizinhanca;
  final Value<String> emRelacaoAVizinhancaDescreve;
  final Value<String> outras;
  final Value<String> outrasDescreve;
  final Value<DateTime> createdAt;
  final Value<DateTime> updatedAt;
  const UsersCompanion({
    this.uuid = const Value.absent(),
    this.nomeDoPesquisador = const Value.absent(),
    this.numeroDeInquerito = const Value.absent(),
    this.data = const Value.absent(),
    this.municipio = const Value.absent(),
    this.bairro = const Value.absent(),
    this.pontoDeReferenciaDaCasa = const Value.absent(),
    this.nome = const Value.absent(),
    this.idade = const Value.absent(),
    this.contacto = const Value.absent(),
    this.eChefeDaFamilia = const Value.absent(),
    this.caoNaoVinculoDeParantesco = const Value.absent(),
    this.idadeDoChefeDaFamilia = const Value.absent(),
    this.estudou = const Value.absent(),
    this.eAlfabetizado = const Value.absent(),
    this.completouEnsinoPrimario = const Value.absent(),
    this.completouEnsinoSecundario = const Value.absent(),
    this.fezCursoSuperior = const Value.absent(),
    this.naoSabe = const Value.absent(),
    this.chefeDeFamiliaTemSalario = const Value.absent(),
    this.quantoGastaParaSustentarFamiliaPorMes = const Value.absent(),
    this.quantasFamiliasResidemNacasa = const Value.absent(),
    this.quantosAdultos = const Value.absent(),
    this.quantosAdultosMaioresDe18anos = const Value.absent(),
    this.quantosAdultosMaioresDe18anosHomens = const Value.absent(),
    this.quantosAdultosMaioresDe18anosHomensTrabalham = const Value.absent(),
    this.quantosAdultosMaioresDe18anosHomensAlfabetizados =
        const Value.absent(),
    this.quantosAdultosM18Mulheres = const Value.absent(),
    this.quantosAdultosM18MulheresTrabalham = const Value.absent(),
    this.quantosAdultosM18MulheresAlfabetizados = const Value.absent(),
    this.relatosDeDeficiencia = const Value.absent(),
    this.totalcriancasAte1anoemeio = const Value.absent(),
    this.totalcriancasAte1anoemeioMeninos = const Value.absent(),
    this.totalcriancasAte1anoemeioMeninas = const Value.absent(),
    this.algumasCriancasAte1anoemeioNaodesenvolvem = const Value.absent(),
    this.algumasCriancasAte1anoemeioNaodesenvolvemQuantas =
        const Value.absent(),
    this.algumaCriancasAte1anoemeioQueTipodeProblema = const Value.absent(),
    this.algumaCriancasAte1anoemeioTemProblemaMotor = const Value.absent(),
    this.algumaCriancasAte1anoemeioTemProblemaMotorQuantas =
        const Value.absent(),
    this.algumaCriancasAte1anoemeioTemMalFormacao = const Value.absent(),
    this.algumaCriancasAte1anoemeioTemMalFormacaoQuantas = const Value.absent(),
    this.algumaCriancasAte1anoemeioNaoReagemAIncentivo = const Value.absent(),
    this.algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas =
        const Value.absent(),
    this.algumaCriancasAte1anoemeioTemReacaoAgressiva = const Value.absent(),
    this.algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas =
        const Value.absent(),
    this.totalcriancasAte1anoemeioA3anos = const Value.absent(),
    this.totalcriancasAte1anoemeioA3anosMeninos = const Value.absent(),
    this.totalcriancasAte1anoemeioA3anosMeninas = const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosNaodesenvolvem = const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas =
        const Value.absent(),
    this.algumaCriancasAte1anoemeioA3anosQueTipodeProblema =
        const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas =
        const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas =
        const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo =
        const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas =
        const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosAlgunsAgridem = const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas =
        const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal =
        const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas =
        const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao =
        const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas =
        const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas =
        const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas =
        const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente =
        const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas =
        const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos =
        const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas =
        const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown =
        const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas =
        const Value.absent(),
    this.totalcriancasde3A6anos = const Value.absent(),
    this.totalcriancasde3A6anosMeninos = const Value.absent(),
    this.totalcriancasde3A6anosMeninas = const Value.absent(),
    this.algumasCriancasde3A6anosNaodesenvolvem = const Value.absent(),
    this.algumasCriancasde3A6anosNaodesenvolvemQuantas = const Value.absent(),
    this.algumaCriancasde3A6anosQueTipodeProblema = const Value.absent(),
    this.algumasCriancasde3A6anosNaoInteragemComPessoas = const Value.absent(),
    this.algumasCriancasde3A6anosNaoInteragemComPessoasQuantas =
        const Value.absent(),
    this.algumasCriancasde3A6anosTemComportamentoAgressivo =
        const Value.absent(),
    this.algumasCriancasde3A6anosTemComportamentoAgressivoQuantas =
        const Value.absent(),
    this.algumasCriancasde3A6anosAlgunsAgridem = const Value.absent(),
    this.algumasCriancasde3A6anosAlgunsAgridemQuantas = const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasAgitadasQueONormal =
        const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas =
        const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasTemMalFormacao = const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas =
        const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasNaoAndamSozinhas = const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas =
        const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente =
        const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas =
        const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos =
        const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas =
        const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasTemSindromeDeDown =
        const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas =
        const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas =
        const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas =
        const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas =
        const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas =
        const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas =
        const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas =
        const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas =
        const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas =
        const Value.absent(),
    this.quantasCriancasde3A6anosEstaoNaEscolinha = const Value.absent(),
    this.criancasDe3A6anosNaoEstaoNaEscolinhaPorque = const Value.absent(),
    this.quantasCriancasde3A6anosTrabalham = const Value.absent(),
    this.criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos =
        const Value.absent(),
    this.totalcriancasde7A9anos = const Value.absent(),
    this.totalcriancasde7A9anosMeninos = const Value.absent(),
    this.totalcriancasde7A9anosMeninas = const Value.absent(),
    this.quantasCriancasde7A9anosEstaoNaEscola = const Value.absent(),
    this.quantasCriancasde7A9anosTrabalham = const Value.absent(),
    this.nosUltimos5AnosQuantasGravidezesTiveNaFamilia = const Value.absent(),
    this.partosOcoridosNoHospital = const Value.absent(),
    this.partosOcoridosEmCasa = const Value.absent(),
    this.partosOcoridosEmOutroLocal = const Value.absent(),
    this.outroLocalDoPartoQualLocal = const Value.absent(),
    this.quantasCriancasNasceramVivas = const Value.absent(),
    this.quantasContinuamVivas = const Value.absent(),
    this.dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram =
        const Value.absent(),
    this.descreverOProblema = const Value.absent(),
    this.aFamiliaProcurouAjudaParaSuperarEsseProblema = const Value.absent(),
    this.seSimQueTipoDeAjudaEOnde = const Value.absent(),
    this.osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia =
        const Value.absent(),
    this.dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida =
        const Value.absent(),
    this.sabeDizerOMotivoDosObitos = const Value.absent(),
    this.seSimQualFoiPrincipalRazaodoObito = const Value.absent(),
    this.voceTemMedoDeTerUmFilhoOuNetoComProblemas = const Value.absent(),
    this.expliquePorQue = const Value.absent(),
    this.porqueVoceAchaQueUmaCriancaPodeNascerComProblema =
        const Value.absent(),
    this.oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema =
        const Value.absent(),
    this.achaQueUmaCriancaQueNasceComProblemaPodeSobreviver =
        const Value.absent(),
    this.expliquePorque = const Value.absent(),
    this.quemPodeAjudarQuandoUmaCriancaNasceComProblema = const Value.absent(),
    this.emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao =
        const Value.absent(),
    this.emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene =
        const Value.absent(),
    this.emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos =
        const Value.absent(),
    this.aPessoaEntrevistadaPareceuSinceraNasSuasRespostas =
        const Value.absent(),
    this.algumasPerguntasIncomodaramAPessoaEntrevistada = const Value.absent(),
    this.seSimQuais3PrincipaisPerguntas = const Value.absent(),
    this.todasAsPerguntasIncomodaram = const Value.absent(),
    this.aResidenciaEraDe = const Value.absent(),
    this.comoAvaliaHigieneDaCasa = const Value.absent(),
    this.oQueEraMal = const Value.absent(),
    this.tinhaCriancasNaCasaDuranteInquerito = const Value.absent(),
    this.tinhaCriancasNaCasaDuranteInqueritoQuantas = const Value.absent(),
    this.tentouIncentivar = const Value.absent(),
    this.tentouIncentivarQuantas = const Value.absent(),
    this.seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos =
        const Value.absent(),
    this.observouCriancasDeAte1Ano = const Value.absent(),
    this.observouCriancasDeAte1AnoQuantos = const Value.absent(),
    this.algumasApresentavamProblemas = const Value.absent(),
    this.algumasApresentavamProblemasQuantos = const Value.absent(),
    this.descreveOProblemaDessasCriancas = const Value.absent(),
    this.observouCriancasDe1AnoEmeioAte3Anos = const Value.absent(),
    this.observouCriancasDe1AnoEmeioAte3AnosQuantas = const Value.absent(),
    this.algumasCriancasDe1anoEmeioObservadasApresentavamProblemas =
        const Value.absent(),
    this.algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas =
        const Value.absent(),
    this.algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve =
        const Value.absent(),
    this.observouCriancasDe3AnosAte6Anos = const Value.absent(),
    this.observouCriancasDe3AnosAte6AnosQuantas = const Value.absent(),
    this.observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas =
        const Value.absent(),
    this.observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas =
        const Value.absent(),
    this.observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve =
        const Value.absent(),
    this.observouSituacoesQueLheChamaramAtencao = const Value.absent(),
    this.emRelacaoAPessoaEntrevistada = const Value.absent(),
    this.emRelacaoAPessoaEntrevistadaDescreve = const Value.absent(),
    this.emRelacaoAOutrosAdultosPresente = const Value.absent(),
    this.emRelacaoAOutrosAdultosPresenteDescreve = const Value.absent(),
    this.emRelacaoAsCriancasPresentes = const Value.absent(),
    this.emRelacaoAsCriancasPresentesDescreve = const Value.absent(),
    this.emRelacaoAVizinhanca = const Value.absent(),
    this.emRelacaoAVizinhancaDescreve = const Value.absent(),
    this.outras = const Value.absent(),
    this.outrasDescreve = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.updatedAt = const Value.absent(),
  });
  UsersCompanion.insert({
    this.uuid = const Value.absent(),
    this.nomeDoPesquisador = const Value.absent(),
    this.numeroDeInquerito = const Value.absent(),
    this.data = const Value.absent(),
    this.municipio = const Value.absent(),
    this.bairro = const Value.absent(),
    this.pontoDeReferenciaDaCasa = const Value.absent(),
    this.nome = const Value.absent(),
    this.idade = const Value.absent(),
    this.contacto = const Value.absent(),
    this.eChefeDaFamilia = const Value.absent(),
    this.caoNaoVinculoDeParantesco = const Value.absent(),
    this.idadeDoChefeDaFamilia = const Value.absent(),
    this.estudou = const Value.absent(),
    this.eAlfabetizado = const Value.absent(),
    this.completouEnsinoPrimario = const Value.absent(),
    this.completouEnsinoSecundario = const Value.absent(),
    this.fezCursoSuperior = const Value.absent(),
    this.naoSabe = const Value.absent(),
    this.chefeDeFamiliaTemSalario = const Value.absent(),
    this.quantoGastaParaSustentarFamiliaPorMes = const Value.absent(),
    this.quantasFamiliasResidemNacasa = const Value.absent(),
    this.quantosAdultos = const Value.absent(),
    this.quantosAdultosMaioresDe18anos = const Value.absent(),
    this.quantosAdultosMaioresDe18anosHomens = const Value.absent(),
    this.quantosAdultosMaioresDe18anosHomensTrabalham = const Value.absent(),
    this.quantosAdultosMaioresDe18anosHomensAlfabetizados =
        const Value.absent(),
    this.quantosAdultosM18Mulheres = const Value.absent(),
    this.quantosAdultosM18MulheresTrabalham = const Value.absent(),
    this.quantosAdultosM18MulheresAlfabetizados = const Value.absent(),
    this.relatosDeDeficiencia = const Value.absent(),
    this.totalcriancasAte1anoemeio = const Value.absent(),
    this.totalcriancasAte1anoemeioMeninos = const Value.absent(),
    this.totalcriancasAte1anoemeioMeninas = const Value.absent(),
    this.algumasCriancasAte1anoemeioNaodesenvolvem = const Value.absent(),
    this.algumasCriancasAte1anoemeioNaodesenvolvemQuantas =
        const Value.absent(),
    this.algumaCriancasAte1anoemeioQueTipodeProblema = const Value.absent(),
    this.algumaCriancasAte1anoemeioTemProblemaMotor = const Value.absent(),
    this.algumaCriancasAte1anoemeioTemProblemaMotorQuantas =
        const Value.absent(),
    this.algumaCriancasAte1anoemeioTemMalFormacao = const Value.absent(),
    this.algumaCriancasAte1anoemeioTemMalFormacaoQuantas = const Value.absent(),
    this.algumaCriancasAte1anoemeioNaoReagemAIncentivo = const Value.absent(),
    this.algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas =
        const Value.absent(),
    this.algumaCriancasAte1anoemeioTemReacaoAgressiva = const Value.absent(),
    this.algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas =
        const Value.absent(),
    this.totalcriancasAte1anoemeioA3anos = const Value.absent(),
    this.totalcriancasAte1anoemeioA3anosMeninos = const Value.absent(),
    this.totalcriancasAte1anoemeioA3anosMeninas = const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosNaodesenvolvem = const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas =
        const Value.absent(),
    this.algumaCriancasAte1anoemeioA3anosQueTipodeProblema =
        const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas =
        const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas =
        const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo =
        const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas =
        const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosAlgunsAgridem = const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas =
        const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal =
        const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas =
        const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao =
        const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas =
        const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas =
        const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas =
        const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente =
        const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas =
        const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos =
        const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas =
        const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown =
        const Value.absent(),
    this.algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas =
        const Value.absent(),
    this.totalcriancasde3A6anos = const Value.absent(),
    this.totalcriancasde3A6anosMeninos = const Value.absent(),
    this.totalcriancasde3A6anosMeninas = const Value.absent(),
    this.algumasCriancasde3A6anosNaodesenvolvem = const Value.absent(),
    this.algumasCriancasde3A6anosNaodesenvolvemQuantas = const Value.absent(),
    this.algumaCriancasde3A6anosQueTipodeProblema = const Value.absent(),
    this.algumasCriancasde3A6anosNaoInteragemComPessoas = const Value.absent(),
    this.algumasCriancasde3A6anosNaoInteragemComPessoasQuantas =
        const Value.absent(),
    this.algumasCriancasde3A6anosTemComportamentoAgressivo =
        const Value.absent(),
    this.algumasCriancasde3A6anosTemComportamentoAgressivoQuantas =
        const Value.absent(),
    this.algumasCriancasde3A6anosAlgunsAgridem = const Value.absent(),
    this.algumasCriancasde3A6anosAlgunsAgridemQuantas = const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasAgitadasQueONormal =
        const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas =
        const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasTemMalFormacao = const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas =
        const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasNaoAndamSozinhas = const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas =
        const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente =
        const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas =
        const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos =
        const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas =
        const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasTemSindromeDeDown =
        const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas =
        const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas =
        const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas =
        const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas =
        const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas =
        const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas =
        const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas =
        const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas =
        const Value.absent(),
    this.algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas =
        const Value.absent(),
    this.quantasCriancasde3A6anosEstaoNaEscolinha = const Value.absent(),
    this.criancasDe3A6anosNaoEstaoNaEscolinhaPorque = const Value.absent(),
    this.quantasCriancasde3A6anosTrabalham = const Value.absent(),
    this.criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos =
        const Value.absent(),
    this.totalcriancasde7A9anos = const Value.absent(),
    this.totalcriancasde7A9anosMeninos = const Value.absent(),
    this.totalcriancasde7A9anosMeninas = const Value.absent(),
    this.quantasCriancasde7A9anosEstaoNaEscola = const Value.absent(),
    this.quantasCriancasde7A9anosTrabalham = const Value.absent(),
    this.nosUltimos5AnosQuantasGravidezesTiveNaFamilia = const Value.absent(),
    this.partosOcoridosNoHospital = const Value.absent(),
    this.partosOcoridosEmCasa = const Value.absent(),
    this.partosOcoridosEmOutroLocal = const Value.absent(),
    this.outroLocalDoPartoQualLocal = const Value.absent(),
    this.quantasCriancasNasceramVivas = const Value.absent(),
    this.quantasContinuamVivas = const Value.absent(),
    this.dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram =
        const Value.absent(),
    this.descreverOProblema = const Value.absent(),
    this.aFamiliaProcurouAjudaParaSuperarEsseProblema = const Value.absent(),
    this.seSimQueTipoDeAjudaEOnde = const Value.absent(),
    this.osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia =
        const Value.absent(),
    this.dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida =
        const Value.absent(),
    this.sabeDizerOMotivoDosObitos = const Value.absent(),
    this.seSimQualFoiPrincipalRazaodoObito = const Value.absent(),
    this.voceTemMedoDeTerUmFilhoOuNetoComProblemas = const Value.absent(),
    this.expliquePorQue = const Value.absent(),
    this.porqueVoceAchaQueUmaCriancaPodeNascerComProblema =
        const Value.absent(),
    this.oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema =
        const Value.absent(),
    this.achaQueUmaCriancaQueNasceComProblemaPodeSobreviver =
        const Value.absent(),
    this.expliquePorque = const Value.absent(),
    this.quemPodeAjudarQuandoUmaCriancaNasceComProblema = const Value.absent(),
    this.emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao =
        const Value.absent(),
    this.emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene =
        const Value.absent(),
    this.emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos =
        const Value.absent(),
    this.aPessoaEntrevistadaPareceuSinceraNasSuasRespostas =
        const Value.absent(),
    this.algumasPerguntasIncomodaramAPessoaEntrevistada = const Value.absent(),
    this.seSimQuais3PrincipaisPerguntas = const Value.absent(),
    this.todasAsPerguntasIncomodaram = const Value.absent(),
    this.aResidenciaEraDe = const Value.absent(),
    this.comoAvaliaHigieneDaCasa = const Value.absent(),
    this.oQueEraMal = const Value.absent(),
    this.tinhaCriancasNaCasaDuranteInquerito = const Value.absent(),
    this.tinhaCriancasNaCasaDuranteInqueritoQuantas = const Value.absent(),
    this.tentouIncentivar = const Value.absent(),
    this.tentouIncentivarQuantas = const Value.absent(),
    this.seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos =
        const Value.absent(),
    this.observouCriancasDeAte1Ano = const Value.absent(),
    this.observouCriancasDeAte1AnoQuantos = const Value.absent(),
    this.algumasApresentavamProblemas = const Value.absent(),
    this.algumasApresentavamProblemasQuantos = const Value.absent(),
    this.descreveOProblemaDessasCriancas = const Value.absent(),
    this.observouCriancasDe1AnoEmeioAte3Anos = const Value.absent(),
    this.observouCriancasDe1AnoEmeioAte3AnosQuantas = const Value.absent(),
    this.algumasCriancasDe1anoEmeioObservadasApresentavamProblemas =
        const Value.absent(),
    this.algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas =
        const Value.absent(),
    this.algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve =
        const Value.absent(),
    this.observouCriancasDe3AnosAte6Anos = const Value.absent(),
    this.observouCriancasDe3AnosAte6AnosQuantas = const Value.absent(),
    this.observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas =
        const Value.absent(),
    this.observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas =
        const Value.absent(),
    this.observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve =
        const Value.absent(),
    this.observouSituacoesQueLheChamaramAtencao = const Value.absent(),
    this.emRelacaoAPessoaEntrevistada = const Value.absent(),
    this.emRelacaoAPessoaEntrevistadaDescreve = const Value.absent(),
    this.emRelacaoAOutrosAdultosPresente = const Value.absent(),
    this.emRelacaoAOutrosAdultosPresenteDescreve = const Value.absent(),
    this.emRelacaoAsCriancasPresentes = const Value.absent(),
    this.emRelacaoAsCriancasPresentesDescreve = const Value.absent(),
    this.emRelacaoAVizinhanca = const Value.absent(),
    this.emRelacaoAVizinhancaDescreve = const Value.absent(),
    this.outras = const Value.absent(),
    this.outrasDescreve = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.updatedAt = const Value.absent(),
  });
  static Insertable<User> custom({
    Expression<String> uuid,
    Expression<String> nomeDoPesquisador,
    Expression<String> numeroDeInquerito,
    Expression<DateTime> data,
    Expression<String> municipio,
    Expression<String> bairro,
    Expression<String> pontoDeReferenciaDaCasa,
    Expression<String> nome,
    Expression<String> idade,
    Expression<String> contacto,
    Expression<String> eChefeDaFamilia,
    Expression<String> caoNaoVinculoDeParantesco,
    Expression<String> idadeDoChefeDaFamilia,
    Expression<String> estudou,
    Expression<String> eAlfabetizado,
    Expression<String> completouEnsinoPrimario,
    Expression<String> completouEnsinoSecundario,
    Expression<String> fezCursoSuperior,
    Expression<String> naoSabe,
    Expression<String> chefeDeFamiliaTemSalario,
    Expression<String> quantoGastaParaSustentarFamiliaPorMes,
    Expression<String> quantasFamiliasResidemNacasa,
    Expression<String> quantosAdultos,
    Expression<String> quantosAdultosMaioresDe18anos,
    Expression<String> quantosAdultosMaioresDe18anosHomens,
    Expression<String> quantosAdultosMaioresDe18anosHomensTrabalham,
    Expression<String> quantosAdultosMaioresDe18anosHomensAlfabetizados,
    Expression<String> quantosAdultosM18Mulheres,
    Expression<String> quantosAdultosM18MulheresTrabalham,
    Expression<String> quantosAdultosM18MulheresAlfabetizados,
    Expression<String> relatosDeDeficiencia,
    Expression<String> totalcriancasAte1anoemeio,
    Expression<String> totalcriancasAte1anoemeioMeninos,
    Expression<String> totalcriancasAte1anoemeioMeninas,
    Expression<String> algumasCriancasAte1anoemeioNaodesenvolvem,
    Expression<String> algumasCriancasAte1anoemeioNaodesenvolvemQuantas,
    Expression<String> algumaCriancasAte1anoemeioQueTipodeProblema,
    Expression<String> algumaCriancasAte1anoemeioTemProblemaMotor,
    Expression<String> algumaCriancasAte1anoemeioTemProblemaMotorQuantas,
    Expression<String> algumaCriancasAte1anoemeioTemMalFormacao,
    Expression<String> algumaCriancasAte1anoemeioTemMalFormacaoQuantas,
    Expression<String> algumaCriancasAte1anoemeioNaoReagemAIncentivo,
    Expression<String> algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas,
    Expression<String> algumaCriancasAte1anoemeioTemReacaoAgressiva,
    Expression<String> algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas,
    Expression<String> totalcriancasAte1anoemeioA3anos,
    Expression<String> totalcriancasAte1anoemeioA3anosMeninos,
    Expression<String> totalcriancasAte1anoemeioA3anosMeninas,
    Expression<String> algumasCriancasAte1anoemeioA3anosNaodesenvolvem,
    Expression<String> algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas,
    Expression<String> algumaCriancasAte1anoemeioA3anosQueTipodeProblema,
    Expression<String> algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas,
    Expression<String>
        algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas,
    Expression<String>
        algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo,
    Expression<String>
        algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas,
    Expression<String> algumasCriancasAte1anoemeioA3anosAlgunsAgridem,
    Expression<String> algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas,
    Expression<String>
        algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal,
    Expression<String>
        algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas,
    Expression<String> algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao,
    Expression<String>
        algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas,
    Expression<String> algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas,
    Expression<String>
        algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas,
    Expression<String>
        algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente,
    Expression<String>
        algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas,
    Expression<String>
        algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos,
    Expression<String>
        algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas,
    Expression<String>
        algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown,
    Expression<String>
        algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas,
    Expression<String> totalcriancasde3A6anos,
    Expression<String> totalcriancasde3A6anosMeninos,
    Expression<String> totalcriancasde3A6anosMeninas,
    Expression<String> algumasCriancasde3A6anosNaodesenvolvem,
    Expression<String> algumasCriancasde3A6anosNaodesenvolvemQuantas,
    Expression<String> algumaCriancasde3A6anosQueTipodeProblema,
    Expression<String> algumasCriancasde3A6anosNaoInteragemComPessoas,
    Expression<String> algumasCriancasde3A6anosNaoInteragemComPessoasQuantas,
    Expression<String> algumasCriancasde3A6anosTemComportamentoAgressivo,
    Expression<String> algumasCriancasde3A6anosTemComportamentoAgressivoQuantas,
    Expression<String> algumasCriancasde3A6anosAlgunsAgridem,
    Expression<String> algumasCriancasde3A6anosAlgunsAgridemQuantas,
    Expression<String> algumasCriancasde3A6anosAlgumasAgitadasQueONormal,
    Expression<String> algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas,
    Expression<String> algumasCriancasde3A6anosAlgumasTemMalFormacao,
    Expression<String> algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas,
    Expression<String> algumasCriancasde3A6anosAlgumasNaoAndamSozinhas,
    Expression<String> algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas,
    Expression<String> algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente,
    Expression<String>
        algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas,
    Expression<String> algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos,
    Expression<String>
        algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas,
    Expression<String> algumasCriancasde3A6anosAlgumasTemSindromeDeDown,
    Expression<String> algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas,
    Expression<String>
        algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas,
    Expression<String>
        algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas,
    Expression<String>
        algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas,
    Expression<String>
        algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas,
    Expression<String>
        algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas,
    Expression<String>
        algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas,
    Expression<String>
        algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas,
    Expression<String>
        algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas,
    Expression<String> quantasCriancasde3A6anosEstaoNaEscolinha,
    Expression<String> criancasDe3A6anosNaoEstaoNaEscolinhaPorque,
    Expression<String> quantasCriancasde3A6anosTrabalham,
    Expression<String> criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos,
    Expression<String> totalcriancasde7A9anos,
    Expression<String> totalcriancasde7A9anosMeninos,
    Expression<String> totalcriancasde7A9anosMeninas,
    Expression<String> quantasCriancasde7A9anosEstaoNaEscola,
    Expression<String> quantasCriancasde7A9anosTrabalham,
    Expression<String> nosUltimos5AnosQuantasGravidezesTiveNaFamilia,
    Expression<String> partosOcoridosNoHospital,
    Expression<String> partosOcoridosEmCasa,
    Expression<String> partosOcoridosEmOutroLocal,
    Expression<String> outroLocalDoPartoQualLocal,
    Expression<String> quantasCriancasNasceramVivas,
    Expression<String> quantasContinuamVivas,
    Expression<String>
        dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram,
    Expression<String> descreverOProblema,
    Expression<String> aFamiliaProcurouAjudaParaSuperarEsseProblema,
    Expression<String> seSimQueTipoDeAjudaEOnde,
    Expression<String>
        osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia,
    Expression<String>
        dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida,
    Expression<String> sabeDizerOMotivoDosObitos,
    Expression<String> seSimQualFoiPrincipalRazaodoObito,
    Expression<String> voceTemMedoDeTerUmFilhoOuNetoComProblemas,
    Expression<String> expliquePorQue,
    Expression<String> porqueVoceAchaQueUmaCriancaPodeNascerComProblema,
    Expression<String> oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema,
    Expression<String> achaQueUmaCriancaQueNasceComProblemaPodeSobreviver,
    Expression<String> expliquePorque,
    Expression<String> quemPodeAjudarQuandoUmaCriancaNasceComProblema,
    Expression<String> emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao,
    Expression<String>
        emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene,
    Expression<String> emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos,
    Expression<String> aPessoaEntrevistadaPareceuSinceraNasSuasRespostas,
    Expression<String> algumasPerguntasIncomodaramAPessoaEntrevistada,
    Expression<String> seSimQuais3PrincipaisPerguntas,
    Expression<String> todasAsPerguntasIncomodaram,
    Expression<String> aResidenciaEraDe,
    Expression<String> comoAvaliaHigieneDaCasa,
    Expression<String> oQueEraMal,
    Expression<String> tinhaCriancasNaCasaDuranteInquerito,
    Expression<String> tinhaCriancasNaCasaDuranteInqueritoQuantas,
    Expression<String> tentouIncentivar,
    Expression<String> tentouIncentivarQuantas,
    Expression<String>
        seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos,
    Expression<String> observouCriancasDeAte1Ano,
    Expression<String> observouCriancasDeAte1AnoQuantos,
    Expression<String> algumasApresentavamProblemas,
    Expression<String> algumasApresentavamProblemasQuantos,
    Expression<String> descreveOProblemaDessasCriancas,
    Expression<String> observouCriancasDe1AnoEmeioAte3Anos,
    Expression<String> observouCriancasDe1AnoEmeioAte3AnosQuantas,
    Expression<String>
        algumasCriancasDe1anoEmeioObservadasApresentavamProblemas,
    Expression<String>
        algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas,
    Expression<String>
        algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve,
    Expression<String> observouCriancasDe3AnosAte6Anos,
    Expression<String> observouCriancasDe3AnosAte6AnosQuantas,
    Expression<String>
        observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas,
    Expression<String>
        observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas,
    Expression<String>
        observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve,
    Expression<String> observouSituacoesQueLheChamaramAtencao,
    Expression<String> emRelacaoAPessoaEntrevistada,
    Expression<String> emRelacaoAPessoaEntrevistadaDescreve,
    Expression<String> emRelacaoAOutrosAdultosPresente,
    Expression<String> emRelacaoAOutrosAdultosPresenteDescreve,
    Expression<String> emRelacaoAsCriancasPresentes,
    Expression<String> emRelacaoAsCriancasPresentesDescreve,
    Expression<String> emRelacaoAVizinhanca,
    Expression<String> emRelacaoAVizinhancaDescreve,
    Expression<String> outras,
    Expression<String> outrasDescreve,
    Expression<DateTime> createdAt,
    Expression<DateTime> updatedAt,
  }) {
    return RawValuesInsertable({
      if (uuid != null) 'uuid': uuid,
      if (nomeDoPesquisador != null) 'nome_do_pesquisador': nomeDoPesquisador,
      if (numeroDeInquerito != null) 'numero_de_inquerito': numeroDeInquerito,
      if (data != null) 'data': data,
      if (municipio != null) 'municipio': municipio,
      if (bairro != null) 'bairro': bairro,
      if (pontoDeReferenciaDaCasa != null)
        'ponto_de_referencia_da_casa': pontoDeReferenciaDaCasa,
      if (nome != null) 'nome': nome,
      if (idade != null) 'idade': idade,
      if (contacto != null) 'contacto': contacto,
      if (eChefeDaFamilia != null) 'e_chefe_da_familia': eChefeDaFamilia,
      if (caoNaoVinculoDeParantesco != null)
        'cao_nao_vinculo_de_parantesco': caoNaoVinculoDeParantesco,
      if (idadeDoChefeDaFamilia != null)
        'idade_do_chefe_da_familia': idadeDoChefeDaFamilia,
      if (estudou != null) 'estudou': estudou,
      if (eAlfabetizado != null) 'e_alfabetizado': eAlfabetizado,
      if (completouEnsinoPrimario != null)
        'completou_ensino_primario': completouEnsinoPrimario,
      if (completouEnsinoSecundario != null)
        'completou_ensino_secundario': completouEnsinoSecundario,
      if (fezCursoSuperior != null) 'fez_curso_superior': fezCursoSuperior,
      if (naoSabe != null) 'nao_sabe': naoSabe,
      if (chefeDeFamiliaTemSalario != null)
        'chefe_de_familia_tem_salario': chefeDeFamiliaTemSalario,
      if (quantoGastaParaSustentarFamiliaPorMes != null)
        'quanto_gasta_para_sustentar_familia_por_mes':
            quantoGastaParaSustentarFamiliaPorMes,
      if (quantasFamiliasResidemNacasa != null)
        'quantas_familias_residem_nacasa': quantasFamiliasResidemNacasa,
      if (quantosAdultos != null) 'quantos_adultos': quantosAdultos,
      if (quantosAdultosMaioresDe18anos != null)
        'quantos_adultos_maiores_de18anos': quantosAdultosMaioresDe18anos,
      if (quantosAdultosMaioresDe18anosHomens != null)
        'quantos_adultos_maiores_de18anos_homens':
            quantosAdultosMaioresDe18anosHomens,
      if (quantosAdultosMaioresDe18anosHomensTrabalham != null)
        'quantos_adultos_maiores_de18anos_homens_trabalham':
            quantosAdultosMaioresDe18anosHomensTrabalham,
      if (quantosAdultosMaioresDe18anosHomensAlfabetizados != null)
        'quantos_adultos_maiores_de18anos_homens_alfabetizados':
            quantosAdultosMaioresDe18anosHomensAlfabetizados,
      if (quantosAdultosM18Mulheres != null)
        'quantos_adultos_m18_mulheres': quantosAdultosM18Mulheres,
      if (quantosAdultosM18MulheresTrabalham != null)
        'quantos_adultos_m18_mulheres_trabalham':
            quantosAdultosM18MulheresTrabalham,
      if (quantosAdultosM18MulheresAlfabetizados != null)
        'quantos_adultos_m18_mulheres_alfabetizados':
            quantosAdultosM18MulheresAlfabetizados,
      if (relatosDeDeficiencia != null)
        'relatos_de_deficiencia': relatosDeDeficiencia,
      if (totalcriancasAte1anoemeio != null)
        'totalcriancas_ate1anoemeio': totalcriancasAte1anoemeio,
      if (totalcriancasAte1anoemeioMeninos != null)
        'totalcriancas_ate1anoemeio_meninos': totalcriancasAte1anoemeioMeninos,
      if (totalcriancasAte1anoemeioMeninas != null)
        'totalcriancas_ate1anoemeio_meninas': totalcriancasAte1anoemeioMeninas,
      if (algumasCriancasAte1anoemeioNaodesenvolvem != null)
        'algumas_criancas_ate1anoemeio_naodesenvolvem':
            algumasCriancasAte1anoemeioNaodesenvolvem,
      if (algumasCriancasAte1anoemeioNaodesenvolvemQuantas != null)
        'algumas_criancas_ate1anoemeio_naodesenvolvem_quantas':
            algumasCriancasAte1anoemeioNaodesenvolvemQuantas,
      if (algumaCriancasAte1anoemeioQueTipodeProblema != null)
        'alguma_criancas_ate1anoemeio_que_tipode_problema':
            algumaCriancasAte1anoemeioQueTipodeProblema,
      if (algumaCriancasAte1anoemeioTemProblemaMotor != null)
        'alguma_criancas_ate1anoemeio_tem_problema_motor':
            algumaCriancasAte1anoemeioTemProblemaMotor,
      if (algumaCriancasAte1anoemeioTemProblemaMotorQuantas != null)
        'alguma_criancas_ate1anoemeio_tem_problema_motor_quantas':
            algumaCriancasAte1anoemeioTemProblemaMotorQuantas,
      if (algumaCriancasAte1anoemeioTemMalFormacao != null)
        'alguma_criancas_ate1anoemeio_tem_mal_formacao':
            algumaCriancasAte1anoemeioTemMalFormacao,
      if (algumaCriancasAte1anoemeioTemMalFormacaoQuantas != null)
        'alguma_criancas_ate1anoemeio_tem_mal_formacao_quantas':
            algumaCriancasAte1anoemeioTemMalFormacaoQuantas,
      if (algumaCriancasAte1anoemeioNaoReagemAIncentivo != null)
        'alguma_criancas_ate1anoemeio_nao_reagem_a_incentivo':
            algumaCriancasAte1anoemeioNaoReagemAIncentivo,
      if (algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas != null)
        'alguma_criancas_ate1anoemeio_nao_reagem_a_incentivo_quantas':
            algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas,
      if (algumaCriancasAte1anoemeioTemReacaoAgressiva != null)
        'alguma_criancas_ate1anoemeio_tem_reacao_agressiva':
            algumaCriancasAte1anoemeioTemReacaoAgressiva,
      if (algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas != null)
        'alguma_criancas_ate1anoemeio_tem_reacao_agressiva_quantas':
            algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas,
      if (totalcriancasAte1anoemeioA3anos != null)
        'totalcriancas_ate1anoemeio_a3anos': totalcriancasAte1anoemeioA3anos,
      if (totalcriancasAte1anoemeioA3anosMeninos != null)
        'totalcriancas_ate1anoemeio_a3anos_meninos':
            totalcriancasAte1anoemeioA3anosMeninos,
      if (totalcriancasAte1anoemeioA3anosMeninas != null)
        'totalcriancas_ate1anoemeio_a3anos_meninas':
            totalcriancasAte1anoemeioA3anosMeninas,
      if (algumasCriancasAte1anoemeioA3anosNaodesenvolvem != null)
        'algumas_criancas_ate1anoemeio_a3anos_naodesenvolvem':
            algumasCriancasAte1anoemeioA3anosNaodesenvolvem,
      if (algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas != null)
        'algumas_criancas_ate1anoemeio_a3anos_naodesenvolvem_quantas':
            algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas,
      if (algumaCriancasAte1anoemeioA3anosQueTipodeProblema != null)
        'alguma_criancas_ate1anoemeio_a3anos_que_tipode_problema':
            algumaCriancasAte1anoemeioA3anosQueTipodeProblema,
      if (algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas != null)
        'algumas_criancas_ate1anoemeio_a3anos_nao_interagem_com_pessoas':
            algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas,
      if (algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas !=
          null)
        'algumas_criancas_ate1anoemeio_a3anos_nao_interagem_com_pessoas_quantas':
            algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas,
      if (algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo != null)
        'algumas_criancas_ate1anoemeio_a3anos_tem_comportamento_agressivo':
            algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo,
      if (algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas !=
          null)
        'algumas_criancas_ate1anoemeio_a3anos_tem_comportamento_agressivo_quantas':
            algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas,
      if (algumasCriancasAte1anoemeioA3anosAlgunsAgridem != null)
        'algumas_criancas_ate1anoemeio_a3anos_alguns_agridem':
            algumasCriancasAte1anoemeioA3anosAlgunsAgridem,
      if (algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas != null)
        'algumas_criancas_ate1anoemeio_a3anos_alguns_agridem_quantas':
            algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas,
      if (algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal != null)
        'algumas_criancas_ate1anoemeio_a3anos_algumas_agitadas_que_o_normal':
            algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal,
      if (algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas !=
          null)
        'algumas_criancas_ate1anoemeio_a3anos_algumas_agitadas_que_o_normal_quantas':
            algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas,
      if (algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao != null)
        'algumas_criancas_ate1anoemeio_a3anos_algumas_tem_mal_formacao':
            algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao,
      if (algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas != null)
        'algumas_criancas_ate1anoemeio_a3anos_algumas_tem_mal_formacao_quantas':
            algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas,
      if (algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas != null)
        'algumas_criancas_ate1anoemeio_a3anos_algumas_nao_andam_sozinhas':
            algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas,
      if (algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas !=
          null)
        'algumas_criancas_ate1anoemeio_a3anos_algumas_nao_andam_sozinhas_quantas':
            algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas,
      if (algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente !=
          null)
        'algumas_criancas_ate1anoemeio_a3anos_algumas_nao_expressam_verbalmente':
            algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente,
      if (algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas !=
          null)
        'algumas_criancas_ate1anoemeio_a3anos_algumas_nao_expressam_verbalmente_quantas':
            algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas,
      if (algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos !=
          null)
        'algumas_criancas_ate1anoemeio_a3anos_algumas_nao_expressam_sentimentos':
            algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos,
      if (algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas !=
          null)
        'algumas_criancas_ate1anoemeio_a3anos_algumas_nao_expressam_sentimentos_quantas':
            algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas,
      if (algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown != null)
        'algumas_criancas_ate1anoemeio_a3anos_algumas_tem_sindrome_de_down':
            algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown,
      if (algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas !=
          null)
        'algumas_criancas_ate1anoemeio_a3anos_algumas_tem_sindrome_de_down_quantas':
            algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas,
      if (totalcriancasde3A6anos != null)
        'totalcriancasde3_a6anos': totalcriancasde3A6anos,
      if (totalcriancasde3A6anosMeninos != null)
        'totalcriancasde3_a6anos_meninos': totalcriancasde3A6anosMeninos,
      if (totalcriancasde3A6anosMeninas != null)
        'totalcriancasde3_a6anos_meninas': totalcriancasde3A6anosMeninas,
      if (algumasCriancasde3A6anosNaodesenvolvem != null)
        'algumas_criancasde3_a6anos_naodesenvolvem':
            algumasCriancasde3A6anosNaodesenvolvem,
      if (algumasCriancasde3A6anosNaodesenvolvemQuantas != null)
        'algumas_criancasde3_a6anos_naodesenvolvem_quantas':
            algumasCriancasde3A6anosNaodesenvolvemQuantas,
      if (algumaCriancasde3A6anosQueTipodeProblema != null)
        'alguma_criancasde3_a6anos_que_tipode_problema':
            algumaCriancasde3A6anosQueTipodeProblema,
      if (algumasCriancasde3A6anosNaoInteragemComPessoas != null)
        'algumas_criancasde3_a6anos_nao_interagem_com_pessoas':
            algumasCriancasde3A6anosNaoInteragemComPessoas,
      if (algumasCriancasde3A6anosNaoInteragemComPessoasQuantas != null)
        'algumas_criancasde3_a6anos_nao_interagem_com_pessoas_quantas':
            algumasCriancasde3A6anosNaoInteragemComPessoasQuantas,
      if (algumasCriancasde3A6anosTemComportamentoAgressivo != null)
        'algumas_criancasde3_a6anos_tem_comportamento_agressivo':
            algumasCriancasde3A6anosTemComportamentoAgressivo,
      if (algumasCriancasde3A6anosTemComportamentoAgressivoQuantas != null)
        'algumas_criancasde3_a6anos_tem_comportamento_agressivo_quantas':
            algumasCriancasde3A6anosTemComportamentoAgressivoQuantas,
      if (algumasCriancasde3A6anosAlgunsAgridem != null)
        'algumas_criancasde3_a6anos_alguns_agridem':
            algumasCriancasde3A6anosAlgunsAgridem,
      if (algumasCriancasde3A6anosAlgunsAgridemQuantas != null)
        'algumas_criancasde3_a6anos_alguns_agridem_quantas':
            algumasCriancasde3A6anosAlgunsAgridemQuantas,
      if (algumasCriancasde3A6anosAlgumasAgitadasQueONormal != null)
        'algumas_criancasde3_a6anos_algumas_agitadas_que_o_normal':
            algumasCriancasde3A6anosAlgumasAgitadasQueONormal,
      if (algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas != null)
        'algumas_criancasde3_a6anos_algumas_agitadas_que_o_normal_quantas':
            algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas,
      if (algumasCriancasde3A6anosAlgumasTemMalFormacao != null)
        'algumas_criancasde3_a6anos_algumas_tem_mal_formacao':
            algumasCriancasde3A6anosAlgumasTemMalFormacao,
      if (algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas != null)
        'algumas_criancasde3_a6anos_algumas_tem_mal_formacao_quantas':
            algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas,
      if (algumasCriancasde3A6anosAlgumasNaoAndamSozinhas != null)
        'algumas_criancasde3_a6anos_algumas_nao_andam_sozinhas':
            algumasCriancasde3A6anosAlgumasNaoAndamSozinhas,
      if (algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas != null)
        'algumas_criancasde3_a6anos_algumas_nao_andam_sozinhas_quantas':
            algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas,
      if (algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente != null)
        'algumas_criancasde3_a6anos_algumas_nao_expressam_verbalmente':
            algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente,
      if (algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas != null)
        'algumas_criancasde3_a6anos_algumas_nao_expressam_verbalmente_quantas':
            algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas,
      if (algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos != null)
        'algumas_criancasde3_a6anos_algumas_nao_expressam_sentimentos':
            algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos,
      if (algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas != null)
        'algumas_criancasde3_a6anos_algumas_nao_expressam_sentimentos_quantas':
            algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas,
      if (algumasCriancasde3A6anosAlgumasTemSindromeDeDown != null)
        'algumas_criancasde3_a6anos_algumas_tem_sindrome_de_down':
            algumasCriancasde3A6anosAlgumasTemSindromeDeDown,
      if (algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas != null)
        'algumas_criancasde3_a6anos_algumas_tem_sindrome_de_down_quantas':
            algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas,
      if (algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas != null)
        'algumas_criancasde3_a6anos_algumas_nao_sabem_se_alimentar_sozinhas':
            algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas,
      if (algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas !=
          null)
        'algumas_criancasde3_a6anos_algumas_nao_sabe_se_alimentar_sozinhas_quantas':
            algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas,
      if (algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas != null)
        'algumas_criancasde3_a6anos_algumas_nao_conseguem_tomar_banho_sozinhas':
            algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas,
      if (algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas !=
          null)
        'algumas_criancasde3_a6anos_algumas_nao_conseguem_tomar_banho_sozinhas_quantas':
            algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas,
      if (algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas !=
          null)
        'algumas_criancasde3_a6anos_algumas_nao_conseguem_ir_a_casa_de_banho_sozinhas':
            algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas,
      if (algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas !=
          null)
        'algumas_criancasde3_a6anos_algumas_nao_conseguem_ir_a_casa_de_banho_sozinhas_quantas':
            algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas,
      if (algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas != null)
        'algumas_criancasde3_a6anos_algumas_nao_conseguem_se_vestir_sozinhas':
            algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas,
      if (algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas !=
          null)
        'algumas_criancasde3_a6anos_algumas_nao_conseguem_se_vestir_sozinhas_quantas':
            algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas,
      if (quantasCriancasde3A6anosEstaoNaEscolinha != null)
        'quantas_criancasde3_a6anos_estao_na_escolinha':
            quantasCriancasde3A6anosEstaoNaEscolinha,
      if (criancasDe3A6anosNaoEstaoNaEscolinhaPorque != null)
        'criancas_de3_a6anos_nao_estao_na_escolinha_porque':
            criancasDe3A6anosNaoEstaoNaEscolinhaPorque,
      if (quantasCriancasde3A6anosTrabalham != null)
        'quantas_criancasde3_a6anos_trabalham':
            quantasCriancasde3A6anosTrabalham,
      if (criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos != null)
        'criancas_de3_a6anos_nao_estao_na_escolinha_por_outos_motivos':
            criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos,
      if (totalcriancasde7A9anos != null)
        'totalcriancasde7_a9anos': totalcriancasde7A9anos,
      if (totalcriancasde7A9anosMeninos != null)
        'totalcriancasde7_a9anos_meninos': totalcriancasde7A9anosMeninos,
      if (totalcriancasde7A9anosMeninas != null)
        'totalcriancasde7_a9anos_meninas': totalcriancasde7A9anosMeninas,
      if (quantasCriancasde7A9anosEstaoNaEscola != null)
        'quantas_criancasde7_a9anos_estao_na_escola':
            quantasCriancasde7A9anosEstaoNaEscola,
      if (quantasCriancasde7A9anosTrabalham != null)
        'quantas_criancasde7_a9anos_trabalham':
            quantasCriancasde7A9anosTrabalham,
      if (nosUltimos5AnosQuantasGravidezesTiveNaFamilia != null)
        'nos_ultimos5_anos_quantas_gravidezes_tive_na_familia':
            nosUltimos5AnosQuantasGravidezesTiveNaFamilia,
      if (partosOcoridosNoHospital != null)
        'partos_ocoridos_no_hospital': partosOcoridosNoHospital,
      if (partosOcoridosEmCasa != null)
        'partos_ocoridos_em_casa': partosOcoridosEmCasa,
      if (partosOcoridosEmOutroLocal != null)
        'partos_ocoridos_em_outro_local': partosOcoridosEmOutroLocal,
      if (outroLocalDoPartoQualLocal != null)
        'outro_local_do_parto_qual_local': outroLocalDoPartoQualLocal,
      if (quantasCriancasNasceramVivas != null)
        'quantas_criancas_nasceram_vivas': quantasCriancasNasceramVivas,
      if (quantasContinuamVivas != null)
        'quantas_continuam_vivas': quantasContinuamVivas,
      if (dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram !=
          null)
        'das_vivas_algumas_apresentam_problemas_de_desenvolvimento_que_outros_nao_tiveram':
            dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram,
      if (descreverOProblema != null)
        'descrever_o_problema': descreverOProblema,
      if (aFamiliaProcurouAjudaParaSuperarEsseProblema != null)
        'a_familia_procurou_ajuda_para_superar_esse_problema':
            aFamiliaProcurouAjudaParaSuperarEsseProblema,
      if (seSimQueTipoDeAjudaEOnde != null)
        'se_sim_que_tipo_de_ajuda_e_onde': seSimQueTipoDeAjudaEOnde,
      if (osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia !=
          null)
        'os_vizinhos_se_preocuparam_quando_nasceu_uma_crianca_com_problema_na_familia':
            osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia,
      if (dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida !=
          null)
        'das_criancas_que_nasceram_vivas_quantas_nao_sobreviveram_nos2_primeiros_anos_de_vida':
            dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida,
      if (sabeDizerOMotivoDosObitos != null)
        'sabe_dizer_o_motivo_dos_obitos': sabeDizerOMotivoDosObitos,
      if (seSimQualFoiPrincipalRazaodoObito != null)
        'se_sim_qual_foi_principal_razaodo_obito':
            seSimQualFoiPrincipalRazaodoObito,
      if (voceTemMedoDeTerUmFilhoOuNetoComProblemas != null)
        'voce_tem_medo_de_ter_um_filho_ou_neto_com_problemas':
            voceTemMedoDeTerUmFilhoOuNetoComProblemas,
      if (expliquePorQue != null) 'explique_por_que': expliquePorQue,
      if (porqueVoceAchaQueUmaCriancaPodeNascerComProblema != null)
        'porque_voce_acha_que_uma_crianca_pode_nascer_com_problema':
            porqueVoceAchaQueUmaCriancaPodeNascerComProblema,
      if (oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema != null)
        'o_que_acontece_no_seu_bairro_quando_nasce_uma_crianca_com_problema':
            oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema,
      if (achaQueUmaCriancaQueNasceComProblemaPodeSobreviver != null)
        'acha_que_uma_crianca_que_nasce_com_problema_pode_sobreviver':
            achaQueUmaCriancaQueNasceComProblemaPodeSobreviver,
      if (expliquePorque != null) 'explique_porque': expliquePorque,
      if (quemPodeAjudarQuandoUmaCriancaNasceComProblema != null)
        'quem_pode_ajudar_quando_uma_crianca_nasce_com_problema':
            quemPodeAjudarQuandoUmaCriancaNasceComProblema,
      if (emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao != null)
        'em_casa_quem_cuida_mais_das_criancas_ate6_anos_para_alimentacao':
            emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao,
      if (emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene != null)
        'em_casa_quem_cuida_mais_das_criancas_ate6_anos_para_os_cuidados_de_higiene':
            emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene,
      if (emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos != null)
        'em_casa_quem_cuida_mais_das_criancas_ate6_anos_para_os_estudos':
            emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos,
      if (aPessoaEntrevistadaPareceuSinceraNasSuasRespostas != null)
        'a_pessoa_entrevistada_pareceu_sincera_nas_suas_respostas':
            aPessoaEntrevistadaPareceuSinceraNasSuasRespostas,
      if (algumasPerguntasIncomodaramAPessoaEntrevistada != null)
        'algumas_perguntas_incomodaram_a_pessoa_entrevistada':
            algumasPerguntasIncomodaramAPessoaEntrevistada,
      if (seSimQuais3PrincipaisPerguntas != null)
        'se_sim_quais3_principais_perguntas': seSimQuais3PrincipaisPerguntas,
      if (todasAsPerguntasIncomodaram != null)
        'todas_as_perguntas_incomodaram': todasAsPerguntasIncomodaram,
      if (aResidenciaEraDe != null) 'a_residencia_era_de': aResidenciaEraDe,
      if (comoAvaliaHigieneDaCasa != null)
        'como_avalia_higiene_da_casa': comoAvaliaHigieneDaCasa,
      if (oQueEraMal != null) 'o_que_era_mal': oQueEraMal,
      if (tinhaCriancasNaCasaDuranteInquerito != null)
        'tinha_criancas_na_casa_durante_inquerito':
            tinhaCriancasNaCasaDuranteInquerito,
      if (tinhaCriancasNaCasaDuranteInqueritoQuantas != null)
        'tinha_criancas_na_casa_durante_inquerito_quantas':
            tinhaCriancasNaCasaDuranteInqueritoQuantas,
      if (tentouIncentivar != null) 'tentou_incentivar': tentouIncentivar,
      if (tentouIncentivarQuantas != null)
        'tentou_incentivar_quantas': tentouIncentivarQuantas,
      if (seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos != null)
        'se_incentivou_quantas_reagiram_positivamente_aos_seus_incentivos':
            seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos,
      if (observouCriancasDeAte1Ano != null)
        'observou_criancas_de_ate1_ano': observouCriancasDeAte1Ano,
      if (observouCriancasDeAte1AnoQuantos != null)
        'observou_criancas_de_ate1_ano_quantos':
            observouCriancasDeAte1AnoQuantos,
      if (algumasApresentavamProblemas != null)
        'algumas_apresentavam_problemas': algumasApresentavamProblemas,
      if (algumasApresentavamProblemasQuantos != null)
        'algumas_apresentavam_problemas_quantos':
            algumasApresentavamProblemasQuantos,
      if (descreveOProblemaDessasCriancas != null)
        'descreve_o_problema_dessas_criancas': descreveOProblemaDessasCriancas,
      if (observouCriancasDe1AnoEmeioAte3Anos != null)
        'observou_criancas_de1_ano_emeio_ate3_anos':
            observouCriancasDe1AnoEmeioAte3Anos,
      if (observouCriancasDe1AnoEmeioAte3AnosQuantas != null)
        'observou_criancas_de1_ano_emeio_ate3_anos_quantas':
            observouCriancasDe1AnoEmeioAte3AnosQuantas,
      if (algumasCriancasDe1anoEmeioObservadasApresentavamProblemas != null)
        'algumas_criancas_de1ano_emeio_observadas_apresentavam_problemas':
            algumasCriancasDe1anoEmeioObservadasApresentavamProblemas,
      if (algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas !=
          null)
        'algumas_criancas_de1ano_emeio_observadas_apresentavam_problemas_quantas':
            algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas,
      if (algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve !=
          null)
        'algumas_criancas_de1ano_emeio_observadas_apresentavam_problemas_descreve':
            algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve,
      if (observouCriancasDe3AnosAte6Anos != null)
        'observou_criancas_de3_anos_ate6_anos': observouCriancasDe3AnosAte6Anos,
      if (observouCriancasDe3AnosAte6AnosQuantas != null)
        'observou_criancas_de3_anos_ate6_anos_quantas':
            observouCriancasDe3AnosAte6AnosQuantas,
      if (observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas !=
          null)
        'observou_criancas_de3_anos_ate6_anos_que_algumas_apresentavam_problemas':
            observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas,
      if (observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas !=
          null)
        'observou_criancas_de3_anos_ate6_anos_que_algumas_apresentavam_problemas_quantas':
            observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas,
      if (observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve !=
          null)
        'observou_criancas_de3_anos_ate6_anos_que_algumas_apresentavam_problemas_descreve':
            observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve,
      if (observouSituacoesQueLheChamaramAtencao != null)
        'observou_situacoes_que_lhe_chamaram_atencao':
            observouSituacoesQueLheChamaramAtencao,
      if (emRelacaoAPessoaEntrevistada != null)
        'em_relacao_a_pessoa_entrevistada': emRelacaoAPessoaEntrevistada,
      if (emRelacaoAPessoaEntrevistadaDescreve != null)
        'em_relacao_a_pessoa_entrevistada_descreve':
            emRelacaoAPessoaEntrevistadaDescreve,
      if (emRelacaoAOutrosAdultosPresente != null)
        'em_relacao_a_outros_adultos_presente': emRelacaoAOutrosAdultosPresente,
      if (emRelacaoAOutrosAdultosPresenteDescreve != null)
        'em_relacao_a_outros_adultos_presente_descreve':
            emRelacaoAOutrosAdultosPresenteDescreve,
      if (emRelacaoAsCriancasPresentes != null)
        'em_relacao_as_criancas_presentes': emRelacaoAsCriancasPresentes,
      if (emRelacaoAsCriancasPresentesDescreve != null)
        'em_relacao_as_criancas_presentes_descreve':
            emRelacaoAsCriancasPresentesDescreve,
      if (emRelacaoAVizinhanca != null)
        'em_relacao_a_vizinhanca': emRelacaoAVizinhanca,
      if (emRelacaoAVizinhancaDescreve != null)
        'em_relacao_a_vizinhanca_descreve': emRelacaoAVizinhancaDescreve,
      if (outras != null) 'outras': outras,
      if (outrasDescreve != null) 'outras_descreve': outrasDescreve,
      if (createdAt != null) 'created_at': createdAt,
      if (updatedAt != null) 'updated_at': updatedAt,
    });
  }

  UsersCompanion copyWith(
      {Value<String> uuid,
      Value<String> nomeDoPesquisador,
      Value<String> numeroDeInquerito,
      Value<DateTime> data,
      Value<String> municipio,
      Value<String> bairro,
      Value<String> pontoDeReferenciaDaCasa,
      Value<String> nome,
      Value<String> idade,
      Value<String> contacto,
      Value<String> eChefeDaFamilia,
      Value<String> caoNaoVinculoDeParantesco,
      Value<String> idadeDoChefeDaFamilia,
      Value<String> estudou,
      Value<String> eAlfabetizado,
      Value<String> completouEnsinoPrimario,
      Value<String> completouEnsinoSecundario,
      Value<String> fezCursoSuperior,
      Value<String> naoSabe,
      Value<String> chefeDeFamiliaTemSalario,
      Value<String> quantoGastaParaSustentarFamiliaPorMes,
      Value<String> quantasFamiliasResidemNacasa,
      Value<String> quantosAdultos,
      Value<String> quantosAdultosMaioresDe18anos,
      Value<String> quantosAdultosMaioresDe18anosHomens,
      Value<String> quantosAdultosMaioresDe18anosHomensTrabalham,
      Value<String> quantosAdultosMaioresDe18anosHomensAlfabetizados,
      Value<String> quantosAdultosM18Mulheres,
      Value<String> quantosAdultosM18MulheresTrabalham,
      Value<String> quantosAdultosM18MulheresAlfabetizados,
      Value<String> relatosDeDeficiencia,
      Value<String> totalcriancasAte1anoemeio,
      Value<String> totalcriancasAte1anoemeioMeninos,
      Value<String> totalcriancasAte1anoemeioMeninas,
      Value<String> algumasCriancasAte1anoemeioNaodesenvolvem,
      Value<String> algumasCriancasAte1anoemeioNaodesenvolvemQuantas,
      Value<String> algumaCriancasAte1anoemeioQueTipodeProblema,
      Value<String> algumaCriancasAte1anoemeioTemProblemaMotor,
      Value<String> algumaCriancasAte1anoemeioTemProblemaMotorQuantas,
      Value<String> algumaCriancasAte1anoemeioTemMalFormacao,
      Value<String> algumaCriancasAte1anoemeioTemMalFormacaoQuantas,
      Value<String> algumaCriancasAte1anoemeioNaoReagemAIncentivo,
      Value<String> algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas,
      Value<String> algumaCriancasAte1anoemeioTemReacaoAgressiva,
      Value<String> algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas,
      Value<String> totalcriancasAte1anoemeioA3anos,
      Value<String> totalcriancasAte1anoemeioA3anosMeninos,
      Value<String> totalcriancasAte1anoemeioA3anosMeninas,
      Value<String> algumasCriancasAte1anoemeioA3anosNaodesenvolvem,
      Value<String> algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas,
      Value<String> algumaCriancasAte1anoemeioA3anosQueTipodeProblema,
      Value<String> algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas,
      Value<String>
          algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas,
      Value<String> algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo,
      Value<String>
          algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas,
      Value<String> algumasCriancasAte1anoemeioA3anosAlgunsAgridem,
      Value<String> algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas,
      Value<String> algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal,
      Value<String>
          algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas,
      Value<String> algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao,
      Value<String>
          algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas,
      Value<String> algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas,
      Value<String>
          algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas,
      Value<String>
          algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente,
      Value<String>
          algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas,
      Value<String>
          algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos,
      Value<String>
          algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas,
      Value<String> algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown,
      Value<String>
          algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas,
      Value<String> totalcriancasde3A6anos,
      Value<String> totalcriancasde3A6anosMeninos,
      Value<String> totalcriancasde3A6anosMeninas,
      Value<String> algumasCriancasde3A6anosNaodesenvolvem,
      Value<String> algumasCriancasde3A6anosNaodesenvolvemQuantas,
      Value<String> algumaCriancasde3A6anosQueTipodeProblema,
      Value<String> algumasCriancasde3A6anosNaoInteragemComPessoas,
      Value<String> algumasCriancasde3A6anosNaoInteragemComPessoasQuantas,
      Value<String> algumasCriancasde3A6anosTemComportamentoAgressivo,
      Value<String> algumasCriancasde3A6anosTemComportamentoAgressivoQuantas,
      Value<String> algumasCriancasde3A6anosAlgunsAgridem,
      Value<String> algumasCriancasde3A6anosAlgunsAgridemQuantas,
      Value<String> algumasCriancasde3A6anosAlgumasAgitadasQueONormal,
      Value<String> algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas,
      Value<String> algumasCriancasde3A6anosAlgumasTemMalFormacao,
      Value<String> algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas,
      Value<String> algumasCriancasde3A6anosAlgumasNaoAndamSozinhas,
      Value<String> algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas,
      Value<String> algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente,
      Value<String>
          algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas,
      Value<String> algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos,
      Value<String>
          algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas,
      Value<String> algumasCriancasde3A6anosAlgumasTemSindromeDeDown,
      Value<String> algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas,
      Value<String> algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas,
      Value<String>
          algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas,
      Value<String>
          algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas,
      Value<String>
          algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas,
      Value<String>
          algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas,
      Value<String>
          algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas,
      Value<String> algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas,
      Value<String>
          algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas,
      Value<String> quantasCriancasde3A6anosEstaoNaEscolinha,
      Value<String> criancasDe3A6anosNaoEstaoNaEscolinhaPorque,
      Value<String> quantasCriancasde3A6anosTrabalham,
      Value<String> criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos,
      Value<String> totalcriancasde7A9anos,
      Value<String> totalcriancasde7A9anosMeninos,
      Value<String> totalcriancasde7A9anosMeninas,
      Value<String> quantasCriancasde7A9anosEstaoNaEscola,
      Value<String> quantasCriancasde7A9anosTrabalham,
      Value<String> nosUltimos5AnosQuantasGravidezesTiveNaFamilia,
      Value<String> partosOcoridosNoHospital,
      Value<String> partosOcoridosEmCasa,
      Value<String> partosOcoridosEmOutroLocal,
      Value<String> outroLocalDoPartoQualLocal,
      Value<String> quantasCriancasNasceramVivas,
      Value<String> quantasContinuamVivas,
      Value<String>
          dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram,
      Value<String> descreverOProblema,
      Value<String> aFamiliaProcurouAjudaParaSuperarEsseProblema,
      Value<String> seSimQueTipoDeAjudaEOnde,
      Value<String>
          osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia,
      Value<String>
          dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida,
      Value<String> sabeDizerOMotivoDosObitos,
      Value<String> seSimQualFoiPrincipalRazaodoObito,
      Value<String> voceTemMedoDeTerUmFilhoOuNetoComProblemas,
      Value<String> expliquePorQue,
      Value<String> porqueVoceAchaQueUmaCriancaPodeNascerComProblema,
      Value<String> oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema,
      Value<String> achaQueUmaCriancaQueNasceComProblemaPodeSobreviver,
      Value<String> expliquePorque,
      Value<String> quemPodeAjudarQuandoUmaCriancaNasceComProblema,
      Value<String> emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao,
      Value<String>
          emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene,
      Value<String> emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos,
      Value<String> aPessoaEntrevistadaPareceuSinceraNasSuasRespostas,
      Value<String> algumasPerguntasIncomodaramAPessoaEntrevistada,
      Value<String> seSimQuais3PrincipaisPerguntas,
      Value<String> todasAsPerguntasIncomodaram,
      Value<String> aResidenciaEraDe,
      Value<String> comoAvaliaHigieneDaCasa,
      Value<String> oQueEraMal,
      Value<String> tinhaCriancasNaCasaDuranteInquerito,
      Value<String> tinhaCriancasNaCasaDuranteInqueritoQuantas,
      Value<String> tentouIncentivar,
      Value<String> tentouIncentivarQuantas,
      Value<String> seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos,
      Value<String> observouCriancasDeAte1Ano,
      Value<String> observouCriancasDeAte1AnoQuantos,
      Value<String> algumasApresentavamProblemas,
      Value<String> algumasApresentavamProblemasQuantos,
      Value<String> descreveOProblemaDessasCriancas,
      Value<String> observouCriancasDe1AnoEmeioAte3Anos,
      Value<String> observouCriancasDe1AnoEmeioAte3AnosQuantas,
      Value<String> algumasCriancasDe1anoEmeioObservadasApresentavamProblemas,
      Value<String>
          algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas,
      Value<String>
          algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve,
      Value<String> observouCriancasDe3AnosAte6Anos,
      Value<String> observouCriancasDe3AnosAte6AnosQuantas,
      Value<String>
          observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas,
      Value<String>
          observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas,
      Value<String>
          observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve,
      Value<String> observouSituacoesQueLheChamaramAtencao,
      Value<String> emRelacaoAPessoaEntrevistada,
      Value<String> emRelacaoAPessoaEntrevistadaDescreve,
      Value<String> emRelacaoAOutrosAdultosPresente,
      Value<String> emRelacaoAOutrosAdultosPresenteDescreve,
      Value<String> emRelacaoAsCriancasPresentes,
      Value<String> emRelacaoAsCriancasPresentesDescreve,
      Value<String> emRelacaoAVizinhanca,
      Value<String> emRelacaoAVizinhancaDescreve,
      Value<String> outras,
      Value<String> outrasDescreve,
      Value<DateTime> createdAt,
      Value<DateTime> updatedAt}) {
    return UsersCompanion(
      uuid: uuid ?? this.uuid,
      nomeDoPesquisador: nomeDoPesquisador ?? this.nomeDoPesquisador,
      numeroDeInquerito: numeroDeInquerito ?? this.numeroDeInquerito,
      data: data ?? this.data,
      municipio: municipio ?? this.municipio,
      bairro: bairro ?? this.bairro,
      pontoDeReferenciaDaCasa:
          pontoDeReferenciaDaCasa ?? this.pontoDeReferenciaDaCasa,
      nome: nome ?? this.nome,
      idade: idade ?? this.idade,
      contacto: contacto ?? this.contacto,
      eChefeDaFamilia: eChefeDaFamilia ?? this.eChefeDaFamilia,
      caoNaoVinculoDeParantesco:
          caoNaoVinculoDeParantesco ?? this.caoNaoVinculoDeParantesco,
      idadeDoChefeDaFamilia:
          idadeDoChefeDaFamilia ?? this.idadeDoChefeDaFamilia,
      estudou: estudou ?? this.estudou,
      eAlfabetizado: eAlfabetizado ?? this.eAlfabetizado,
      completouEnsinoPrimario:
          completouEnsinoPrimario ?? this.completouEnsinoPrimario,
      completouEnsinoSecundario:
          completouEnsinoSecundario ?? this.completouEnsinoSecundario,
      fezCursoSuperior: fezCursoSuperior ?? this.fezCursoSuperior,
      naoSabe: naoSabe ?? this.naoSabe,
      chefeDeFamiliaTemSalario:
          chefeDeFamiliaTemSalario ?? this.chefeDeFamiliaTemSalario,
      quantoGastaParaSustentarFamiliaPorMes:
          quantoGastaParaSustentarFamiliaPorMes ??
              this.quantoGastaParaSustentarFamiliaPorMes,
      quantasFamiliasResidemNacasa:
          quantasFamiliasResidemNacasa ?? this.quantasFamiliasResidemNacasa,
      quantosAdultos: quantosAdultos ?? this.quantosAdultos,
      quantosAdultosMaioresDe18anos:
          quantosAdultosMaioresDe18anos ?? this.quantosAdultosMaioresDe18anos,
      quantosAdultosMaioresDe18anosHomens:
          quantosAdultosMaioresDe18anosHomens ??
              this.quantosAdultosMaioresDe18anosHomens,
      quantosAdultosMaioresDe18anosHomensTrabalham:
          quantosAdultosMaioresDe18anosHomensTrabalham ??
              this.quantosAdultosMaioresDe18anosHomensTrabalham,
      quantosAdultosMaioresDe18anosHomensAlfabetizados:
          quantosAdultosMaioresDe18anosHomensAlfabetizados ??
              this.quantosAdultosMaioresDe18anosHomensAlfabetizados,
      quantosAdultosM18Mulheres:
          quantosAdultosM18Mulheres ?? this.quantosAdultosM18Mulheres,
      quantosAdultosM18MulheresTrabalham: quantosAdultosM18MulheresTrabalham ??
          this.quantosAdultosM18MulheresTrabalham,
      quantosAdultosM18MulheresAlfabetizados:
          quantosAdultosM18MulheresAlfabetizados ??
              this.quantosAdultosM18MulheresAlfabetizados,
      relatosDeDeficiencia: relatosDeDeficiencia ?? this.relatosDeDeficiencia,
      totalcriancasAte1anoemeio:
          totalcriancasAte1anoemeio ?? this.totalcriancasAte1anoemeio,
      totalcriancasAte1anoemeioMeninos: totalcriancasAte1anoemeioMeninos ??
          this.totalcriancasAte1anoemeioMeninos,
      totalcriancasAte1anoemeioMeninas: totalcriancasAte1anoemeioMeninas ??
          this.totalcriancasAte1anoemeioMeninas,
      algumasCriancasAte1anoemeioNaodesenvolvem:
          algumasCriancasAte1anoemeioNaodesenvolvem ??
              this.algumasCriancasAte1anoemeioNaodesenvolvem,
      algumasCriancasAte1anoemeioNaodesenvolvemQuantas:
          algumasCriancasAte1anoemeioNaodesenvolvemQuantas ??
              this.algumasCriancasAte1anoemeioNaodesenvolvemQuantas,
      algumaCriancasAte1anoemeioQueTipodeProblema:
          algumaCriancasAte1anoemeioQueTipodeProblema ??
              this.algumaCriancasAte1anoemeioQueTipodeProblema,
      algumaCriancasAte1anoemeioTemProblemaMotor:
          algumaCriancasAte1anoemeioTemProblemaMotor ??
              this.algumaCriancasAte1anoemeioTemProblemaMotor,
      algumaCriancasAte1anoemeioTemProblemaMotorQuantas:
          algumaCriancasAte1anoemeioTemProblemaMotorQuantas ??
              this.algumaCriancasAte1anoemeioTemProblemaMotorQuantas,
      algumaCriancasAte1anoemeioTemMalFormacao:
          algumaCriancasAte1anoemeioTemMalFormacao ??
              this.algumaCriancasAte1anoemeioTemMalFormacao,
      algumaCriancasAte1anoemeioTemMalFormacaoQuantas:
          algumaCriancasAte1anoemeioTemMalFormacaoQuantas ??
              this.algumaCriancasAte1anoemeioTemMalFormacaoQuantas,
      algumaCriancasAte1anoemeioNaoReagemAIncentivo:
          algumaCriancasAte1anoemeioNaoReagemAIncentivo ??
              this.algumaCriancasAte1anoemeioNaoReagemAIncentivo,
      algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas:
          algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas ??
              this.algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas,
      algumaCriancasAte1anoemeioTemReacaoAgressiva:
          algumaCriancasAte1anoemeioTemReacaoAgressiva ??
              this.algumaCriancasAte1anoemeioTemReacaoAgressiva,
      algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas:
          algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas ??
              this.algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas,
      totalcriancasAte1anoemeioA3anos: totalcriancasAte1anoemeioA3anos ??
          this.totalcriancasAte1anoemeioA3anos,
      totalcriancasAte1anoemeioA3anosMeninos:
          totalcriancasAte1anoemeioA3anosMeninos ??
              this.totalcriancasAte1anoemeioA3anosMeninos,
      totalcriancasAte1anoemeioA3anosMeninas:
          totalcriancasAte1anoemeioA3anosMeninas ??
              this.totalcriancasAte1anoemeioA3anosMeninas,
      algumasCriancasAte1anoemeioA3anosNaodesenvolvem:
          algumasCriancasAte1anoemeioA3anosNaodesenvolvem ??
              this.algumasCriancasAte1anoemeioA3anosNaodesenvolvem,
      algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas:
          algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas ??
              this.algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas,
      algumaCriancasAte1anoemeioA3anosQueTipodeProblema:
          algumaCriancasAte1anoemeioA3anosQueTipodeProblema ??
              this.algumaCriancasAte1anoemeioA3anosQueTipodeProblema,
      algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas:
          algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas ??
              this.algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas,
      algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas:
          algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas ??
              this.algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas,
      algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo:
          algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo ??
              this.algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo,
      algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas:
          algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas ??
              this.algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas,
      algumasCriancasAte1anoemeioA3anosAlgunsAgridem:
          algumasCriancasAte1anoemeioA3anosAlgunsAgridem ??
              this.algumasCriancasAte1anoemeioA3anosAlgunsAgridem,
      algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas:
          algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas ??
              this.algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas,
      algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal:
          algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal ??
              this.algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal,
      algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas:
          algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas ??
              this.algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas,
      algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao:
          algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao ??
              this.algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao,
      algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas:
          algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas ??
              this.algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas,
      algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas:
          algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas ??
              this.algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas,
      algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas:
          algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas ??
              this.algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas,
      algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente:
          algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente ??
              this.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente,
      algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas:
          algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas ??
              this.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas,
      algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos:
          algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos ??
              this.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos,
      algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas:
          algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas ??
              this.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas,
      algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown:
          algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown ??
              this.algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown,
      algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas:
          algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas ??
              this.algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas,
      totalcriancasde3A6anos:
          totalcriancasde3A6anos ?? this.totalcriancasde3A6anos,
      totalcriancasde3A6anosMeninos:
          totalcriancasde3A6anosMeninos ?? this.totalcriancasde3A6anosMeninos,
      totalcriancasde3A6anosMeninas:
          totalcriancasde3A6anosMeninas ?? this.totalcriancasde3A6anosMeninas,
      algumasCriancasde3A6anosNaodesenvolvem:
          algumasCriancasde3A6anosNaodesenvolvem ??
              this.algumasCriancasde3A6anosNaodesenvolvem,
      algumasCriancasde3A6anosNaodesenvolvemQuantas:
          algumasCriancasde3A6anosNaodesenvolvemQuantas ??
              this.algumasCriancasde3A6anosNaodesenvolvemQuantas,
      algumaCriancasde3A6anosQueTipodeProblema:
          algumaCriancasde3A6anosQueTipodeProblema ??
              this.algumaCriancasde3A6anosQueTipodeProblema,
      algumasCriancasde3A6anosNaoInteragemComPessoas:
          algumasCriancasde3A6anosNaoInteragemComPessoas ??
              this.algumasCriancasde3A6anosNaoInteragemComPessoas,
      algumasCriancasde3A6anosNaoInteragemComPessoasQuantas:
          algumasCriancasde3A6anosNaoInteragemComPessoasQuantas ??
              this.algumasCriancasde3A6anosNaoInteragemComPessoasQuantas,
      algumasCriancasde3A6anosTemComportamentoAgressivo:
          algumasCriancasde3A6anosTemComportamentoAgressivo ??
              this.algumasCriancasde3A6anosTemComportamentoAgressivo,
      algumasCriancasde3A6anosTemComportamentoAgressivoQuantas:
          algumasCriancasde3A6anosTemComportamentoAgressivoQuantas ??
              this.algumasCriancasde3A6anosTemComportamentoAgressivoQuantas,
      algumasCriancasde3A6anosAlgunsAgridem:
          algumasCriancasde3A6anosAlgunsAgridem ??
              this.algumasCriancasde3A6anosAlgunsAgridem,
      algumasCriancasde3A6anosAlgunsAgridemQuantas:
          algumasCriancasde3A6anosAlgunsAgridemQuantas ??
              this.algumasCriancasde3A6anosAlgunsAgridemQuantas,
      algumasCriancasde3A6anosAlgumasAgitadasQueONormal:
          algumasCriancasde3A6anosAlgumasAgitadasQueONormal ??
              this.algumasCriancasde3A6anosAlgumasAgitadasQueONormal,
      algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas:
          algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas ??
              this.algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas,
      algumasCriancasde3A6anosAlgumasTemMalFormacao:
          algumasCriancasde3A6anosAlgumasTemMalFormacao ??
              this.algumasCriancasde3A6anosAlgumasTemMalFormacao,
      algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas:
          algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas ??
              this.algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas,
      algumasCriancasde3A6anosAlgumasNaoAndamSozinhas:
          algumasCriancasde3A6anosAlgumasNaoAndamSozinhas ??
              this.algumasCriancasde3A6anosAlgumasNaoAndamSozinhas,
      algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas:
          algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas ??
              this.algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas,
      algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente:
          algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente ??
              this.algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente,
      algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas:
          algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas ??
              this.algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas,
      algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos:
          algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos ??
              this.algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos,
      algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas:
          algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas ??
              this.algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas,
      algumasCriancasde3A6anosAlgumasTemSindromeDeDown:
          algumasCriancasde3A6anosAlgumasTemSindromeDeDown ??
              this.algumasCriancasde3A6anosAlgumasTemSindromeDeDown,
      algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas:
          algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas ??
              this.algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas,
      algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas:
          algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas ??
              this.algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas,
      algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas:
          algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas ??
              this.algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas,
      algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas:
          algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas ??
              this.algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas,
      algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas:
          algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas ??
              this.algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas,
      algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas:
          algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas ??
              this.algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas,
      algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas:
          algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas ??
              this.algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas,
      algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas:
          algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas ??
              this.algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas,
      algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas:
          algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas ??
              this.algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas,
      quantasCriancasde3A6anosEstaoNaEscolinha:
          quantasCriancasde3A6anosEstaoNaEscolinha ??
              this.quantasCriancasde3A6anosEstaoNaEscolinha,
      criancasDe3A6anosNaoEstaoNaEscolinhaPorque:
          criancasDe3A6anosNaoEstaoNaEscolinhaPorque ??
              this.criancasDe3A6anosNaoEstaoNaEscolinhaPorque,
      quantasCriancasde3A6anosTrabalham: quantasCriancasde3A6anosTrabalham ??
          this.quantasCriancasde3A6anosTrabalham,
      criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos:
          criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos ??
              this.criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos,
      totalcriancasde7A9anos:
          totalcriancasde7A9anos ?? this.totalcriancasde7A9anos,
      totalcriancasde7A9anosMeninos:
          totalcriancasde7A9anosMeninos ?? this.totalcriancasde7A9anosMeninos,
      totalcriancasde7A9anosMeninas:
          totalcriancasde7A9anosMeninas ?? this.totalcriancasde7A9anosMeninas,
      quantasCriancasde7A9anosEstaoNaEscola:
          quantasCriancasde7A9anosEstaoNaEscola ??
              this.quantasCriancasde7A9anosEstaoNaEscola,
      quantasCriancasde7A9anosTrabalham: quantasCriancasde7A9anosTrabalham ??
          this.quantasCriancasde7A9anosTrabalham,
      nosUltimos5AnosQuantasGravidezesTiveNaFamilia:
          nosUltimos5AnosQuantasGravidezesTiveNaFamilia ??
              this.nosUltimos5AnosQuantasGravidezesTiveNaFamilia,
      partosOcoridosNoHospital:
          partosOcoridosNoHospital ?? this.partosOcoridosNoHospital,
      partosOcoridosEmCasa: partosOcoridosEmCasa ?? this.partosOcoridosEmCasa,
      partosOcoridosEmOutroLocal:
          partosOcoridosEmOutroLocal ?? this.partosOcoridosEmOutroLocal,
      outroLocalDoPartoQualLocal:
          outroLocalDoPartoQualLocal ?? this.outroLocalDoPartoQualLocal,
      quantasCriancasNasceramVivas:
          quantasCriancasNasceramVivas ?? this.quantasCriancasNasceramVivas,
      quantasContinuamVivas:
          quantasContinuamVivas ?? this.quantasContinuamVivas,
      dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram:
          dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram ??
              this.dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram,
      descreverOProblema: descreverOProblema ?? this.descreverOProblema,
      aFamiliaProcurouAjudaParaSuperarEsseProblema:
          aFamiliaProcurouAjudaParaSuperarEsseProblema ??
              this.aFamiliaProcurouAjudaParaSuperarEsseProblema,
      seSimQueTipoDeAjudaEOnde:
          seSimQueTipoDeAjudaEOnde ?? this.seSimQueTipoDeAjudaEOnde,
      osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia:
          osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia ??
              this.osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia,
      dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida:
          dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida ??
              this.dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida,
      sabeDizerOMotivoDosObitos:
          sabeDizerOMotivoDosObitos ?? this.sabeDizerOMotivoDosObitos,
      seSimQualFoiPrincipalRazaodoObito: seSimQualFoiPrincipalRazaodoObito ??
          this.seSimQualFoiPrincipalRazaodoObito,
      voceTemMedoDeTerUmFilhoOuNetoComProblemas:
          voceTemMedoDeTerUmFilhoOuNetoComProblemas ??
              this.voceTemMedoDeTerUmFilhoOuNetoComProblemas,
      expliquePorQue: expliquePorQue ?? this.expliquePorQue,
      porqueVoceAchaQueUmaCriancaPodeNascerComProblema:
          porqueVoceAchaQueUmaCriancaPodeNascerComProblema ??
              this.porqueVoceAchaQueUmaCriancaPodeNascerComProblema,
      oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema:
          oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema ??
              this.oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema,
      achaQueUmaCriancaQueNasceComProblemaPodeSobreviver:
          achaQueUmaCriancaQueNasceComProblemaPodeSobreviver ??
              this.achaQueUmaCriancaQueNasceComProblemaPodeSobreviver,
      expliquePorque: expliquePorque ?? this.expliquePorque,
      quemPodeAjudarQuandoUmaCriancaNasceComProblema:
          quemPodeAjudarQuandoUmaCriancaNasceComProblema ??
              this.quemPodeAjudarQuandoUmaCriancaNasceComProblema,
      emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao:
          emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao ??
              this.emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao,
      emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene:
          emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene ??
              this.emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene,
      emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos:
          emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos ??
              this.emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos,
      aPessoaEntrevistadaPareceuSinceraNasSuasRespostas:
          aPessoaEntrevistadaPareceuSinceraNasSuasRespostas ??
              this.aPessoaEntrevistadaPareceuSinceraNasSuasRespostas,
      algumasPerguntasIncomodaramAPessoaEntrevistada:
          algumasPerguntasIncomodaramAPessoaEntrevistada ??
              this.algumasPerguntasIncomodaramAPessoaEntrevistada,
      seSimQuais3PrincipaisPerguntas:
          seSimQuais3PrincipaisPerguntas ?? this.seSimQuais3PrincipaisPerguntas,
      todasAsPerguntasIncomodaram:
          todasAsPerguntasIncomodaram ?? this.todasAsPerguntasIncomodaram,
      aResidenciaEraDe: aResidenciaEraDe ?? this.aResidenciaEraDe,
      comoAvaliaHigieneDaCasa:
          comoAvaliaHigieneDaCasa ?? this.comoAvaliaHigieneDaCasa,
      oQueEraMal: oQueEraMal ?? this.oQueEraMal,
      tinhaCriancasNaCasaDuranteInquerito:
          tinhaCriancasNaCasaDuranteInquerito ??
              this.tinhaCriancasNaCasaDuranteInquerito,
      tinhaCriancasNaCasaDuranteInqueritoQuantas:
          tinhaCriancasNaCasaDuranteInqueritoQuantas ??
              this.tinhaCriancasNaCasaDuranteInqueritoQuantas,
      tentouIncentivar: tentouIncentivar ?? this.tentouIncentivar,
      tentouIncentivarQuantas:
          tentouIncentivarQuantas ?? this.tentouIncentivarQuantas,
      seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos:
          seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos ??
              this.seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos,
      observouCriancasDeAte1Ano:
          observouCriancasDeAte1Ano ?? this.observouCriancasDeAte1Ano,
      observouCriancasDeAte1AnoQuantos: observouCriancasDeAte1AnoQuantos ??
          this.observouCriancasDeAte1AnoQuantos,
      algumasApresentavamProblemas:
          algumasApresentavamProblemas ?? this.algumasApresentavamProblemas,
      algumasApresentavamProblemasQuantos:
          algumasApresentavamProblemasQuantos ??
              this.algumasApresentavamProblemasQuantos,
      descreveOProblemaDessasCriancas: descreveOProblemaDessasCriancas ??
          this.descreveOProblemaDessasCriancas,
      observouCriancasDe1AnoEmeioAte3Anos:
          observouCriancasDe1AnoEmeioAte3Anos ??
              this.observouCriancasDe1AnoEmeioAte3Anos,
      observouCriancasDe1AnoEmeioAte3AnosQuantas:
          observouCriancasDe1AnoEmeioAte3AnosQuantas ??
              this.observouCriancasDe1AnoEmeioAte3AnosQuantas,
      algumasCriancasDe1anoEmeioObservadasApresentavamProblemas:
          algumasCriancasDe1anoEmeioObservadasApresentavamProblemas ??
              this.algumasCriancasDe1anoEmeioObservadasApresentavamProblemas,
      algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas:
          algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas ??
              this.algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas,
      algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve:
          algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve ??
              this.algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve,
      observouCriancasDe3AnosAte6Anos: observouCriancasDe3AnosAte6Anos ??
          this.observouCriancasDe3AnosAte6Anos,
      observouCriancasDe3AnosAte6AnosQuantas:
          observouCriancasDe3AnosAte6AnosQuantas ??
              this.observouCriancasDe3AnosAte6AnosQuantas,
      observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas:
          observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas ??
              this.observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas,
      observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas:
          observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas ??
              this.observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas,
      observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve:
          observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve ??
              this.observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve,
      observouSituacoesQueLheChamaramAtencao:
          observouSituacoesQueLheChamaramAtencao ??
              this.observouSituacoesQueLheChamaramAtencao,
      emRelacaoAPessoaEntrevistada:
          emRelacaoAPessoaEntrevistada ?? this.emRelacaoAPessoaEntrevistada,
      emRelacaoAPessoaEntrevistadaDescreve:
          emRelacaoAPessoaEntrevistadaDescreve ??
              this.emRelacaoAPessoaEntrevistadaDescreve,
      emRelacaoAOutrosAdultosPresente: emRelacaoAOutrosAdultosPresente ??
          this.emRelacaoAOutrosAdultosPresente,
      emRelacaoAOutrosAdultosPresenteDescreve:
          emRelacaoAOutrosAdultosPresenteDescreve ??
              this.emRelacaoAOutrosAdultosPresenteDescreve,
      emRelacaoAsCriancasPresentes:
          emRelacaoAsCriancasPresentes ?? this.emRelacaoAsCriancasPresentes,
      emRelacaoAsCriancasPresentesDescreve:
          emRelacaoAsCriancasPresentesDescreve ??
              this.emRelacaoAsCriancasPresentesDescreve,
      emRelacaoAVizinhanca: emRelacaoAVizinhanca ?? this.emRelacaoAVizinhanca,
      emRelacaoAVizinhancaDescreve:
          emRelacaoAVizinhancaDescreve ?? this.emRelacaoAVizinhancaDescreve,
      outras: outras ?? this.outras,
      outrasDescreve: outrasDescreve ?? this.outrasDescreve,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (uuid.present) {
      map['uuid'] = Variable<String>(uuid.value);
    }
    if (nomeDoPesquisador.present) {
      map['nome_do_pesquisador'] = Variable<String>(nomeDoPesquisador.value);
    }
    if (numeroDeInquerito.present) {
      map['numero_de_inquerito'] = Variable<String>(numeroDeInquerito.value);
    }
    if (data.present) {
      map['data'] = Variable<DateTime>(data.value);
    }
    if (municipio.present) {
      map['municipio'] = Variable<String>(municipio.value);
    }
    if (bairro.present) {
      map['bairro'] = Variable<String>(bairro.value);
    }
    if (pontoDeReferenciaDaCasa.present) {
      map['ponto_de_referencia_da_casa'] =
          Variable<String>(pontoDeReferenciaDaCasa.value);
    }
    if (nome.present) {
      map['nome'] = Variable<String>(nome.value);
    }
    if (idade.present) {
      map['idade'] = Variable<String>(idade.value);
    }
    if (contacto.present) {
      map['contacto'] = Variable<String>(contacto.value);
    }
    if (eChefeDaFamilia.present) {
      map['e_chefe_da_familia'] = Variable<String>(eChefeDaFamilia.value);
    }
    if (caoNaoVinculoDeParantesco.present) {
      map['cao_nao_vinculo_de_parantesco'] =
          Variable<String>(caoNaoVinculoDeParantesco.value);
    }
    if (idadeDoChefeDaFamilia.present) {
      map['idade_do_chefe_da_familia'] =
          Variable<String>(idadeDoChefeDaFamilia.value);
    }
    if (estudou.present) {
      map['estudou'] = Variable<String>(estudou.value);
    }
    if (eAlfabetizado.present) {
      map['e_alfabetizado'] = Variable<String>(eAlfabetizado.value);
    }
    if (completouEnsinoPrimario.present) {
      map['completou_ensino_primario'] =
          Variable<String>(completouEnsinoPrimario.value);
    }
    if (completouEnsinoSecundario.present) {
      map['completou_ensino_secundario'] =
          Variable<String>(completouEnsinoSecundario.value);
    }
    if (fezCursoSuperior.present) {
      map['fez_curso_superior'] = Variable<String>(fezCursoSuperior.value);
    }
    if (naoSabe.present) {
      map['nao_sabe'] = Variable<String>(naoSabe.value);
    }
    if (chefeDeFamiliaTemSalario.present) {
      map['chefe_de_familia_tem_salario'] =
          Variable<String>(chefeDeFamiliaTemSalario.value);
    }
    if (quantoGastaParaSustentarFamiliaPorMes.present) {
      map['quanto_gasta_para_sustentar_familia_por_mes'] =
          Variable<String>(quantoGastaParaSustentarFamiliaPorMes.value);
    }
    if (quantasFamiliasResidemNacasa.present) {
      map['quantas_familias_residem_nacasa'] =
          Variable<String>(quantasFamiliasResidemNacasa.value);
    }
    if (quantosAdultos.present) {
      map['quantos_adultos'] = Variable<String>(quantosAdultos.value);
    }
    if (quantosAdultosMaioresDe18anos.present) {
      map['quantos_adultos_maiores_de18anos'] =
          Variable<String>(quantosAdultosMaioresDe18anos.value);
    }
    if (quantosAdultosMaioresDe18anosHomens.present) {
      map['quantos_adultos_maiores_de18anos_homens'] =
          Variable<String>(quantosAdultosMaioresDe18anosHomens.value);
    }
    if (quantosAdultosMaioresDe18anosHomensTrabalham.present) {
      map['quantos_adultos_maiores_de18anos_homens_trabalham'] =
          Variable<String>(quantosAdultosMaioresDe18anosHomensTrabalham.value);
    }
    if (quantosAdultosMaioresDe18anosHomensAlfabetizados.present) {
      map['quantos_adultos_maiores_de18anos_homens_alfabetizados'] =
          Variable<String>(
              quantosAdultosMaioresDe18anosHomensAlfabetizados.value);
    }
    if (quantosAdultosM18Mulheres.present) {
      map['quantos_adultos_m18_mulheres'] =
          Variable<String>(quantosAdultosM18Mulheres.value);
    }
    if (quantosAdultosM18MulheresTrabalham.present) {
      map['quantos_adultos_m18_mulheres_trabalham'] =
          Variable<String>(quantosAdultosM18MulheresTrabalham.value);
    }
    if (quantosAdultosM18MulheresAlfabetizados.present) {
      map['quantos_adultos_m18_mulheres_alfabetizados'] =
          Variable<String>(quantosAdultosM18MulheresAlfabetizados.value);
    }
    if (relatosDeDeficiencia.present) {
      map['relatos_de_deficiencia'] =
          Variable<String>(relatosDeDeficiencia.value);
    }
    if (totalcriancasAte1anoemeio.present) {
      map['totalcriancas_ate1anoemeio'] =
          Variable<String>(totalcriancasAte1anoemeio.value);
    }
    if (totalcriancasAte1anoemeioMeninos.present) {
      map['totalcriancas_ate1anoemeio_meninos'] =
          Variable<String>(totalcriancasAte1anoemeioMeninos.value);
    }
    if (totalcriancasAte1anoemeioMeninas.present) {
      map['totalcriancas_ate1anoemeio_meninas'] =
          Variable<String>(totalcriancasAte1anoemeioMeninas.value);
    }
    if (algumasCriancasAte1anoemeioNaodesenvolvem.present) {
      map['algumas_criancas_ate1anoemeio_naodesenvolvem'] =
          Variable<String>(algumasCriancasAte1anoemeioNaodesenvolvem.value);
    }
    if (algumasCriancasAte1anoemeioNaodesenvolvemQuantas.present) {
      map['algumas_criancas_ate1anoemeio_naodesenvolvem_quantas'] =
          Variable<String>(
              algumasCriancasAte1anoemeioNaodesenvolvemQuantas.value);
    }
    if (algumaCriancasAte1anoemeioQueTipodeProblema.present) {
      map['alguma_criancas_ate1anoemeio_que_tipode_problema'] =
          Variable<String>(algumaCriancasAte1anoemeioQueTipodeProblema.value);
    }
    if (algumaCriancasAte1anoemeioTemProblemaMotor.present) {
      map['alguma_criancas_ate1anoemeio_tem_problema_motor'] =
          Variable<String>(algumaCriancasAte1anoemeioTemProblemaMotor.value);
    }
    if (algumaCriancasAte1anoemeioTemProblemaMotorQuantas.present) {
      map['alguma_criancas_ate1anoemeio_tem_problema_motor_quantas'] =
          Variable<String>(
              algumaCriancasAte1anoemeioTemProblemaMotorQuantas.value);
    }
    if (algumaCriancasAte1anoemeioTemMalFormacao.present) {
      map['alguma_criancas_ate1anoemeio_tem_mal_formacao'] =
          Variable<String>(algumaCriancasAte1anoemeioTemMalFormacao.value);
    }
    if (algumaCriancasAte1anoemeioTemMalFormacaoQuantas.present) {
      map['alguma_criancas_ate1anoemeio_tem_mal_formacao_quantas'] =
          Variable<String>(
              algumaCriancasAte1anoemeioTemMalFormacaoQuantas.value);
    }
    if (algumaCriancasAte1anoemeioNaoReagemAIncentivo.present) {
      map['alguma_criancas_ate1anoemeio_nao_reagem_a_incentivo'] =
          Variable<String>(algumaCriancasAte1anoemeioNaoReagemAIncentivo.value);
    }
    if (algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas.present) {
      map['alguma_criancas_ate1anoemeio_nao_reagem_a_incentivo_quantas'] =
          Variable<String>(
              algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas.value);
    }
    if (algumaCriancasAte1anoemeioTemReacaoAgressiva.present) {
      map['alguma_criancas_ate1anoemeio_tem_reacao_agressiva'] =
          Variable<String>(algumaCriancasAte1anoemeioTemReacaoAgressiva.value);
    }
    if (algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas.present) {
      map['alguma_criancas_ate1anoemeio_tem_reacao_agressiva_quantas'] =
          Variable<String>(
              algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas.value);
    }
    if (totalcriancasAte1anoemeioA3anos.present) {
      map['totalcriancas_ate1anoemeio_a3anos'] =
          Variable<String>(totalcriancasAte1anoemeioA3anos.value);
    }
    if (totalcriancasAte1anoemeioA3anosMeninos.present) {
      map['totalcriancas_ate1anoemeio_a3anos_meninos'] =
          Variable<String>(totalcriancasAte1anoemeioA3anosMeninos.value);
    }
    if (totalcriancasAte1anoemeioA3anosMeninas.present) {
      map['totalcriancas_ate1anoemeio_a3anos_meninas'] =
          Variable<String>(totalcriancasAte1anoemeioA3anosMeninas.value);
    }
    if (algumasCriancasAte1anoemeioA3anosNaodesenvolvem.present) {
      map['algumas_criancas_ate1anoemeio_a3anos_naodesenvolvem'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosNaodesenvolvem.value);
    }
    if (algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas.present) {
      map['algumas_criancas_ate1anoemeio_a3anos_naodesenvolvem_quantas'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas.value);
    }
    if (algumaCriancasAte1anoemeioA3anosQueTipodeProblema.present) {
      map['alguma_criancas_ate1anoemeio_a3anos_que_tipode_problema'] =
          Variable<String>(
              algumaCriancasAte1anoemeioA3anosQueTipodeProblema.value);
    }
    if (algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas.present) {
      map['algumas_criancas_ate1anoemeio_a3anos_nao_interagem_com_pessoas'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas.value);
    }
    if (algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas
        .present) {
      map['algumas_criancas_ate1anoemeio_a3anos_nao_interagem_com_pessoas_quantas'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas
                  .value);
    }
    if (algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo.present) {
      map['algumas_criancas_ate1anoemeio_a3anos_tem_comportamento_agressivo'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo.value);
    }
    if (algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas
        .present) {
      map['algumas_criancas_ate1anoemeio_a3anos_tem_comportamento_agressivo_quantas'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas
                  .value);
    }
    if (algumasCriancasAte1anoemeioA3anosAlgunsAgridem.present) {
      map['algumas_criancas_ate1anoemeio_a3anos_alguns_agridem'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosAlgunsAgridem.value);
    }
    if (algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas.present) {
      map['algumas_criancas_ate1anoemeio_a3anos_alguns_agridem_quantas'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas.value);
    }
    if (algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal.present) {
      map['algumas_criancas_ate1anoemeio_a3anos_algumas_agitadas_que_o_normal'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal.value);
    }
    if (algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas
        .present) {
      map['algumas_criancas_ate1anoemeio_a3anos_algumas_agitadas_que_o_normal_quantas'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas
                  .value);
    }
    if (algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao.present) {
      map['algumas_criancas_ate1anoemeio_a3anos_algumas_tem_mal_formacao'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao.value);
    }
    if (algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas.present) {
      map['algumas_criancas_ate1anoemeio_a3anos_algumas_tem_mal_formacao_quantas'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas
                  .value);
    }
    if (algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas.present) {
      map['algumas_criancas_ate1anoemeio_a3anos_algumas_nao_andam_sozinhas'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas.value);
    }
    if (algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas
        .present) {
      map['algumas_criancas_ate1anoemeio_a3anos_algumas_nao_andam_sozinhas_quantas'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas
                  .value);
    }
    if (algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente
        .present) {
      map['algumas_criancas_ate1anoemeio_a3anos_algumas_nao_expressam_verbalmente'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente
                  .value);
    }
    if (algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas
        .present) {
      map['algumas_criancas_ate1anoemeio_a3anos_algumas_nao_expressam_verbalmente_quantas'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas
                  .value);
    }
    if (algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos
        .present) {
      map['algumas_criancas_ate1anoemeio_a3anos_algumas_nao_expressam_sentimentos'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos
                  .value);
    }
    if (algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas
        .present) {
      map['algumas_criancas_ate1anoemeio_a3anos_algumas_nao_expressam_sentimentos_quantas'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas
                  .value);
    }
    if (algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown.present) {
      map['algumas_criancas_ate1anoemeio_a3anos_algumas_tem_sindrome_de_down'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown.value);
    }
    if (algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas
        .present) {
      map['algumas_criancas_ate1anoemeio_a3anos_algumas_tem_sindrome_de_down_quantas'] =
          Variable<String>(
              algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas
                  .value);
    }
    if (totalcriancasde3A6anos.present) {
      map['totalcriancasde3_a6anos'] =
          Variable<String>(totalcriancasde3A6anos.value);
    }
    if (totalcriancasde3A6anosMeninos.present) {
      map['totalcriancasde3_a6anos_meninos'] =
          Variable<String>(totalcriancasde3A6anosMeninos.value);
    }
    if (totalcriancasde3A6anosMeninas.present) {
      map['totalcriancasde3_a6anos_meninas'] =
          Variable<String>(totalcriancasde3A6anosMeninas.value);
    }
    if (algumasCriancasde3A6anosNaodesenvolvem.present) {
      map['algumas_criancasde3_a6anos_naodesenvolvem'] =
          Variable<String>(algumasCriancasde3A6anosNaodesenvolvem.value);
    }
    if (algumasCriancasde3A6anosNaodesenvolvemQuantas.present) {
      map['algumas_criancasde3_a6anos_naodesenvolvem_quantas'] =
          Variable<String>(algumasCriancasde3A6anosNaodesenvolvemQuantas.value);
    }
    if (algumaCriancasde3A6anosQueTipodeProblema.present) {
      map['alguma_criancasde3_a6anos_que_tipode_problema'] =
          Variable<String>(algumaCriancasde3A6anosQueTipodeProblema.value);
    }
    if (algumasCriancasde3A6anosNaoInteragemComPessoas.present) {
      map['algumas_criancasde3_a6anos_nao_interagem_com_pessoas'] =
          Variable<String>(
              algumasCriancasde3A6anosNaoInteragemComPessoas.value);
    }
    if (algumasCriancasde3A6anosNaoInteragemComPessoasQuantas.present) {
      map['algumas_criancasde3_a6anos_nao_interagem_com_pessoas_quantas'] =
          Variable<String>(
              algumasCriancasde3A6anosNaoInteragemComPessoasQuantas.value);
    }
    if (algumasCriancasde3A6anosTemComportamentoAgressivo.present) {
      map['algumas_criancasde3_a6anos_tem_comportamento_agressivo'] =
          Variable<String>(
              algumasCriancasde3A6anosTemComportamentoAgressivo.value);
    }
    if (algumasCriancasde3A6anosTemComportamentoAgressivoQuantas.present) {
      map['algumas_criancasde3_a6anos_tem_comportamento_agressivo_quantas'] =
          Variable<String>(
              algumasCriancasde3A6anosTemComportamentoAgressivoQuantas.value);
    }
    if (algumasCriancasde3A6anosAlgunsAgridem.present) {
      map['algumas_criancasde3_a6anos_alguns_agridem'] =
          Variable<String>(algumasCriancasde3A6anosAlgunsAgridem.value);
    }
    if (algumasCriancasde3A6anosAlgunsAgridemQuantas.present) {
      map['algumas_criancasde3_a6anos_alguns_agridem_quantas'] =
          Variable<String>(algumasCriancasde3A6anosAlgunsAgridemQuantas.value);
    }
    if (algumasCriancasde3A6anosAlgumasAgitadasQueONormal.present) {
      map['algumas_criancasde3_a6anos_algumas_agitadas_que_o_normal'] =
          Variable<String>(
              algumasCriancasde3A6anosAlgumasAgitadasQueONormal.value);
    }
    if (algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas.present) {
      map['algumas_criancasde3_a6anos_algumas_agitadas_que_o_normal_quantas'] =
          Variable<String>(
              algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas.value);
    }
    if (algumasCriancasde3A6anosAlgumasTemMalFormacao.present) {
      map['algumas_criancasde3_a6anos_algumas_tem_mal_formacao'] =
          Variable<String>(algumasCriancasde3A6anosAlgumasTemMalFormacao.value);
    }
    if (algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas.present) {
      map['algumas_criancasde3_a6anos_algumas_tem_mal_formacao_quantas'] =
          Variable<String>(
              algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas.value);
    }
    if (algumasCriancasde3A6anosAlgumasNaoAndamSozinhas.present) {
      map['algumas_criancasde3_a6anos_algumas_nao_andam_sozinhas'] =
          Variable<String>(
              algumasCriancasde3A6anosAlgumasNaoAndamSozinhas.value);
    }
    if (algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas.present) {
      map['algumas_criancasde3_a6anos_algumas_nao_andam_sozinhas_quantas'] =
          Variable<String>(
              algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas.value);
    }
    if (algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente.present) {
      map['algumas_criancasde3_a6anos_algumas_nao_expressam_verbalmente'] =
          Variable<String>(
              algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente.value);
    }
    if (algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas.present) {
      map['algumas_criancasde3_a6anos_algumas_nao_expressam_verbalmente_quantas'] =
          Variable<String>(
              algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas
                  .value);
    }
    if (algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos.present) {
      map['algumas_criancasde3_a6anos_algumas_nao_expressam_sentimentos'] =
          Variable<String>(
              algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos.value);
    }
    if (algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas.present) {
      map['algumas_criancasde3_a6anos_algumas_nao_expressam_sentimentos_quantas'] =
          Variable<String>(
              algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas
                  .value);
    }
    if (algumasCriancasde3A6anosAlgumasTemSindromeDeDown.present) {
      map['algumas_criancasde3_a6anos_algumas_tem_sindrome_de_down'] =
          Variable<String>(
              algumasCriancasde3A6anosAlgumasTemSindromeDeDown.value);
    }
    if (algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas.present) {
      map['algumas_criancasde3_a6anos_algumas_tem_sindrome_de_down_quantas'] =
          Variable<String>(
              algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas.value);
    }
    if (algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas.present) {
      map['algumas_criancasde3_a6anos_algumas_nao_sabem_se_alimentar_sozinhas'] =
          Variable<String>(
              algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas.value);
    }
    if (algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas
        .present) {
      map['algumas_criancasde3_a6anos_algumas_nao_sabe_se_alimentar_sozinhas_quantas'] =
          Variable<String>(
              algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas
                  .value);
    }
    if (algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas.present) {
      map['algumas_criancasde3_a6anos_algumas_nao_conseguem_tomar_banho_sozinhas'] =
          Variable<String>(
              algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas
                  .value);
    }
    if (algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas
        .present) {
      map['algumas_criancasde3_a6anos_algumas_nao_conseguem_tomar_banho_sozinhas_quantas'] =
          Variable<String>(
              algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas
                  .value);
    }
    if (algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas
        .present) {
      map['algumas_criancasde3_a6anos_algumas_nao_conseguem_ir_a_casa_de_banho_sozinhas'] =
          Variable<String>(
              algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas
                  .value);
    }
    if (algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas
        .present) {
      map['algumas_criancasde3_a6anos_algumas_nao_conseguem_ir_a_casa_de_banho_sozinhas_quantas'] =
          Variable<String>(
              algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas
                  .value);
    }
    if (algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas.present) {
      map['algumas_criancasde3_a6anos_algumas_nao_conseguem_se_vestir_sozinhas'] =
          Variable<String>(
              algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas
                  .value);
    }
    if (algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas
        .present) {
      map['algumas_criancasde3_a6anos_algumas_nao_conseguem_se_vestir_sozinhas_quantas'] =
          Variable<String>(
              algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas
                  .value);
    }
    if (quantasCriancasde3A6anosEstaoNaEscolinha.present) {
      map['quantas_criancasde3_a6anos_estao_na_escolinha'] =
          Variable<String>(quantasCriancasde3A6anosEstaoNaEscolinha.value);
    }
    if (criancasDe3A6anosNaoEstaoNaEscolinhaPorque.present) {
      map['criancas_de3_a6anos_nao_estao_na_escolinha_porque'] =
          Variable<String>(criancasDe3A6anosNaoEstaoNaEscolinhaPorque.value);
    }
    if (quantasCriancasde3A6anosTrabalham.present) {
      map['quantas_criancasde3_a6anos_trabalham'] =
          Variable<String>(quantasCriancasde3A6anosTrabalham.value);
    }
    if (criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos.present) {
      map['criancas_de3_a6anos_nao_estao_na_escolinha_por_outos_motivos'] =
          Variable<String>(
              criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos.value);
    }
    if (totalcriancasde7A9anos.present) {
      map['totalcriancasde7_a9anos'] =
          Variable<String>(totalcriancasde7A9anos.value);
    }
    if (totalcriancasde7A9anosMeninos.present) {
      map['totalcriancasde7_a9anos_meninos'] =
          Variable<String>(totalcriancasde7A9anosMeninos.value);
    }
    if (totalcriancasde7A9anosMeninas.present) {
      map['totalcriancasde7_a9anos_meninas'] =
          Variable<String>(totalcriancasde7A9anosMeninas.value);
    }
    if (quantasCriancasde7A9anosEstaoNaEscola.present) {
      map['quantas_criancasde7_a9anos_estao_na_escola'] =
          Variable<String>(quantasCriancasde7A9anosEstaoNaEscola.value);
    }
    if (quantasCriancasde7A9anosTrabalham.present) {
      map['quantas_criancasde7_a9anos_trabalham'] =
          Variable<String>(quantasCriancasde7A9anosTrabalham.value);
    }
    if (nosUltimos5AnosQuantasGravidezesTiveNaFamilia.present) {
      map['nos_ultimos5_anos_quantas_gravidezes_tive_na_familia'] =
          Variable<String>(nosUltimos5AnosQuantasGravidezesTiveNaFamilia.value);
    }
    if (partosOcoridosNoHospital.present) {
      map['partos_ocoridos_no_hospital'] =
          Variable<String>(partosOcoridosNoHospital.value);
    }
    if (partosOcoridosEmCasa.present) {
      map['partos_ocoridos_em_casa'] =
          Variable<String>(partosOcoridosEmCasa.value);
    }
    if (partosOcoridosEmOutroLocal.present) {
      map['partos_ocoridos_em_outro_local'] =
          Variable<String>(partosOcoridosEmOutroLocal.value);
    }
    if (outroLocalDoPartoQualLocal.present) {
      map['outro_local_do_parto_qual_local'] =
          Variable<String>(outroLocalDoPartoQualLocal.value);
    }
    if (quantasCriancasNasceramVivas.present) {
      map['quantas_criancas_nasceram_vivas'] =
          Variable<String>(quantasCriancasNasceramVivas.value);
    }
    if (quantasContinuamVivas.present) {
      map['quantas_continuam_vivas'] =
          Variable<String>(quantasContinuamVivas.value);
    }
    if (dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram
        .present) {
      map['das_vivas_algumas_apresentam_problemas_de_desenvolvimento_que_outros_nao_tiveram'] =
          Variable<String>(
              dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram
                  .value);
    }
    if (descreverOProblema.present) {
      map['descrever_o_problema'] = Variable<String>(descreverOProblema.value);
    }
    if (aFamiliaProcurouAjudaParaSuperarEsseProblema.present) {
      map['a_familia_procurou_ajuda_para_superar_esse_problema'] =
          Variable<String>(aFamiliaProcurouAjudaParaSuperarEsseProblema.value);
    }
    if (seSimQueTipoDeAjudaEOnde.present) {
      map['se_sim_que_tipo_de_ajuda_e_onde'] =
          Variable<String>(seSimQueTipoDeAjudaEOnde.value);
    }
    if (osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia
        .present) {
      map['os_vizinhos_se_preocuparam_quando_nasceu_uma_crianca_com_problema_na_familia'] =
          Variable<String>(
              osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia
                  .value);
    }
    if (dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida
        .present) {
      map['das_criancas_que_nasceram_vivas_quantas_nao_sobreviveram_nos2_primeiros_anos_de_vida'] =
          Variable<String>(
              dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida
                  .value);
    }
    if (sabeDizerOMotivoDosObitos.present) {
      map['sabe_dizer_o_motivo_dos_obitos'] =
          Variable<String>(sabeDizerOMotivoDosObitos.value);
    }
    if (seSimQualFoiPrincipalRazaodoObito.present) {
      map['se_sim_qual_foi_principal_razaodo_obito'] =
          Variable<String>(seSimQualFoiPrincipalRazaodoObito.value);
    }
    if (voceTemMedoDeTerUmFilhoOuNetoComProblemas.present) {
      map['voce_tem_medo_de_ter_um_filho_ou_neto_com_problemas'] =
          Variable<String>(voceTemMedoDeTerUmFilhoOuNetoComProblemas.value);
    }
    if (expliquePorQue.present) {
      map['explique_por_que'] = Variable<String>(expliquePorQue.value);
    }
    if (porqueVoceAchaQueUmaCriancaPodeNascerComProblema.present) {
      map['porque_voce_acha_que_uma_crianca_pode_nascer_com_problema'] =
          Variable<String>(
              porqueVoceAchaQueUmaCriancaPodeNascerComProblema.value);
    }
    if (oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema.present) {
      map['o_que_acontece_no_seu_bairro_quando_nasce_uma_crianca_com_problema'] =
          Variable<String>(
              oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema.value);
    }
    if (achaQueUmaCriancaQueNasceComProblemaPodeSobreviver.present) {
      map['acha_que_uma_crianca_que_nasce_com_problema_pode_sobreviver'] =
          Variable<String>(
              achaQueUmaCriancaQueNasceComProblemaPodeSobreviver.value);
    }
    if (expliquePorque.present) {
      map['explique_porque'] = Variable<String>(expliquePorque.value);
    }
    if (quemPodeAjudarQuandoUmaCriancaNasceComProblema.present) {
      map['quem_pode_ajudar_quando_uma_crianca_nasce_com_problema'] =
          Variable<String>(
              quemPodeAjudarQuandoUmaCriancaNasceComProblema.value);
    }
    if (emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao.present) {
      map['em_casa_quem_cuida_mais_das_criancas_ate6_anos_para_alimentacao'] =
          Variable<String>(
              emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao.value);
    }
    if (emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene.present) {
      map['em_casa_quem_cuida_mais_das_criancas_ate6_anos_para_os_cuidados_de_higiene'] =
          Variable<String>(
              emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene
                  .value);
    }
    if (emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos.present) {
      map['em_casa_quem_cuida_mais_das_criancas_ate6_anos_para_os_estudos'] =
          Variable<String>(
              emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos.value);
    }
    if (aPessoaEntrevistadaPareceuSinceraNasSuasRespostas.present) {
      map['a_pessoa_entrevistada_pareceu_sincera_nas_suas_respostas'] =
          Variable<String>(
              aPessoaEntrevistadaPareceuSinceraNasSuasRespostas.value);
    }
    if (algumasPerguntasIncomodaramAPessoaEntrevistada.present) {
      map['algumas_perguntas_incomodaram_a_pessoa_entrevistada'] =
          Variable<String>(
              algumasPerguntasIncomodaramAPessoaEntrevistada.value);
    }
    if (seSimQuais3PrincipaisPerguntas.present) {
      map['se_sim_quais3_principais_perguntas'] =
          Variable<String>(seSimQuais3PrincipaisPerguntas.value);
    }
    if (todasAsPerguntasIncomodaram.present) {
      map['todas_as_perguntas_incomodaram'] =
          Variable<String>(todasAsPerguntasIncomodaram.value);
    }
    if (aResidenciaEraDe.present) {
      map['a_residencia_era_de'] = Variable<String>(aResidenciaEraDe.value);
    }
    if (comoAvaliaHigieneDaCasa.present) {
      map['como_avalia_higiene_da_casa'] =
          Variable<String>(comoAvaliaHigieneDaCasa.value);
    }
    if (oQueEraMal.present) {
      map['o_que_era_mal'] = Variable<String>(oQueEraMal.value);
    }
    if (tinhaCriancasNaCasaDuranteInquerito.present) {
      map['tinha_criancas_na_casa_durante_inquerito'] =
          Variable<String>(tinhaCriancasNaCasaDuranteInquerito.value);
    }
    if (tinhaCriancasNaCasaDuranteInqueritoQuantas.present) {
      map['tinha_criancas_na_casa_durante_inquerito_quantas'] =
          Variable<String>(tinhaCriancasNaCasaDuranteInqueritoQuantas.value);
    }
    if (tentouIncentivar.present) {
      map['tentou_incentivar'] = Variable<String>(tentouIncentivar.value);
    }
    if (tentouIncentivarQuantas.present) {
      map['tentou_incentivar_quantas'] =
          Variable<String>(tentouIncentivarQuantas.value);
    }
    if (seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos.present) {
      map['se_incentivou_quantas_reagiram_positivamente_aos_seus_incentivos'] =
          Variable<String>(
              seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos.value);
    }
    if (observouCriancasDeAte1Ano.present) {
      map['observou_criancas_de_ate1_ano'] =
          Variable<String>(observouCriancasDeAte1Ano.value);
    }
    if (observouCriancasDeAte1AnoQuantos.present) {
      map['observou_criancas_de_ate1_ano_quantos'] =
          Variable<String>(observouCriancasDeAte1AnoQuantos.value);
    }
    if (algumasApresentavamProblemas.present) {
      map['algumas_apresentavam_problemas'] =
          Variable<String>(algumasApresentavamProblemas.value);
    }
    if (algumasApresentavamProblemasQuantos.present) {
      map['algumas_apresentavam_problemas_quantos'] =
          Variable<String>(algumasApresentavamProblemasQuantos.value);
    }
    if (descreveOProblemaDessasCriancas.present) {
      map['descreve_o_problema_dessas_criancas'] =
          Variable<String>(descreveOProblemaDessasCriancas.value);
    }
    if (observouCriancasDe1AnoEmeioAte3Anos.present) {
      map['observou_criancas_de1_ano_emeio_ate3_anos'] =
          Variable<String>(observouCriancasDe1AnoEmeioAte3Anos.value);
    }
    if (observouCriancasDe1AnoEmeioAte3AnosQuantas.present) {
      map['observou_criancas_de1_ano_emeio_ate3_anos_quantas'] =
          Variable<String>(observouCriancasDe1AnoEmeioAte3AnosQuantas.value);
    }
    if (algumasCriancasDe1anoEmeioObservadasApresentavamProblemas.present) {
      map['algumas_criancas_de1ano_emeio_observadas_apresentavam_problemas'] =
          Variable<String>(
              algumasCriancasDe1anoEmeioObservadasApresentavamProblemas.value);
    }
    if (algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas
        .present) {
      map['algumas_criancas_de1ano_emeio_observadas_apresentavam_problemas_quantas'] =
          Variable<String>(
              algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas
                  .value);
    }
    if (algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve
        .present) {
      map['algumas_criancas_de1ano_emeio_observadas_apresentavam_problemas_descreve'] =
          Variable<String>(
              algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve
                  .value);
    }
    if (observouCriancasDe3AnosAte6Anos.present) {
      map['observou_criancas_de3_anos_ate6_anos'] =
          Variable<String>(observouCriancasDe3AnosAte6Anos.value);
    }
    if (observouCriancasDe3AnosAte6AnosQuantas.present) {
      map['observou_criancas_de3_anos_ate6_anos_quantas'] =
          Variable<String>(observouCriancasDe3AnosAte6AnosQuantas.value);
    }
    if (observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas
        .present) {
      map['observou_criancas_de3_anos_ate6_anos_que_algumas_apresentavam_problemas'] =
          Variable<String>(
              observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas
                  .value);
    }
    if (observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas
        .present) {
      map['observou_criancas_de3_anos_ate6_anos_que_algumas_apresentavam_problemas_quantas'] =
          Variable<String>(
              observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas
                  .value);
    }
    if (observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve
        .present) {
      map['observou_criancas_de3_anos_ate6_anos_que_algumas_apresentavam_problemas_descreve'] =
          Variable<String>(
              observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve
                  .value);
    }
    if (observouSituacoesQueLheChamaramAtencao.present) {
      map['observou_situacoes_que_lhe_chamaram_atencao'] =
          Variable<String>(observouSituacoesQueLheChamaramAtencao.value);
    }
    if (emRelacaoAPessoaEntrevistada.present) {
      map['em_relacao_a_pessoa_entrevistada'] =
          Variable<String>(emRelacaoAPessoaEntrevistada.value);
    }
    if (emRelacaoAPessoaEntrevistadaDescreve.present) {
      map['em_relacao_a_pessoa_entrevistada_descreve'] =
          Variable<String>(emRelacaoAPessoaEntrevistadaDescreve.value);
    }
    if (emRelacaoAOutrosAdultosPresente.present) {
      map['em_relacao_a_outros_adultos_presente'] =
          Variable<String>(emRelacaoAOutrosAdultosPresente.value);
    }
    if (emRelacaoAOutrosAdultosPresenteDescreve.present) {
      map['em_relacao_a_outros_adultos_presente_descreve'] =
          Variable<String>(emRelacaoAOutrosAdultosPresenteDescreve.value);
    }
    if (emRelacaoAsCriancasPresentes.present) {
      map['em_relacao_as_criancas_presentes'] =
          Variable<String>(emRelacaoAsCriancasPresentes.value);
    }
    if (emRelacaoAsCriancasPresentesDescreve.present) {
      map['em_relacao_as_criancas_presentes_descreve'] =
          Variable<String>(emRelacaoAsCriancasPresentesDescreve.value);
    }
    if (emRelacaoAVizinhanca.present) {
      map['em_relacao_a_vizinhanca'] =
          Variable<String>(emRelacaoAVizinhanca.value);
    }
    if (emRelacaoAVizinhancaDescreve.present) {
      map['em_relacao_a_vizinhanca_descreve'] =
          Variable<String>(emRelacaoAVizinhancaDescreve.value);
    }
    if (outras.present) {
      map['outras'] = Variable<String>(outras.value);
    }
    if (outrasDescreve.present) {
      map['outras_descreve'] = Variable<String>(outrasDescreve.value);
    }
    if (createdAt.present) {
      map['created_at'] = Variable<DateTime>(createdAt.value);
    }
    if (updatedAt.present) {
      map['updated_at'] = Variable<DateTime>(updatedAt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('UsersCompanion(')
          ..write('uuid: $uuid, ')
          ..write('nomeDoPesquisador: $nomeDoPesquisador, ')
          ..write('numeroDeInquerito: $numeroDeInquerito, ')
          ..write('data: $data, ')
          ..write('municipio: $municipio, ')
          ..write('bairro: $bairro, ')
          ..write('pontoDeReferenciaDaCasa: $pontoDeReferenciaDaCasa, ')
          ..write('nome: $nome, ')
          ..write('idade: $idade, ')
          ..write('contacto: $contacto, ')
          ..write('eChefeDaFamilia: $eChefeDaFamilia, ')
          ..write('caoNaoVinculoDeParantesco: $caoNaoVinculoDeParantesco, ')
          ..write('idadeDoChefeDaFamilia: $idadeDoChefeDaFamilia, ')
          ..write('estudou: $estudou, ')
          ..write('eAlfabetizado: $eAlfabetizado, ')
          ..write('completouEnsinoPrimario: $completouEnsinoPrimario, ')
          ..write('completouEnsinoSecundario: $completouEnsinoSecundario, ')
          ..write('fezCursoSuperior: $fezCursoSuperior, ')
          ..write('naoSabe: $naoSabe, ')
          ..write('chefeDeFamiliaTemSalario: $chefeDeFamiliaTemSalario, ')
          ..write(
              'quantoGastaParaSustentarFamiliaPorMes: $quantoGastaParaSustentarFamiliaPorMes, ')
          ..write(
              'quantasFamiliasResidemNacasa: $quantasFamiliasResidemNacasa, ')
          ..write('quantosAdultos: $quantosAdultos, ')
          ..write(
              'quantosAdultosMaioresDe18anos: $quantosAdultosMaioresDe18anos, ')
          ..write(
              'quantosAdultosMaioresDe18anosHomens: $quantosAdultosMaioresDe18anosHomens, ')
          ..write(
              'quantosAdultosMaioresDe18anosHomensTrabalham: $quantosAdultosMaioresDe18anosHomensTrabalham, ')
          ..write(
              'quantosAdultosMaioresDe18anosHomensAlfabetizados: $quantosAdultosMaioresDe18anosHomensAlfabetizados, ')
          ..write('quantosAdultosM18Mulheres: $quantosAdultosM18Mulheres, ')
          ..write(
              'quantosAdultosM18MulheresTrabalham: $quantosAdultosM18MulheresTrabalham, ')
          ..write(
              'quantosAdultosM18MulheresAlfabetizados: $quantosAdultosM18MulheresAlfabetizados, ')
          ..write('relatosDeDeficiencia: $relatosDeDeficiencia, ')
          ..write('totalcriancasAte1anoemeio: $totalcriancasAte1anoemeio, ')
          ..write(
              'totalcriancasAte1anoemeioMeninos: $totalcriancasAte1anoemeioMeninos, ')
          ..write(
              'totalcriancasAte1anoemeioMeninas: $totalcriancasAte1anoemeioMeninas, ')
          ..write(
              'algumasCriancasAte1anoemeioNaodesenvolvem: $algumasCriancasAte1anoemeioNaodesenvolvem, ')
          ..write(
              'algumasCriancasAte1anoemeioNaodesenvolvemQuantas: $algumasCriancasAte1anoemeioNaodesenvolvemQuantas, ')
          ..write(
              'algumaCriancasAte1anoemeioQueTipodeProblema: $algumaCriancasAte1anoemeioQueTipodeProblema, ')
          ..write(
              'algumaCriancasAte1anoemeioTemProblemaMotor: $algumaCriancasAte1anoemeioTemProblemaMotor, ')
          ..write(
              'algumaCriancasAte1anoemeioTemProblemaMotorQuantas: $algumaCriancasAte1anoemeioTemProblemaMotorQuantas, ')
          ..write(
              'algumaCriancasAte1anoemeioTemMalFormacao: $algumaCriancasAte1anoemeioTemMalFormacao, ')
          ..write(
              'algumaCriancasAte1anoemeioTemMalFormacaoQuantas: $algumaCriancasAte1anoemeioTemMalFormacaoQuantas, ')
          ..write(
              'algumaCriancasAte1anoemeioNaoReagemAIncentivo: $algumaCriancasAte1anoemeioNaoReagemAIncentivo, ')
          ..write(
              'algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas: $algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas, ')
          ..write(
              'algumaCriancasAte1anoemeioTemReacaoAgressiva: $algumaCriancasAte1anoemeioTemReacaoAgressiva, ')
          ..write(
              'algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas: $algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas, ')
          ..write(
              'totalcriancasAte1anoemeioA3anos: $totalcriancasAte1anoemeioA3anos, ')
          ..write(
              'totalcriancasAte1anoemeioA3anosMeninos: $totalcriancasAte1anoemeioA3anosMeninos, ')
          ..write(
              'totalcriancasAte1anoemeioA3anosMeninas: $totalcriancasAte1anoemeioA3anosMeninas, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosNaodesenvolvem: $algumasCriancasAte1anoemeioA3anosNaodesenvolvem, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas: $algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas, ')
          ..write(
              'algumaCriancasAte1anoemeioA3anosQueTipodeProblema: $algumaCriancasAte1anoemeioA3anosQueTipodeProblema, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas: $algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas: $algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo: $algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas: $algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosAlgunsAgridem: $algumasCriancasAte1anoemeioA3anosAlgunsAgridem, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas: $algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal: $algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas: $algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao: $algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas: $algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas: $algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas: $algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente: $algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas: $algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos: $algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas: $algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown: $algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown, ')
          ..write(
              'algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas: $algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas, ')
          ..write('totalcriancasde3A6anos: $totalcriancasde3A6anos, ')
          ..write(
              'totalcriancasde3A6anosMeninos: $totalcriancasde3A6anosMeninos, ')
          ..write(
              'totalcriancasde3A6anosMeninas: $totalcriancasde3A6anosMeninas, ')
          ..write(
              'algumasCriancasde3A6anosNaodesenvolvem: $algumasCriancasde3A6anosNaodesenvolvem, ')
          ..write(
              'algumasCriancasde3A6anosNaodesenvolvemQuantas: $algumasCriancasde3A6anosNaodesenvolvemQuantas, ')
          ..write(
              'algumaCriancasde3A6anosQueTipodeProblema: $algumaCriancasde3A6anosQueTipodeProblema, ')
          ..write(
              'algumasCriancasde3A6anosNaoInteragemComPessoas: $algumasCriancasde3A6anosNaoInteragemComPessoas, ')
          ..write(
              'algumasCriancasde3A6anosNaoInteragemComPessoasQuantas: $algumasCriancasde3A6anosNaoInteragemComPessoasQuantas, ')
          ..write(
              'algumasCriancasde3A6anosTemComportamentoAgressivo: $algumasCriancasde3A6anosTemComportamentoAgressivo, ')
          ..write(
              'algumasCriancasde3A6anosTemComportamentoAgressivoQuantas: $algumasCriancasde3A6anosTemComportamentoAgressivoQuantas, ')
          ..write(
              'algumasCriancasde3A6anosAlgunsAgridem: $algumasCriancasde3A6anosAlgunsAgridem, ')
          ..write(
              'algumasCriancasde3A6anosAlgunsAgridemQuantas: $algumasCriancasde3A6anosAlgunsAgridemQuantas, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasAgitadasQueONormal: $algumasCriancasde3A6anosAlgumasAgitadasQueONormal, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas: $algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasTemMalFormacao: $algumasCriancasde3A6anosAlgumasTemMalFormacao, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas: $algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasNaoAndamSozinhas: $algumasCriancasde3A6anosAlgumasNaoAndamSozinhas, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas: $algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente: $algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas: $algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos: $algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas: $algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasTemSindromeDeDown: $algumasCriancasde3A6anosAlgumasTemSindromeDeDown, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas: $algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas: $algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas: $algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas: $algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas: $algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas: $algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas: $algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas: $algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas, ')
          ..write(
              'algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas: $algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas, ')
          ..write(
              'quantasCriancasde3A6anosEstaoNaEscolinha: $quantasCriancasde3A6anosEstaoNaEscolinha, ')
          ..write(
              'criancasDe3A6anosNaoEstaoNaEscolinhaPorque: $criancasDe3A6anosNaoEstaoNaEscolinhaPorque, ')
          ..write(
              'quantasCriancasde3A6anosTrabalham: $quantasCriancasde3A6anosTrabalham, ')
          ..write(
              'criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos: $criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos, ')
          ..write('totalcriancasde7A9anos: $totalcriancasde7A9anos, ')
          ..write(
              'totalcriancasde7A9anosMeninos: $totalcriancasde7A9anosMeninos, ')
          ..write(
              'totalcriancasde7A9anosMeninas: $totalcriancasde7A9anosMeninas, ')
          ..write(
              'quantasCriancasde7A9anosEstaoNaEscola: $quantasCriancasde7A9anosEstaoNaEscola, ')
          ..write(
              'quantasCriancasde7A9anosTrabalham: $quantasCriancasde7A9anosTrabalham, ')
          ..write(
              'nosUltimos5AnosQuantasGravidezesTiveNaFamilia: $nosUltimos5AnosQuantasGravidezesTiveNaFamilia, ')
          ..write('partosOcoridosNoHospital: $partosOcoridosNoHospital, ')
          ..write('partosOcoridosEmCasa: $partosOcoridosEmCasa, ')
          ..write('partosOcoridosEmOutroLocal: $partosOcoridosEmOutroLocal, ')
          ..write('outroLocalDoPartoQualLocal: $outroLocalDoPartoQualLocal, ')
          ..write(
              'quantasCriancasNasceramVivas: $quantasCriancasNasceramVivas, ')
          ..write('quantasContinuamVivas: $quantasContinuamVivas, ')
          ..write(
              'dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram: $dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram, ')
          ..write('descreverOProblema: $descreverOProblema, ')
          ..write(
              'aFamiliaProcurouAjudaParaSuperarEsseProblema: $aFamiliaProcurouAjudaParaSuperarEsseProblema, ')
          ..write('seSimQueTipoDeAjudaEOnde: $seSimQueTipoDeAjudaEOnde, ')
          ..write(
              'osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia: $osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia, ')
          ..write(
              'dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida: $dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida, ')
          ..write('sabeDizerOMotivoDosObitos: $sabeDizerOMotivoDosObitos, ')
          ..write(
              'seSimQualFoiPrincipalRazaodoObito: $seSimQualFoiPrincipalRazaodoObito, ')
          ..write(
              'voceTemMedoDeTerUmFilhoOuNetoComProblemas: $voceTemMedoDeTerUmFilhoOuNetoComProblemas, ')
          ..write('expliquePorQue: $expliquePorQue, ')
          ..write(
              'porqueVoceAchaQueUmaCriancaPodeNascerComProblema: $porqueVoceAchaQueUmaCriancaPodeNascerComProblema, ')
          ..write(
              'oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema: $oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema, ')
          ..write(
              'achaQueUmaCriancaQueNasceComProblemaPodeSobreviver: $achaQueUmaCriancaQueNasceComProblemaPodeSobreviver, ')
          ..write('expliquePorque: $expliquePorque, ')
          ..write(
              'quemPodeAjudarQuandoUmaCriancaNasceComProblema: $quemPodeAjudarQuandoUmaCriancaNasceComProblema, ')
          ..write(
              'emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao: $emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao, ')
          ..write(
              'emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene: $emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene, ')
          ..write(
              'emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos: $emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos, ')
          ..write(
              'aPessoaEntrevistadaPareceuSinceraNasSuasRespostas: $aPessoaEntrevistadaPareceuSinceraNasSuasRespostas, ')
          ..write(
              'algumasPerguntasIncomodaramAPessoaEntrevistada: $algumasPerguntasIncomodaramAPessoaEntrevistada, ')
          ..write(
              'seSimQuais3PrincipaisPerguntas: $seSimQuais3PrincipaisPerguntas, ')
          ..write('todasAsPerguntasIncomodaram: $todasAsPerguntasIncomodaram, ')
          ..write('aResidenciaEraDe: $aResidenciaEraDe, ')
          ..write('comoAvaliaHigieneDaCasa: $comoAvaliaHigieneDaCasa, ')
          ..write('oQueEraMal: $oQueEraMal, ')
          ..write(
              'tinhaCriancasNaCasaDuranteInquerito: $tinhaCriancasNaCasaDuranteInquerito, ')
          ..write(
              'tinhaCriancasNaCasaDuranteInqueritoQuantas: $tinhaCriancasNaCasaDuranteInqueritoQuantas, ')
          ..write('tentouIncentivar: $tentouIncentivar, ')
          ..write('tentouIncentivarQuantas: $tentouIncentivarQuantas, ')
          ..write(
              'seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos: $seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos, ')
          ..write('observouCriancasDeAte1Ano: $observouCriancasDeAte1Ano, ')
          ..write(
              'observouCriancasDeAte1AnoQuantos: $observouCriancasDeAte1AnoQuantos, ')
          ..write(
              'algumasApresentavamProblemas: $algumasApresentavamProblemas, ')
          ..write(
              'algumasApresentavamProblemasQuantos: $algumasApresentavamProblemasQuantos, ')
          ..write(
              'descreveOProblemaDessasCriancas: $descreveOProblemaDessasCriancas, ')
          ..write(
              'observouCriancasDe1AnoEmeioAte3Anos: $observouCriancasDe1AnoEmeioAte3Anos, ')
          ..write(
              'observouCriancasDe1AnoEmeioAte3AnosQuantas: $observouCriancasDe1AnoEmeioAte3AnosQuantas, ')
          ..write(
              'algumasCriancasDe1anoEmeioObservadasApresentavamProblemas: $algumasCriancasDe1anoEmeioObservadasApresentavamProblemas, ')
          ..write(
              'algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas: $algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas, ')
          ..write(
              'algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve: $algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve, ')
          ..write(
              'observouCriancasDe3AnosAte6Anos: $observouCriancasDe3AnosAte6Anos, ')
          ..write(
              'observouCriancasDe3AnosAte6AnosQuantas: $observouCriancasDe3AnosAte6AnosQuantas, ')
          ..write(
              'observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas: $observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas, ')
          ..write(
              'observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas: $observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas, ')
          ..write(
              'observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve: $observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve, ')
          ..write(
              'observouSituacoesQueLheChamaramAtencao: $observouSituacoesQueLheChamaramAtencao, ')
          ..write(
              'emRelacaoAPessoaEntrevistada: $emRelacaoAPessoaEntrevistada, ')
          ..write(
              'emRelacaoAPessoaEntrevistadaDescreve: $emRelacaoAPessoaEntrevistadaDescreve, ')
          ..write(
              'emRelacaoAOutrosAdultosPresente: $emRelacaoAOutrosAdultosPresente, ')
          ..write(
              'emRelacaoAOutrosAdultosPresenteDescreve: $emRelacaoAOutrosAdultosPresenteDescreve, ')
          ..write(
              'emRelacaoAsCriancasPresentes: $emRelacaoAsCriancasPresentes, ')
          ..write(
              'emRelacaoAsCriancasPresentesDescreve: $emRelacaoAsCriancasPresentesDescreve, ')
          ..write('emRelacaoAVizinhanca: $emRelacaoAVizinhanca, ')
          ..write(
              'emRelacaoAVizinhancaDescreve: $emRelacaoAVizinhancaDescreve, ')
          ..write('outras: $outras, ')
          ..write('outrasDescreve: $outrasDescreve, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }
}

class $UsersTable extends Users with TableInfo<$UsersTable, User> {
  final GeneratedDatabase _db;
  final String _alias;
  $UsersTable(this._db, [this._alias]);
  final VerificationMeta _uuidMeta = const VerificationMeta('uuid');
  GeneratedTextColumn _uuid;
  @override
  GeneratedTextColumn get uuid => _uuid ??= _constructUuid();
  GeneratedTextColumn _constructUuid() {
    return GeneratedTextColumn('uuid', $tableName, true,
        maxTextLength: 191, $customConstraints: 'UNIQUE')
      ..clientDefault = () => unique.v4();
  }

  final VerificationMeta _nomeDoPesquisadorMeta =
      const VerificationMeta('nomeDoPesquisador');
  GeneratedTextColumn _nomeDoPesquisador;
  @override
  GeneratedTextColumn get nomeDoPesquisador =>
      _nomeDoPesquisador ??= _constructNomeDoPesquisador();
  GeneratedTextColumn _constructNomeDoPesquisador() {
    return GeneratedTextColumn(
      'nome_do_pesquisador',
      $tableName,
      true,
    );
  }

  final VerificationMeta _numeroDeInqueritoMeta =
      const VerificationMeta('numeroDeInquerito');
  GeneratedTextColumn _numeroDeInquerito;
  @override
  GeneratedTextColumn get numeroDeInquerito =>
      _numeroDeInquerito ??= _constructNumeroDeInquerito();
  GeneratedTextColumn _constructNumeroDeInquerito() {
    return GeneratedTextColumn(
      'numero_de_inquerito',
      $tableName,
      true,
    );
  }

  final VerificationMeta _dataMeta = const VerificationMeta('data');
  GeneratedDateTimeColumn _data;
  @override
  GeneratedDateTimeColumn get data => _data ??= _constructData();
  GeneratedDateTimeColumn _constructData() {
    return GeneratedDateTimeColumn(
      'data',
      $tableName,
      true,
    );
  }

  final VerificationMeta _municipioMeta = const VerificationMeta('municipio');
  GeneratedTextColumn _municipio;
  @override
  GeneratedTextColumn get municipio => _municipio ??= _constructMunicipio();
  GeneratedTextColumn _constructMunicipio() {
    return GeneratedTextColumn('municipio', $tableName, true,
        maxTextLength: 50);
  }

  final VerificationMeta _bairroMeta = const VerificationMeta('bairro');
  GeneratedTextColumn _bairro;
  @override
  GeneratedTextColumn get bairro => _bairro ??= _constructBairro();
  GeneratedTextColumn _constructBairro() {
    return GeneratedTextColumn('bairro', $tableName, true, maxTextLength: 50);
  }

  final VerificationMeta _pontoDeReferenciaDaCasaMeta =
      const VerificationMeta('pontoDeReferenciaDaCasa');
  GeneratedTextColumn _pontoDeReferenciaDaCasa;
  @override
  GeneratedTextColumn get pontoDeReferenciaDaCasa =>
      _pontoDeReferenciaDaCasa ??= _constructPontoDeReferenciaDaCasa();
  GeneratedTextColumn _constructPontoDeReferenciaDaCasa() {
    return GeneratedTextColumn('ponto_de_referencia_da_casa', $tableName, true,
        maxTextLength: 50);
  }

  final VerificationMeta _nomeMeta = const VerificationMeta('nome');
  GeneratedTextColumn _nome;
  @override
  GeneratedTextColumn get nome => _nome ??= _constructNome();
  GeneratedTextColumn _constructNome() {
    return GeneratedTextColumn('nome', $tableName, true, maxTextLength: 50);
  }

  final VerificationMeta _idadeMeta = const VerificationMeta('idade');
  GeneratedTextColumn _idade;
  @override
  GeneratedTextColumn get idade => _idade ??= _constructIdade();
  GeneratedTextColumn _constructIdade() {
    return GeneratedTextColumn(
      'idade',
      $tableName,
      true,
    );
  }

  final VerificationMeta _contactoMeta = const VerificationMeta('contacto');
  GeneratedTextColumn _contacto;
  @override
  GeneratedTextColumn get contacto => _contacto ??= _constructContacto();
  GeneratedTextColumn _constructContacto() {
    return GeneratedTextColumn(
      'contacto',
      $tableName,
      true,
    );
  }

  final VerificationMeta _eChefeDaFamiliaMeta =
      const VerificationMeta('eChefeDaFamilia');
  GeneratedTextColumn _eChefeDaFamilia;
  @override
  GeneratedTextColumn get eChefeDaFamilia =>
      _eChefeDaFamilia ??= _constructEChefeDaFamilia();
  GeneratedTextColumn _constructEChefeDaFamilia() {
    return GeneratedTextColumn(
      'e_chefe_da_familia',
      $tableName,
      true,
    );
  }

  final VerificationMeta _caoNaoVinculoDeParantescoMeta =
      const VerificationMeta('caoNaoVinculoDeParantesco');
  GeneratedTextColumn _caoNaoVinculoDeParantesco;
  @override
  GeneratedTextColumn get caoNaoVinculoDeParantesco =>
      _caoNaoVinculoDeParantesco ??= _constructCaoNaoVinculoDeParantesco();
  GeneratedTextColumn _constructCaoNaoVinculoDeParantesco() {
    return GeneratedTextColumn(
        'cao_nao_vinculo_de_parantesco', $tableName, true,
        maxTextLength: 50);
  }

  final VerificationMeta _idadeDoChefeDaFamiliaMeta =
      const VerificationMeta('idadeDoChefeDaFamilia');
  GeneratedTextColumn _idadeDoChefeDaFamilia;
  @override
  GeneratedTextColumn get idadeDoChefeDaFamilia =>
      _idadeDoChefeDaFamilia ??= _constructIdadeDoChefeDaFamilia();
  GeneratedTextColumn _constructIdadeDoChefeDaFamilia() {
    return GeneratedTextColumn(
      'idade_do_chefe_da_familia',
      $tableName,
      true,
    );
  }

  final VerificationMeta _estudouMeta = const VerificationMeta('estudou');
  GeneratedTextColumn _estudou;
  @override
  GeneratedTextColumn get estudou => _estudou ??= _constructEstudou();
  GeneratedTextColumn _constructEstudou() {
    return GeneratedTextColumn(
      'estudou',
      $tableName,
      true,
    );
  }

  final VerificationMeta _eAlfabetizadoMeta =
      const VerificationMeta('eAlfabetizado');
  GeneratedTextColumn _eAlfabetizado;
  @override
  GeneratedTextColumn get eAlfabetizado =>
      _eAlfabetizado ??= _constructEAlfabetizado();
  GeneratedTextColumn _constructEAlfabetizado() {
    return GeneratedTextColumn(
      'e_alfabetizado',
      $tableName,
      true,
    );
  }

  final VerificationMeta _completouEnsinoPrimarioMeta =
      const VerificationMeta('completouEnsinoPrimario');
  GeneratedTextColumn _completouEnsinoPrimario;
  @override
  GeneratedTextColumn get completouEnsinoPrimario =>
      _completouEnsinoPrimario ??= _constructCompletouEnsinoPrimario();
  GeneratedTextColumn _constructCompletouEnsinoPrimario() {
    return GeneratedTextColumn(
      'completou_ensino_primario',
      $tableName,
      true,
    );
  }

  final VerificationMeta _completouEnsinoSecundarioMeta =
      const VerificationMeta('completouEnsinoSecundario');
  GeneratedTextColumn _completouEnsinoSecundario;
  @override
  GeneratedTextColumn get completouEnsinoSecundario =>
      _completouEnsinoSecundario ??= _constructCompletouEnsinoSecundario();
  GeneratedTextColumn _constructCompletouEnsinoSecundario() {
    return GeneratedTextColumn(
      'completou_ensino_secundario',
      $tableName,
      true,
    );
  }

  final VerificationMeta _fezCursoSuperiorMeta =
      const VerificationMeta('fezCursoSuperior');
  GeneratedTextColumn _fezCursoSuperior;
  @override
  GeneratedTextColumn get fezCursoSuperior =>
      _fezCursoSuperior ??= _constructFezCursoSuperior();
  GeneratedTextColumn _constructFezCursoSuperior() {
    return GeneratedTextColumn(
      'fez_curso_superior',
      $tableName,
      true,
    );
  }

  final VerificationMeta _naoSabeMeta = const VerificationMeta('naoSabe');
  GeneratedTextColumn _naoSabe;
  @override
  GeneratedTextColumn get naoSabe => _naoSabe ??= _constructNaoSabe();
  GeneratedTextColumn _constructNaoSabe() {
    return GeneratedTextColumn(
      'nao_sabe',
      $tableName,
      true,
    );
  }

  final VerificationMeta _chefeDeFamiliaTemSalarioMeta =
      const VerificationMeta('chefeDeFamiliaTemSalario');
  GeneratedTextColumn _chefeDeFamiliaTemSalario;
  @override
  GeneratedTextColumn get chefeDeFamiliaTemSalario =>
      _chefeDeFamiliaTemSalario ??= _constructChefeDeFamiliaTemSalario();
  GeneratedTextColumn _constructChefeDeFamiliaTemSalario() {
    return GeneratedTextColumn(
      'chefe_de_familia_tem_salario',
      $tableName,
      true,
    );
  }

  final VerificationMeta _quantoGastaParaSustentarFamiliaPorMesMeta =
      const VerificationMeta('quantoGastaParaSustentarFamiliaPorMes');
  GeneratedTextColumn _quantoGastaParaSustentarFamiliaPorMes;
  @override
  GeneratedTextColumn get quantoGastaParaSustentarFamiliaPorMes =>
      _quantoGastaParaSustentarFamiliaPorMes ??=
          _constructQuantoGastaParaSustentarFamiliaPorMes();
  GeneratedTextColumn _constructQuantoGastaParaSustentarFamiliaPorMes() {
    return GeneratedTextColumn(
      'quanto_gasta_para_sustentar_familia_por_mes',
      $tableName,
      true,
    );
  }

  final VerificationMeta _quantasFamiliasResidemNacasaMeta =
      const VerificationMeta('quantasFamiliasResidemNacasa');
  GeneratedTextColumn _quantasFamiliasResidemNacasa;
  @override
  GeneratedTextColumn get quantasFamiliasResidemNacasa =>
      _quantasFamiliasResidemNacasa ??=
          _constructQuantasFamiliasResidemNacasa();
  GeneratedTextColumn _constructQuantasFamiliasResidemNacasa() {
    return GeneratedTextColumn(
      'quantas_familias_residem_nacasa',
      $tableName,
      true,
    );
  }

  final VerificationMeta _quantosAdultosMeta =
      const VerificationMeta('quantosAdultos');
  GeneratedTextColumn _quantosAdultos;
  @override
  GeneratedTextColumn get quantosAdultos =>
      _quantosAdultos ??= _constructQuantosAdultos();
  GeneratedTextColumn _constructQuantosAdultos() {
    return GeneratedTextColumn(
      'quantos_adultos',
      $tableName,
      true,
    );
  }

  final VerificationMeta _quantosAdultosMaioresDe18anosMeta =
      const VerificationMeta('quantosAdultosMaioresDe18anos');
  GeneratedTextColumn _quantosAdultosMaioresDe18anos;
  @override
  GeneratedTextColumn get quantosAdultosMaioresDe18anos =>
      _quantosAdultosMaioresDe18anos ??=
          _constructQuantosAdultosMaioresDe18anos();
  GeneratedTextColumn _constructQuantosAdultosMaioresDe18anos() {
    return GeneratedTextColumn(
      'quantos_adultos_maiores_de18anos',
      $tableName,
      true,
    );
  }

  final VerificationMeta _quantosAdultosMaioresDe18anosHomensMeta =
      const VerificationMeta('quantosAdultosMaioresDe18anosHomens');
  GeneratedTextColumn _quantosAdultosMaioresDe18anosHomens;
  @override
  GeneratedTextColumn get quantosAdultosMaioresDe18anosHomens =>
      _quantosAdultosMaioresDe18anosHomens ??=
          _constructQuantosAdultosMaioresDe18anosHomens();
  GeneratedTextColumn _constructQuantosAdultosMaioresDe18anosHomens() {
    return GeneratedTextColumn(
      'quantos_adultos_maiores_de18anos_homens',
      $tableName,
      true,
    );
  }

  final VerificationMeta _quantosAdultosMaioresDe18anosHomensTrabalhamMeta =
      const VerificationMeta('quantosAdultosMaioresDe18anosHomensTrabalham');
  GeneratedTextColumn _quantosAdultosMaioresDe18anosHomensTrabalham;
  @override
  GeneratedTextColumn get quantosAdultosMaioresDe18anosHomensTrabalham =>
      _quantosAdultosMaioresDe18anosHomensTrabalham ??=
          _constructQuantosAdultosMaioresDe18anosHomensTrabalham();
  GeneratedTextColumn _constructQuantosAdultosMaioresDe18anosHomensTrabalham() {
    return GeneratedTextColumn(
      'quantos_adultos_maiores_de18anos_homens_trabalham',
      $tableName,
      true,
    );
  }

  final VerificationMeta _quantosAdultosMaioresDe18anosHomensAlfabetizadosMeta =
      const VerificationMeta(
          'quantosAdultosMaioresDe18anosHomensAlfabetizados');
  GeneratedTextColumn _quantosAdultosMaioresDe18anosHomensAlfabetizados;
  @override
  GeneratedTextColumn get quantosAdultosMaioresDe18anosHomensAlfabetizados =>
      _quantosAdultosMaioresDe18anosHomensAlfabetizados ??=
          _constructQuantosAdultosMaioresDe18anosHomensAlfabetizados();
  GeneratedTextColumn
      _constructQuantosAdultosMaioresDe18anosHomensAlfabetizados() {
    return GeneratedTextColumn(
      'quantos_adultos_maiores_de18anos_homens_alfabetizados',
      $tableName,
      true,
    );
  }

  final VerificationMeta _quantosAdultosM18MulheresMeta =
      const VerificationMeta('quantosAdultosM18Mulheres');
  GeneratedTextColumn _quantosAdultosM18Mulheres;
  @override
  GeneratedTextColumn get quantosAdultosM18Mulheres =>
      _quantosAdultosM18Mulheres ??= _constructQuantosAdultosM18Mulheres();
  GeneratedTextColumn _constructQuantosAdultosM18Mulheres() {
    return GeneratedTextColumn(
      'quantos_adultos_m18_mulheres',
      $tableName,
      true,
    );
  }

  final VerificationMeta _quantosAdultosM18MulheresTrabalhamMeta =
      const VerificationMeta('quantosAdultosM18MulheresTrabalham');
  GeneratedTextColumn _quantosAdultosM18MulheresTrabalham;
  @override
  GeneratedTextColumn get quantosAdultosM18MulheresTrabalham =>
      _quantosAdultosM18MulheresTrabalham ??=
          _constructQuantosAdultosM18MulheresTrabalham();
  GeneratedTextColumn _constructQuantosAdultosM18MulheresTrabalham() {
    return GeneratedTextColumn(
      'quantos_adultos_m18_mulheres_trabalham',
      $tableName,
      true,
    );
  }

  final VerificationMeta _quantosAdultosM18MulheresAlfabetizadosMeta =
      const VerificationMeta('quantosAdultosM18MulheresAlfabetizados');
  GeneratedTextColumn _quantosAdultosM18MulheresAlfabetizados;
  @override
  GeneratedTextColumn get quantosAdultosM18MulheresAlfabetizados =>
      _quantosAdultosM18MulheresAlfabetizados ??=
          _constructQuantosAdultosM18MulheresAlfabetizados();
  GeneratedTextColumn _constructQuantosAdultosM18MulheresAlfabetizados() {
    return GeneratedTextColumn(
      'quantos_adultos_m18_mulheres_alfabetizados',
      $tableName,
      true,
    );
  }

  final VerificationMeta _relatosDeDeficienciaMeta =
      const VerificationMeta('relatosDeDeficiencia');
  GeneratedTextColumn _relatosDeDeficiencia;
  @override
  GeneratedTextColumn get relatosDeDeficiencia =>
      _relatosDeDeficiencia ??= _constructRelatosDeDeficiencia();
  GeneratedTextColumn _constructRelatosDeDeficiencia() {
    return GeneratedTextColumn(
      'relatos_de_deficiencia',
      $tableName,
      true,
    );
  }

  final VerificationMeta _totalcriancasAte1anoemeioMeta =
      const VerificationMeta('totalcriancasAte1anoemeio');
  GeneratedTextColumn _totalcriancasAte1anoemeio;
  @override
  GeneratedTextColumn get totalcriancasAte1anoemeio =>
      _totalcriancasAte1anoemeio ??= _constructTotalcriancasAte1anoemeio();
  GeneratedTextColumn _constructTotalcriancasAte1anoemeio() {
    return GeneratedTextColumn(
      'totalcriancas_ate1anoemeio',
      $tableName,
      true,
    );
  }

  final VerificationMeta _totalcriancasAte1anoemeioMeninosMeta =
      const VerificationMeta('totalcriancasAte1anoemeioMeninos');
  GeneratedTextColumn _totalcriancasAte1anoemeioMeninos;
  @override
  GeneratedTextColumn get totalcriancasAte1anoemeioMeninos =>
      _totalcriancasAte1anoemeioMeninos ??=
          _constructTotalcriancasAte1anoemeioMeninos();
  GeneratedTextColumn _constructTotalcriancasAte1anoemeioMeninos() {
    return GeneratedTextColumn(
      'totalcriancas_ate1anoemeio_meninos',
      $tableName,
      true,
    );
  }

  final VerificationMeta _totalcriancasAte1anoemeioMeninasMeta =
      const VerificationMeta('totalcriancasAte1anoemeioMeninas');
  GeneratedTextColumn _totalcriancasAte1anoemeioMeninas;
  @override
  GeneratedTextColumn get totalcriancasAte1anoemeioMeninas =>
      _totalcriancasAte1anoemeioMeninas ??=
          _constructTotalcriancasAte1anoemeioMeninas();
  GeneratedTextColumn _constructTotalcriancasAte1anoemeioMeninas() {
    return GeneratedTextColumn(
      'totalcriancas_ate1anoemeio_meninas',
      $tableName,
      true,
    );
  }

  final VerificationMeta _algumasCriancasAte1anoemeioNaodesenvolvemMeta =
      const VerificationMeta('algumasCriancasAte1anoemeioNaodesenvolvem');
  GeneratedTextColumn _algumasCriancasAte1anoemeioNaodesenvolvem;
  @override
  GeneratedTextColumn get algumasCriancasAte1anoemeioNaodesenvolvem =>
      _algumasCriancasAte1anoemeioNaodesenvolvem ??=
          _constructAlgumasCriancasAte1anoemeioNaodesenvolvem();
  GeneratedTextColumn _constructAlgumasCriancasAte1anoemeioNaodesenvolvem() {
    return GeneratedTextColumn(
      'algumas_criancas_ate1anoemeio_naodesenvolvem',
      $tableName,
      true,
    );
  }

  final VerificationMeta _algumasCriancasAte1anoemeioNaodesenvolvemQuantasMeta =
      const VerificationMeta(
          'algumasCriancasAte1anoemeioNaodesenvolvemQuantas');
  GeneratedTextColumn _algumasCriancasAte1anoemeioNaodesenvolvemQuantas;
  @override
  GeneratedTextColumn get algumasCriancasAte1anoemeioNaodesenvolvemQuantas =>
      _algumasCriancasAte1anoemeioNaodesenvolvemQuantas ??=
          _constructAlgumasCriancasAte1anoemeioNaodesenvolvemQuantas();
  GeneratedTextColumn
      _constructAlgumasCriancasAte1anoemeioNaodesenvolvemQuantas() {
    return GeneratedTextColumn(
      'algumas_criancas_ate1anoemeio_naodesenvolvem_quantas',
      $tableName,
      true,
    );
  }

  final VerificationMeta _algumaCriancasAte1anoemeioQueTipodeProblemaMeta =
      const VerificationMeta('algumaCriancasAte1anoemeioQueTipodeProblema');
  GeneratedTextColumn _algumaCriancasAte1anoemeioQueTipodeProblema;
  @override
  GeneratedTextColumn get algumaCriancasAte1anoemeioQueTipodeProblema =>
      _algumaCriancasAte1anoemeioQueTipodeProblema ??=
          _constructAlgumaCriancasAte1anoemeioQueTipodeProblema();
  GeneratedTextColumn _constructAlgumaCriancasAte1anoemeioQueTipodeProblema() {
    return GeneratedTextColumn(
      'alguma_criancas_ate1anoemeio_que_tipode_problema',
      $tableName,
      true,
    );
  }

  final VerificationMeta _algumaCriancasAte1anoemeioTemProblemaMotorMeta =
      const VerificationMeta('algumaCriancasAte1anoemeioTemProblemaMotor');
  GeneratedTextColumn _algumaCriancasAte1anoemeioTemProblemaMotor;
  @override
  GeneratedTextColumn get algumaCriancasAte1anoemeioTemProblemaMotor =>
      _algumaCriancasAte1anoemeioTemProblemaMotor ??=
          _constructAlgumaCriancasAte1anoemeioTemProblemaMotor();
  GeneratedTextColumn _constructAlgumaCriancasAte1anoemeioTemProblemaMotor() {
    return GeneratedTextColumn(
      'alguma_criancas_ate1anoemeio_tem_problema_motor',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumaCriancasAte1anoemeioTemProblemaMotorQuantasMeta =
      const VerificationMeta(
          'algumaCriancasAte1anoemeioTemProblemaMotorQuantas');
  GeneratedTextColumn _algumaCriancasAte1anoemeioTemProblemaMotorQuantas;
  @override
  GeneratedTextColumn get algumaCriancasAte1anoemeioTemProblemaMotorQuantas =>
      _algumaCriancasAte1anoemeioTemProblemaMotorQuantas ??=
          _constructAlgumaCriancasAte1anoemeioTemProblemaMotorQuantas();
  GeneratedTextColumn
      _constructAlgumaCriancasAte1anoemeioTemProblemaMotorQuantas() {
    return GeneratedTextColumn(
      'alguma_criancas_ate1anoemeio_tem_problema_motor_quantas',
      $tableName,
      true,
    );
  }

  final VerificationMeta _algumaCriancasAte1anoemeioTemMalFormacaoMeta =
      const VerificationMeta('algumaCriancasAte1anoemeioTemMalFormacao');
  GeneratedTextColumn _algumaCriancasAte1anoemeioTemMalFormacao;
  @override
  GeneratedTextColumn get algumaCriancasAte1anoemeioTemMalFormacao =>
      _algumaCriancasAte1anoemeioTemMalFormacao ??=
          _constructAlgumaCriancasAte1anoemeioTemMalFormacao();
  GeneratedTextColumn _constructAlgumaCriancasAte1anoemeioTemMalFormacao() {
    return GeneratedTextColumn(
      'alguma_criancas_ate1anoemeio_tem_mal_formacao',
      $tableName,
      true,
    );
  }

  final VerificationMeta _algumaCriancasAte1anoemeioTemMalFormacaoQuantasMeta =
      const VerificationMeta('algumaCriancasAte1anoemeioTemMalFormacaoQuantas');
  GeneratedTextColumn _algumaCriancasAte1anoemeioTemMalFormacaoQuantas;
  @override
  GeneratedTextColumn get algumaCriancasAte1anoemeioTemMalFormacaoQuantas =>
      _algumaCriancasAte1anoemeioTemMalFormacaoQuantas ??=
          _constructAlgumaCriancasAte1anoemeioTemMalFormacaoQuantas();
  GeneratedTextColumn
      _constructAlgumaCriancasAte1anoemeioTemMalFormacaoQuantas() {
    return GeneratedTextColumn(
      'alguma_criancas_ate1anoemeio_tem_mal_formacao_quantas',
      $tableName,
      true,
    );
  }

  final VerificationMeta _algumaCriancasAte1anoemeioNaoReagemAIncentivoMeta =
      const VerificationMeta('algumaCriancasAte1anoemeioNaoReagemAIncentivo');
  GeneratedTextColumn _algumaCriancasAte1anoemeioNaoReagemAIncentivo;
  @override
  GeneratedTextColumn get algumaCriancasAte1anoemeioNaoReagemAIncentivo =>
      _algumaCriancasAte1anoemeioNaoReagemAIncentivo ??=
          _constructAlgumaCriancasAte1anoemeioNaoReagemAIncentivo();
  GeneratedTextColumn
      _constructAlgumaCriancasAte1anoemeioNaoReagemAIncentivo() {
    return GeneratedTextColumn(
      'alguma_criancas_ate1anoemeio_nao_reagem_a_incentivo',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantasMeta =
      const VerificationMeta(
          'algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas');
  GeneratedTextColumn _algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas;
  @override
  GeneratedTextColumn
      get algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas =>
          _algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas ??=
              _constructAlgumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas();
  GeneratedTextColumn
      _constructAlgumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas() {
    return GeneratedTextColumn(
      'alguma_criancas_ate1anoemeio_nao_reagem_a_incentivo_quantas',
      $tableName,
      true,
    );
  }

  final VerificationMeta _algumaCriancasAte1anoemeioTemReacaoAgressivaMeta =
      const VerificationMeta('algumaCriancasAte1anoemeioTemReacaoAgressiva');
  GeneratedTextColumn _algumaCriancasAte1anoemeioTemReacaoAgressiva;
  @override
  GeneratedTextColumn get algumaCriancasAte1anoemeioTemReacaoAgressiva =>
      _algumaCriancasAte1anoemeioTemReacaoAgressiva ??=
          _constructAlgumaCriancasAte1anoemeioTemReacaoAgressiva();
  GeneratedTextColumn _constructAlgumaCriancasAte1anoemeioTemReacaoAgressiva() {
    return GeneratedTextColumn(
      'alguma_criancas_ate1anoemeio_tem_reacao_agressiva',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumaCriancasAte1anoemeioTemReacaoAgressivaQuantasMeta =
      const VerificationMeta(
          'algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas');
  GeneratedTextColumn _algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas;
  @override
  GeneratedTextColumn get algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas =>
      _algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas ??=
          _constructAlgumaCriancasAte1anoemeioTemReacaoAgressivaQuantas();
  GeneratedTextColumn
      _constructAlgumaCriancasAte1anoemeioTemReacaoAgressivaQuantas() {
    return GeneratedTextColumn(
      'alguma_criancas_ate1anoemeio_tem_reacao_agressiva_quantas',
      $tableName,
      true,
    );
  }

  final VerificationMeta _totalcriancasAte1anoemeioA3anosMeta =
      const VerificationMeta('totalcriancasAte1anoemeioA3anos');
  GeneratedTextColumn _totalcriancasAte1anoemeioA3anos;
  @override
  GeneratedTextColumn get totalcriancasAte1anoemeioA3anos =>
      _totalcriancasAte1anoemeioA3anos ??=
          _constructTotalcriancasAte1anoemeioA3anos();
  GeneratedTextColumn _constructTotalcriancasAte1anoemeioA3anos() {
    return GeneratedTextColumn(
      'totalcriancas_ate1anoemeio_a3anos',
      $tableName,
      true,
    );
  }

  final VerificationMeta _totalcriancasAte1anoemeioA3anosMeninosMeta =
      const VerificationMeta('totalcriancasAte1anoemeioA3anosMeninos');
  GeneratedTextColumn _totalcriancasAte1anoemeioA3anosMeninos;
  @override
  GeneratedTextColumn get totalcriancasAte1anoemeioA3anosMeninos =>
      _totalcriancasAte1anoemeioA3anosMeninos ??=
          _constructTotalcriancasAte1anoemeioA3anosMeninos();
  GeneratedTextColumn _constructTotalcriancasAte1anoemeioA3anosMeninos() {
    return GeneratedTextColumn(
      'totalcriancas_ate1anoemeio_a3anos_meninos',
      $tableName,
      true,
    );
  }

  final VerificationMeta _totalcriancasAte1anoemeioA3anosMeninasMeta =
      const VerificationMeta('totalcriancasAte1anoemeioA3anosMeninas');
  GeneratedTextColumn _totalcriancasAte1anoemeioA3anosMeninas;
  @override
  GeneratedTextColumn get totalcriancasAte1anoemeioA3anosMeninas =>
      _totalcriancasAte1anoemeioA3anosMeninas ??=
          _constructTotalcriancasAte1anoemeioA3anosMeninas();
  GeneratedTextColumn _constructTotalcriancasAte1anoemeioA3anosMeninas() {
    return GeneratedTextColumn(
      'totalcriancas_ate1anoemeio_a3anos_meninas',
      $tableName,
      true,
    );
  }

  final VerificationMeta _algumasCriancasAte1anoemeioA3anosNaodesenvolvemMeta =
      const VerificationMeta('algumasCriancasAte1anoemeioA3anosNaodesenvolvem');
  GeneratedTextColumn _algumasCriancasAte1anoemeioA3anosNaodesenvolvem;
  @override
  GeneratedTextColumn get algumasCriancasAte1anoemeioA3anosNaodesenvolvem =>
      _algumasCriancasAte1anoemeioA3anosNaodesenvolvem ??=
          _constructAlgumasCriancasAte1anoemeioA3anosNaodesenvolvem();
  GeneratedTextColumn
      _constructAlgumasCriancasAte1anoemeioA3anosNaodesenvolvem() {
    return GeneratedTextColumn(
      'algumas_criancas_ate1anoemeio_a3anos_naodesenvolvem',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantasMeta =
      const VerificationMeta(
          'algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas');
  GeneratedTextColumn _algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas;
  @override
  GeneratedTextColumn
      get algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas =>
          _algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas ??=
              _constructAlgumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas();
  GeneratedTextColumn
      _constructAlgumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas() {
    return GeneratedTextColumn(
      'algumas_criancas_ate1anoemeio_a3anos_naodesenvolvem_quantas',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumaCriancasAte1anoemeioA3anosQueTipodeProblemaMeta =
      const VerificationMeta(
          'algumaCriancasAte1anoemeioA3anosQueTipodeProblema');
  GeneratedTextColumn _algumaCriancasAte1anoemeioA3anosQueTipodeProblema;
  @override
  GeneratedTextColumn get algumaCriancasAte1anoemeioA3anosQueTipodeProblema =>
      _algumaCriancasAte1anoemeioA3anosQueTipodeProblema ??=
          _constructAlgumaCriancasAte1anoemeioA3anosQueTipodeProblema();
  GeneratedTextColumn
      _constructAlgumaCriancasAte1anoemeioA3anosQueTipodeProblema() {
    return GeneratedTextColumn(
      'alguma_criancas_ate1anoemeio_a3anos_que_tipode_problema',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasMeta =
      const VerificationMeta(
          'algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas');
  GeneratedTextColumn _algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas;
  @override
  GeneratedTextColumn
      get algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas =>
          _algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas ??=
              _constructAlgumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas();
  GeneratedTextColumn
      _constructAlgumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas() {
    return GeneratedTextColumn(
      'algumas_criancas_ate1anoemeio_a3anos_nao_interagem_com_pessoas',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantasMeta =
      const VerificationMeta(
          'algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas');
  GeneratedTextColumn
      _algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas;
  @override
  GeneratedTextColumn
      get algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas =>
          _algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas ??=
              _constructAlgumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas();
  GeneratedTextColumn
      _constructAlgumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas() {
    return GeneratedTextColumn(
      'algumas_criancas_ate1anoemeio_a3anos_nao_interagem_com_pessoas_quantas',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoMeta =
      const VerificationMeta(
          'algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo');
  GeneratedTextColumn
      _algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo;
  @override
  GeneratedTextColumn
      get algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo =>
          _algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo ??=
              _constructAlgumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo();
  GeneratedTextColumn
      _constructAlgumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo() {
    return GeneratedTextColumn(
      'algumas_criancas_ate1anoemeio_a3anos_tem_comportamento_agressivo',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantasMeta =
      const VerificationMeta(
          'algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas');
  GeneratedTextColumn
      _algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas;
  @override
  GeneratedTextColumn
      get algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas =>
          _algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas ??=
              _constructAlgumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas();
  GeneratedTextColumn
      _constructAlgumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas() {
    return GeneratedTextColumn(
      'algumas_criancas_ate1anoemeio_a3anos_tem_comportamento_agressivo_quantas',
      $tableName,
      true,
    );
  }

  final VerificationMeta _algumasCriancasAte1anoemeioA3anosAlgunsAgridemMeta =
      const VerificationMeta('algumasCriancasAte1anoemeioA3anosAlgunsAgridem');
  GeneratedTextColumn _algumasCriancasAte1anoemeioA3anosAlgunsAgridem;
  @override
  GeneratedTextColumn get algumasCriancasAte1anoemeioA3anosAlgunsAgridem =>
      _algumasCriancasAte1anoemeioA3anosAlgunsAgridem ??=
          _constructAlgumasCriancasAte1anoemeioA3anosAlgunsAgridem();
  GeneratedTextColumn
      _constructAlgumasCriancasAte1anoemeioA3anosAlgunsAgridem() {
    return GeneratedTextColumn(
      'algumas_criancas_ate1anoemeio_a3anos_alguns_agridem',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantasMeta =
      const VerificationMeta(
          'algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas');
  GeneratedTextColumn _algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas;
  @override
  GeneratedTextColumn
      get algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas =>
          _algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas ??=
              _constructAlgumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas();
  GeneratedTextColumn
      _constructAlgumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas() {
    return GeneratedTextColumn(
      'algumas_criancas_ate1anoemeio_a3anos_alguns_agridem_quantas',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalMeta =
      const VerificationMeta(
          'algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal');
  GeneratedTextColumn
      _algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal;
  @override
  GeneratedTextColumn
      get algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal =>
          _algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal ??=
              _constructAlgumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal();
  GeneratedTextColumn
      _constructAlgumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal() {
    return GeneratedTextColumn(
      'algumas_criancas_ate1anoemeio_a3anos_algumas_agitadas_que_o_normal',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantasMeta =
      const VerificationMeta(
          'algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas');
  GeneratedTextColumn
      _algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas;
  @override
  GeneratedTextColumn
      get algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas =>
          _algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas ??=
              _constructAlgumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas();
  GeneratedTextColumn
      _constructAlgumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas() {
    return GeneratedTextColumn(
      'algumas_criancas_ate1anoemeio_a3anos_algumas_agitadas_que_o_normal_quantas',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoMeta =
      const VerificationMeta(
          'algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao');
  GeneratedTextColumn _algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao;
  @override
  GeneratedTextColumn
      get algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao =>
          _algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao ??=
              _constructAlgumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao();
  GeneratedTextColumn
      _constructAlgumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao() {
    return GeneratedTextColumn(
      'algumas_criancas_ate1anoemeio_a3anos_algumas_tem_mal_formacao',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantasMeta =
      const VerificationMeta(
          'algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas');
  GeneratedTextColumn
      _algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas;
  @override
  GeneratedTextColumn
      get algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas =>
          _algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas ??=
              _constructAlgumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas();
  GeneratedTextColumn
      _constructAlgumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas() {
    return GeneratedTextColumn(
      'algumas_criancas_ate1anoemeio_a3anos_algumas_tem_mal_formacao_quantas',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasMeta =
      const VerificationMeta(
          'algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas');
  GeneratedTextColumn _algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas;
  @override
  GeneratedTextColumn
      get algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas =>
          _algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas ??=
              _constructAlgumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas();
  GeneratedTextColumn
      _constructAlgumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas() {
    return GeneratedTextColumn(
      'algumas_criancas_ate1anoemeio_a3anos_algumas_nao_andam_sozinhas',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantasMeta =
      const VerificationMeta(
          'algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas');
  GeneratedTextColumn
      _algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas;
  @override
  GeneratedTextColumn
      get algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas =>
          _algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas ??=
              _constructAlgumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas();
  GeneratedTextColumn
      _constructAlgumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas() {
    return GeneratedTextColumn(
      'algumas_criancas_ate1anoemeio_a3anos_algumas_nao_andam_sozinhas_quantas',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteMeta =
      const VerificationMeta(
          'algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente');
  GeneratedTextColumn
      _algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente;
  @override
  GeneratedTextColumn
      get algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente =>
          _algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente ??=
              _constructAlgumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente();
  GeneratedTextColumn
      _constructAlgumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente() {
    return GeneratedTextColumn(
      'algumas_criancas_ate1anoemeio_a3anos_algumas_nao_expressam_verbalmente',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantasMeta =
      const VerificationMeta(
          'algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas');
  GeneratedTextColumn
      _algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas;
  @override
  GeneratedTextColumn
      get algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas =>
          _algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas ??=
              _constructAlgumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas();
  GeneratedTextColumn
      _constructAlgumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas() {
    return GeneratedTextColumn(
      'algumas_criancas_ate1anoemeio_a3anos_algumas_nao_expressam_verbalmente_quantas',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosMeta =
      const VerificationMeta(
          'algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos');
  GeneratedTextColumn
      _algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos;
  @override
  GeneratedTextColumn
      get algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos =>
          _algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos ??=
              _constructAlgumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos();
  GeneratedTextColumn
      _constructAlgumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos() {
    return GeneratedTextColumn(
      'algumas_criancas_ate1anoemeio_a3anos_algumas_nao_expressam_sentimentos',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantasMeta =
      const VerificationMeta(
          'algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas');
  GeneratedTextColumn
      _algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas;
  @override
  GeneratedTextColumn
      get algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas =>
          _algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas ??=
              _constructAlgumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas();
  GeneratedTextColumn
      _constructAlgumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas() {
    return GeneratedTextColumn(
      'algumas_criancas_ate1anoemeio_a3anos_algumas_nao_expressam_sentimentos_quantas',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownMeta =
      const VerificationMeta(
          'algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown');
  GeneratedTextColumn
      _algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown;
  @override
  GeneratedTextColumn
      get algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown =>
          _algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown ??=
              _constructAlgumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown();
  GeneratedTextColumn
      _constructAlgumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown() {
    return GeneratedTextColumn(
      'algumas_criancas_ate1anoemeio_a3anos_algumas_tem_sindrome_de_down',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantasMeta =
      const VerificationMeta(
          'algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas');
  GeneratedTextColumn
      _algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas;
  @override
  GeneratedTextColumn
      get algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas =>
          _algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas ??=
              _constructAlgumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas();
  GeneratedTextColumn
      _constructAlgumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas() {
    return GeneratedTextColumn(
      'algumas_criancas_ate1anoemeio_a3anos_algumas_tem_sindrome_de_down_quantas',
      $tableName,
      true,
    );
  }

  final VerificationMeta _totalcriancasde3A6anosMeta =
      const VerificationMeta('totalcriancasde3A6anos');
  GeneratedTextColumn _totalcriancasde3A6anos;
  @override
  GeneratedTextColumn get totalcriancasde3A6anos =>
      _totalcriancasde3A6anos ??= _constructTotalcriancasde3A6anos();
  GeneratedTextColumn _constructTotalcriancasde3A6anos() {
    return GeneratedTextColumn(
      'totalcriancasde3_a6anos',
      $tableName,
      true,
    );
  }

  final VerificationMeta _totalcriancasde3A6anosMeninosMeta =
      const VerificationMeta('totalcriancasde3A6anosMeninos');
  GeneratedTextColumn _totalcriancasde3A6anosMeninos;
  @override
  GeneratedTextColumn get totalcriancasde3A6anosMeninos =>
      _totalcriancasde3A6anosMeninos ??=
          _constructTotalcriancasde3A6anosMeninos();
  GeneratedTextColumn _constructTotalcriancasde3A6anosMeninos() {
    return GeneratedTextColumn(
      'totalcriancasde3_a6anos_meninos',
      $tableName,
      true,
    );
  }

  final VerificationMeta _totalcriancasde3A6anosMeninasMeta =
      const VerificationMeta('totalcriancasde3A6anosMeninas');
  GeneratedTextColumn _totalcriancasde3A6anosMeninas;
  @override
  GeneratedTextColumn get totalcriancasde3A6anosMeninas =>
      _totalcriancasde3A6anosMeninas ??=
          _constructTotalcriancasde3A6anosMeninas();
  GeneratedTextColumn _constructTotalcriancasde3A6anosMeninas() {
    return GeneratedTextColumn(
      'totalcriancasde3_a6anos_meninas',
      $tableName,
      true,
    );
  }

  final VerificationMeta _algumasCriancasde3A6anosNaodesenvolvemMeta =
      const VerificationMeta('algumasCriancasde3A6anosNaodesenvolvem');
  GeneratedTextColumn _algumasCriancasde3A6anosNaodesenvolvem;
  @override
  GeneratedTextColumn get algumasCriancasde3A6anosNaodesenvolvem =>
      _algumasCriancasde3A6anosNaodesenvolvem ??=
          _constructAlgumasCriancasde3A6anosNaodesenvolvem();
  GeneratedTextColumn _constructAlgumasCriancasde3A6anosNaodesenvolvem() {
    return GeneratedTextColumn(
      'algumas_criancasde3_a6anos_naodesenvolvem',
      $tableName,
      true,
    );
  }

  final VerificationMeta _algumasCriancasde3A6anosNaodesenvolvemQuantasMeta =
      const VerificationMeta('algumasCriancasde3A6anosNaodesenvolvemQuantas');
  GeneratedTextColumn _algumasCriancasde3A6anosNaodesenvolvemQuantas;
  @override
  GeneratedTextColumn get algumasCriancasde3A6anosNaodesenvolvemQuantas =>
      _algumasCriancasde3A6anosNaodesenvolvemQuantas ??=
          _constructAlgumasCriancasde3A6anosNaodesenvolvemQuantas();
  GeneratedTextColumn
      _constructAlgumasCriancasde3A6anosNaodesenvolvemQuantas() {
    return GeneratedTextColumn(
      'algumas_criancasde3_a6anos_naodesenvolvem_quantas',
      $tableName,
      true,
    );
  }

  final VerificationMeta _algumaCriancasde3A6anosQueTipodeProblemaMeta =
      const VerificationMeta('algumaCriancasde3A6anosQueTipodeProblema');
  GeneratedTextColumn _algumaCriancasde3A6anosQueTipodeProblema;
  @override
  GeneratedTextColumn get algumaCriancasde3A6anosQueTipodeProblema =>
      _algumaCriancasde3A6anosQueTipodeProblema ??=
          _constructAlgumaCriancasde3A6anosQueTipodeProblema();
  GeneratedTextColumn _constructAlgumaCriancasde3A6anosQueTipodeProblema() {
    return GeneratedTextColumn(
      'alguma_criancasde3_a6anos_que_tipode_problema',
      $tableName,
      true,
    );
  }

  final VerificationMeta _algumasCriancasde3A6anosNaoInteragemComPessoasMeta =
      const VerificationMeta('algumasCriancasde3A6anosNaoInteragemComPessoas');
  GeneratedTextColumn _algumasCriancasde3A6anosNaoInteragemComPessoas;
  @override
  GeneratedTextColumn get algumasCriancasde3A6anosNaoInteragemComPessoas =>
      _algumasCriancasde3A6anosNaoInteragemComPessoas ??=
          _constructAlgumasCriancasde3A6anosNaoInteragemComPessoas();
  GeneratedTextColumn
      _constructAlgumasCriancasde3A6anosNaoInteragemComPessoas() {
    return GeneratedTextColumn(
      'algumas_criancasde3_a6anos_nao_interagem_com_pessoas',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasde3A6anosNaoInteragemComPessoasQuantasMeta =
      const VerificationMeta(
          'algumasCriancasde3A6anosNaoInteragemComPessoasQuantas');
  GeneratedTextColumn _algumasCriancasde3A6anosNaoInteragemComPessoasQuantas;
  @override
  GeneratedTextColumn
      get algumasCriancasde3A6anosNaoInteragemComPessoasQuantas =>
          _algumasCriancasde3A6anosNaoInteragemComPessoasQuantas ??=
              _constructAlgumasCriancasde3A6anosNaoInteragemComPessoasQuantas();
  GeneratedTextColumn
      _constructAlgumasCriancasde3A6anosNaoInteragemComPessoasQuantas() {
    return GeneratedTextColumn(
      'algumas_criancasde3_a6anos_nao_interagem_com_pessoas_quantas',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasde3A6anosTemComportamentoAgressivoMeta =
      const VerificationMeta(
          'algumasCriancasde3A6anosTemComportamentoAgressivo');
  GeneratedTextColumn _algumasCriancasde3A6anosTemComportamentoAgressivo;
  @override
  GeneratedTextColumn get algumasCriancasde3A6anosTemComportamentoAgressivo =>
      _algumasCriancasde3A6anosTemComportamentoAgressivo ??=
          _constructAlgumasCriancasde3A6anosTemComportamentoAgressivo();
  GeneratedTextColumn
      _constructAlgumasCriancasde3A6anosTemComportamentoAgressivo() {
    return GeneratedTextColumn(
      'algumas_criancasde3_a6anos_tem_comportamento_agressivo',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasde3A6anosTemComportamentoAgressivoQuantasMeta =
      const VerificationMeta(
          'algumasCriancasde3A6anosTemComportamentoAgressivoQuantas');
  GeneratedTextColumn _algumasCriancasde3A6anosTemComportamentoAgressivoQuantas;
  @override
  GeneratedTextColumn
      get algumasCriancasde3A6anosTemComportamentoAgressivoQuantas =>
          _algumasCriancasde3A6anosTemComportamentoAgressivoQuantas ??=
              _constructAlgumasCriancasde3A6anosTemComportamentoAgressivoQuantas();
  GeneratedTextColumn
      _constructAlgumasCriancasde3A6anosTemComportamentoAgressivoQuantas() {
    return GeneratedTextColumn(
      'algumas_criancasde3_a6anos_tem_comportamento_agressivo_quantas',
      $tableName,
      true,
    );
  }

  final VerificationMeta _algumasCriancasde3A6anosAlgunsAgridemMeta =
      const VerificationMeta('algumasCriancasde3A6anosAlgunsAgridem');
  GeneratedTextColumn _algumasCriancasde3A6anosAlgunsAgridem;
  @override
  GeneratedTextColumn get algumasCriancasde3A6anosAlgunsAgridem =>
      _algumasCriancasde3A6anosAlgunsAgridem ??=
          _constructAlgumasCriancasde3A6anosAlgunsAgridem();
  GeneratedTextColumn _constructAlgumasCriancasde3A6anosAlgunsAgridem() {
    return GeneratedTextColumn(
      'algumas_criancasde3_a6anos_alguns_agridem',
      $tableName,
      true,
    );
  }

  final VerificationMeta _algumasCriancasde3A6anosAlgunsAgridemQuantasMeta =
      const VerificationMeta('algumasCriancasde3A6anosAlgunsAgridemQuantas');
  GeneratedTextColumn _algumasCriancasde3A6anosAlgunsAgridemQuantas;
  @override
  GeneratedTextColumn get algumasCriancasde3A6anosAlgunsAgridemQuantas =>
      _algumasCriancasde3A6anosAlgunsAgridemQuantas ??=
          _constructAlgumasCriancasde3A6anosAlgunsAgridemQuantas();
  GeneratedTextColumn _constructAlgumasCriancasde3A6anosAlgunsAgridemQuantas() {
    return GeneratedTextColumn(
      'algumas_criancasde3_a6anos_alguns_agridem_quantas',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasde3A6anosAlgumasAgitadasQueONormalMeta =
      const VerificationMeta(
          'algumasCriancasde3A6anosAlgumasAgitadasQueONormal');
  GeneratedTextColumn _algumasCriancasde3A6anosAlgumasAgitadasQueONormal;
  @override
  GeneratedTextColumn get algumasCriancasde3A6anosAlgumasAgitadasQueONormal =>
      _algumasCriancasde3A6anosAlgumasAgitadasQueONormal ??=
          _constructAlgumasCriancasde3A6anosAlgumasAgitadasQueONormal();
  GeneratedTextColumn
      _constructAlgumasCriancasde3A6anosAlgumasAgitadasQueONormal() {
    return GeneratedTextColumn(
      'algumas_criancasde3_a6anos_algumas_agitadas_que_o_normal',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantasMeta =
      const VerificationMeta(
          'algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas');
  GeneratedTextColumn _algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas;
  @override
  GeneratedTextColumn
      get algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas =>
          _algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas ??=
              _constructAlgumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas();
  GeneratedTextColumn
      _constructAlgumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas() {
    return GeneratedTextColumn(
      'algumas_criancasde3_a6anos_algumas_agitadas_que_o_normal_quantas',
      $tableName,
      true,
    );
  }

  final VerificationMeta _algumasCriancasde3A6anosAlgumasTemMalFormacaoMeta =
      const VerificationMeta('algumasCriancasde3A6anosAlgumasTemMalFormacao');
  GeneratedTextColumn _algumasCriancasde3A6anosAlgumasTemMalFormacao;
  @override
  GeneratedTextColumn get algumasCriancasde3A6anosAlgumasTemMalFormacao =>
      _algumasCriancasde3A6anosAlgumasTemMalFormacao ??=
          _constructAlgumasCriancasde3A6anosAlgumasTemMalFormacao();
  GeneratedTextColumn
      _constructAlgumasCriancasde3A6anosAlgumasTemMalFormacao() {
    return GeneratedTextColumn(
      'algumas_criancasde3_a6anos_algumas_tem_mal_formacao',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantasMeta =
      const VerificationMeta(
          'algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas');
  GeneratedTextColumn _algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas;
  @override
  GeneratedTextColumn
      get algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas =>
          _algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas ??=
              _constructAlgumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas();
  GeneratedTextColumn
      _constructAlgumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas() {
    return GeneratedTextColumn(
      'algumas_criancasde3_a6anos_algumas_tem_mal_formacao_quantas',
      $tableName,
      true,
    );
  }

  final VerificationMeta _algumasCriancasde3A6anosAlgumasNaoAndamSozinhasMeta =
      const VerificationMeta('algumasCriancasde3A6anosAlgumasNaoAndamSozinhas');
  GeneratedTextColumn _algumasCriancasde3A6anosAlgumasNaoAndamSozinhas;
  @override
  GeneratedTextColumn get algumasCriancasde3A6anosAlgumasNaoAndamSozinhas =>
      _algumasCriancasde3A6anosAlgumasNaoAndamSozinhas ??=
          _constructAlgumasCriancasde3A6anosAlgumasNaoAndamSozinhas();
  GeneratedTextColumn
      _constructAlgumasCriancasde3A6anosAlgumasNaoAndamSozinhas() {
    return GeneratedTextColumn(
      'algumas_criancasde3_a6anos_algumas_nao_andam_sozinhas',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantasMeta =
      const VerificationMeta(
          'algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas');
  GeneratedTextColumn _algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas;
  @override
  GeneratedTextColumn
      get algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas =>
          _algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas ??=
              _constructAlgumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas();
  GeneratedTextColumn
      _constructAlgumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas() {
    return GeneratedTextColumn(
      'algumas_criancasde3_a6anos_algumas_nao_andam_sozinhas_quantas',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteMeta =
      const VerificationMeta(
          'algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente');
  GeneratedTextColumn _algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente;
  @override
  GeneratedTextColumn
      get algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente =>
          _algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente ??=
              _constructAlgumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente();
  GeneratedTextColumn
      _constructAlgumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente() {
    return GeneratedTextColumn(
      'algumas_criancasde3_a6anos_algumas_nao_expressam_verbalmente',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantasMeta =
      const VerificationMeta(
          'algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas');
  GeneratedTextColumn
      _algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas;
  @override
  GeneratedTextColumn
      get algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas =>
          _algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas ??=
              _constructAlgumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas();
  GeneratedTextColumn
      _constructAlgumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas() {
    return GeneratedTextColumn(
      'algumas_criancasde3_a6anos_algumas_nao_expressam_verbalmente_quantas',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosMeta =
      const VerificationMeta(
          'algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos');
  GeneratedTextColumn _algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos;
  @override
  GeneratedTextColumn
      get algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos =>
          _algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos ??=
              _constructAlgumasCriancasde3A6anosAlgumasNaoExpressamSentimentos();
  GeneratedTextColumn
      _constructAlgumasCriancasde3A6anosAlgumasNaoExpressamSentimentos() {
    return GeneratedTextColumn(
      'algumas_criancasde3_a6anos_algumas_nao_expressam_sentimentos',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantasMeta =
      const VerificationMeta(
          'algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas');
  GeneratedTextColumn
      _algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas;
  @override
  GeneratedTextColumn
      get algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas =>
          _algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas ??=
              _constructAlgumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas();
  GeneratedTextColumn
      _constructAlgumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas() {
    return GeneratedTextColumn(
      'algumas_criancasde3_a6anos_algumas_nao_expressam_sentimentos_quantas',
      $tableName,
      true,
    );
  }

  final VerificationMeta _algumasCriancasde3A6anosAlgumasTemSindromeDeDownMeta =
      const VerificationMeta(
          'algumasCriancasde3A6anosAlgumasTemSindromeDeDown');
  GeneratedTextColumn _algumasCriancasde3A6anosAlgumasTemSindromeDeDown;
  @override
  GeneratedTextColumn get algumasCriancasde3A6anosAlgumasTemSindromeDeDown =>
      _algumasCriancasde3A6anosAlgumasTemSindromeDeDown ??=
          _constructAlgumasCriancasde3A6anosAlgumasTemSindromeDeDown();
  GeneratedTextColumn
      _constructAlgumasCriancasde3A6anosAlgumasTemSindromeDeDown() {
    return GeneratedTextColumn(
      'algumas_criancasde3_a6anos_algumas_tem_sindrome_de_down',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantasMeta =
      const VerificationMeta(
          'algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas');
  GeneratedTextColumn _algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas;
  @override
  GeneratedTextColumn
      get algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas =>
          _algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas ??=
              _constructAlgumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas();
  GeneratedTextColumn
      _constructAlgumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas() {
    return GeneratedTextColumn(
      'algumas_criancasde3_a6anos_algumas_tem_sindrome_de_down_quantas',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhasMeta =
      const VerificationMeta(
          'algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas');
  GeneratedTextColumn
      _algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas;
  @override
  GeneratedTextColumn
      get algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas =>
          _algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas ??=
              _constructAlgumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas();
  GeneratedTextColumn
      _constructAlgumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas() {
    return GeneratedTextColumn(
      'algumas_criancasde3_a6anos_algumas_nao_sabem_se_alimentar_sozinhas',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantasMeta =
      const VerificationMeta(
          'algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas');
  GeneratedTextColumn
      _algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas;
  @override
  GeneratedTextColumn
      get algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas =>
          _algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas ??=
              _constructAlgumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas();
  GeneratedTextColumn
      _constructAlgumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas() {
    return GeneratedTextColumn(
      'algumas_criancasde3_a6anos_algumas_nao_sabe_se_alimentar_sozinhas_quantas',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasMeta =
      const VerificationMeta(
          'algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas');
  GeneratedTextColumn
      _algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas;
  @override
  GeneratedTextColumn
      get algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas =>
          _algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas ??=
              _constructAlgumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas();
  GeneratedTextColumn
      _constructAlgumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas() {
    return GeneratedTextColumn(
      'algumas_criancasde3_a6anos_algumas_nao_conseguem_tomar_banho_sozinhas',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantasMeta =
      const VerificationMeta(
          'algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas');
  GeneratedTextColumn
      _algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas;
  @override
  GeneratedTextColumn
      get algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas =>
          _algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas ??=
              _constructAlgumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas();
  GeneratedTextColumn
      _constructAlgumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas() {
    return GeneratedTextColumn(
      'algumas_criancasde3_a6anos_algumas_nao_conseguem_tomar_banho_sozinhas_quantas',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasMeta =
      const VerificationMeta(
          'algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas');
  GeneratedTextColumn
      _algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas;
  @override
  GeneratedTextColumn
      get algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas =>
          _algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas ??=
              _constructAlgumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas();
  GeneratedTextColumn
      _constructAlgumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas() {
    return GeneratedTextColumn(
      'algumas_criancasde3_a6anos_algumas_nao_conseguem_ir_a_casa_de_banho_sozinhas',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantasMeta =
      const VerificationMeta(
          'algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas');
  GeneratedTextColumn
      _algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas;
  @override
  GeneratedTextColumn
      get algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas =>
          _algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas ??=
              _constructAlgumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas();
  GeneratedTextColumn
      _constructAlgumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas() {
    return GeneratedTextColumn(
      'algumas_criancasde3_a6anos_algumas_nao_conseguem_ir_a_casa_de_banho_sozinhas_quantas',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasMeta =
      const VerificationMeta(
          'algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas');
  GeneratedTextColumn
      _algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas;
  @override
  GeneratedTextColumn
      get algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas =>
          _algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas ??=
              _constructAlgumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas();
  GeneratedTextColumn
      _constructAlgumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas() {
    return GeneratedTextColumn(
      'algumas_criancasde3_a6anos_algumas_nao_conseguem_se_vestir_sozinhas',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantasMeta =
      const VerificationMeta(
          'algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas');
  GeneratedTextColumn
      _algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas;
  @override
  GeneratedTextColumn
      get algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas =>
          _algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas ??=
              _constructAlgumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas();
  GeneratedTextColumn
      _constructAlgumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas() {
    return GeneratedTextColumn(
      'algumas_criancasde3_a6anos_algumas_nao_conseguem_se_vestir_sozinhas_quantas',
      $tableName,
      true,
    );
  }

  final VerificationMeta _quantasCriancasde3A6anosEstaoNaEscolinhaMeta =
      const VerificationMeta('quantasCriancasde3A6anosEstaoNaEscolinha');
  GeneratedTextColumn _quantasCriancasde3A6anosEstaoNaEscolinha;
  @override
  GeneratedTextColumn get quantasCriancasde3A6anosEstaoNaEscolinha =>
      _quantasCriancasde3A6anosEstaoNaEscolinha ??=
          _constructQuantasCriancasde3A6anosEstaoNaEscolinha();
  GeneratedTextColumn _constructQuantasCriancasde3A6anosEstaoNaEscolinha() {
    return GeneratedTextColumn(
      'quantas_criancasde3_a6anos_estao_na_escolinha',
      $tableName,
      true,
    );
  }

  final VerificationMeta _criancasDe3A6anosNaoEstaoNaEscolinhaPorqueMeta =
      const VerificationMeta('criancasDe3A6anosNaoEstaoNaEscolinhaPorque');
  GeneratedTextColumn _criancasDe3A6anosNaoEstaoNaEscolinhaPorque;
  @override
  GeneratedTextColumn get criancasDe3A6anosNaoEstaoNaEscolinhaPorque =>
      _criancasDe3A6anosNaoEstaoNaEscolinhaPorque ??=
          _constructCriancasDe3A6anosNaoEstaoNaEscolinhaPorque();
  GeneratedTextColumn _constructCriancasDe3A6anosNaoEstaoNaEscolinhaPorque() {
    return GeneratedTextColumn(
      'criancas_de3_a6anos_nao_estao_na_escolinha_porque',
      $tableName,
      true,
    );
  }

  final VerificationMeta _quantasCriancasde3A6anosTrabalhamMeta =
      const VerificationMeta('quantasCriancasde3A6anosTrabalham');
  GeneratedTextColumn _quantasCriancasde3A6anosTrabalham;
  @override
  GeneratedTextColumn get quantasCriancasde3A6anosTrabalham =>
      _quantasCriancasde3A6anosTrabalham ??=
          _constructQuantasCriancasde3A6anosTrabalham();
  GeneratedTextColumn _constructQuantasCriancasde3A6anosTrabalham() {
    return GeneratedTextColumn(
      'quantas_criancasde3_a6anos_trabalham',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivosMeta =
      const VerificationMeta(
          'criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos');
  GeneratedTextColumn _criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos;
  @override
  GeneratedTextColumn get criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos =>
      _criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos ??=
          _constructCriancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos();
  GeneratedTextColumn
      _constructCriancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos() {
    return GeneratedTextColumn(
      'criancas_de3_a6anos_nao_estao_na_escolinha_por_outos_motivos',
      $tableName,
      true,
    );
  }

  final VerificationMeta _totalcriancasde7A9anosMeta =
      const VerificationMeta('totalcriancasde7A9anos');
  GeneratedTextColumn _totalcriancasde7A9anos;
  @override
  GeneratedTextColumn get totalcriancasde7A9anos =>
      _totalcriancasde7A9anos ??= _constructTotalcriancasde7A9anos();
  GeneratedTextColumn _constructTotalcriancasde7A9anos() {
    return GeneratedTextColumn(
      'totalcriancasde7_a9anos',
      $tableName,
      true,
    );
  }

  final VerificationMeta _totalcriancasde7A9anosMeninosMeta =
      const VerificationMeta('totalcriancasde7A9anosMeninos');
  GeneratedTextColumn _totalcriancasde7A9anosMeninos;
  @override
  GeneratedTextColumn get totalcriancasde7A9anosMeninos =>
      _totalcriancasde7A9anosMeninos ??=
          _constructTotalcriancasde7A9anosMeninos();
  GeneratedTextColumn _constructTotalcriancasde7A9anosMeninos() {
    return GeneratedTextColumn(
      'totalcriancasde7_a9anos_meninos',
      $tableName,
      true,
    );
  }

  final VerificationMeta _totalcriancasde7A9anosMeninasMeta =
      const VerificationMeta('totalcriancasde7A9anosMeninas');
  GeneratedTextColumn _totalcriancasde7A9anosMeninas;
  @override
  GeneratedTextColumn get totalcriancasde7A9anosMeninas =>
      _totalcriancasde7A9anosMeninas ??=
          _constructTotalcriancasde7A9anosMeninas();
  GeneratedTextColumn _constructTotalcriancasde7A9anosMeninas() {
    return GeneratedTextColumn(
      'totalcriancasde7_a9anos_meninas',
      $tableName,
      true,
    );
  }

  final VerificationMeta _quantasCriancasde7A9anosEstaoNaEscolaMeta =
      const VerificationMeta('quantasCriancasde7A9anosEstaoNaEscola');
  GeneratedTextColumn _quantasCriancasde7A9anosEstaoNaEscola;
  @override
  GeneratedTextColumn get quantasCriancasde7A9anosEstaoNaEscola =>
      _quantasCriancasde7A9anosEstaoNaEscola ??=
          _constructQuantasCriancasde7A9anosEstaoNaEscola();
  GeneratedTextColumn _constructQuantasCriancasde7A9anosEstaoNaEscola() {
    return GeneratedTextColumn(
      'quantas_criancasde7_a9anos_estao_na_escola',
      $tableName,
      true,
    );
  }

  final VerificationMeta _quantasCriancasde7A9anosTrabalhamMeta =
      const VerificationMeta('quantasCriancasde7A9anosTrabalham');
  GeneratedTextColumn _quantasCriancasde7A9anosTrabalham;
  @override
  GeneratedTextColumn get quantasCriancasde7A9anosTrabalham =>
      _quantasCriancasde7A9anosTrabalham ??=
          _constructQuantasCriancasde7A9anosTrabalham();
  GeneratedTextColumn _constructQuantasCriancasde7A9anosTrabalham() {
    return GeneratedTextColumn(
      'quantas_criancasde7_a9anos_trabalham',
      $tableName,
      true,
    );
  }

  final VerificationMeta _nosUltimos5AnosQuantasGravidezesTiveNaFamiliaMeta =
      const VerificationMeta('nosUltimos5AnosQuantasGravidezesTiveNaFamilia');
  GeneratedTextColumn _nosUltimos5AnosQuantasGravidezesTiveNaFamilia;
  @override
  GeneratedTextColumn get nosUltimos5AnosQuantasGravidezesTiveNaFamilia =>
      _nosUltimos5AnosQuantasGravidezesTiveNaFamilia ??=
          _constructNosUltimos5AnosQuantasGravidezesTiveNaFamilia();
  GeneratedTextColumn
      _constructNosUltimos5AnosQuantasGravidezesTiveNaFamilia() {
    return GeneratedTextColumn(
      'nos_ultimos5_anos_quantas_gravidezes_tive_na_familia',
      $tableName,
      true,
    );
  }

  final VerificationMeta _partosOcoridosNoHospitalMeta =
      const VerificationMeta('partosOcoridosNoHospital');
  GeneratedTextColumn _partosOcoridosNoHospital;
  @override
  GeneratedTextColumn get partosOcoridosNoHospital =>
      _partosOcoridosNoHospital ??= _constructPartosOcoridosNoHospital();
  GeneratedTextColumn _constructPartosOcoridosNoHospital() {
    return GeneratedTextColumn(
      'partos_ocoridos_no_hospital',
      $tableName,
      true,
    );
  }

  final VerificationMeta _partosOcoridosEmCasaMeta =
      const VerificationMeta('partosOcoridosEmCasa');
  GeneratedTextColumn _partosOcoridosEmCasa;
  @override
  GeneratedTextColumn get partosOcoridosEmCasa =>
      _partosOcoridosEmCasa ??= _constructPartosOcoridosEmCasa();
  GeneratedTextColumn _constructPartosOcoridosEmCasa() {
    return GeneratedTextColumn(
      'partos_ocoridos_em_casa',
      $tableName,
      true,
    );
  }

  final VerificationMeta _partosOcoridosEmOutroLocalMeta =
      const VerificationMeta('partosOcoridosEmOutroLocal');
  GeneratedTextColumn _partosOcoridosEmOutroLocal;
  @override
  GeneratedTextColumn get partosOcoridosEmOutroLocal =>
      _partosOcoridosEmOutroLocal ??= _constructPartosOcoridosEmOutroLocal();
  GeneratedTextColumn _constructPartosOcoridosEmOutroLocal() {
    return GeneratedTextColumn(
      'partos_ocoridos_em_outro_local',
      $tableName,
      true,
    );
  }

  final VerificationMeta _outroLocalDoPartoQualLocalMeta =
      const VerificationMeta('outroLocalDoPartoQualLocal');
  GeneratedTextColumn _outroLocalDoPartoQualLocal;
  @override
  GeneratedTextColumn get outroLocalDoPartoQualLocal =>
      _outroLocalDoPartoQualLocal ??= _constructOutroLocalDoPartoQualLocal();
  GeneratedTextColumn _constructOutroLocalDoPartoQualLocal() {
    return GeneratedTextColumn(
      'outro_local_do_parto_qual_local',
      $tableName,
      true,
    );
  }

  final VerificationMeta _quantasCriancasNasceramVivasMeta =
      const VerificationMeta('quantasCriancasNasceramVivas');
  GeneratedTextColumn _quantasCriancasNasceramVivas;
  @override
  GeneratedTextColumn get quantasCriancasNasceramVivas =>
      _quantasCriancasNasceramVivas ??=
          _constructQuantasCriancasNasceramVivas();
  GeneratedTextColumn _constructQuantasCriancasNasceramVivas() {
    return GeneratedTextColumn(
      'quantas_criancas_nasceram_vivas',
      $tableName,
      true,
    );
  }

  final VerificationMeta _quantasContinuamVivasMeta =
      const VerificationMeta('quantasContinuamVivas');
  GeneratedTextColumn _quantasContinuamVivas;
  @override
  GeneratedTextColumn get quantasContinuamVivas =>
      _quantasContinuamVivas ??= _constructQuantasContinuamVivas();
  GeneratedTextColumn _constructQuantasContinuamVivas() {
    return GeneratedTextColumn(
      'quantas_continuam_vivas',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveramMeta =
      const VerificationMeta(
          'dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram');
  GeneratedTextColumn
      _dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram;
  @override
  GeneratedTextColumn
      get dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram =>
          _dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram ??=
              _constructDasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram();
  GeneratedTextColumn
      _constructDasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram() {
    return GeneratedTextColumn(
      'das_vivas_algumas_apresentam_problemas_de_desenvolvimento_que_outros_nao_tiveram',
      $tableName,
      true,
    );
  }

  final VerificationMeta _descreverOProblemaMeta =
      const VerificationMeta('descreverOProblema');
  GeneratedTextColumn _descreverOProblema;
  @override
  GeneratedTextColumn get descreverOProblema =>
      _descreverOProblema ??= _constructDescreverOProblema();
  GeneratedTextColumn _constructDescreverOProblema() {
    return GeneratedTextColumn(
      'descrever_o_problema',
      $tableName,
      true,
    );
  }

  final VerificationMeta _aFamiliaProcurouAjudaParaSuperarEsseProblemaMeta =
      const VerificationMeta('aFamiliaProcurouAjudaParaSuperarEsseProblema');
  GeneratedTextColumn _aFamiliaProcurouAjudaParaSuperarEsseProblema;
  @override
  GeneratedTextColumn get aFamiliaProcurouAjudaParaSuperarEsseProblema =>
      _aFamiliaProcurouAjudaParaSuperarEsseProblema ??=
          _constructAFamiliaProcurouAjudaParaSuperarEsseProblema();
  GeneratedTextColumn _constructAFamiliaProcurouAjudaParaSuperarEsseProblema() {
    return GeneratedTextColumn(
      'a_familia_procurou_ajuda_para_superar_esse_problema',
      $tableName,
      true,
    );
  }

  final VerificationMeta _seSimQueTipoDeAjudaEOndeMeta =
      const VerificationMeta('seSimQueTipoDeAjudaEOnde');
  GeneratedTextColumn _seSimQueTipoDeAjudaEOnde;
  @override
  GeneratedTextColumn get seSimQueTipoDeAjudaEOnde =>
      _seSimQueTipoDeAjudaEOnde ??= _constructSeSimQueTipoDeAjudaEOnde();
  GeneratedTextColumn _constructSeSimQueTipoDeAjudaEOnde() {
    return GeneratedTextColumn(
      'se_sim_que_tipo_de_ajuda_e_onde',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamiliaMeta =
      const VerificationMeta(
          'osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia');
  GeneratedTextColumn
      _osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia;
  @override
  GeneratedTextColumn
      get osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia =>
          _osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia ??=
              _constructOsVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia();
  GeneratedTextColumn
      _constructOsVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia() {
    return GeneratedTextColumn(
      'os_vizinhos_se_preocuparam_quando_nasceu_uma_crianca_com_problema_na_familia',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVidaMeta =
      const VerificationMeta(
          'dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida');
  GeneratedTextColumn
      _dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida;
  @override
  GeneratedTextColumn
      get dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida =>
          _dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida ??=
              _constructDasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida();
  GeneratedTextColumn
      _constructDasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida() {
    return GeneratedTextColumn(
      'das_criancas_que_nasceram_vivas_quantas_nao_sobreviveram_nos2_primeiros_anos_de_vida',
      $tableName,
      true,
    );
  }

  final VerificationMeta _sabeDizerOMotivoDosObitosMeta =
      const VerificationMeta('sabeDizerOMotivoDosObitos');
  GeneratedTextColumn _sabeDizerOMotivoDosObitos;
  @override
  GeneratedTextColumn get sabeDizerOMotivoDosObitos =>
      _sabeDizerOMotivoDosObitos ??= _constructSabeDizerOMotivoDosObitos();
  GeneratedTextColumn _constructSabeDizerOMotivoDosObitos() {
    return GeneratedTextColumn(
      'sabe_dizer_o_motivo_dos_obitos',
      $tableName,
      true,
    );
  }

  final VerificationMeta _seSimQualFoiPrincipalRazaodoObitoMeta =
      const VerificationMeta('seSimQualFoiPrincipalRazaodoObito');
  GeneratedTextColumn _seSimQualFoiPrincipalRazaodoObito;
  @override
  GeneratedTextColumn get seSimQualFoiPrincipalRazaodoObito =>
      _seSimQualFoiPrincipalRazaodoObito ??=
          _constructSeSimQualFoiPrincipalRazaodoObito();
  GeneratedTextColumn _constructSeSimQualFoiPrincipalRazaodoObito() {
    return GeneratedTextColumn(
      'se_sim_qual_foi_principal_razaodo_obito',
      $tableName,
      true,
    );
  }

  final VerificationMeta _voceTemMedoDeTerUmFilhoOuNetoComProblemasMeta =
      const VerificationMeta('voceTemMedoDeTerUmFilhoOuNetoComProblemas');
  GeneratedTextColumn _voceTemMedoDeTerUmFilhoOuNetoComProblemas;
  @override
  GeneratedTextColumn get voceTemMedoDeTerUmFilhoOuNetoComProblemas =>
      _voceTemMedoDeTerUmFilhoOuNetoComProblemas ??=
          _constructVoceTemMedoDeTerUmFilhoOuNetoComProblemas();
  GeneratedTextColumn _constructVoceTemMedoDeTerUmFilhoOuNetoComProblemas() {
    return GeneratedTextColumn(
      'voce_tem_medo_de_ter_um_filho_ou_neto_com_problemas',
      $tableName,
      true,
    );
  }

  final VerificationMeta _expliquePorQueMeta =
      const VerificationMeta('expliquePorQue');
  GeneratedTextColumn _expliquePorQue;
  @override
  GeneratedTextColumn get expliquePorQue =>
      _expliquePorQue ??= _constructExpliquePorQue();
  GeneratedTextColumn _constructExpliquePorQue() {
    return GeneratedTextColumn(
      'explique_por_que',
      $tableName,
      true,
    );
  }

  final VerificationMeta _porqueVoceAchaQueUmaCriancaPodeNascerComProblemaMeta =
      const VerificationMeta(
          'porqueVoceAchaQueUmaCriancaPodeNascerComProblema');
  GeneratedTextColumn _porqueVoceAchaQueUmaCriancaPodeNascerComProblema;
  @override
  GeneratedTextColumn get porqueVoceAchaQueUmaCriancaPodeNascerComProblema =>
      _porqueVoceAchaQueUmaCriancaPodeNascerComProblema ??=
          _constructPorqueVoceAchaQueUmaCriancaPodeNascerComProblema();
  GeneratedTextColumn
      _constructPorqueVoceAchaQueUmaCriancaPodeNascerComProblema() {
    return GeneratedTextColumn(
      'porque_voce_acha_que_uma_crianca_pode_nascer_com_problema',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblemaMeta =
      const VerificationMeta(
          'oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema');
  GeneratedTextColumn _oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema;
  @override
  GeneratedTextColumn
      get oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema =>
          _oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema ??=
              _constructOQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema();
  GeneratedTextColumn
      _constructOQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema() {
    return GeneratedTextColumn(
      'o_que_acontece_no_seu_bairro_quando_nasce_uma_crianca_com_problema',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _achaQueUmaCriancaQueNasceComProblemaPodeSobreviverMeta =
      const VerificationMeta(
          'achaQueUmaCriancaQueNasceComProblemaPodeSobreviver');
  GeneratedTextColumn _achaQueUmaCriancaQueNasceComProblemaPodeSobreviver;
  @override
  GeneratedTextColumn get achaQueUmaCriancaQueNasceComProblemaPodeSobreviver =>
      _achaQueUmaCriancaQueNasceComProblemaPodeSobreviver ??=
          _constructAchaQueUmaCriancaQueNasceComProblemaPodeSobreviver();
  GeneratedTextColumn
      _constructAchaQueUmaCriancaQueNasceComProblemaPodeSobreviver() {
    return GeneratedTextColumn(
      'acha_que_uma_crianca_que_nasce_com_problema_pode_sobreviver',
      $tableName,
      true,
    );
  }

  final VerificationMeta _expliquePorqueMeta =
      const VerificationMeta('expliquePorque');
  GeneratedTextColumn _expliquePorque;
  @override
  GeneratedTextColumn get expliquePorque =>
      _expliquePorque ??= _constructExpliquePorque();
  GeneratedTextColumn _constructExpliquePorque() {
    return GeneratedTextColumn(
      'explique_porque',
      $tableName,
      true,
    );
  }

  final VerificationMeta _quemPodeAjudarQuandoUmaCriancaNasceComProblemaMeta =
      const VerificationMeta('quemPodeAjudarQuandoUmaCriancaNasceComProblema');
  GeneratedTextColumn _quemPodeAjudarQuandoUmaCriancaNasceComProblema;
  @override
  GeneratedTextColumn get quemPodeAjudarQuandoUmaCriancaNasceComProblema =>
      _quemPodeAjudarQuandoUmaCriancaNasceComProblema ??=
          _constructQuemPodeAjudarQuandoUmaCriancaNasceComProblema();
  GeneratedTextColumn
      _constructQuemPodeAjudarQuandoUmaCriancaNasceComProblema() {
    return GeneratedTextColumn(
      'quem_pode_ajudar_quando_uma_crianca_nasce_com_problema',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacaoMeta =
      const VerificationMeta(
          'emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao');
  GeneratedTextColumn _emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao;
  @override
  GeneratedTextColumn
      get emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao =>
          _emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao ??=
              _constructEmCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao();
  GeneratedTextColumn
      _constructEmCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao() {
    return GeneratedTextColumn(
      'em_casa_quem_cuida_mais_das_criancas_ate6_anos_para_alimentacao',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigieneMeta =
      const VerificationMeta(
          'emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene');
  GeneratedTextColumn
      _emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene;
  @override
  GeneratedTextColumn
      get emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene =>
          _emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene ??=
              _constructEmCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene();
  GeneratedTextColumn
      _constructEmCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene() {
    return GeneratedTextColumn(
      'em_casa_quem_cuida_mais_das_criancas_ate6_anos_para_os_cuidados_de_higiene',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudosMeta =
      const VerificationMeta(
          'emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos');
  GeneratedTextColumn _emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos;
  @override
  GeneratedTextColumn get emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos =>
      _emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos ??=
          _constructEmCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos();
  GeneratedTextColumn
      _constructEmCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos() {
    return GeneratedTextColumn(
      'em_casa_quem_cuida_mais_das_criancas_ate6_anos_para_os_estudos',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _aPessoaEntrevistadaPareceuSinceraNasSuasRespostasMeta =
      const VerificationMeta(
          'aPessoaEntrevistadaPareceuSinceraNasSuasRespostas');
  GeneratedTextColumn _aPessoaEntrevistadaPareceuSinceraNasSuasRespostas;
  @override
  GeneratedTextColumn get aPessoaEntrevistadaPareceuSinceraNasSuasRespostas =>
      _aPessoaEntrevistadaPareceuSinceraNasSuasRespostas ??=
          _constructAPessoaEntrevistadaPareceuSinceraNasSuasRespostas();
  GeneratedTextColumn
      _constructAPessoaEntrevistadaPareceuSinceraNasSuasRespostas() {
    return GeneratedTextColumn(
      'a_pessoa_entrevistada_pareceu_sincera_nas_suas_respostas',
      $tableName,
      true,
    );
  }

  final VerificationMeta _algumasPerguntasIncomodaramAPessoaEntrevistadaMeta =
      const VerificationMeta('algumasPerguntasIncomodaramAPessoaEntrevistada');
  GeneratedTextColumn _algumasPerguntasIncomodaramAPessoaEntrevistada;
  @override
  GeneratedTextColumn get algumasPerguntasIncomodaramAPessoaEntrevistada =>
      _algumasPerguntasIncomodaramAPessoaEntrevistada ??=
          _constructAlgumasPerguntasIncomodaramAPessoaEntrevistada();
  GeneratedTextColumn
      _constructAlgumasPerguntasIncomodaramAPessoaEntrevistada() {
    return GeneratedTextColumn(
      'algumas_perguntas_incomodaram_a_pessoa_entrevistada',
      $tableName,
      true,
    );
  }

  final VerificationMeta _seSimQuais3PrincipaisPerguntasMeta =
      const VerificationMeta('seSimQuais3PrincipaisPerguntas');
  GeneratedTextColumn _seSimQuais3PrincipaisPerguntas;
  @override
  GeneratedTextColumn get seSimQuais3PrincipaisPerguntas =>
      _seSimQuais3PrincipaisPerguntas ??=
          _constructSeSimQuais3PrincipaisPerguntas();
  GeneratedTextColumn _constructSeSimQuais3PrincipaisPerguntas() {
    return GeneratedTextColumn(
      'se_sim_quais3_principais_perguntas',
      $tableName,
      true,
    );
  }

  final VerificationMeta _todasAsPerguntasIncomodaramMeta =
      const VerificationMeta('todasAsPerguntasIncomodaram');
  GeneratedTextColumn _todasAsPerguntasIncomodaram;
  @override
  GeneratedTextColumn get todasAsPerguntasIncomodaram =>
      _todasAsPerguntasIncomodaram ??= _constructTodasAsPerguntasIncomodaram();
  GeneratedTextColumn _constructTodasAsPerguntasIncomodaram() {
    return GeneratedTextColumn(
      'todas_as_perguntas_incomodaram',
      $tableName,
      true,
    );
  }

  final VerificationMeta _aResidenciaEraDeMeta =
      const VerificationMeta('aResidenciaEraDe');
  GeneratedTextColumn _aResidenciaEraDe;
  @override
  GeneratedTextColumn get aResidenciaEraDe =>
      _aResidenciaEraDe ??= _constructAResidenciaEraDe();
  GeneratedTextColumn _constructAResidenciaEraDe() {
    return GeneratedTextColumn(
      'a_residencia_era_de',
      $tableName,
      true,
    );
  }

  final VerificationMeta _comoAvaliaHigieneDaCasaMeta =
      const VerificationMeta('comoAvaliaHigieneDaCasa');
  GeneratedTextColumn _comoAvaliaHigieneDaCasa;
  @override
  GeneratedTextColumn get comoAvaliaHigieneDaCasa =>
      _comoAvaliaHigieneDaCasa ??= _constructComoAvaliaHigieneDaCasa();
  GeneratedTextColumn _constructComoAvaliaHigieneDaCasa() {
    return GeneratedTextColumn(
      'como_avalia_higiene_da_casa',
      $tableName,
      true,
    );
  }

  final VerificationMeta _oQueEraMalMeta = const VerificationMeta('oQueEraMal');
  GeneratedTextColumn _oQueEraMal;
  @override
  GeneratedTextColumn get oQueEraMal => _oQueEraMal ??= _constructOQueEraMal();
  GeneratedTextColumn _constructOQueEraMal() {
    return GeneratedTextColumn(
      'o_que_era_mal',
      $tableName,
      true,
    );
  }

  final VerificationMeta _tinhaCriancasNaCasaDuranteInqueritoMeta =
      const VerificationMeta('tinhaCriancasNaCasaDuranteInquerito');
  GeneratedTextColumn _tinhaCriancasNaCasaDuranteInquerito;
  @override
  GeneratedTextColumn get tinhaCriancasNaCasaDuranteInquerito =>
      _tinhaCriancasNaCasaDuranteInquerito ??=
          _constructTinhaCriancasNaCasaDuranteInquerito();
  GeneratedTextColumn _constructTinhaCriancasNaCasaDuranteInquerito() {
    return GeneratedTextColumn(
      'tinha_criancas_na_casa_durante_inquerito',
      $tableName,
      true,
    );
  }

  final VerificationMeta _tinhaCriancasNaCasaDuranteInqueritoQuantasMeta =
      const VerificationMeta('tinhaCriancasNaCasaDuranteInqueritoQuantas');
  GeneratedTextColumn _tinhaCriancasNaCasaDuranteInqueritoQuantas;
  @override
  GeneratedTextColumn get tinhaCriancasNaCasaDuranteInqueritoQuantas =>
      _tinhaCriancasNaCasaDuranteInqueritoQuantas ??=
          _constructTinhaCriancasNaCasaDuranteInqueritoQuantas();
  GeneratedTextColumn _constructTinhaCriancasNaCasaDuranteInqueritoQuantas() {
    return GeneratedTextColumn(
      'tinha_criancas_na_casa_durante_inquerito_quantas',
      $tableName,
      true,
    );
  }

  final VerificationMeta _tentouIncentivarMeta =
      const VerificationMeta('tentouIncentivar');
  GeneratedTextColumn _tentouIncentivar;
  @override
  GeneratedTextColumn get tentouIncentivar =>
      _tentouIncentivar ??= _constructTentouIncentivar();
  GeneratedTextColumn _constructTentouIncentivar() {
    return GeneratedTextColumn(
      'tentou_incentivar',
      $tableName,
      true,
    );
  }

  final VerificationMeta _tentouIncentivarQuantasMeta =
      const VerificationMeta('tentouIncentivarQuantas');
  GeneratedTextColumn _tentouIncentivarQuantas;
  @override
  GeneratedTextColumn get tentouIncentivarQuantas =>
      _tentouIncentivarQuantas ??= _constructTentouIncentivarQuantas();
  GeneratedTextColumn _constructTentouIncentivarQuantas() {
    return GeneratedTextColumn(
      'tentou_incentivar_quantas',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _seIncentivouQuantasReagiramPositivamenteAosSeusIncentivosMeta =
      const VerificationMeta(
          'seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos');
  GeneratedTextColumn
      _seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos;
  @override
  GeneratedTextColumn
      get seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos =>
          _seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos ??=
              _constructSeIncentivouQuantasReagiramPositivamenteAosSeusIncentivos();
  GeneratedTextColumn
      _constructSeIncentivouQuantasReagiramPositivamenteAosSeusIncentivos() {
    return GeneratedTextColumn(
      'se_incentivou_quantas_reagiram_positivamente_aos_seus_incentivos',
      $tableName,
      true,
    );
  }

  final VerificationMeta _observouCriancasDeAte1AnoMeta =
      const VerificationMeta('observouCriancasDeAte1Ano');
  GeneratedTextColumn _observouCriancasDeAte1Ano;
  @override
  GeneratedTextColumn get observouCriancasDeAte1Ano =>
      _observouCriancasDeAte1Ano ??= _constructObservouCriancasDeAte1Ano();
  GeneratedTextColumn _constructObservouCriancasDeAte1Ano() {
    return GeneratedTextColumn(
      'observou_criancas_de_ate1_ano',
      $tableName,
      true,
    );
  }

  final VerificationMeta _observouCriancasDeAte1AnoQuantosMeta =
      const VerificationMeta('observouCriancasDeAte1AnoQuantos');
  GeneratedTextColumn _observouCriancasDeAte1AnoQuantos;
  @override
  GeneratedTextColumn get observouCriancasDeAte1AnoQuantos =>
      _observouCriancasDeAte1AnoQuantos ??=
          _constructObservouCriancasDeAte1AnoQuantos();
  GeneratedTextColumn _constructObservouCriancasDeAte1AnoQuantos() {
    return GeneratedTextColumn(
      'observou_criancas_de_ate1_ano_quantos',
      $tableName,
      true,
    );
  }

  final VerificationMeta _algumasApresentavamProblemasMeta =
      const VerificationMeta('algumasApresentavamProblemas');
  GeneratedTextColumn _algumasApresentavamProblemas;
  @override
  GeneratedTextColumn get algumasApresentavamProblemas =>
      _algumasApresentavamProblemas ??=
          _constructAlgumasApresentavamProblemas();
  GeneratedTextColumn _constructAlgumasApresentavamProblemas() {
    return GeneratedTextColumn(
      'algumas_apresentavam_problemas',
      $tableName,
      true,
    );
  }

  final VerificationMeta _algumasApresentavamProblemasQuantosMeta =
      const VerificationMeta('algumasApresentavamProblemasQuantos');
  GeneratedTextColumn _algumasApresentavamProblemasQuantos;
  @override
  GeneratedTextColumn get algumasApresentavamProblemasQuantos =>
      _algumasApresentavamProblemasQuantos ??=
          _constructAlgumasApresentavamProblemasQuantos();
  GeneratedTextColumn _constructAlgumasApresentavamProblemasQuantos() {
    return GeneratedTextColumn(
      'algumas_apresentavam_problemas_quantos',
      $tableName,
      true,
    );
  }

  final VerificationMeta _descreveOProblemaDessasCriancasMeta =
      const VerificationMeta('descreveOProblemaDessasCriancas');
  GeneratedTextColumn _descreveOProblemaDessasCriancas;
  @override
  GeneratedTextColumn get descreveOProblemaDessasCriancas =>
      _descreveOProblemaDessasCriancas ??=
          _constructDescreveOProblemaDessasCriancas();
  GeneratedTextColumn _constructDescreveOProblemaDessasCriancas() {
    return GeneratedTextColumn(
      'descreve_o_problema_dessas_criancas',
      $tableName,
      true,
    );
  }

  final VerificationMeta _observouCriancasDe1AnoEmeioAte3AnosMeta =
      const VerificationMeta('observouCriancasDe1AnoEmeioAte3Anos');
  GeneratedTextColumn _observouCriancasDe1AnoEmeioAte3Anos;
  @override
  GeneratedTextColumn get observouCriancasDe1AnoEmeioAte3Anos =>
      _observouCriancasDe1AnoEmeioAte3Anos ??=
          _constructObservouCriancasDe1AnoEmeioAte3Anos();
  GeneratedTextColumn _constructObservouCriancasDe1AnoEmeioAte3Anos() {
    return GeneratedTextColumn(
      'observou_criancas_de1_ano_emeio_ate3_anos',
      $tableName,
      true,
    );
  }

  final VerificationMeta _observouCriancasDe1AnoEmeioAte3AnosQuantasMeta =
      const VerificationMeta('observouCriancasDe1AnoEmeioAte3AnosQuantas');
  GeneratedTextColumn _observouCriancasDe1AnoEmeioAte3AnosQuantas;
  @override
  GeneratedTextColumn get observouCriancasDe1AnoEmeioAte3AnosQuantas =>
      _observouCriancasDe1AnoEmeioAte3AnosQuantas ??=
          _constructObservouCriancasDe1AnoEmeioAte3AnosQuantas();
  GeneratedTextColumn _constructObservouCriancasDe1AnoEmeioAte3AnosQuantas() {
    return GeneratedTextColumn(
      'observou_criancas_de1_ano_emeio_ate3_anos_quantas',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasDe1anoEmeioObservadasApresentavamProblemasMeta =
      const VerificationMeta(
          'algumasCriancasDe1anoEmeioObservadasApresentavamProblemas');
  GeneratedTextColumn
      _algumasCriancasDe1anoEmeioObservadasApresentavamProblemas;
  @override
  GeneratedTextColumn
      get algumasCriancasDe1anoEmeioObservadasApresentavamProblemas =>
          _algumasCriancasDe1anoEmeioObservadasApresentavamProblemas ??=
              _constructAlgumasCriancasDe1anoEmeioObservadasApresentavamProblemas();
  GeneratedTextColumn
      _constructAlgumasCriancasDe1anoEmeioObservadasApresentavamProblemas() {
    return GeneratedTextColumn(
      'algumas_criancas_de1ano_emeio_observadas_apresentavam_problemas',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantasMeta =
      const VerificationMeta(
          'algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas');
  GeneratedTextColumn
      _algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas;
  @override
  GeneratedTextColumn
      get algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas =>
          _algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas ??=
              _constructAlgumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas();
  GeneratedTextColumn
      _constructAlgumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas() {
    return GeneratedTextColumn(
      'algumas_criancas_de1ano_emeio_observadas_apresentavam_problemas_quantas',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreveMeta =
      const VerificationMeta(
          'algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve');
  GeneratedTextColumn
      _algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve;
  @override
  GeneratedTextColumn
      get algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve =>
          _algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve ??=
              _constructAlgumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve();
  GeneratedTextColumn
      _constructAlgumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve() {
    return GeneratedTextColumn(
      'algumas_criancas_de1ano_emeio_observadas_apresentavam_problemas_descreve',
      $tableName,
      true,
    );
  }

  final VerificationMeta _observouCriancasDe3AnosAte6AnosMeta =
      const VerificationMeta('observouCriancasDe3AnosAte6Anos');
  GeneratedTextColumn _observouCriancasDe3AnosAte6Anos;
  @override
  GeneratedTextColumn get observouCriancasDe3AnosAte6Anos =>
      _observouCriancasDe3AnosAte6Anos ??=
          _constructObservouCriancasDe3AnosAte6Anos();
  GeneratedTextColumn _constructObservouCriancasDe3AnosAte6Anos() {
    return GeneratedTextColumn(
      'observou_criancas_de3_anos_ate6_anos',
      $tableName,
      true,
    );
  }

  final VerificationMeta _observouCriancasDe3AnosAte6AnosQuantasMeta =
      const VerificationMeta('observouCriancasDe3AnosAte6AnosQuantas');
  GeneratedTextColumn _observouCriancasDe3AnosAte6AnosQuantas;
  @override
  GeneratedTextColumn get observouCriancasDe3AnosAte6AnosQuantas =>
      _observouCriancasDe3AnosAte6AnosQuantas ??=
          _constructObservouCriancasDe3AnosAte6AnosQuantas();
  GeneratedTextColumn _constructObservouCriancasDe3AnosAte6AnosQuantas() {
    return GeneratedTextColumn(
      'observou_criancas_de3_anos_ate6_anos_quantas',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasMeta =
      const VerificationMeta(
          'observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas');
  GeneratedTextColumn
      _observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas;
  @override
  GeneratedTextColumn
      get observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas =>
          _observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas ??=
              _constructObservouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas();
  GeneratedTextColumn
      _constructObservouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas() {
    return GeneratedTextColumn(
      'observou_criancas_de3_anos_ate6_anos_que_algumas_apresentavam_problemas',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantasMeta =
      const VerificationMeta(
          'observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas');
  GeneratedTextColumn
      _observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas;
  @override
  GeneratedTextColumn
      get observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas =>
          _observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas ??=
              _constructObservouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas();
  GeneratedTextColumn
      _constructObservouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas() {
    return GeneratedTextColumn(
      'observou_criancas_de3_anos_ate6_anos_que_algumas_apresentavam_problemas_quantas',
      $tableName,
      true,
    );
  }

  final VerificationMeta
      _observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreveMeta =
      const VerificationMeta(
          'observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve');
  GeneratedTextColumn
      _observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve;
  @override
  GeneratedTextColumn
      get observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve =>
          _observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve ??=
              _constructObservouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve();
  GeneratedTextColumn
      _constructObservouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve() {
    return GeneratedTextColumn(
      'observou_criancas_de3_anos_ate6_anos_que_algumas_apresentavam_problemas_descreve',
      $tableName,
      true,
    );
  }

  final VerificationMeta _observouSituacoesQueLheChamaramAtencaoMeta =
      const VerificationMeta('observouSituacoesQueLheChamaramAtencao');
  GeneratedTextColumn _observouSituacoesQueLheChamaramAtencao;
  @override
  GeneratedTextColumn get observouSituacoesQueLheChamaramAtencao =>
      _observouSituacoesQueLheChamaramAtencao ??=
          _constructObservouSituacoesQueLheChamaramAtencao();
  GeneratedTextColumn _constructObservouSituacoesQueLheChamaramAtencao() {
    return GeneratedTextColumn(
      'observou_situacoes_que_lhe_chamaram_atencao',
      $tableName,
      true,
    );
  }

  final VerificationMeta _emRelacaoAPessoaEntrevistadaMeta =
      const VerificationMeta('emRelacaoAPessoaEntrevistada');
  GeneratedTextColumn _emRelacaoAPessoaEntrevistada;
  @override
  GeneratedTextColumn get emRelacaoAPessoaEntrevistada =>
      _emRelacaoAPessoaEntrevistada ??=
          _constructEmRelacaoAPessoaEntrevistada();
  GeneratedTextColumn _constructEmRelacaoAPessoaEntrevistada() {
    return GeneratedTextColumn(
      'em_relacao_a_pessoa_entrevistada',
      $tableName,
      true,
    );
  }

  final VerificationMeta _emRelacaoAPessoaEntrevistadaDescreveMeta =
      const VerificationMeta('emRelacaoAPessoaEntrevistadaDescreve');
  GeneratedTextColumn _emRelacaoAPessoaEntrevistadaDescreve;
  @override
  GeneratedTextColumn get emRelacaoAPessoaEntrevistadaDescreve =>
      _emRelacaoAPessoaEntrevistadaDescreve ??=
          _constructEmRelacaoAPessoaEntrevistadaDescreve();
  GeneratedTextColumn _constructEmRelacaoAPessoaEntrevistadaDescreve() {
    return GeneratedTextColumn(
      'em_relacao_a_pessoa_entrevistada_descreve',
      $tableName,
      true,
    );
  }

  final VerificationMeta _emRelacaoAOutrosAdultosPresenteMeta =
      const VerificationMeta('emRelacaoAOutrosAdultosPresente');
  GeneratedTextColumn _emRelacaoAOutrosAdultosPresente;
  @override
  GeneratedTextColumn get emRelacaoAOutrosAdultosPresente =>
      _emRelacaoAOutrosAdultosPresente ??=
          _constructEmRelacaoAOutrosAdultosPresente();
  GeneratedTextColumn _constructEmRelacaoAOutrosAdultosPresente() {
    return GeneratedTextColumn(
      'em_relacao_a_outros_adultos_presente',
      $tableName,
      true,
    );
  }

  final VerificationMeta _emRelacaoAOutrosAdultosPresenteDescreveMeta =
      const VerificationMeta('emRelacaoAOutrosAdultosPresenteDescreve');
  GeneratedTextColumn _emRelacaoAOutrosAdultosPresenteDescreve;
  @override
  GeneratedTextColumn get emRelacaoAOutrosAdultosPresenteDescreve =>
      _emRelacaoAOutrosAdultosPresenteDescreve ??=
          _constructEmRelacaoAOutrosAdultosPresenteDescreve();
  GeneratedTextColumn _constructEmRelacaoAOutrosAdultosPresenteDescreve() {
    return GeneratedTextColumn(
      'em_relacao_a_outros_adultos_presente_descreve',
      $tableName,
      true,
    );
  }

  final VerificationMeta _emRelacaoAsCriancasPresentesMeta =
      const VerificationMeta('emRelacaoAsCriancasPresentes');
  GeneratedTextColumn _emRelacaoAsCriancasPresentes;
  @override
  GeneratedTextColumn get emRelacaoAsCriancasPresentes =>
      _emRelacaoAsCriancasPresentes ??=
          _constructEmRelacaoAsCriancasPresentes();
  GeneratedTextColumn _constructEmRelacaoAsCriancasPresentes() {
    return GeneratedTextColumn(
      'em_relacao_as_criancas_presentes',
      $tableName,
      true,
    );
  }

  final VerificationMeta _emRelacaoAsCriancasPresentesDescreveMeta =
      const VerificationMeta('emRelacaoAsCriancasPresentesDescreve');
  GeneratedTextColumn _emRelacaoAsCriancasPresentesDescreve;
  @override
  GeneratedTextColumn get emRelacaoAsCriancasPresentesDescreve =>
      _emRelacaoAsCriancasPresentesDescreve ??=
          _constructEmRelacaoAsCriancasPresentesDescreve();
  GeneratedTextColumn _constructEmRelacaoAsCriancasPresentesDescreve() {
    return GeneratedTextColumn(
      'em_relacao_as_criancas_presentes_descreve',
      $tableName,
      true,
    );
  }

  final VerificationMeta _emRelacaoAVizinhancaMeta =
      const VerificationMeta('emRelacaoAVizinhanca');
  GeneratedTextColumn _emRelacaoAVizinhanca;
  @override
  GeneratedTextColumn get emRelacaoAVizinhanca =>
      _emRelacaoAVizinhanca ??= _constructEmRelacaoAVizinhanca();
  GeneratedTextColumn _constructEmRelacaoAVizinhanca() {
    return GeneratedTextColumn(
      'em_relacao_a_vizinhanca',
      $tableName,
      true,
    );
  }

  final VerificationMeta _emRelacaoAVizinhancaDescreveMeta =
      const VerificationMeta('emRelacaoAVizinhancaDescreve');
  GeneratedTextColumn _emRelacaoAVizinhancaDescreve;
  @override
  GeneratedTextColumn get emRelacaoAVizinhancaDescreve =>
      _emRelacaoAVizinhancaDescreve ??=
          _constructEmRelacaoAVizinhancaDescreve();
  GeneratedTextColumn _constructEmRelacaoAVizinhancaDescreve() {
    return GeneratedTextColumn(
      'em_relacao_a_vizinhanca_descreve',
      $tableName,
      true,
    );
  }

  final VerificationMeta _outrasMeta = const VerificationMeta('outras');
  GeneratedTextColumn _outras;
  @override
  GeneratedTextColumn get outras => _outras ??= _constructOutras();
  GeneratedTextColumn _constructOutras() {
    return GeneratedTextColumn(
      'outras',
      $tableName,
      true,
    );
  }

  final VerificationMeta _outrasDescreveMeta =
      const VerificationMeta('outrasDescreve');
  GeneratedTextColumn _outrasDescreve;
  @override
  GeneratedTextColumn get outrasDescreve =>
      _outrasDescreve ??= _constructOutrasDescreve();
  GeneratedTextColumn _constructOutrasDescreve() {
    return GeneratedTextColumn(
      'outras_descreve',
      $tableName,
      true,
    );
  }

  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  GeneratedDateTimeColumn _createdAt;
  @override
  GeneratedDateTimeColumn get createdAt => _createdAt ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      true,
    );
  }

  final VerificationMeta _updatedAtMeta = const VerificationMeta('updatedAt');
  GeneratedDateTimeColumn _updatedAt;
  @override
  GeneratedDateTimeColumn get updatedAt => _updatedAt ??= _constructUpdatedAt();
  GeneratedDateTimeColumn _constructUpdatedAt() {
    return GeneratedDateTimeColumn(
      'updated_at',
      $tableName,
      true,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [
        uuid,
        nomeDoPesquisador,
        numeroDeInquerito,
        data,
        municipio,
        bairro,
        pontoDeReferenciaDaCasa,
        nome,
        idade,
        contacto,
        eChefeDaFamilia,
        caoNaoVinculoDeParantesco,
        idadeDoChefeDaFamilia,
        estudou,
        eAlfabetizado,
        completouEnsinoPrimario,
        completouEnsinoSecundario,
        fezCursoSuperior,
        naoSabe,
        chefeDeFamiliaTemSalario,
        quantoGastaParaSustentarFamiliaPorMes,
        quantasFamiliasResidemNacasa,
        quantosAdultos,
        quantosAdultosMaioresDe18anos,
        quantosAdultosMaioresDe18anosHomens,
        quantosAdultosMaioresDe18anosHomensTrabalham,
        quantosAdultosMaioresDe18anosHomensAlfabetizados,
        quantosAdultosM18Mulheres,
        quantosAdultosM18MulheresTrabalham,
        quantosAdultosM18MulheresAlfabetizados,
        relatosDeDeficiencia,
        totalcriancasAte1anoemeio,
        totalcriancasAte1anoemeioMeninos,
        totalcriancasAte1anoemeioMeninas,
        algumasCriancasAte1anoemeioNaodesenvolvem,
        algumasCriancasAte1anoemeioNaodesenvolvemQuantas,
        algumaCriancasAte1anoemeioQueTipodeProblema,
        algumaCriancasAte1anoemeioTemProblemaMotor,
        algumaCriancasAte1anoemeioTemProblemaMotorQuantas,
        algumaCriancasAte1anoemeioTemMalFormacao,
        algumaCriancasAte1anoemeioTemMalFormacaoQuantas,
        algumaCriancasAte1anoemeioNaoReagemAIncentivo,
        algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas,
        algumaCriancasAte1anoemeioTemReacaoAgressiva,
        algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas,
        totalcriancasAte1anoemeioA3anos,
        totalcriancasAte1anoemeioA3anosMeninos,
        totalcriancasAte1anoemeioA3anosMeninas,
        algumasCriancasAte1anoemeioA3anosNaodesenvolvem,
        algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas,
        algumaCriancasAte1anoemeioA3anosQueTipodeProblema,
        algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas,
        algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas,
        algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo,
        algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas,
        algumasCriancasAte1anoemeioA3anosAlgunsAgridem,
        algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas,
        algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal,
        algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas,
        algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao,
        algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas,
        algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas,
        algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas,
        algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente,
        algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas,
        algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos,
        algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas,
        algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown,
        algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas,
        totalcriancasde3A6anos,
        totalcriancasde3A6anosMeninos,
        totalcriancasde3A6anosMeninas,
        algumasCriancasde3A6anosNaodesenvolvem,
        algumasCriancasde3A6anosNaodesenvolvemQuantas,
        algumaCriancasde3A6anosQueTipodeProblema,
        algumasCriancasde3A6anosNaoInteragemComPessoas,
        algumasCriancasde3A6anosNaoInteragemComPessoasQuantas,
        algumasCriancasde3A6anosTemComportamentoAgressivo,
        algumasCriancasde3A6anosTemComportamentoAgressivoQuantas,
        algumasCriancasde3A6anosAlgunsAgridem,
        algumasCriancasde3A6anosAlgunsAgridemQuantas,
        algumasCriancasde3A6anosAlgumasAgitadasQueONormal,
        algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas,
        algumasCriancasde3A6anosAlgumasTemMalFormacao,
        algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas,
        algumasCriancasde3A6anosAlgumasNaoAndamSozinhas,
        algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas,
        algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente,
        algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas,
        algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos,
        algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas,
        algumasCriancasde3A6anosAlgumasTemSindromeDeDown,
        algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas,
        algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas,
        algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas,
        algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas,
        algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas,
        algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas,
        algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas,
        algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas,
        algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas,
        quantasCriancasde3A6anosEstaoNaEscolinha,
        criancasDe3A6anosNaoEstaoNaEscolinhaPorque,
        quantasCriancasde3A6anosTrabalham,
        criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos,
        totalcriancasde7A9anos,
        totalcriancasde7A9anosMeninos,
        totalcriancasde7A9anosMeninas,
        quantasCriancasde7A9anosEstaoNaEscola,
        quantasCriancasde7A9anosTrabalham,
        nosUltimos5AnosQuantasGravidezesTiveNaFamilia,
        partosOcoridosNoHospital,
        partosOcoridosEmCasa,
        partosOcoridosEmOutroLocal,
        outroLocalDoPartoQualLocal,
        quantasCriancasNasceramVivas,
        quantasContinuamVivas,
        dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram,
        descreverOProblema,
        aFamiliaProcurouAjudaParaSuperarEsseProblema,
        seSimQueTipoDeAjudaEOnde,
        osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia,
        dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida,
        sabeDizerOMotivoDosObitos,
        seSimQualFoiPrincipalRazaodoObito,
        voceTemMedoDeTerUmFilhoOuNetoComProblemas,
        expliquePorQue,
        porqueVoceAchaQueUmaCriancaPodeNascerComProblema,
        oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema,
        achaQueUmaCriancaQueNasceComProblemaPodeSobreviver,
        expliquePorque,
        quemPodeAjudarQuandoUmaCriancaNasceComProblema,
        emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao,
        emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene,
        emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos,
        aPessoaEntrevistadaPareceuSinceraNasSuasRespostas,
        algumasPerguntasIncomodaramAPessoaEntrevistada,
        seSimQuais3PrincipaisPerguntas,
        todasAsPerguntasIncomodaram,
        aResidenciaEraDe,
        comoAvaliaHigieneDaCasa,
        oQueEraMal,
        tinhaCriancasNaCasaDuranteInquerito,
        tinhaCriancasNaCasaDuranteInqueritoQuantas,
        tentouIncentivar,
        tentouIncentivarQuantas,
        seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos,
        observouCriancasDeAte1Ano,
        observouCriancasDeAte1AnoQuantos,
        algumasApresentavamProblemas,
        algumasApresentavamProblemasQuantos,
        descreveOProblemaDessasCriancas,
        observouCriancasDe1AnoEmeioAte3Anos,
        observouCriancasDe1AnoEmeioAte3AnosQuantas,
        algumasCriancasDe1anoEmeioObservadasApresentavamProblemas,
        algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas,
        algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve,
        observouCriancasDe3AnosAte6Anos,
        observouCriancasDe3AnosAte6AnosQuantas,
        observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas,
        observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas,
        observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve,
        observouSituacoesQueLheChamaramAtencao,
        emRelacaoAPessoaEntrevistada,
        emRelacaoAPessoaEntrevistadaDescreve,
        emRelacaoAOutrosAdultosPresente,
        emRelacaoAOutrosAdultosPresenteDescreve,
        emRelacaoAsCriancasPresentes,
        emRelacaoAsCriancasPresentesDescreve,
        emRelacaoAVizinhanca,
        emRelacaoAVizinhancaDescreve,
        outras,
        outrasDescreve,
        createdAt,
        updatedAt
      ];
  @override
  $UsersTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'users';
  @override
  final String actualTableName = 'users';
  @override
  VerificationContext validateIntegrity(Insertable<User> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('uuid')) {
      context.handle(
          _uuidMeta, uuid.isAcceptableOrUnknown(data['uuid'], _uuidMeta));
    }
    if (data.containsKey('nome_do_pesquisador')) {
      context.handle(
          _nomeDoPesquisadorMeta,
          nomeDoPesquisador.isAcceptableOrUnknown(
              data['nome_do_pesquisador'], _nomeDoPesquisadorMeta));
    }
    if (data.containsKey('numero_de_inquerito')) {
      context.handle(
          _numeroDeInqueritoMeta,
          numeroDeInquerito.isAcceptableOrUnknown(
              data['numero_de_inquerito'], _numeroDeInqueritoMeta));
    }
    if (data.containsKey('data')) {
      context.handle(
          _dataMeta, this.data.isAcceptableOrUnknown(data['data'], _dataMeta));
    }
    if (data.containsKey('municipio')) {
      context.handle(_municipioMeta,
          municipio.isAcceptableOrUnknown(data['municipio'], _municipioMeta));
    }
    if (data.containsKey('bairro')) {
      context.handle(_bairroMeta,
          bairro.isAcceptableOrUnknown(data['bairro'], _bairroMeta));
    }
    if (data.containsKey('ponto_de_referencia_da_casa')) {
      context.handle(
          _pontoDeReferenciaDaCasaMeta,
          pontoDeReferenciaDaCasa.isAcceptableOrUnknown(
              data['ponto_de_referencia_da_casa'],
              _pontoDeReferenciaDaCasaMeta));
    }
    if (data.containsKey('nome')) {
      context.handle(
          _nomeMeta, nome.isAcceptableOrUnknown(data['nome'], _nomeMeta));
    }
    if (data.containsKey('idade')) {
      context.handle(
          _idadeMeta, idade.isAcceptableOrUnknown(data['idade'], _idadeMeta));
    }
    if (data.containsKey('contacto')) {
      context.handle(_contactoMeta,
          contacto.isAcceptableOrUnknown(data['contacto'], _contactoMeta));
    }
    if (data.containsKey('e_chefe_da_familia')) {
      context.handle(
          _eChefeDaFamiliaMeta,
          eChefeDaFamilia.isAcceptableOrUnknown(
              data['e_chefe_da_familia'], _eChefeDaFamiliaMeta));
    }
    if (data.containsKey('cao_nao_vinculo_de_parantesco')) {
      context.handle(
          _caoNaoVinculoDeParantescoMeta,
          caoNaoVinculoDeParantesco.isAcceptableOrUnknown(
              data['cao_nao_vinculo_de_parantesco'],
              _caoNaoVinculoDeParantescoMeta));
    }
    if (data.containsKey('idade_do_chefe_da_familia')) {
      context.handle(
          _idadeDoChefeDaFamiliaMeta,
          idadeDoChefeDaFamilia.isAcceptableOrUnknown(
              data['idade_do_chefe_da_familia'], _idadeDoChefeDaFamiliaMeta));
    }
    if (data.containsKey('estudou')) {
      context.handle(_estudouMeta,
          estudou.isAcceptableOrUnknown(data['estudou'], _estudouMeta));
    }
    if (data.containsKey('e_alfabetizado')) {
      context.handle(
          _eAlfabetizadoMeta,
          eAlfabetizado.isAcceptableOrUnknown(
              data['e_alfabetizado'], _eAlfabetizadoMeta));
    }
    if (data.containsKey('completou_ensino_primario')) {
      context.handle(
          _completouEnsinoPrimarioMeta,
          completouEnsinoPrimario.isAcceptableOrUnknown(
              data['completou_ensino_primario'], _completouEnsinoPrimarioMeta));
    }
    if (data.containsKey('completou_ensino_secundario')) {
      context.handle(
          _completouEnsinoSecundarioMeta,
          completouEnsinoSecundario.isAcceptableOrUnknown(
              data['completou_ensino_secundario'],
              _completouEnsinoSecundarioMeta));
    }
    if (data.containsKey('fez_curso_superior')) {
      context.handle(
          _fezCursoSuperiorMeta,
          fezCursoSuperior.isAcceptableOrUnknown(
              data['fez_curso_superior'], _fezCursoSuperiorMeta));
    }
    if (data.containsKey('nao_sabe')) {
      context.handle(_naoSabeMeta,
          naoSabe.isAcceptableOrUnknown(data['nao_sabe'], _naoSabeMeta));
    }
    if (data.containsKey('chefe_de_familia_tem_salario')) {
      context.handle(
          _chefeDeFamiliaTemSalarioMeta,
          chefeDeFamiliaTemSalario.isAcceptableOrUnknown(
              data['chefe_de_familia_tem_salario'],
              _chefeDeFamiliaTemSalarioMeta));
    }
    if (data.containsKey('quanto_gasta_para_sustentar_familia_por_mes')) {
      context.handle(
          _quantoGastaParaSustentarFamiliaPorMesMeta,
          quantoGastaParaSustentarFamiliaPorMes.isAcceptableOrUnknown(
              data['quanto_gasta_para_sustentar_familia_por_mes'],
              _quantoGastaParaSustentarFamiliaPorMesMeta));
    }
    if (data.containsKey('quantas_familias_residem_nacasa')) {
      context.handle(
          _quantasFamiliasResidemNacasaMeta,
          quantasFamiliasResidemNacasa.isAcceptableOrUnknown(
              data['quantas_familias_residem_nacasa'],
              _quantasFamiliasResidemNacasaMeta));
    }
    if (data.containsKey('quantos_adultos')) {
      context.handle(
          _quantosAdultosMeta,
          quantosAdultos.isAcceptableOrUnknown(
              data['quantos_adultos'], _quantosAdultosMeta));
    }
    if (data.containsKey('quantos_adultos_maiores_de18anos')) {
      context.handle(
          _quantosAdultosMaioresDe18anosMeta,
          quantosAdultosMaioresDe18anos.isAcceptableOrUnknown(
              data['quantos_adultos_maiores_de18anos'],
              _quantosAdultosMaioresDe18anosMeta));
    }
    if (data.containsKey('quantos_adultos_maiores_de18anos_homens')) {
      context.handle(
          _quantosAdultosMaioresDe18anosHomensMeta,
          quantosAdultosMaioresDe18anosHomens.isAcceptableOrUnknown(
              data['quantos_adultos_maiores_de18anos_homens'],
              _quantosAdultosMaioresDe18anosHomensMeta));
    }
    if (data.containsKey('quantos_adultos_maiores_de18anos_homens_trabalham')) {
      context.handle(
          _quantosAdultosMaioresDe18anosHomensTrabalhamMeta,
          quantosAdultosMaioresDe18anosHomensTrabalham.isAcceptableOrUnknown(
              data['quantos_adultos_maiores_de18anos_homens_trabalham'],
              _quantosAdultosMaioresDe18anosHomensTrabalhamMeta));
    }
    if (data
        .containsKey('quantos_adultos_maiores_de18anos_homens_alfabetizados')) {
      context.handle(
          _quantosAdultosMaioresDe18anosHomensAlfabetizadosMeta,
          quantosAdultosMaioresDe18anosHomensAlfabetizados
              .isAcceptableOrUnknown(
                  data['quantos_adultos_maiores_de18anos_homens_alfabetizados'],
                  _quantosAdultosMaioresDe18anosHomensAlfabetizadosMeta));
    }
    if (data.containsKey('quantos_adultos_m18_mulheres')) {
      context.handle(
          _quantosAdultosM18MulheresMeta,
          quantosAdultosM18Mulheres.isAcceptableOrUnknown(
              data['quantos_adultos_m18_mulheres'],
              _quantosAdultosM18MulheresMeta));
    }
    if (data.containsKey('quantos_adultos_m18_mulheres_trabalham')) {
      context.handle(
          _quantosAdultosM18MulheresTrabalhamMeta,
          quantosAdultosM18MulheresTrabalham.isAcceptableOrUnknown(
              data['quantos_adultos_m18_mulheres_trabalham'],
              _quantosAdultosM18MulheresTrabalhamMeta));
    }
    if (data.containsKey('quantos_adultos_m18_mulheres_alfabetizados')) {
      context.handle(
          _quantosAdultosM18MulheresAlfabetizadosMeta,
          quantosAdultosM18MulheresAlfabetizados.isAcceptableOrUnknown(
              data['quantos_adultos_m18_mulheres_alfabetizados'],
              _quantosAdultosM18MulheresAlfabetizadosMeta));
    }
    if (data.containsKey('relatos_de_deficiencia')) {
      context.handle(
          _relatosDeDeficienciaMeta,
          relatosDeDeficiencia.isAcceptableOrUnknown(
              data['relatos_de_deficiencia'], _relatosDeDeficienciaMeta));
    }
    if (data.containsKey('totalcriancas_ate1anoemeio')) {
      context.handle(
          _totalcriancasAte1anoemeioMeta,
          totalcriancasAte1anoemeio.isAcceptableOrUnknown(
              data['totalcriancas_ate1anoemeio'],
              _totalcriancasAte1anoemeioMeta));
    }
    if (data.containsKey('totalcriancas_ate1anoemeio_meninos')) {
      context.handle(
          _totalcriancasAte1anoemeioMeninosMeta,
          totalcriancasAte1anoemeioMeninos.isAcceptableOrUnknown(
              data['totalcriancas_ate1anoemeio_meninos'],
              _totalcriancasAte1anoemeioMeninosMeta));
    }
    if (data.containsKey('totalcriancas_ate1anoemeio_meninas')) {
      context.handle(
          _totalcriancasAte1anoemeioMeninasMeta,
          totalcriancasAte1anoemeioMeninas.isAcceptableOrUnknown(
              data['totalcriancas_ate1anoemeio_meninas'],
              _totalcriancasAte1anoemeioMeninasMeta));
    }
    if (data.containsKey('algumas_criancas_ate1anoemeio_naodesenvolvem')) {
      context.handle(
          _algumasCriancasAte1anoemeioNaodesenvolvemMeta,
          algumasCriancasAte1anoemeioNaodesenvolvem.isAcceptableOrUnknown(
              data['algumas_criancas_ate1anoemeio_naodesenvolvem'],
              _algumasCriancasAte1anoemeioNaodesenvolvemMeta));
    }
    if (data
        .containsKey('algumas_criancas_ate1anoemeio_naodesenvolvem_quantas')) {
      context.handle(
          _algumasCriancasAte1anoemeioNaodesenvolvemQuantasMeta,
          algumasCriancasAte1anoemeioNaodesenvolvemQuantas
              .isAcceptableOrUnknown(
                  data['algumas_criancas_ate1anoemeio_naodesenvolvem_quantas'],
                  _algumasCriancasAte1anoemeioNaodesenvolvemQuantasMeta));
    }
    if (data.containsKey('alguma_criancas_ate1anoemeio_que_tipode_problema')) {
      context.handle(
          _algumaCriancasAte1anoemeioQueTipodeProblemaMeta,
          algumaCriancasAte1anoemeioQueTipodeProblema.isAcceptableOrUnknown(
              data['alguma_criancas_ate1anoemeio_que_tipode_problema'],
              _algumaCriancasAte1anoemeioQueTipodeProblemaMeta));
    }
    if (data.containsKey('alguma_criancas_ate1anoemeio_tem_problema_motor')) {
      context.handle(
          _algumaCriancasAte1anoemeioTemProblemaMotorMeta,
          algumaCriancasAte1anoemeioTemProblemaMotor.isAcceptableOrUnknown(
              data['alguma_criancas_ate1anoemeio_tem_problema_motor'],
              _algumaCriancasAte1anoemeioTemProblemaMotorMeta));
    }
    if (data.containsKey(
        'alguma_criancas_ate1anoemeio_tem_problema_motor_quantas')) {
      context.handle(
          _algumaCriancasAte1anoemeioTemProblemaMotorQuantasMeta,
          algumaCriancasAte1anoemeioTemProblemaMotorQuantas
              .isAcceptableOrUnknown(
                  data[
                      'alguma_criancas_ate1anoemeio_tem_problema_motor_quantas'],
                  _algumaCriancasAte1anoemeioTemProblemaMotorQuantasMeta));
    }
    if (data.containsKey('alguma_criancas_ate1anoemeio_tem_mal_formacao')) {
      context.handle(
          _algumaCriancasAte1anoemeioTemMalFormacaoMeta,
          algumaCriancasAte1anoemeioTemMalFormacao.isAcceptableOrUnknown(
              data['alguma_criancas_ate1anoemeio_tem_mal_formacao'],
              _algumaCriancasAte1anoemeioTemMalFormacaoMeta));
    }
    if (data
        .containsKey('alguma_criancas_ate1anoemeio_tem_mal_formacao_quantas')) {
      context.handle(
          _algumaCriancasAte1anoemeioTemMalFormacaoQuantasMeta,
          algumaCriancasAte1anoemeioTemMalFormacaoQuantas.isAcceptableOrUnknown(
              data['alguma_criancas_ate1anoemeio_tem_mal_formacao_quantas'],
              _algumaCriancasAte1anoemeioTemMalFormacaoQuantasMeta));
    }
    if (data
        .containsKey('alguma_criancas_ate1anoemeio_nao_reagem_a_incentivo')) {
      context.handle(
          _algumaCriancasAte1anoemeioNaoReagemAIncentivoMeta,
          algumaCriancasAte1anoemeioNaoReagemAIncentivo.isAcceptableOrUnknown(
              data['alguma_criancas_ate1anoemeio_nao_reagem_a_incentivo'],
              _algumaCriancasAte1anoemeioNaoReagemAIncentivoMeta));
    }
    if (data.containsKey(
        'alguma_criancas_ate1anoemeio_nao_reagem_a_incentivo_quantas')) {
      context.handle(
          _algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantasMeta,
          algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas
              .isAcceptableOrUnknown(
                  data[
                      'alguma_criancas_ate1anoemeio_nao_reagem_a_incentivo_quantas'],
                  _algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantasMeta));
    }
    if (data.containsKey('alguma_criancas_ate1anoemeio_tem_reacao_agressiva')) {
      context.handle(
          _algumaCriancasAte1anoemeioTemReacaoAgressivaMeta,
          algumaCriancasAte1anoemeioTemReacaoAgressiva.isAcceptableOrUnknown(
              data['alguma_criancas_ate1anoemeio_tem_reacao_agressiva'],
              _algumaCriancasAte1anoemeioTemReacaoAgressivaMeta));
    }
    if (data.containsKey(
        'alguma_criancas_ate1anoemeio_tem_reacao_agressiva_quantas')) {
      context.handle(
          _algumaCriancasAte1anoemeioTemReacaoAgressivaQuantasMeta,
          algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas
              .isAcceptableOrUnknown(
                  data[
                      'alguma_criancas_ate1anoemeio_tem_reacao_agressiva_quantas'],
                  _algumaCriancasAte1anoemeioTemReacaoAgressivaQuantasMeta));
    }
    if (data.containsKey('totalcriancas_ate1anoemeio_a3anos')) {
      context.handle(
          _totalcriancasAte1anoemeioA3anosMeta,
          totalcriancasAte1anoemeioA3anos.isAcceptableOrUnknown(
              data['totalcriancas_ate1anoemeio_a3anos'],
              _totalcriancasAte1anoemeioA3anosMeta));
    }
    if (data.containsKey('totalcriancas_ate1anoemeio_a3anos_meninos')) {
      context.handle(
          _totalcriancasAte1anoemeioA3anosMeninosMeta,
          totalcriancasAte1anoemeioA3anosMeninos.isAcceptableOrUnknown(
              data['totalcriancas_ate1anoemeio_a3anos_meninos'],
              _totalcriancasAte1anoemeioA3anosMeninosMeta));
    }
    if (data.containsKey('totalcriancas_ate1anoemeio_a3anos_meninas')) {
      context.handle(
          _totalcriancasAte1anoemeioA3anosMeninasMeta,
          totalcriancasAte1anoemeioA3anosMeninas.isAcceptableOrUnknown(
              data['totalcriancas_ate1anoemeio_a3anos_meninas'],
              _totalcriancasAte1anoemeioA3anosMeninasMeta));
    }
    if (data
        .containsKey('algumas_criancas_ate1anoemeio_a3anos_naodesenvolvem')) {
      context.handle(
          _algumasCriancasAte1anoemeioA3anosNaodesenvolvemMeta,
          algumasCriancasAte1anoemeioA3anosNaodesenvolvem.isAcceptableOrUnknown(
              data['algumas_criancas_ate1anoemeio_a3anos_naodesenvolvem'],
              _algumasCriancasAte1anoemeioA3anosNaodesenvolvemMeta));
    }
    if (data.containsKey(
        'algumas_criancas_ate1anoemeio_a3anos_naodesenvolvem_quantas')) {
      context.handle(
          _algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantasMeta,
          algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancas_ate1anoemeio_a3anos_naodesenvolvem_quantas'],
                  _algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantasMeta));
    }
    if (data.containsKey(
        'alguma_criancas_ate1anoemeio_a3anos_que_tipode_problema')) {
      context.handle(
          _algumaCriancasAte1anoemeioA3anosQueTipodeProblemaMeta,
          algumaCriancasAte1anoemeioA3anosQueTipodeProblema
              .isAcceptableOrUnknown(
                  data[
                      'alguma_criancas_ate1anoemeio_a3anos_que_tipode_problema'],
                  _algumaCriancasAte1anoemeioA3anosQueTipodeProblemaMeta));
    }
    if (data.containsKey(
        'algumas_criancas_ate1anoemeio_a3anos_nao_interagem_com_pessoas')) {
      context.handle(
          _algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasMeta,
          algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancas_ate1anoemeio_a3anos_nao_interagem_com_pessoas'],
                  _algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasMeta));
    }
    if (data.containsKey(
        'algumas_criancas_ate1anoemeio_a3anos_nao_interagem_com_pessoas_quantas')) {
      context.handle(
          _algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantasMeta,
          algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancas_ate1anoemeio_a3anos_nao_interagem_com_pessoas_quantas'],
                  _algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantasMeta));
    }
    if (data.containsKey(
        'algumas_criancas_ate1anoemeio_a3anos_tem_comportamento_agressivo')) {
      context.handle(
          _algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoMeta,
          algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancas_ate1anoemeio_a3anos_tem_comportamento_agressivo'],
                  _algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoMeta));
    }
    if (data.containsKey(
        'algumas_criancas_ate1anoemeio_a3anos_tem_comportamento_agressivo_quantas')) {
      context.handle(
          _algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantasMeta,
          algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancas_ate1anoemeio_a3anos_tem_comportamento_agressivo_quantas'],
                  _algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantasMeta));
    }
    if (data
        .containsKey('algumas_criancas_ate1anoemeio_a3anos_alguns_agridem')) {
      context.handle(
          _algumasCriancasAte1anoemeioA3anosAlgunsAgridemMeta,
          algumasCriancasAte1anoemeioA3anosAlgunsAgridem.isAcceptableOrUnknown(
              data['algumas_criancas_ate1anoemeio_a3anos_alguns_agridem'],
              _algumasCriancasAte1anoemeioA3anosAlgunsAgridemMeta));
    }
    if (data.containsKey(
        'algumas_criancas_ate1anoemeio_a3anos_alguns_agridem_quantas')) {
      context.handle(
          _algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantasMeta,
          algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancas_ate1anoemeio_a3anos_alguns_agridem_quantas'],
                  _algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantasMeta));
    }
    if (data.containsKey(
        'algumas_criancas_ate1anoemeio_a3anos_algumas_agitadas_que_o_normal')) {
      context.handle(
          _algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalMeta,
          algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancas_ate1anoemeio_a3anos_algumas_agitadas_que_o_normal'],
                  _algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalMeta));
    }
    if (data.containsKey(
        'algumas_criancas_ate1anoemeio_a3anos_algumas_agitadas_que_o_normal_quantas')) {
      context.handle(
          _algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantasMeta,
          algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancas_ate1anoemeio_a3anos_algumas_agitadas_que_o_normal_quantas'],
                  _algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantasMeta));
    }
    if (data.containsKey(
        'algumas_criancas_ate1anoemeio_a3anos_algumas_tem_mal_formacao')) {
      context.handle(
          _algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoMeta,
          algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancas_ate1anoemeio_a3anos_algumas_tem_mal_formacao'],
                  _algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoMeta));
    }
    if (data.containsKey(
        'algumas_criancas_ate1anoemeio_a3anos_algumas_tem_mal_formacao_quantas')) {
      context.handle(
          _algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantasMeta,
          algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancas_ate1anoemeio_a3anos_algumas_tem_mal_formacao_quantas'],
                  _algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantasMeta));
    }
    if (data.containsKey(
        'algumas_criancas_ate1anoemeio_a3anos_algumas_nao_andam_sozinhas')) {
      context.handle(
          _algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasMeta,
          algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancas_ate1anoemeio_a3anos_algumas_nao_andam_sozinhas'],
                  _algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasMeta));
    }
    if (data.containsKey(
        'algumas_criancas_ate1anoemeio_a3anos_algumas_nao_andam_sozinhas_quantas')) {
      context.handle(
          _algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantasMeta,
          algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancas_ate1anoemeio_a3anos_algumas_nao_andam_sozinhas_quantas'],
                  _algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantasMeta));
    }
    if (data.containsKey(
        'algumas_criancas_ate1anoemeio_a3anos_algumas_nao_expressam_verbalmente')) {
      context.handle(
          _algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteMeta,
          algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancas_ate1anoemeio_a3anos_algumas_nao_expressam_verbalmente'],
                  _algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteMeta));
    }
    if (data.containsKey(
        'algumas_criancas_ate1anoemeio_a3anos_algumas_nao_expressam_verbalmente_quantas')) {
      context.handle(
          _algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantasMeta,
          algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancas_ate1anoemeio_a3anos_algumas_nao_expressam_verbalmente_quantas'],
                  _algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantasMeta));
    }
    if (data.containsKey(
        'algumas_criancas_ate1anoemeio_a3anos_algumas_nao_expressam_sentimentos')) {
      context.handle(
          _algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosMeta,
          algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancas_ate1anoemeio_a3anos_algumas_nao_expressam_sentimentos'],
                  _algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosMeta));
    }
    if (data.containsKey(
        'algumas_criancas_ate1anoemeio_a3anos_algumas_nao_expressam_sentimentos_quantas')) {
      context.handle(
          _algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantasMeta,
          algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancas_ate1anoemeio_a3anos_algumas_nao_expressam_sentimentos_quantas'],
                  _algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantasMeta));
    }
    if (data.containsKey(
        'algumas_criancas_ate1anoemeio_a3anos_algumas_tem_sindrome_de_down')) {
      context.handle(
          _algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownMeta,
          algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancas_ate1anoemeio_a3anos_algumas_tem_sindrome_de_down'],
                  _algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownMeta));
    }
    if (data.containsKey(
        'algumas_criancas_ate1anoemeio_a3anos_algumas_tem_sindrome_de_down_quantas')) {
      context.handle(
          _algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantasMeta,
          algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancas_ate1anoemeio_a3anos_algumas_tem_sindrome_de_down_quantas'],
                  _algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantasMeta));
    }
    if (data.containsKey('totalcriancasde3_a6anos')) {
      context.handle(
          _totalcriancasde3A6anosMeta,
          totalcriancasde3A6anos.isAcceptableOrUnknown(
              data['totalcriancasde3_a6anos'], _totalcriancasde3A6anosMeta));
    }
    if (data.containsKey('totalcriancasde3_a6anos_meninos')) {
      context.handle(
          _totalcriancasde3A6anosMeninosMeta,
          totalcriancasde3A6anosMeninos.isAcceptableOrUnknown(
              data['totalcriancasde3_a6anos_meninos'],
              _totalcriancasde3A6anosMeninosMeta));
    }
    if (data.containsKey('totalcriancasde3_a6anos_meninas')) {
      context.handle(
          _totalcriancasde3A6anosMeninasMeta,
          totalcriancasde3A6anosMeninas.isAcceptableOrUnknown(
              data['totalcriancasde3_a6anos_meninas'],
              _totalcriancasde3A6anosMeninasMeta));
    }
    if (data.containsKey('algumas_criancasde3_a6anos_naodesenvolvem')) {
      context.handle(
          _algumasCriancasde3A6anosNaodesenvolvemMeta,
          algumasCriancasde3A6anosNaodesenvolvem.isAcceptableOrUnknown(
              data['algumas_criancasde3_a6anos_naodesenvolvem'],
              _algumasCriancasde3A6anosNaodesenvolvemMeta));
    }
    if (data.containsKey('algumas_criancasde3_a6anos_naodesenvolvem_quantas')) {
      context.handle(
          _algumasCriancasde3A6anosNaodesenvolvemQuantasMeta,
          algumasCriancasde3A6anosNaodesenvolvemQuantas.isAcceptableOrUnknown(
              data['algumas_criancasde3_a6anos_naodesenvolvem_quantas'],
              _algumasCriancasde3A6anosNaodesenvolvemQuantasMeta));
    }
    if (data.containsKey('alguma_criancasde3_a6anos_que_tipode_problema')) {
      context.handle(
          _algumaCriancasde3A6anosQueTipodeProblemaMeta,
          algumaCriancasde3A6anosQueTipodeProblema.isAcceptableOrUnknown(
              data['alguma_criancasde3_a6anos_que_tipode_problema'],
              _algumaCriancasde3A6anosQueTipodeProblemaMeta));
    }
    if (data
        .containsKey('algumas_criancasde3_a6anos_nao_interagem_com_pessoas')) {
      context.handle(
          _algumasCriancasde3A6anosNaoInteragemComPessoasMeta,
          algumasCriancasde3A6anosNaoInteragemComPessoas.isAcceptableOrUnknown(
              data['algumas_criancasde3_a6anos_nao_interagem_com_pessoas'],
              _algumasCriancasde3A6anosNaoInteragemComPessoasMeta));
    }
    if (data.containsKey(
        'algumas_criancasde3_a6anos_nao_interagem_com_pessoas_quantas')) {
      context.handle(
          _algumasCriancasde3A6anosNaoInteragemComPessoasQuantasMeta,
          algumasCriancasde3A6anosNaoInteragemComPessoasQuantas
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancasde3_a6anos_nao_interagem_com_pessoas_quantas'],
                  _algumasCriancasde3A6anosNaoInteragemComPessoasQuantasMeta));
    }
    if (data.containsKey(
        'algumas_criancasde3_a6anos_tem_comportamento_agressivo')) {
      context.handle(
          _algumasCriancasde3A6anosTemComportamentoAgressivoMeta,
          algumasCriancasde3A6anosTemComportamentoAgressivo
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancasde3_a6anos_tem_comportamento_agressivo'],
                  _algumasCriancasde3A6anosTemComportamentoAgressivoMeta));
    }
    if (data.containsKey(
        'algumas_criancasde3_a6anos_tem_comportamento_agressivo_quantas')) {
      context.handle(
          _algumasCriancasde3A6anosTemComportamentoAgressivoQuantasMeta,
          algumasCriancasde3A6anosTemComportamentoAgressivoQuantas
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancasde3_a6anos_tem_comportamento_agressivo_quantas'],
                  _algumasCriancasde3A6anosTemComportamentoAgressivoQuantasMeta));
    }
    if (data.containsKey('algumas_criancasde3_a6anos_alguns_agridem')) {
      context.handle(
          _algumasCriancasde3A6anosAlgunsAgridemMeta,
          algumasCriancasde3A6anosAlgunsAgridem.isAcceptableOrUnknown(
              data['algumas_criancasde3_a6anos_alguns_agridem'],
              _algumasCriancasde3A6anosAlgunsAgridemMeta));
    }
    if (data.containsKey('algumas_criancasde3_a6anos_alguns_agridem_quantas')) {
      context.handle(
          _algumasCriancasde3A6anosAlgunsAgridemQuantasMeta,
          algumasCriancasde3A6anosAlgunsAgridemQuantas.isAcceptableOrUnknown(
              data['algumas_criancasde3_a6anos_alguns_agridem_quantas'],
              _algumasCriancasde3A6anosAlgunsAgridemQuantasMeta));
    }
    if (data.containsKey(
        'algumas_criancasde3_a6anos_algumas_agitadas_que_o_normal')) {
      context.handle(
          _algumasCriancasde3A6anosAlgumasAgitadasQueONormalMeta,
          algumasCriancasde3A6anosAlgumasAgitadasQueONormal.isAcceptableOrUnknown(
              data['algumas_criancasde3_a6anos_algumas_agitadas_que_o_normal'],
              _algumasCriancasde3A6anosAlgumasAgitadasQueONormalMeta));
    }
    if (data.containsKey(
        'algumas_criancasde3_a6anos_algumas_agitadas_que_o_normal_quantas')) {
      context.handle(
          _algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantasMeta,
          algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancasde3_a6anos_algumas_agitadas_que_o_normal_quantas'],
                  _algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantasMeta));
    }
    if (data
        .containsKey('algumas_criancasde3_a6anos_algumas_tem_mal_formacao')) {
      context.handle(
          _algumasCriancasde3A6anosAlgumasTemMalFormacaoMeta,
          algumasCriancasde3A6anosAlgumasTemMalFormacao.isAcceptableOrUnknown(
              data['algumas_criancasde3_a6anos_algumas_tem_mal_formacao'],
              _algumasCriancasde3A6anosAlgumasTemMalFormacaoMeta));
    }
    if (data.containsKey(
        'algumas_criancasde3_a6anos_algumas_tem_mal_formacao_quantas')) {
      context.handle(
          _algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantasMeta,
          algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancasde3_a6anos_algumas_tem_mal_formacao_quantas'],
                  _algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantasMeta));
    }
    if (data
        .containsKey('algumas_criancasde3_a6anos_algumas_nao_andam_sozinhas')) {
      context.handle(
          _algumasCriancasde3A6anosAlgumasNaoAndamSozinhasMeta,
          algumasCriancasde3A6anosAlgumasNaoAndamSozinhas.isAcceptableOrUnknown(
              data['algumas_criancasde3_a6anos_algumas_nao_andam_sozinhas'],
              _algumasCriancasde3A6anosAlgumasNaoAndamSozinhasMeta));
    }
    if (data.containsKey(
        'algumas_criancasde3_a6anos_algumas_nao_andam_sozinhas_quantas')) {
      context.handle(
          _algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantasMeta,
          algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancasde3_a6anos_algumas_nao_andam_sozinhas_quantas'],
                  _algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantasMeta));
    }
    if (data.containsKey(
        'algumas_criancasde3_a6anos_algumas_nao_expressam_verbalmente')) {
      context.handle(
          _algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteMeta,
          algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancasde3_a6anos_algumas_nao_expressam_verbalmente'],
                  _algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteMeta));
    }
    if (data.containsKey(
        'algumas_criancasde3_a6anos_algumas_nao_expressam_verbalmente_quantas')) {
      context.handle(
          _algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantasMeta,
          algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancasde3_a6anos_algumas_nao_expressam_verbalmente_quantas'],
                  _algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantasMeta));
    }
    if (data.containsKey(
        'algumas_criancasde3_a6anos_algumas_nao_expressam_sentimentos')) {
      context.handle(
          _algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosMeta,
          algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancasde3_a6anos_algumas_nao_expressam_sentimentos'],
                  _algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosMeta));
    }
    if (data.containsKey(
        'algumas_criancasde3_a6anos_algumas_nao_expressam_sentimentos_quantas')) {
      context.handle(
          _algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantasMeta,
          algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancasde3_a6anos_algumas_nao_expressam_sentimentos_quantas'],
                  _algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantasMeta));
    }
    if (data.containsKey(
        'algumas_criancasde3_a6anos_algumas_tem_sindrome_de_down')) {
      context.handle(
          _algumasCriancasde3A6anosAlgumasTemSindromeDeDownMeta,
          algumasCriancasde3A6anosAlgumasTemSindromeDeDown.isAcceptableOrUnknown(
              data['algumas_criancasde3_a6anos_algumas_tem_sindrome_de_down'],
              _algumasCriancasde3A6anosAlgumasTemSindromeDeDownMeta));
    }
    if (data.containsKey(
        'algumas_criancasde3_a6anos_algumas_tem_sindrome_de_down_quantas')) {
      context.handle(
          _algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantasMeta,
          algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancasde3_a6anos_algumas_tem_sindrome_de_down_quantas'],
                  _algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantasMeta));
    }
    if (data.containsKey(
        'algumas_criancasde3_a6anos_algumas_nao_sabem_se_alimentar_sozinhas')) {
      context.handle(
          _algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhasMeta,
          algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancasde3_a6anos_algumas_nao_sabem_se_alimentar_sozinhas'],
                  _algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhasMeta));
    }
    if (data.containsKey(
        'algumas_criancasde3_a6anos_algumas_nao_sabe_se_alimentar_sozinhas_quantas')) {
      context.handle(
          _algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantasMeta,
          algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancasde3_a6anos_algumas_nao_sabe_se_alimentar_sozinhas_quantas'],
                  _algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantasMeta));
    }
    if (data.containsKey(
        'algumas_criancasde3_a6anos_algumas_nao_conseguem_tomar_banho_sozinhas')) {
      context.handle(
          _algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasMeta,
          algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancasde3_a6anos_algumas_nao_conseguem_tomar_banho_sozinhas'],
                  _algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasMeta));
    }
    if (data.containsKey(
        'algumas_criancasde3_a6anos_algumas_nao_conseguem_tomar_banho_sozinhas_quantas')) {
      context.handle(
          _algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantasMeta,
          algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancasde3_a6anos_algumas_nao_conseguem_tomar_banho_sozinhas_quantas'],
                  _algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantasMeta));
    }
    if (data.containsKey(
        'algumas_criancasde3_a6anos_algumas_nao_conseguem_ir_a_casa_de_banho_sozinhas')) {
      context.handle(
          _algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasMeta,
          algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancasde3_a6anos_algumas_nao_conseguem_ir_a_casa_de_banho_sozinhas'],
                  _algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasMeta));
    }
    if (data.containsKey(
        'algumas_criancasde3_a6anos_algumas_nao_conseguem_ir_a_casa_de_banho_sozinhas_quantas')) {
      context.handle(
          _algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantasMeta,
          algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancasde3_a6anos_algumas_nao_conseguem_ir_a_casa_de_banho_sozinhas_quantas'],
                  _algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantasMeta));
    }
    if (data.containsKey(
        'algumas_criancasde3_a6anos_algumas_nao_conseguem_se_vestir_sozinhas')) {
      context.handle(
          _algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasMeta,
          algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancasde3_a6anos_algumas_nao_conseguem_se_vestir_sozinhas'],
                  _algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasMeta));
    }
    if (data.containsKey(
        'algumas_criancasde3_a6anos_algumas_nao_conseguem_se_vestir_sozinhas_quantas')) {
      context.handle(
          _algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantasMeta,
          algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancasde3_a6anos_algumas_nao_conseguem_se_vestir_sozinhas_quantas'],
                  _algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantasMeta));
    }
    if (data.containsKey('quantas_criancasde3_a6anos_estao_na_escolinha')) {
      context.handle(
          _quantasCriancasde3A6anosEstaoNaEscolinhaMeta,
          quantasCriancasde3A6anosEstaoNaEscolinha.isAcceptableOrUnknown(
              data['quantas_criancasde3_a6anos_estao_na_escolinha'],
              _quantasCriancasde3A6anosEstaoNaEscolinhaMeta));
    }
    if (data.containsKey('criancas_de3_a6anos_nao_estao_na_escolinha_porque')) {
      context.handle(
          _criancasDe3A6anosNaoEstaoNaEscolinhaPorqueMeta,
          criancasDe3A6anosNaoEstaoNaEscolinhaPorque.isAcceptableOrUnknown(
              data['criancas_de3_a6anos_nao_estao_na_escolinha_porque'],
              _criancasDe3A6anosNaoEstaoNaEscolinhaPorqueMeta));
    }
    if (data.containsKey('quantas_criancasde3_a6anos_trabalham')) {
      context.handle(
          _quantasCriancasde3A6anosTrabalhamMeta,
          quantasCriancasde3A6anosTrabalham.isAcceptableOrUnknown(
              data['quantas_criancasde3_a6anos_trabalham'],
              _quantasCriancasde3A6anosTrabalhamMeta));
    }
    if (data.containsKey(
        'criancas_de3_a6anos_nao_estao_na_escolinha_por_outos_motivos')) {
      context.handle(
          _criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivosMeta,
          criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos.isAcceptableOrUnknown(
              data[
                  'criancas_de3_a6anos_nao_estao_na_escolinha_por_outos_motivos'],
              _criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivosMeta));
    }
    if (data.containsKey('totalcriancasde7_a9anos')) {
      context.handle(
          _totalcriancasde7A9anosMeta,
          totalcriancasde7A9anos.isAcceptableOrUnknown(
              data['totalcriancasde7_a9anos'], _totalcriancasde7A9anosMeta));
    }
    if (data.containsKey('totalcriancasde7_a9anos_meninos')) {
      context.handle(
          _totalcriancasde7A9anosMeninosMeta,
          totalcriancasde7A9anosMeninos.isAcceptableOrUnknown(
              data['totalcriancasde7_a9anos_meninos'],
              _totalcriancasde7A9anosMeninosMeta));
    }
    if (data.containsKey('totalcriancasde7_a9anos_meninas')) {
      context.handle(
          _totalcriancasde7A9anosMeninasMeta,
          totalcriancasde7A9anosMeninas.isAcceptableOrUnknown(
              data['totalcriancasde7_a9anos_meninas'],
              _totalcriancasde7A9anosMeninasMeta));
    }
    if (data.containsKey('quantas_criancasde7_a9anos_estao_na_escola')) {
      context.handle(
          _quantasCriancasde7A9anosEstaoNaEscolaMeta,
          quantasCriancasde7A9anosEstaoNaEscola.isAcceptableOrUnknown(
              data['quantas_criancasde7_a9anos_estao_na_escola'],
              _quantasCriancasde7A9anosEstaoNaEscolaMeta));
    }
    if (data.containsKey('quantas_criancasde7_a9anos_trabalham')) {
      context.handle(
          _quantasCriancasde7A9anosTrabalhamMeta,
          quantasCriancasde7A9anosTrabalham.isAcceptableOrUnknown(
              data['quantas_criancasde7_a9anos_trabalham'],
              _quantasCriancasde7A9anosTrabalhamMeta));
    }
    if (data
        .containsKey('nos_ultimos5_anos_quantas_gravidezes_tive_na_familia')) {
      context.handle(
          _nosUltimos5AnosQuantasGravidezesTiveNaFamiliaMeta,
          nosUltimos5AnosQuantasGravidezesTiveNaFamilia.isAcceptableOrUnknown(
              data['nos_ultimos5_anos_quantas_gravidezes_tive_na_familia'],
              _nosUltimos5AnosQuantasGravidezesTiveNaFamiliaMeta));
    }
    if (data.containsKey('partos_ocoridos_no_hospital')) {
      context.handle(
          _partosOcoridosNoHospitalMeta,
          partosOcoridosNoHospital.isAcceptableOrUnknown(
              data['partos_ocoridos_no_hospital'],
              _partosOcoridosNoHospitalMeta));
    }
    if (data.containsKey('partos_ocoridos_em_casa')) {
      context.handle(
          _partosOcoridosEmCasaMeta,
          partosOcoridosEmCasa.isAcceptableOrUnknown(
              data['partos_ocoridos_em_casa'], _partosOcoridosEmCasaMeta));
    }
    if (data.containsKey('partos_ocoridos_em_outro_local')) {
      context.handle(
          _partosOcoridosEmOutroLocalMeta,
          partosOcoridosEmOutroLocal.isAcceptableOrUnknown(
              data['partos_ocoridos_em_outro_local'],
              _partosOcoridosEmOutroLocalMeta));
    }
    if (data.containsKey('outro_local_do_parto_qual_local')) {
      context.handle(
          _outroLocalDoPartoQualLocalMeta,
          outroLocalDoPartoQualLocal.isAcceptableOrUnknown(
              data['outro_local_do_parto_qual_local'],
              _outroLocalDoPartoQualLocalMeta));
    }
    if (data.containsKey('quantas_criancas_nasceram_vivas')) {
      context.handle(
          _quantasCriancasNasceramVivasMeta,
          quantasCriancasNasceramVivas.isAcceptableOrUnknown(
              data['quantas_criancas_nasceram_vivas'],
              _quantasCriancasNasceramVivasMeta));
    }
    if (data.containsKey('quantas_continuam_vivas')) {
      context.handle(
          _quantasContinuamVivasMeta,
          quantasContinuamVivas.isAcceptableOrUnknown(
              data['quantas_continuam_vivas'], _quantasContinuamVivasMeta));
    }
    if (data.containsKey(
        'das_vivas_algumas_apresentam_problemas_de_desenvolvimento_que_outros_nao_tiveram')) {
      context.handle(
          _dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveramMeta,
          dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram
              .isAcceptableOrUnknown(
                  data[
                      'das_vivas_algumas_apresentam_problemas_de_desenvolvimento_que_outros_nao_tiveram'],
                  _dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveramMeta));
    }
    if (data.containsKey('descrever_o_problema')) {
      context.handle(
          _descreverOProblemaMeta,
          descreverOProblema.isAcceptableOrUnknown(
              data['descrever_o_problema'], _descreverOProblemaMeta));
    }
    if (data
        .containsKey('a_familia_procurou_ajuda_para_superar_esse_problema')) {
      context.handle(
          _aFamiliaProcurouAjudaParaSuperarEsseProblemaMeta,
          aFamiliaProcurouAjudaParaSuperarEsseProblema.isAcceptableOrUnknown(
              data['a_familia_procurou_ajuda_para_superar_esse_problema'],
              _aFamiliaProcurouAjudaParaSuperarEsseProblemaMeta));
    }
    if (data.containsKey('se_sim_que_tipo_de_ajuda_e_onde')) {
      context.handle(
          _seSimQueTipoDeAjudaEOndeMeta,
          seSimQueTipoDeAjudaEOnde.isAcceptableOrUnknown(
              data['se_sim_que_tipo_de_ajuda_e_onde'],
              _seSimQueTipoDeAjudaEOndeMeta));
    }
    if (data.containsKey(
        'os_vizinhos_se_preocuparam_quando_nasceu_uma_crianca_com_problema_na_familia')) {
      context.handle(
          _osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamiliaMeta,
          osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia
              .isAcceptableOrUnknown(
                  data[
                      'os_vizinhos_se_preocuparam_quando_nasceu_uma_crianca_com_problema_na_familia'],
                  _osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamiliaMeta));
    }
    if (data.containsKey(
        'das_criancas_que_nasceram_vivas_quantas_nao_sobreviveram_nos2_primeiros_anos_de_vida')) {
      context.handle(
          _dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVidaMeta,
          dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida
              .isAcceptableOrUnknown(
                  data[
                      'das_criancas_que_nasceram_vivas_quantas_nao_sobreviveram_nos2_primeiros_anos_de_vida'],
                  _dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVidaMeta));
    }
    if (data.containsKey('sabe_dizer_o_motivo_dos_obitos')) {
      context.handle(
          _sabeDizerOMotivoDosObitosMeta,
          sabeDizerOMotivoDosObitos.isAcceptableOrUnknown(
              data['sabe_dizer_o_motivo_dos_obitos'],
              _sabeDizerOMotivoDosObitosMeta));
    }
    if (data.containsKey('se_sim_qual_foi_principal_razaodo_obito')) {
      context.handle(
          _seSimQualFoiPrincipalRazaodoObitoMeta,
          seSimQualFoiPrincipalRazaodoObito.isAcceptableOrUnknown(
              data['se_sim_qual_foi_principal_razaodo_obito'],
              _seSimQualFoiPrincipalRazaodoObitoMeta));
    }
    if (data
        .containsKey('voce_tem_medo_de_ter_um_filho_ou_neto_com_problemas')) {
      context.handle(
          _voceTemMedoDeTerUmFilhoOuNetoComProblemasMeta,
          voceTemMedoDeTerUmFilhoOuNetoComProblemas.isAcceptableOrUnknown(
              data['voce_tem_medo_de_ter_um_filho_ou_neto_com_problemas'],
              _voceTemMedoDeTerUmFilhoOuNetoComProblemasMeta));
    }
    if (data.containsKey('explique_por_que')) {
      context.handle(
          _expliquePorQueMeta,
          expliquePorQue.isAcceptableOrUnknown(
              data['explique_por_que'], _expliquePorQueMeta));
    }
    if (data.containsKey(
        'porque_voce_acha_que_uma_crianca_pode_nascer_com_problema')) {
      context.handle(
          _porqueVoceAchaQueUmaCriancaPodeNascerComProblemaMeta,
          porqueVoceAchaQueUmaCriancaPodeNascerComProblema.isAcceptableOrUnknown(
              data['porque_voce_acha_que_uma_crianca_pode_nascer_com_problema'],
              _porqueVoceAchaQueUmaCriancaPodeNascerComProblemaMeta));
    }
    if (data.containsKey(
        'o_que_acontece_no_seu_bairro_quando_nasce_uma_crianca_com_problema')) {
      context.handle(
          _oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblemaMeta,
          oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema
              .isAcceptableOrUnknown(
                  data[
                      'o_que_acontece_no_seu_bairro_quando_nasce_uma_crianca_com_problema'],
                  _oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblemaMeta));
    }
    if (data.containsKey(
        'acha_que_uma_crianca_que_nasce_com_problema_pode_sobreviver')) {
      context.handle(
          _achaQueUmaCriancaQueNasceComProblemaPodeSobreviverMeta,
          achaQueUmaCriancaQueNasceComProblemaPodeSobreviver.isAcceptableOrUnknown(
              data[
                  'acha_que_uma_crianca_que_nasce_com_problema_pode_sobreviver'],
              _achaQueUmaCriancaQueNasceComProblemaPodeSobreviverMeta));
    }
    if (data.containsKey('explique_porque')) {
      context.handle(
          _expliquePorqueMeta,
          expliquePorque.isAcceptableOrUnknown(
              data['explique_porque'], _expliquePorqueMeta));
    }
    if (data.containsKey(
        'quem_pode_ajudar_quando_uma_crianca_nasce_com_problema')) {
      context.handle(
          _quemPodeAjudarQuandoUmaCriancaNasceComProblemaMeta,
          quemPodeAjudarQuandoUmaCriancaNasceComProblema.isAcceptableOrUnknown(
              data['quem_pode_ajudar_quando_uma_crianca_nasce_com_problema'],
              _quemPodeAjudarQuandoUmaCriancaNasceComProblemaMeta));
    }
    if (data.containsKey(
        'em_casa_quem_cuida_mais_das_criancas_ate6_anos_para_alimentacao')) {
      context.handle(
          _emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacaoMeta,
          emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao
              .isAcceptableOrUnknown(
                  data[
                      'em_casa_quem_cuida_mais_das_criancas_ate6_anos_para_alimentacao'],
                  _emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacaoMeta));
    }
    if (data.containsKey(
        'em_casa_quem_cuida_mais_das_criancas_ate6_anos_para_os_cuidados_de_higiene')) {
      context.handle(
          _emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigieneMeta,
          emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene
              .isAcceptableOrUnknown(
                  data[
                      'em_casa_quem_cuida_mais_das_criancas_ate6_anos_para_os_cuidados_de_higiene'],
                  _emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigieneMeta));
    }
    if (data.containsKey(
        'em_casa_quem_cuida_mais_das_criancas_ate6_anos_para_os_estudos')) {
      context.handle(
          _emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudosMeta,
          emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos.isAcceptableOrUnknown(
              data[
                  'em_casa_quem_cuida_mais_das_criancas_ate6_anos_para_os_estudos'],
              _emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudosMeta));
    }
    if (data.containsKey(
        'a_pessoa_entrevistada_pareceu_sincera_nas_suas_respostas')) {
      context.handle(
          _aPessoaEntrevistadaPareceuSinceraNasSuasRespostasMeta,
          aPessoaEntrevistadaPareceuSinceraNasSuasRespostas.isAcceptableOrUnknown(
              data['a_pessoa_entrevistada_pareceu_sincera_nas_suas_respostas'],
              _aPessoaEntrevistadaPareceuSinceraNasSuasRespostasMeta));
    }
    if (data
        .containsKey('algumas_perguntas_incomodaram_a_pessoa_entrevistada')) {
      context.handle(
          _algumasPerguntasIncomodaramAPessoaEntrevistadaMeta,
          algumasPerguntasIncomodaramAPessoaEntrevistada.isAcceptableOrUnknown(
              data['algumas_perguntas_incomodaram_a_pessoa_entrevistada'],
              _algumasPerguntasIncomodaramAPessoaEntrevistadaMeta));
    }
    if (data.containsKey('se_sim_quais3_principais_perguntas')) {
      context.handle(
          _seSimQuais3PrincipaisPerguntasMeta,
          seSimQuais3PrincipaisPerguntas.isAcceptableOrUnknown(
              data['se_sim_quais3_principais_perguntas'],
              _seSimQuais3PrincipaisPerguntasMeta));
    }
    if (data.containsKey('todas_as_perguntas_incomodaram')) {
      context.handle(
          _todasAsPerguntasIncomodaramMeta,
          todasAsPerguntasIncomodaram.isAcceptableOrUnknown(
              data['todas_as_perguntas_incomodaram'],
              _todasAsPerguntasIncomodaramMeta));
    }
    if (data.containsKey('a_residencia_era_de')) {
      context.handle(
          _aResidenciaEraDeMeta,
          aResidenciaEraDe.isAcceptableOrUnknown(
              data['a_residencia_era_de'], _aResidenciaEraDeMeta));
    }
    if (data.containsKey('como_avalia_higiene_da_casa')) {
      context.handle(
          _comoAvaliaHigieneDaCasaMeta,
          comoAvaliaHigieneDaCasa.isAcceptableOrUnknown(
              data['como_avalia_higiene_da_casa'],
              _comoAvaliaHigieneDaCasaMeta));
    }
    if (data.containsKey('o_que_era_mal')) {
      context.handle(
          _oQueEraMalMeta,
          oQueEraMal.isAcceptableOrUnknown(
              data['o_que_era_mal'], _oQueEraMalMeta));
    }
    if (data.containsKey('tinha_criancas_na_casa_durante_inquerito')) {
      context.handle(
          _tinhaCriancasNaCasaDuranteInqueritoMeta,
          tinhaCriancasNaCasaDuranteInquerito.isAcceptableOrUnknown(
              data['tinha_criancas_na_casa_durante_inquerito'],
              _tinhaCriancasNaCasaDuranteInqueritoMeta));
    }
    if (data.containsKey('tinha_criancas_na_casa_durante_inquerito_quantas')) {
      context.handle(
          _tinhaCriancasNaCasaDuranteInqueritoQuantasMeta,
          tinhaCriancasNaCasaDuranteInqueritoQuantas.isAcceptableOrUnknown(
              data['tinha_criancas_na_casa_durante_inquerito_quantas'],
              _tinhaCriancasNaCasaDuranteInqueritoQuantasMeta));
    }
    if (data.containsKey('tentou_incentivar')) {
      context.handle(
          _tentouIncentivarMeta,
          tentouIncentivar.isAcceptableOrUnknown(
              data['tentou_incentivar'], _tentouIncentivarMeta));
    }
    if (data.containsKey('tentou_incentivar_quantas')) {
      context.handle(
          _tentouIncentivarQuantasMeta,
          tentouIncentivarQuantas.isAcceptableOrUnknown(
              data['tentou_incentivar_quantas'], _tentouIncentivarQuantasMeta));
    }
    if (data.containsKey(
        'se_incentivou_quantas_reagiram_positivamente_aos_seus_incentivos')) {
      context.handle(
          _seIncentivouQuantasReagiramPositivamenteAosSeusIncentivosMeta,
          seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos
              .isAcceptableOrUnknown(
                  data[
                      'se_incentivou_quantas_reagiram_positivamente_aos_seus_incentivos'],
                  _seIncentivouQuantasReagiramPositivamenteAosSeusIncentivosMeta));
    }
    if (data.containsKey('observou_criancas_de_ate1_ano')) {
      context.handle(
          _observouCriancasDeAte1AnoMeta,
          observouCriancasDeAte1Ano.isAcceptableOrUnknown(
              data['observou_criancas_de_ate1_ano'],
              _observouCriancasDeAte1AnoMeta));
    }
    if (data.containsKey('observou_criancas_de_ate1_ano_quantos')) {
      context.handle(
          _observouCriancasDeAte1AnoQuantosMeta,
          observouCriancasDeAte1AnoQuantos.isAcceptableOrUnknown(
              data['observou_criancas_de_ate1_ano_quantos'],
              _observouCriancasDeAte1AnoQuantosMeta));
    }
    if (data.containsKey('algumas_apresentavam_problemas')) {
      context.handle(
          _algumasApresentavamProblemasMeta,
          algumasApresentavamProblemas.isAcceptableOrUnknown(
              data['algumas_apresentavam_problemas'],
              _algumasApresentavamProblemasMeta));
    }
    if (data.containsKey('algumas_apresentavam_problemas_quantos')) {
      context.handle(
          _algumasApresentavamProblemasQuantosMeta,
          algumasApresentavamProblemasQuantos.isAcceptableOrUnknown(
              data['algumas_apresentavam_problemas_quantos'],
              _algumasApresentavamProblemasQuantosMeta));
    }
    if (data.containsKey('descreve_o_problema_dessas_criancas')) {
      context.handle(
          _descreveOProblemaDessasCriancasMeta,
          descreveOProblemaDessasCriancas.isAcceptableOrUnknown(
              data['descreve_o_problema_dessas_criancas'],
              _descreveOProblemaDessasCriancasMeta));
    }
    if (data.containsKey('observou_criancas_de1_ano_emeio_ate3_anos')) {
      context.handle(
          _observouCriancasDe1AnoEmeioAte3AnosMeta,
          observouCriancasDe1AnoEmeioAte3Anos.isAcceptableOrUnknown(
              data['observou_criancas_de1_ano_emeio_ate3_anos'],
              _observouCriancasDe1AnoEmeioAte3AnosMeta));
    }
    if (data.containsKey('observou_criancas_de1_ano_emeio_ate3_anos_quantas')) {
      context.handle(
          _observouCriancasDe1AnoEmeioAte3AnosQuantasMeta,
          observouCriancasDe1AnoEmeioAte3AnosQuantas.isAcceptableOrUnknown(
              data['observou_criancas_de1_ano_emeio_ate3_anos_quantas'],
              _observouCriancasDe1AnoEmeioAte3AnosQuantasMeta));
    }
    if (data.containsKey(
        'algumas_criancas_de1ano_emeio_observadas_apresentavam_problemas')) {
      context.handle(
          _algumasCriancasDe1anoEmeioObservadasApresentavamProblemasMeta,
          algumasCriancasDe1anoEmeioObservadasApresentavamProblemas
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancas_de1ano_emeio_observadas_apresentavam_problemas'],
                  _algumasCriancasDe1anoEmeioObservadasApresentavamProblemasMeta));
    }
    if (data.containsKey(
        'algumas_criancas_de1ano_emeio_observadas_apresentavam_problemas_quantas')) {
      context.handle(
          _algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantasMeta,
          algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancas_de1ano_emeio_observadas_apresentavam_problemas_quantas'],
                  _algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantasMeta));
    }
    if (data.containsKey(
        'algumas_criancas_de1ano_emeio_observadas_apresentavam_problemas_descreve')) {
      context.handle(
          _algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreveMeta,
          algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve
              .isAcceptableOrUnknown(
                  data[
                      'algumas_criancas_de1ano_emeio_observadas_apresentavam_problemas_descreve'],
                  _algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreveMeta));
    }
    if (data.containsKey('observou_criancas_de3_anos_ate6_anos')) {
      context.handle(
          _observouCriancasDe3AnosAte6AnosMeta,
          observouCriancasDe3AnosAte6Anos.isAcceptableOrUnknown(
              data['observou_criancas_de3_anos_ate6_anos'],
              _observouCriancasDe3AnosAte6AnosMeta));
    }
    if (data.containsKey('observou_criancas_de3_anos_ate6_anos_quantas')) {
      context.handle(
          _observouCriancasDe3AnosAte6AnosQuantasMeta,
          observouCriancasDe3AnosAte6AnosQuantas.isAcceptableOrUnknown(
              data['observou_criancas_de3_anos_ate6_anos_quantas'],
              _observouCriancasDe3AnosAte6AnosQuantasMeta));
    }
    if (data.containsKey(
        'observou_criancas_de3_anos_ate6_anos_que_algumas_apresentavam_problemas')) {
      context.handle(
          _observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasMeta,
          observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas
              .isAcceptableOrUnknown(
                  data[
                      'observou_criancas_de3_anos_ate6_anos_que_algumas_apresentavam_problemas'],
                  _observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasMeta));
    }
    if (data.containsKey(
        'observou_criancas_de3_anos_ate6_anos_que_algumas_apresentavam_problemas_quantas')) {
      context.handle(
          _observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantasMeta,
          observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas
              .isAcceptableOrUnknown(
                  data[
                      'observou_criancas_de3_anos_ate6_anos_que_algumas_apresentavam_problemas_quantas'],
                  _observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantasMeta));
    }
    if (data.containsKey(
        'observou_criancas_de3_anos_ate6_anos_que_algumas_apresentavam_problemas_descreve')) {
      context.handle(
          _observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreveMeta,
          observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve
              .isAcceptableOrUnknown(
                  data[
                      'observou_criancas_de3_anos_ate6_anos_que_algumas_apresentavam_problemas_descreve'],
                  _observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreveMeta));
    }
    if (data.containsKey('observou_situacoes_que_lhe_chamaram_atencao')) {
      context.handle(
          _observouSituacoesQueLheChamaramAtencaoMeta,
          observouSituacoesQueLheChamaramAtencao.isAcceptableOrUnknown(
              data['observou_situacoes_que_lhe_chamaram_atencao'],
              _observouSituacoesQueLheChamaramAtencaoMeta));
    }
    if (data.containsKey('em_relacao_a_pessoa_entrevistada')) {
      context.handle(
          _emRelacaoAPessoaEntrevistadaMeta,
          emRelacaoAPessoaEntrevistada.isAcceptableOrUnknown(
              data['em_relacao_a_pessoa_entrevistada'],
              _emRelacaoAPessoaEntrevistadaMeta));
    }
    if (data.containsKey('em_relacao_a_pessoa_entrevistada_descreve')) {
      context.handle(
          _emRelacaoAPessoaEntrevistadaDescreveMeta,
          emRelacaoAPessoaEntrevistadaDescreve.isAcceptableOrUnknown(
              data['em_relacao_a_pessoa_entrevistada_descreve'],
              _emRelacaoAPessoaEntrevistadaDescreveMeta));
    }
    if (data.containsKey('em_relacao_a_outros_adultos_presente')) {
      context.handle(
          _emRelacaoAOutrosAdultosPresenteMeta,
          emRelacaoAOutrosAdultosPresente.isAcceptableOrUnknown(
              data['em_relacao_a_outros_adultos_presente'],
              _emRelacaoAOutrosAdultosPresenteMeta));
    }
    if (data.containsKey('em_relacao_a_outros_adultos_presente_descreve')) {
      context.handle(
          _emRelacaoAOutrosAdultosPresenteDescreveMeta,
          emRelacaoAOutrosAdultosPresenteDescreve.isAcceptableOrUnknown(
              data['em_relacao_a_outros_adultos_presente_descreve'],
              _emRelacaoAOutrosAdultosPresenteDescreveMeta));
    }
    if (data.containsKey('em_relacao_as_criancas_presentes')) {
      context.handle(
          _emRelacaoAsCriancasPresentesMeta,
          emRelacaoAsCriancasPresentes.isAcceptableOrUnknown(
              data['em_relacao_as_criancas_presentes'],
              _emRelacaoAsCriancasPresentesMeta));
    }
    if (data.containsKey('em_relacao_as_criancas_presentes_descreve')) {
      context.handle(
          _emRelacaoAsCriancasPresentesDescreveMeta,
          emRelacaoAsCriancasPresentesDescreve.isAcceptableOrUnknown(
              data['em_relacao_as_criancas_presentes_descreve'],
              _emRelacaoAsCriancasPresentesDescreveMeta));
    }
    if (data.containsKey('em_relacao_a_vizinhanca')) {
      context.handle(
          _emRelacaoAVizinhancaMeta,
          emRelacaoAVizinhanca.isAcceptableOrUnknown(
              data['em_relacao_a_vizinhanca'], _emRelacaoAVizinhancaMeta));
    }
    if (data.containsKey('em_relacao_a_vizinhanca_descreve')) {
      context.handle(
          _emRelacaoAVizinhancaDescreveMeta,
          emRelacaoAVizinhancaDescreve.isAcceptableOrUnknown(
              data['em_relacao_a_vizinhanca_descreve'],
              _emRelacaoAVizinhancaDescreveMeta));
    }
    if (data.containsKey('outras')) {
      context.handle(_outrasMeta,
          outras.isAcceptableOrUnknown(data['outras'], _outrasMeta));
    }
    if (data.containsKey('outras_descreve')) {
      context.handle(
          _outrasDescreveMeta,
          outrasDescreve.isAcceptableOrUnknown(
              data['outras_descreve'], _outrasDescreveMeta));
    }
    if (data.containsKey('created_at')) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableOrUnknown(data['created_at'], _createdAtMeta));
    }
    if (data.containsKey('updated_at')) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableOrUnknown(data['updated_at'], _updatedAtMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => <GeneratedColumn>{};
  @override
  User map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return User.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $UsersTable createAlias(String alias) {
    return $UsersTable(_db, alias);
  }
}

class InsertedUser extends DataClass implements Insertable<InsertedUser> {
  final int id;
  final String uuidInsert;
  final DateTime createdAt;
  final DateTime updatedAt;
  InsertedUser(
      {this.id, @required this.uuidInsert, this.createdAt, this.updatedAt});
  factory InsertedUser.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return InsertedUser(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      uuidInsert: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}uuid_insert']),
      createdAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
      updatedAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}updated_at']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || uuidInsert != null) {
      map['uuid_insert'] = Variable<String>(uuidInsert);
    }
    if (!nullToAbsent || createdAt != null) {
      map['created_at'] = Variable<DateTime>(createdAt);
    }
    if (!nullToAbsent || updatedAt != null) {
      map['updated_at'] = Variable<DateTime>(updatedAt);
    }
    return map;
  }

  InsertedUsersCompanion toCompanion(bool nullToAbsent) {
    return InsertedUsersCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      uuidInsert: uuidInsert == null && nullToAbsent
          ? const Value.absent()
          : Value(uuidInsert),
      createdAt: createdAt == null && nullToAbsent
          ? const Value.absent()
          : Value(createdAt),
      updatedAt: updatedAt == null && nullToAbsent
          ? const Value.absent()
          : Value(updatedAt),
    );
  }

  factory InsertedUser.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return InsertedUser(
      id: serializer.fromJson<int>(json['id']),
      uuidInsert: serializer.fromJson<String>(json['uuidInsert']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
      updatedAt: serializer.fromJson<DateTime>(json['updatedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'uuidInsert': serializer.toJson<String>(uuidInsert),
      'createdAt': serializer.toJson<DateTime>(createdAt),
      'updatedAt': serializer.toJson<DateTime>(updatedAt),
    };
  }

  InsertedUser copyWith(
          {int id,
          String uuidInsert,
          DateTime createdAt,
          DateTime updatedAt}) =>
      InsertedUser(
        id: id ?? this.id,
        uuidInsert: uuidInsert ?? this.uuidInsert,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
      );
  @override
  String toString() {
    return (StringBuffer('InsertedUser(')
          ..write('id: $id, ')
          ..write('uuidInsert: $uuidInsert, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          uuidInsert.hashCode, $mrjc(createdAt.hashCode, updatedAt.hashCode))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is InsertedUser &&
          other.id == this.id &&
          other.uuidInsert == this.uuidInsert &&
          other.createdAt == this.createdAt &&
          other.updatedAt == this.updatedAt);
}

class InsertedUsersCompanion extends UpdateCompanion<InsertedUser> {
  final Value<int> id;
  final Value<String> uuidInsert;
  final Value<DateTime> createdAt;
  final Value<DateTime> updatedAt;
  const InsertedUsersCompanion({
    this.id = const Value.absent(),
    this.uuidInsert = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.updatedAt = const Value.absent(),
  });
  InsertedUsersCompanion.insert({
    this.id = const Value.absent(),
    @required String uuidInsert,
    this.createdAt = const Value.absent(),
    this.updatedAt = const Value.absent(),
  }) : uuidInsert = Value(uuidInsert);
  static Insertable<InsertedUser> custom({
    Expression<int> id,
    Expression<String> uuidInsert,
    Expression<DateTime> createdAt,
    Expression<DateTime> updatedAt,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (uuidInsert != null) 'uuid_insert': uuidInsert,
      if (createdAt != null) 'created_at': createdAt,
      if (updatedAt != null) 'updated_at': updatedAt,
    });
  }

  InsertedUsersCompanion copyWith(
      {Value<int> id,
      Value<String> uuidInsert,
      Value<DateTime> createdAt,
      Value<DateTime> updatedAt}) {
    return InsertedUsersCompanion(
      id: id ?? this.id,
      uuidInsert: uuidInsert ?? this.uuidInsert,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (uuidInsert.present) {
      map['uuid_insert'] = Variable<String>(uuidInsert.value);
    }
    if (createdAt.present) {
      map['created_at'] = Variable<DateTime>(createdAt.value);
    }
    if (updatedAt.present) {
      map['updated_at'] = Variable<DateTime>(updatedAt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('InsertedUsersCompanion(')
          ..write('id: $id, ')
          ..write('uuidInsert: $uuidInsert, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }
}

class $InsertedUsersTable extends InsertedUsers
    with TableInfo<$InsertedUsersTable, InsertedUser> {
  final GeneratedDatabase _db;
  final String _alias;
  $InsertedUsersTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, true,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _uuidInsertMeta = const VerificationMeta('uuidInsert');
  GeneratedTextColumn _uuidInsert;
  @override
  GeneratedTextColumn get uuidInsert => _uuidInsert ??= _constructUuidInsert();
  GeneratedTextColumn _constructUuidInsert() {
    return GeneratedTextColumn('uuid_insert', $tableName, false,
        maxTextLength: 191);
  }

  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  GeneratedDateTimeColumn _createdAt;
  @override
  GeneratedDateTimeColumn get createdAt => _createdAt ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      true,
    );
  }

  final VerificationMeta _updatedAtMeta = const VerificationMeta('updatedAt');
  GeneratedDateTimeColumn _updatedAt;
  @override
  GeneratedDateTimeColumn get updatedAt => _updatedAt ??= _constructUpdatedAt();
  GeneratedDateTimeColumn _constructUpdatedAt() {
    return GeneratedDateTimeColumn(
      'updated_at',
      $tableName,
      true,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, uuidInsert, createdAt, updatedAt];
  @override
  $InsertedUsersTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'inserted_users';
  @override
  final String actualTableName = 'inserted_users';
  @override
  VerificationContext validateIntegrity(Insertable<InsertedUser> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('uuid_insert')) {
      context.handle(
          _uuidInsertMeta,
          uuidInsert.isAcceptableOrUnknown(
              data['uuid_insert'], _uuidInsertMeta));
    } else if (isInserting) {
      context.missing(_uuidInsertMeta);
    }
    if (data.containsKey('created_at')) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableOrUnknown(data['created_at'], _createdAtMeta));
    }
    if (data.containsKey('updated_at')) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableOrUnknown(data['updated_at'], _updatedAtMeta));
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  InsertedUser map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return InsertedUser.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $InsertedUsersTable createAlias(String alias) {
    return $InsertedUsersTable(_db, alias);
  }
}

class UpdatedUser extends DataClass implements Insertable<UpdatedUser> {
  final int id;
  final String uuidUpdate;
  final DateTime createdAt;
  final DateTime updatedAt;
  UpdatedUser(
      {this.id,
      @required this.uuidUpdate,
      @required this.createdAt,
      @required this.updatedAt});
  factory UpdatedUser.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return UpdatedUser(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      uuidUpdate: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}uuid_update']),
      createdAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
      updatedAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}updated_at']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || uuidUpdate != null) {
      map['uuid_update'] = Variable<String>(uuidUpdate);
    }
    if (!nullToAbsent || createdAt != null) {
      map['created_at'] = Variable<DateTime>(createdAt);
    }
    if (!nullToAbsent || updatedAt != null) {
      map['updated_at'] = Variable<DateTime>(updatedAt);
    }
    return map;
  }

  UpdatedUsersCompanion toCompanion(bool nullToAbsent) {
    return UpdatedUsersCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      uuidUpdate: uuidUpdate == null && nullToAbsent
          ? const Value.absent()
          : Value(uuidUpdate),
      createdAt: createdAt == null && nullToAbsent
          ? const Value.absent()
          : Value(createdAt),
      updatedAt: updatedAt == null && nullToAbsent
          ? const Value.absent()
          : Value(updatedAt),
    );
  }

  factory UpdatedUser.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return UpdatedUser(
      id: serializer.fromJson<int>(json['id']),
      uuidUpdate: serializer.fromJson<String>(json['uuidUpdate']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
      updatedAt: serializer.fromJson<DateTime>(json['updatedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'uuidUpdate': serializer.toJson<String>(uuidUpdate),
      'createdAt': serializer.toJson<DateTime>(createdAt),
      'updatedAt': serializer.toJson<DateTime>(updatedAt),
    };
  }

  UpdatedUser copyWith(
          {int id,
          String uuidUpdate,
          DateTime createdAt,
          DateTime updatedAt}) =>
      UpdatedUser(
        id: id ?? this.id,
        uuidUpdate: uuidUpdate ?? this.uuidUpdate,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
      );
  @override
  String toString() {
    return (StringBuffer('UpdatedUser(')
          ..write('id: $id, ')
          ..write('uuidUpdate: $uuidUpdate, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          uuidUpdate.hashCode, $mrjc(createdAt.hashCode, updatedAt.hashCode))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is UpdatedUser &&
          other.id == this.id &&
          other.uuidUpdate == this.uuidUpdate &&
          other.createdAt == this.createdAt &&
          other.updatedAt == this.updatedAt);
}

class UpdatedUsersCompanion extends UpdateCompanion<UpdatedUser> {
  final Value<int> id;
  final Value<String> uuidUpdate;
  final Value<DateTime> createdAt;
  final Value<DateTime> updatedAt;
  const UpdatedUsersCompanion({
    this.id = const Value.absent(),
    this.uuidUpdate = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.updatedAt = const Value.absent(),
  });
  UpdatedUsersCompanion.insert({
    this.id = const Value.absent(),
    @required String uuidUpdate,
    @required DateTime createdAt,
    @required DateTime updatedAt,
  })  : uuidUpdate = Value(uuidUpdate),
        createdAt = Value(createdAt),
        updatedAt = Value(updatedAt);
  static Insertable<UpdatedUser> custom({
    Expression<int> id,
    Expression<String> uuidUpdate,
    Expression<DateTime> createdAt,
    Expression<DateTime> updatedAt,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (uuidUpdate != null) 'uuid_update': uuidUpdate,
      if (createdAt != null) 'created_at': createdAt,
      if (updatedAt != null) 'updated_at': updatedAt,
    });
  }

  UpdatedUsersCompanion copyWith(
      {Value<int> id,
      Value<String> uuidUpdate,
      Value<DateTime> createdAt,
      Value<DateTime> updatedAt}) {
    return UpdatedUsersCompanion(
      id: id ?? this.id,
      uuidUpdate: uuidUpdate ?? this.uuidUpdate,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (uuidUpdate.present) {
      map['uuid_update'] = Variable<String>(uuidUpdate.value);
    }
    if (createdAt.present) {
      map['created_at'] = Variable<DateTime>(createdAt.value);
    }
    if (updatedAt.present) {
      map['updated_at'] = Variable<DateTime>(updatedAt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('UpdatedUsersCompanion(')
          ..write('id: $id, ')
          ..write('uuidUpdate: $uuidUpdate, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }
}

class $UpdatedUsersTable extends UpdatedUsers
    with TableInfo<$UpdatedUsersTable, UpdatedUser> {
  final GeneratedDatabase _db;
  final String _alias;
  $UpdatedUsersTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, true,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _uuidUpdateMeta = const VerificationMeta('uuidUpdate');
  GeneratedTextColumn _uuidUpdate;
  @override
  GeneratedTextColumn get uuidUpdate => _uuidUpdate ??= _constructUuidUpdate();
  GeneratedTextColumn _constructUuidUpdate() {
    return GeneratedTextColumn('uuid_update', $tableName, false,
        maxTextLength: 191);
  }

  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  GeneratedDateTimeColumn _createdAt;
  @override
  GeneratedDateTimeColumn get createdAt => _createdAt ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  final VerificationMeta _updatedAtMeta = const VerificationMeta('updatedAt');
  GeneratedDateTimeColumn _updatedAt;
  @override
  GeneratedDateTimeColumn get updatedAt => _updatedAt ??= _constructUpdatedAt();
  GeneratedDateTimeColumn _constructUpdatedAt() {
    return GeneratedDateTimeColumn(
      'updated_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, uuidUpdate, createdAt, updatedAt];
  @override
  $UpdatedUsersTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'updated_users';
  @override
  final String actualTableName = 'updated_users';
  @override
  VerificationContext validateIntegrity(Insertable<UpdatedUser> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('uuid_update')) {
      context.handle(
          _uuidUpdateMeta,
          uuidUpdate.isAcceptableOrUnknown(
              data['uuid_update'], _uuidUpdateMeta));
    } else if (isInserting) {
      context.missing(_uuidUpdateMeta);
    }
    if (data.containsKey('created_at')) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableOrUnknown(data['created_at'], _createdAtMeta));
    } else if (isInserting) {
      context.missing(_createdAtMeta);
    }
    if (data.containsKey('updated_at')) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableOrUnknown(data['updated_at'], _updatedAtMeta));
    } else if (isInserting) {
      context.missing(_updatedAtMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  UpdatedUser map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return UpdatedUser.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $UpdatedUsersTable createAlias(String alias) {
    return $UpdatedUsersTable(_db, alias);
  }
}

class DeletedUser extends DataClass implements Insertable<DeletedUser> {
  final int id;
  final String uuidDelete;
  final DateTime createdAt;
  final DateTime updatedAt;
  DeletedUser(
      {this.id,
      @required this.uuidDelete,
      @required this.createdAt,
      @required this.updatedAt});
  factory DeletedUser.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final dateTimeType = db.typeSystem.forDartType<DateTime>();
    return DeletedUser(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      uuidDelete: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}uuid_delete']),
      createdAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}created_at']),
      updatedAt: dateTimeType
          .mapFromDatabaseResponse(data['${effectivePrefix}updated_at']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || uuidDelete != null) {
      map['uuid_delete'] = Variable<String>(uuidDelete);
    }
    if (!nullToAbsent || createdAt != null) {
      map['created_at'] = Variable<DateTime>(createdAt);
    }
    if (!nullToAbsent || updatedAt != null) {
      map['updated_at'] = Variable<DateTime>(updatedAt);
    }
    return map;
  }

  DeletedUsersCompanion toCompanion(bool nullToAbsent) {
    return DeletedUsersCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      uuidDelete: uuidDelete == null && nullToAbsent
          ? const Value.absent()
          : Value(uuidDelete),
      createdAt: createdAt == null && nullToAbsent
          ? const Value.absent()
          : Value(createdAt),
      updatedAt: updatedAt == null && nullToAbsent
          ? const Value.absent()
          : Value(updatedAt),
    );
  }

  factory DeletedUser.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return DeletedUser(
      id: serializer.fromJson<int>(json['id']),
      uuidDelete: serializer.fromJson<String>(json['uuidDelete']),
      createdAt: serializer.fromJson<DateTime>(json['createdAt']),
      updatedAt: serializer.fromJson<DateTime>(json['updatedAt']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'uuidDelete': serializer.toJson<String>(uuidDelete),
      'createdAt': serializer.toJson<DateTime>(createdAt),
      'updatedAt': serializer.toJson<DateTime>(updatedAt),
    };
  }

  DeletedUser copyWith(
          {int id,
          String uuidDelete,
          DateTime createdAt,
          DateTime updatedAt}) =>
      DeletedUser(
        id: id ?? this.id,
        uuidDelete: uuidDelete ?? this.uuidDelete,
        createdAt: createdAt ?? this.createdAt,
        updatedAt: updatedAt ?? this.updatedAt,
      );
  @override
  String toString() {
    return (StringBuffer('DeletedUser(')
          ..write('id: $id, ')
          ..write('uuidDelete: $uuidDelete, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(
          uuidDelete.hashCode, $mrjc(createdAt.hashCode, updatedAt.hashCode))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is DeletedUser &&
          other.id == this.id &&
          other.uuidDelete == this.uuidDelete &&
          other.createdAt == this.createdAt &&
          other.updatedAt == this.updatedAt);
}

class DeletedUsersCompanion extends UpdateCompanion<DeletedUser> {
  final Value<int> id;
  final Value<String> uuidDelete;
  final Value<DateTime> createdAt;
  final Value<DateTime> updatedAt;
  const DeletedUsersCompanion({
    this.id = const Value.absent(),
    this.uuidDelete = const Value.absent(),
    this.createdAt = const Value.absent(),
    this.updatedAt = const Value.absent(),
  });
  DeletedUsersCompanion.insert({
    this.id = const Value.absent(),
    @required String uuidDelete,
    @required DateTime createdAt,
    @required DateTime updatedAt,
  })  : uuidDelete = Value(uuidDelete),
        createdAt = Value(createdAt),
        updatedAt = Value(updatedAt);
  static Insertable<DeletedUser> custom({
    Expression<int> id,
    Expression<String> uuidDelete,
    Expression<DateTime> createdAt,
    Expression<DateTime> updatedAt,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (uuidDelete != null) 'uuid_delete': uuidDelete,
      if (createdAt != null) 'created_at': createdAt,
      if (updatedAt != null) 'updated_at': updatedAt,
    });
  }

  DeletedUsersCompanion copyWith(
      {Value<int> id,
      Value<String> uuidDelete,
      Value<DateTime> createdAt,
      Value<DateTime> updatedAt}) {
    return DeletedUsersCompanion(
      id: id ?? this.id,
      uuidDelete: uuidDelete ?? this.uuidDelete,
      createdAt: createdAt ?? this.createdAt,
      updatedAt: updatedAt ?? this.updatedAt,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (uuidDelete.present) {
      map['uuid_delete'] = Variable<String>(uuidDelete.value);
    }
    if (createdAt.present) {
      map['created_at'] = Variable<DateTime>(createdAt.value);
    }
    if (updatedAt.present) {
      map['updated_at'] = Variable<DateTime>(updatedAt.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('DeletedUsersCompanion(')
          ..write('id: $id, ')
          ..write('uuidDelete: $uuidDelete, ')
          ..write('createdAt: $createdAt, ')
          ..write('updatedAt: $updatedAt')
          ..write(')'))
        .toString();
  }
}

class $DeletedUsersTable extends DeletedUsers
    with TableInfo<$DeletedUsersTable, DeletedUser> {
  final GeneratedDatabase _db;
  final String _alias;
  $DeletedUsersTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn('id', $tableName, true,
        hasAutoIncrement: true, declaredAsPrimaryKey: true);
  }

  final VerificationMeta _uuidDeleteMeta = const VerificationMeta('uuidDelete');
  GeneratedTextColumn _uuidDelete;
  @override
  GeneratedTextColumn get uuidDelete => _uuidDelete ??= _constructUuidDelete();
  GeneratedTextColumn _constructUuidDelete() {
    return GeneratedTextColumn('uuid_delete', $tableName, false,
        maxTextLength: 191);
  }

  final VerificationMeta _createdAtMeta = const VerificationMeta('createdAt');
  GeneratedDateTimeColumn _createdAt;
  @override
  GeneratedDateTimeColumn get createdAt => _createdAt ??= _constructCreatedAt();
  GeneratedDateTimeColumn _constructCreatedAt() {
    return GeneratedDateTimeColumn(
      'created_at',
      $tableName,
      false,
    );
  }

  final VerificationMeta _updatedAtMeta = const VerificationMeta('updatedAt');
  GeneratedDateTimeColumn _updatedAt;
  @override
  GeneratedDateTimeColumn get updatedAt => _updatedAt ??= _constructUpdatedAt();
  GeneratedDateTimeColumn _constructUpdatedAt() {
    return GeneratedDateTimeColumn(
      'updated_at',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, uuidDelete, createdAt, updatedAt];
  @override
  $DeletedUsersTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'deleted_users';
  @override
  final String actualTableName = 'deleted_users';
  @override
  VerificationContext validateIntegrity(Insertable<DeletedUser> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('uuid_delete')) {
      context.handle(
          _uuidDeleteMeta,
          uuidDelete.isAcceptableOrUnknown(
              data['uuid_delete'], _uuidDeleteMeta));
    } else if (isInserting) {
      context.missing(_uuidDeleteMeta);
    }
    if (data.containsKey('created_at')) {
      context.handle(_createdAtMeta,
          createdAt.isAcceptableOrUnknown(data['created_at'], _createdAtMeta));
    } else if (isInserting) {
      context.missing(_createdAtMeta);
    }
    if (data.containsKey('updated_at')) {
      context.handle(_updatedAtMeta,
          updatedAt.isAcceptableOrUnknown(data['updated_at'], _updatedAtMeta));
    } else if (isInserting) {
      context.missing(_updatedAtMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  DeletedUser map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return DeletedUser.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $DeletedUsersTable createAlias(String alias) {
    return $DeletedUsersTable(_db, alias);
  }
}

abstract class _$MyDatabase extends GeneratedDatabase {
  _$MyDatabase(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  $UsersTable _users;
  $UsersTable get users => _users ??= $UsersTable(this);
  $InsertedUsersTable _insertedUsers;
  $InsertedUsersTable get insertedUsers =>
      _insertedUsers ??= $InsertedUsersTable(this);
  $UpdatedUsersTable _updatedUsers;
  $UpdatedUsersTable get updatedUsers =>
      _updatedUsers ??= $UpdatedUsersTable(this);
  $DeletedUsersTable _deletedUsers;
  $DeletedUsersTable get deletedUsers =>
      _deletedUsers ??= $DeletedUsersTable(this);
  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities =>
      [users, insertedUsers, updatedUsers, deletedUsers];
}
