import 'package:moor/moor.dart';
// These imports are only needed to open the database
import 'package:moor/ffi.dart';
import 'package:path_provider/path_provider.dart';
import 'package:path/path.dart' as p;
import 'dart:io';
import 'package:uuid/uuid.dart';
import 'dart:collection';
import 'dart:async';
//part of generated tables of database
part 'database.g.dart';
@DataClassName("User")
class Users extends Table{
  //inicio da parte 1	Localização 
  final unique = Uuid();
 // IntColumn  get id => integer().autoIncrement().nullable()();
  TextColumn get uuid => text().clientDefault(() => unique.v4()).withLength(max: 191).customConstraint("UNIQUE").nullable()();
  TextColumn get nomeDoPesquisador => text().nullable()(); //
  TextColumn  get numeroDeInquerito => text().nullable()();
  DateTimeColumn get data => dateTime().nullable()();
  TextColumn get municipio => text().withLength(max: 50).nullable()();//1.1
  TextColumn get bairro => text().withLength(max: 50).nullable()();//1.2
  TextColumn get pontoDeReferenciaDaCasa => text().withLength(max: 50).nullable()();//1.3
  //fim da parte 1 da ficha

  //inicio da parte 2 . dados de Entrevistado 
  TextColumn get nome => text().withLength(max: 50).nullable()();//2.1
  TextColumn  get idade => text().nullable()();//2.2
  TextColumn   get contacto => text().nullable()();
  TextColumn get eChefeDaFamilia => text().nullable()();//2.3
  TextColumn get caoNaoVinculoDeParantesco => text().withLength(max: 50).nullable()();//2.4
  //inicio d aparte 3	Chef de família: 
  TextColumn  get idadeDoChefeDaFamilia => text().nullable()();//3.1
  TextColumn get estudou => text().nullable()();//3.2
  TextColumn get eAlfabetizado => text().nullable()();//3.2.1
  TextColumn get completouEnsinoPrimario => text().nullable()();//3.2.2
  TextColumn get completouEnsinoSecundario => text().nullable()();//3.2.3
  TextColumn get fezCursoSuperior => text().nullable()();//3.2.4
  TextColumn get naoSabe => text().nullable()();//3.2.5
  TextColumn get chefeDeFamiliaTemSalario => text().nullable()();//3.3
  TextColumn get quantoGastaParaSustentarFamiliaPorMes => text().nullable()();//3.3.1
  //inico da parte 4	Família.
  TextColumn  get quantasFamiliasResidemNacasa => text().nullable()();//4.1
  TextColumn  get quantosAdultos => text().nullable()();//4.2
  TextColumn  get quantosAdultosMaioresDe18anos => text().nullable()();//4.2.1

  TextColumn  get quantosAdultosMaioresDe18anosHomens => text().nullable()();//4.2.2
  TextColumn  get quantosAdultosMaioresDe18anosHomensTrabalham => text().nullable()();//4.2.2.1
  TextColumn  get quantosAdultosMaioresDe18anosHomensAlfabetizados => text().nullable()();//4.2.2.2

  TextColumn  get quantosAdultosM18Mulheres => text().nullable()();//4.2.3
  TextColumn  get quantosAdultosM18MulheresTrabalham => text().nullable()();//4.2.3.1
  TextColumn  get quantosAdultosM18MulheresAlfabetizados => text().nullable()();//4.2.3.2

  TextColumn get relatosDeDeficiencia => text().nullable()(); //4.3

  TextColumn  get totalcriancasAte1anoemeio => text().nullable()();//4.4
  TextColumn  get totalcriancasAte1anoemeioMeninos => text().nullable()();//4.4.1
  TextColumn  get totalcriancasAte1anoemeioMeninas => text().nullable()();//4.4.2

  TextColumn get algumasCriancasAte1anoemeioNaodesenvolvem => text().nullable()();//4.4.3
  TextColumn  get algumasCriancasAte1anoemeioNaodesenvolvemQuantas => text().nullable()();//4.4.3

  TextColumn get algumaCriancasAte1anoemeioQueTipodeProblema => text().nullable()();//4.4.3.1
  TextColumn get algumaCriancasAte1anoemeioTemProblemaMotor => text().nullable()();//4.4.3.2
  TextColumn  get algumaCriancasAte1anoemeioTemProblemaMotorQuantas => text().nullable()();//4.4.3.2

  TextColumn get algumaCriancasAte1anoemeioTemMalFormacao => text().nullable()();//4.4.3.3
  TextColumn  get algumaCriancasAte1anoemeioTemMalFormacaoQuantas => text().nullable()();//4.4.3.3

  TextColumn get algumaCriancasAte1anoemeioNaoReagemAIncentivo => text().nullable()();//4.4.3.4
  TextColumn  get algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas => text().nullable()();//4.4.3.4

  TextColumn get algumaCriancasAte1anoemeioTemReacaoAgressiva => text().nullable()();//4.4.3.5
  TextColumn  get algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas => text().nullable()();//4.4.3.5

  //4.5 parte
  TextColumn  get totalcriancasAte1anoemeioA3anos => text().nullable()();//4.5
  TextColumn  get totalcriancasAte1anoemeioA3anosMeninos => text().nullable()();//4.5.1
  TextColumn  get totalcriancasAte1anoemeioA3anosMeninas => text().nullable()();//4.5.2

  TextColumn get algumasCriancasAte1anoemeioA3anosNaodesenvolvem => text().nullable()();//4.5.3
  TextColumn  get algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas => text().nullable()();//4.5.3

  TextColumn get algumaCriancasAte1anoemeioA3anosQueTipodeProblema => text().nullable()();//4.5.4
  //paragem 4.5.4 na base online

  TextColumn get algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas => text().nullable()();//4.5.4.1
  TextColumn get algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas => text().nullable()();//4.5.4.1

  TextColumn get algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo => text().nullable()();//4.5.4.2
  TextColumn get algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas => text().nullable()();//4.5.4.2
  
  TextColumn get algumasCriancasAte1anoemeioA3anosAlgunsAgridem => text().nullable()();//4.5.4.3
  TextColumn get algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas  => text().nullable()();//4.5.4.3
  //paragem 4.5.4.3
  TextColumn get algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal => text().nullable()();//4.5.4.4
  TextColumn get algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas => text().nullable()();//4.5.4.4

  TextColumn get algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao => text().nullable()();//4.5.4.5
  TextColumn get algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas => text().nullable()();//4.5.4.5
  
  TextColumn get algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas => text().nullable()();//4.5.4.6
  TextColumn get algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas => text().nullable()();//4.5.4.6
  
  TextColumn get algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente => text().nullable()();//4.5.4.7
  TextColumn get algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas => text().nullable()();//4.5.4.7

  TextColumn get algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos => text().nullable()();//4.5.4.8
  TextColumn get algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas => text().nullable()();//4.5.4.8

  TextColumn get algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown => text().nullable()();//4.5.4.9
  TextColumn get algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas => text().nullable()();//4.5.4.9
  //4.6

  
  TextColumn  get totalcriancasde3A6anos => text().nullable()();//4.6
  TextColumn  get totalcriancasde3A6anosMeninos => text().nullable()();//4.6.1
  TextColumn  get totalcriancasde3A6anosMeninas => text().nullable()();//4.6.2

  TextColumn get algumasCriancasde3A6anosNaodesenvolvem => text().nullable()();//4.6.3
  TextColumn  get algumasCriancasde3A6anosNaodesenvolvemQuantas => text().nullable()();//4.6.3

  TextColumn get algumaCriancasde3A6anosQueTipodeProblema => text().nullable()();//4.6.3.1
  TextColumn get algumasCriancasde3A6anosNaoInteragemComPessoas => text().nullable()();//4.6.3.2
  TextColumn get algumasCriancasde3A6anosNaoInteragemComPessoasQuantas => text().nullable()();//4.6.3.2

  TextColumn get algumasCriancasde3A6anosTemComportamentoAgressivo => text().nullable()();//4.6.3.3
  TextColumn get algumasCriancasde3A6anosTemComportamentoAgressivoQuantas => text().nullable()();//4.6.3.3
  
  TextColumn get algumasCriancasde3A6anosAlgunsAgridem => text().nullable()();//4.6.3.4
  TextColumn get algumasCriancasde3A6anosAlgunsAgridemQuantas  => text().nullable()();//4.6.3.4
 
  TextColumn get algumasCriancasde3A6anosAlgumasAgitadasQueONormal => text().nullable()();//4.6.3.5
  TextColumn get algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas => text().nullable()();//4.6.3.5

  TextColumn get algumasCriancasde3A6anosAlgumasTemMalFormacao => text().nullable()();//4.6.3.6
  TextColumn get algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas => text().nullable()();//4.6.3.6
  
  TextColumn get algumasCriancasde3A6anosAlgumasNaoAndamSozinhas => text().nullable()();//4.6.3.7
  TextColumn get algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas => text().nullable()();//4.6.3.7
  
  TextColumn get algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente => text().nullable()();//4.6.3.8
  TextColumn get algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas => text().nullable()();//4.6.3.8

  TextColumn get algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos => text().nullable()();//4.6.3.9
  TextColumn get algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas => text().nullable()();//4.6.3.9

  TextColumn get algumasCriancasde3A6anosAlgumasTemSindromeDeDown => text().nullable()();//4.6.3.10
  TextColumn get algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas => text().nullable()();//4.6.3.10

  TextColumn get algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas => text().nullable()();//4.6.3.11
  TextColumn get algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas => text().nullable()();//4.6.3.11

  TextColumn get algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas => text().nullable()();//4.6.3.12
  TextColumn get algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas => text().nullable()();//4.6.3.12

  TextColumn get algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas => text().nullable()();//4.6.3.13
  TextColumn get algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas => text().nullable()();//4.6.3.13

  TextColumn get algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas => text().nullable()();//4.6.3.14
  TextColumn get algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas => text().nullable()();//4.6.3.14
  //4.6.4  
  TextColumn get quantasCriancasde3A6anosEstaoNaEscolinha => text().nullable()();//4.6.4
  TextColumn get criancasDe3A6anosNaoEstaoNaEscolinhaPorque => text().nullable()();//4.6.4.1

   TextColumn get quantasCriancasde3A6anosTrabalham => text().nullable()();//4.6.4.6
   
  TextColumn get criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos => text().nullable()();//4.6.4.6.1

  //4.7
  TextColumn  get totalcriancasde7A9anos => text().nullable()();//4.7
  TextColumn  get totalcriancasde7A9anosMeninos => text().nullable()();//4.7.1
  TextColumn  get totalcriancasde7A9anosMeninas => text().nullable()();//4.7.2
  TextColumn get quantasCriancasde7A9anosEstaoNaEscola => text().nullable()();//4.7.3
  TextColumn get quantasCriancasde7A9anosTrabalham => text().nullable()();//4.7.4
  //5 Gravidez
  TextColumn get nosUltimos5AnosQuantasGravidezesTiveNaFamilia => text().nullable()(); //5.1
  TextColumn get partosOcoridosNoHospital => text().nullable()(); //5.1.1
  TextColumn get partosOcoridosEmCasa => text().nullable()(); //5.1.1
  TextColumn get partosOcoridosEmOutroLocal => text().nullable()(); //5.1.1
  TextColumn get outroLocalDoPartoQualLocal => text().nullable()(); //5.1.1 
  TextColumn get quantasCriancasNasceramVivas => text().nullable()();//5.1.2
  TextColumn get quantasContinuamVivas => text().nullable()();//5.1.3
  TextColumn get dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram => text().nullable()();//5.1.3.1
  TextColumn get descreverOProblema => text().nullable()();//5.1.3.1 Se Sim Descrever
  TextColumn get aFamiliaProcurouAjudaParaSuperarEsseProblema => text().nullable()();//5.1.3.2 ?
  TextColumn get seSimQueTipoDeAjudaEOnde => text().nullable()();//5.1.3.2 
  TextColumn get osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia => text().nullable()();//5.1.4 ?
  TextColumn get dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida => text().nullable()();  //5.1.5
  TextColumn get sabeDizerOMotivoDosObitos => text().nullable()();//5.1.5.1
  TextColumn get seSimQualFoiPrincipalRazaodoObito => text().nullable()();//5.1.5.2
  //6 

  TextColumn get voceTemMedoDeTerUmFilhoOuNetoComProblemas  => text().nullable()(); //6.1
  TextColumn get expliquePorQue => text().nullable()();//6.1.1 
  TextColumn get porqueVoceAchaQueUmaCriancaPodeNascerComProblema => text().nullable()();//6.2 
  TextColumn get oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema => text().nullable()();//6.3 ?
  TextColumn get achaQueUmaCriancaQueNasceComProblemaPodeSobreviver => text().nullable()();//6.4?
  TextColumn get expliquePorque => text().nullable()();//6.4.1 ?
  TextColumn get quemPodeAjudarQuandoUmaCriancaNasceComProblema => text().nullable()();//6.5 ?
  TextColumn get emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao => text().nullable()();//6.6 ?
  TextColumn get emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene => text().nullable()();//6.6 
  TextColumn get emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos => text().nullable()();//6.6 
  TextColumn get aPessoaEntrevistadaPareceuSinceraNasSuasRespostas => text().nullable()(); //7.1 ?
  TextColumn get algumasPerguntasIncomodaramAPessoaEntrevistada => text().nullable()();//7.1.1 ?
  TextColumn get seSimQuais3PrincipaisPerguntas => text().nullable()();//7.1.1.1 
  TextColumn get todasAsPerguntasIncomodaram => text().nullable()();//7.1.1.2
  TextColumn get aResidenciaEraDe => text().nullable()();//7.2?
  TextColumn get comoAvaliaHigieneDaCasa => text().nullable()();//7.3 
  TextColumn get oQueEraMal => text().nullable()();//7.3.2.1 ?
  TextColumn get tinhaCriancasNaCasaDuranteInquerito => text().nullable()(); //7.4 ?
  TextColumn  get tinhaCriancasNaCasaDuranteInqueritoQuantas => text().nullable()();//7.4.1

  TextColumn get tentouIncentivar => text().nullable()();//7.4.2
  TextColumn get tentouIncentivarQuantas => text().nullable()();//4.4.2.1
  TextColumn get seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos => text().nullable()();//7.4.2.2

  TextColumn get observouCriancasDeAte1Ano => text().nullable()();//7.5
  TextColumn get observouCriancasDeAte1AnoQuantos => text().nullable()();//7.5.1
  
  TextColumn get algumasApresentavamProblemas => text().nullable()();//7.5.1.1
  TextColumn  get algumasApresentavamProblemasQuantos => text().nullable()();//7.5.1.1
  TextColumn get descreveOProblemaDessasCriancas => text().nullable()();
  TextColumn get observouCriancasDe1AnoEmeioAte3Anos => text().nullable()();//7.6
  TextColumn  get observouCriancasDe1AnoEmeioAte3AnosQuantas => text().nullable()();//7.6.1
  TextColumn get algumasCriancasDe1anoEmeioObservadasApresentavamProblemas => text().nullable()();//7.6.1.1
  TextColumn  get algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas => text().nullable()();//7.6.1.1
  TextColumn get algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve => text().nullable()();//7.6.1.1 Descreve

  TextColumn get observouCriancasDe3AnosAte6Anos => text().nullable()();//7.7
  TextColumn  get observouCriancasDe3AnosAte6AnosQuantas => text().nullable()();//7.7.1
  TextColumn get observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas => text().nullable()();//7.7.1.1 Algumas apresentavam problemas
  TextColumn  get observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas => text().nullable()();//7.7.1.1 se sim quantas
  TextColumn get observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve => text().nullable()(); //7.7.1.1 Descreve
  TextColumn get observouSituacoesQueLheChamaramAtencao => text().nullable()();//7.8
  TextColumn get emRelacaoAPessoaEntrevistada => text().nullable()();//7.8.1
  TextColumn get emRelacaoAPessoaEntrevistadaDescreve => text().nullable()();//7.8.1.1 Se sim, 
  TextColumn get emRelacaoAOutrosAdultosPresente => text().nullable()();//7.8.2
  TextColumn get emRelacaoAOutrosAdultosPresenteDescreve => text().nullable()();//7.8.2.1 Se sim, descreve
  TextColumn get emRelacaoAsCriancasPresentes => text().nullable()();//7.8.3
  TextColumn get emRelacaoAsCriancasPresentesDescreve => text().nullable()();//7.8.3.1 
  TextColumn get emRelacaoAVizinhanca => text().nullable()();//7.9
  TextColumn get emRelacaoAVizinhancaDescreve => text().nullable()();//7.9 
  TextColumn get outras => text().nullable()();//7.9.2
  TextColumn get outrasDescreve => text().nullable()();//7.9.2.1 
  // ignore: non_constant_identifier_names
  DateTimeColumn get createdAt => dateTime().nullable()();
  // ignore: non_constant_identifier_names
  DateTimeColumn get updatedAt => dateTime().nullable()();
}
@DataClassName("InsertedUser")
class InsertedUsers extends Table{
  IntColumn get id => integer().autoIncrement().nullable()();
  TextColumn get uuidInsert => text().withLength(max: 191)();
  DateTimeColumn get createdAt => dateTime().nullable()();
  // ignore: non_constant_identifier_names
  DateTimeColumn get updatedAt => dateTime().nullable()();
}
@DataClassName("UpdatedUser")
class UpdatedUsers extends Table{
  IntColumn get id => integer().autoIncrement().nullable()();
  TextColumn get uuidUpdate => text().withLength(max: 191)();
  DateTimeColumn get createdAt => dateTime()();
  // ignore: non_constant_identifier_names
  DateTimeColumn get updatedAt => dateTime()();
}
@DataClassName("DeletedUser")
class DeletedUsers extends Table{
  IntColumn get id => integer().autoIncrement().nullable()();
  TextColumn get uuidDelete => text().withLength(max: 191)();
  DateTimeColumn get createdAt => dateTime()();
  // ignore: non_constant_identifier_names
  DateTimeColumn get updatedAt => dateTime()();
}
LazyDatabase _openConnection() {
  // the LazyDatabase util lets us find the right location for the file async.
  return LazyDatabase(() async {
    // put the database file, called db.sqlite here, into the documents folder
    // for your app.
    final dbFolder = await getApplicationDocumentsDirectory();
    final file = File(p.join(dbFolder.path, 'epb.sqlite'));
    return VmDatabase(file);
  });
}
@UseMoor(tables:[Users,InsertedUsers,UpdatedUsers,DeletedUsers])
class MyDatabase extends _$MyDatabase {
  // we tell the database where to store the data with this constructor
  MyDatabase() : super(_openConnection());

  @override
  int get schemaVersion => 1;

  Stream<List<User>> watchUsers (){
    return select(users).watch();
  }
  Stream<List<InsertedUser>> watchInsertedUsers (){
    return select(insertedUsers).watch();
  }
   Stream<List<UpdatedUser>> watchUpdatedUsers (){
    return select(updatedUsers).watch();
  }
  Stream<List<DeletedUser>> watchDeletedUsers (){
    return select(deletedUsers).watch();
  }

  Future<List<User>> get allUsers => select(users).get();
  Future<List<InsertedUser>> get allInsertedUsers => select(insertedUsers).get();
  Future<List<UpdatedUser>> get allUpdatedUsers => select(updatedUsers).get();
  Future<List<DeletedUser>> get allDeletedUsers => select(deletedUsers).get();

  Future<User> userById(String uuid){
    return (select(users)..where((tbl) => tbl.uuid.equals(uuid))).getSingle();
  }

  Future<int> addUserCompanion(UsersCompanion entry) {
    return into(users).insert(entry).then((value) {
      into(insertedUsers).insert(InsertedUser(
        uuidInsert: entry.uuid.value,
        createdAt: entry.createdAt.value,
        updatedAt: entry.updatedAt.value
      ));
      return value;
    });
  }
  Future<int> addUserCompanionFromServer(UsersCompanion entry) {
    return into(users).insert(entry);
  }
  Future<int> addUser(User entry) {
    return into(users).insert(entry).then((value) {
      into(insertedUsers).insert(InsertedUser(
        uuidInsert: entry.uuid,
        createdAt: entry.createdAt,
        updatedAt: entry.updatedAt
      ));
      return value;
    });
  }
  Future<int> addUserFromServer(User entry) {
      return into(users).insert(
        entry
      );
  }

  Future updateUser(User entry) {
  return (update(users)..where((tbl) => tbl.uuid.equals(entry.uuid))).write(entry.toCompanion(true)).then((value){
    into(updatedUsers).insert(UpdatedUser(
        uuidUpdate: entry.uuid,
        createdAt: entry.createdAt,
        updatedAt: entry.updatedAt
       ));
      return value;
    });
  }
  Future updateUserFromServer(User entry) {
    return (update(users)..where((tbl) => tbl.uuid.equals(entry.uuid))).write(entry.toCompanion(true));
  }

  Future deleteUserFromServer(String uuid) async{
    return (delete(users)..where((t) => t.uuid.equals(uuid))).go();
  }
  Future deleteUser(String uuid) async{
    final  user = await userById(uuid);
    into(deletedUsers).insert(DeletedUser(
        uuidDelete: user.uuid,
        createdAt: user.createdAt,
        updatedAt: user.updatedAt
    ));
    return (delete(users)..where((t) => t.uuid.equals(uuid))).go();
  }
  
  Future emptyUsers() {
    return (delete(users)).go();
  }

  Future deleteInsertedUserById(int id) {
    return (delete(insertedUsers)..where((t) => t.id.equals(id))).go();
  }
   Future<InsertedUser> insertedUsersById(int id){
    return (select(insertedUsers)..where((tbl) => tbl.id.equals(id))).getSingle();
    }
  Future emptyInsertedUsers() {
    return (delete(insertedUsers)).go();
  }
  Future deleteUpdatedUserById(int id) {
    return (delete(updatedUsers)..where((t) => t.id.equals(id))).go();
  }
   Future<UpdatedUser> updatedUsersUsersById(int id){
    return (select(updatedUsers)..where((tbl) => tbl.id.equals(id))).getSingle();
    }
  Future emptyUpdatedUsers() {
    return (delete(updatedUsers)).go();
  }
  Future deleteDeletedUserById(int id) {
    return (delete(deletedUsers)..where((t) => t.id.equals(id))).go();
  }
  Future<DeletedUser> deletedUsersById(int id){
    return (select(deletedUsers)..where((tbl) => tbl.id.equals(id))).getSingle();
    }
  Future emptyDeletedUsers() {
    return (delete(deletedUsers)).go();
  }

  Future<ListQueue<User>> getInsertedUsers () async{
    ListQueue<User> aux =  new ListQueue();
    final dart = await allInsertedUsers;
    dart.forEach((element) async{ 
      final tx = await userById(element.uuidInsert);
        aux.add(tx);
    });
    return aux;
  }
  Future<ListQueue<User>> getUpdatedUsers () async{
    ListQueue<User> aux =  new ListQueue();
    final dart = await allUpdatedUsers;
    dart.forEach((element) async{ 
      final tx = await userById(element.uuidUpdate);
        aux.add(tx);
    });
    return aux;
  }

  Future<ListQueue<User>> getDeletedUsers () async{
    ListQueue<User> aux =  new ListQueue();
    final dart = await allDeletedUsers;

    dart.forEach((element) async{ 
      final tx = new User(
        uuid: element.uuidDelete,
        nome: "For Delete",
        nomeDoPesquisador: "Server",
        createdAt: element.createdAt,
        updatedAt: element.updatedAt,
        
      );
        aux.add(tx);
    });
    return aux;
  }
  

Stream<int> dart() async* {
  final stream = NumberCreator().stream;
  await for (var value in stream){
    int  inserted = await  select(insertedUsers).get().then((event) => (event.length));
    int  updated = await select(updatedUsers).get().then((event) => event.length);
    int  deleted = await select(deletedUsers).get().then((event) => (event.length));
      var result = inserted + updated + deleted;
        if(result>=99){
          result = 99;
        }
      yield (result);
  }
}


}


class NumberCreator{
  NumberCreator(){
    Timer.periodic(Duration(seconds:1), (timer) { 
        _controller.sink.add(_count);
        _count++;
    });
  }
   var _count =0;
    final _controller = new StreamController<int>();
    Stream<int> get stream => _controller.stream;
}