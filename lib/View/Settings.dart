import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class Settigs extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Definições'),
        backgroundColor: Colors.deepPurple,
      ),
      body:new Container(
        child: new Column(
          children: <Widget>[
            Container(
              child:ListTile(
                title: new Text('Nome',style: TextStyle(
                    color: Colors.deepPurple,
                    fontWeight: FontWeight.bold
                ),),
                subtitle: new Text('Epbeira Base de dados de estimulação precoce.'),
                onTap: (){},
              ),
              decoration: BoxDecoration(
                border: Border(bottom: BorderSide(
                    width: 1,
                    color: Colors.black12
                )),
              ),
            ),
            Container(
              child: ListTile(
                title: new Text('Data de Lançamento',style: TextStyle(
                    color: Colors.deepPurple,
                    fontWeight: FontWeight.bold
                ),),
                subtitle: new Text('20 de Novembro de 2020'),
                onTap: (){},
              ),
              decoration: BoxDecoration(
                border: Border(bottom: BorderSide(
                    width: 1,
                    color: Colors.black12
                )),
              ),
            ),
            Container(
              child: ListTile(
                title: new Text('Versão',style: TextStyle(
                    color: Colors.deepPurple,
                    fontWeight: FontWeight.bold
                ),),
                subtitle: new Text('1.0.0+ (Stable)'),
                onTap: (){},
              ),
              decoration: BoxDecoration(
                border: Border(bottom: BorderSide(
                    width: 1,
                    color: Colors.black12
                )),
              ),
            ),
            Container(
              child: ListTile(
                title: new Text('Parceiros de implementação',style: TextStyle(
                    color: Colors.deepPurple,
                    fontWeight: FontWeight.bold
                ),),
                subtitle: new Text('Sumburero, ESSOR, UFPB e AGACC '),
                onTap: (){},
              ),
              decoration: BoxDecoration(
                border: Border(bottom: BorderSide(
                    width: 1,
                    color: Colors.black12
                )),
              ),
            ),
            Container(
              child: ListTile(
                title: new Text('Informação sobre o programador',style: TextStyle(
                    color: Colors.deepPurple,
                    fontWeight: FontWeight.bold
                ),),
                subtitle: new Text(' Nome: Nelson Alexandre Mutane \n Email: Nelsonmutane@gmail.com ou Nelson.mutane@uzambeze.ac.mz \n Contacto: (+258) 847607095',
                ),
                onTap: () async{
                  const url = 'mailto:nelsonmutane@gmail.com,nelson.mutane@uzambeze.ac.mz?body=';
                  if (await canLaunch(url) != null) {
                    await launch(url);
                  } else {
                    throw 'Could not launch $url';
                  }
                },

              ),
              decoration: BoxDecoration(
                border: Border(bottom: BorderSide(
                    width: 1,
                    color: Colors.black12
                )),
              ),
            ),
            Expanded(
                child: GridView.count(
                  crossAxisCount: 4,
                  crossAxisSpacing: 2,
                  mainAxisSpacing: 5,
                  children: [
                    Card(
                      color: Colors.transparent,
                      elevation: 0,
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            image: DecorationImage(
                              fit: BoxFit.fitWidth,
                              image: AssetImage('images/img/sumburero.png'),
                            )

                        ),
                      ),
                    ),
                    Card(
                      color: Colors.transparent,
                      elevation: 0,
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            image: DecorationImage(
                              fit: BoxFit.fitWidth,
                              image: AssetImage('images/img/essor.png'),
                            )

                        ),
                      ),
                    ),
                    Card(
                      color: Colors.transparent,
                      elevation: 0,
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            image: DecorationImage(
                              fit: BoxFit.fitWidth,
                              image: AssetImage('images/img/ufpb.png'),
                            )

                        ),
                      ),
                    ),
                    Card(
                      color: Colors.transparent,
                      elevation: 0,
                      child: Container(
                        decoration: BoxDecoration(
                            borderRadius: BorderRadius.circular(20),
                            image: DecorationImage(
                              fit: BoxFit.fitWidth,
                              image: AssetImage('images/img/agacc.png'),
                            )

                        ),
                      ),
                    )
                  ],
                )
            )
          ],
        ),
      ) ,
    );
  }
}
