
import 'package:epbeira/Database/database.dart';
import 'package:epbeira/Model/Datastore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:progress_dialog/progress_dialog.dart';
import 'package:provider/provider.dart';
import 'package:data_connection_checker/data_connection_checker.dart';

class SyncGoogleSheet extends StatefulWidget {

  @override
  _SyncGoogleSheetState createState() => _SyncGoogleSheetState();
}

class _SyncGoogleSheetState extends State<SyncGoogleSheet> {


  ProgressDialog progressDialog;
  @override
  Widget build(BuildContext context) {
    MyDatabase _db = Provider.of<MyDatabase>(context);
    progressDialog = ProgressDialog(
      context,
      type: ProgressDialogType.Normal,
      showLogs: true,
      isDismissible: false,
    );
    progressDialog.style(
      progressWidgetAlignment:Alignment.bottomLeft,
      padding: EdgeInsets.all(10.0),
      message: "Em sincronização...",
      progress: 80,
        messageTextStyle: TextStyle(
            color:
            Colors.blue,fontWeight: FontWeight.w800,

        ),
      progressWidget: Container(
          padding: EdgeInsets.all(13.0), child: CircularProgressIndicator()),
    );


    void sync() async{
      progressDialog.show();
      bool result = await DataConnectionChecker().hasConnection;

      if(result == true) {
        try{
          Datastore.syncronization(_db).then((value) {
            progressDialog.update(message: "100% Complete",messageTextStyle: TextStyle(
                color:
                Colors.green,fontWeight: FontWeight.w800
            ));
            Future.delayed(Duration(seconds: 15)).then((value){
              progressDialog.hide().then((value) {
                 Navigator.pop(context);
              });
            } );
          });
        }catch(e){
          print("Erro: $e");
          progressDialog.update(message:"Dispositivo sem conexão a internet ",messageTextStyle:TextStyle(color: Colors.red,fontWeight: FontWeight.w800));
          Future.delayed(Duration(seconds: 15)).then((value){
            progressDialog.hide().then((value) {
               Navigator.pop(context);
            });
          } );

        }

      } else {
        print('No internet :( Reason:');
        progressDialog.update(message:"Dispositivo sem conexão a internet." ,messageTextStyle: TextStyle(
          color:
            Colors.red,fontWeight: FontWeight.w800
        ));

        Future.delayed(Duration(seconds: 15)).then((value){
          progressDialog.hide().then((value) {
             Navigator.pop(context);
          });
        } );

      }


    }
    Future.delayed(Duration(seconds: 0)).then((value) =>sync());

    return Scaffold(

      body:  Container(
        decoration: BoxDecoration(
            image: DecorationImage(
                image: AssetImage('images/bg.png'),
                fit: BoxFit.cover
            )
        ),
      ),

    );
  }
}

