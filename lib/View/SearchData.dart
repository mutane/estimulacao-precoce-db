
import 'package:epbeira/Database/database.dart';
import 'package:flutter/material.dart';
import 'package:flutter/cupertino.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:provider/provider.dart';

import 'Form.dart';
import 'dart:math' as math;
//part 'package:epbeira/Controller/AnimateCss.dart';

class SearchData extends SearchDelegate<User>{

  Route _createRoute(args,object) {
    return PageRouteBuilder(

      pageBuilder: (context, animation, secondaryAnimation) => object,
      settings: RouteSettings(
          arguments: args
      ),
      transitionsBuilder: (context, animation, secondaryAnimation, child) {
        var begin = Offset(2.0, 0.0);
        var end = Offset.zero;
        var curve = Curves.ease;
        var tween = Tween(begin:begin,end:end).chain(CurveTween(curve: curve));
        return SlideTransition(
          position: animation.drive(tween),
          child: child,
        );
      },
    );
  }

  @override
  List<Widget> buildActions(BuildContext context) {
    return [
      IconButton(icon: Icon(Icons.clear), onPressed: (){
        query = "";

      }),
    ];
  }

  @override
  Widget buildLeading(BuildContext context) {
    return IconButton(icon: AnimatedIcon(icon: AnimatedIcons.menu_arrow, progress: transitionAnimation), onPressed: (){
      close(context, null);
    });
  }

  @override
  Widget buildResults(BuildContext context) {

    throw UnimplementedError();
  }

  @override
  Widget buildSuggestions(BuildContext context) {
    final users = Provider.of<MyDatabase>(context);
    
    //var cities =[];
    // final sugestions = query.isEmpty?cities:cities;// .where((q) => q.startsWith(query)).toList();   );
    return FutureBuilder<List<User>>(
      // ignore: missing_return
      future: (users.allUsers.then((value) {
     
        List<User> toReturn;
        toReturn = value.where(
                (el) {
              return (
                      el.nome.toLowerCase().startsWith(query.toLowerCase()) || el.uuid.toString().startsWith(query.toLowerCase())
              );
            }
        ).toList();
        return toReturn;
      })
      ),
      builder: (context, snapshot) {
        if (snapshot.hasData) {
          return new ListView.builder(
              itemCount: snapshot.data.length,
              itemBuilder: (context, index) {
                return new Column(
                    crossAxisAlignment: CrossAxisAlignment.start,
                    children: <Widget>[
                      Container(
                        child: ListTile(

                          leading: CircleAvatar(child:Text('${snapshot.data[index].nome[0].toUpperCase()}',
                                  style: TextStyle(
                                    color: Colors.white,
                                    fontSize: 20,
                                  ),
                                ),
                            backgroundColor: Color((math.Random().nextDouble() * 0xFFFFFF).toInt()).withOpacity(1.0),),
                          title: Text(capitalize(snapshot.data[index].nome),
                            style: GoogleFonts.roboto(
                                fontWeight: FontWeight.w400
                            )),
                          subtitle: Text('${snapshot.data[index].uuid}'),
                          trailing: Container(
                            width: 100,
                            child: Row(
                              children: <Widget>[

                                IconButton(
                                    icon: Icon(Icons.edit), onPressed: () {
                                      Navigator.of(context).push(_createRoute(snapshot.data[index],CreateOrUpdateUser()));
                                }),
                                IconButton(
                                    icon: Icon(Icons.delete), onPressed: () {
                                  //
                                  showDialog(
                                      context: context,
                                      builder: (ctx) =>
                                          AlertDialog(
                                            title: Text('Apagar registo'),
                                            content: Text('Tem certeza?'),
                                            actions: <Widget>[
                                              FlatButton(
                                                onPressed: () {
                                                  Navigator.of(ctx).pop();
                                                 // Navigator.of(context).pop();
                                                },
                                                child: Text('Não',
                                                  style: TextStyle(
                                                      color: Colors.blueGrey),),

                                              ),
                                              FlatButton(
                                                onPressed: () {

                                                  Navigator.of(ctx).pop();
                                                  users.deleteUser(snapshot.data[index].uuid);
                                                  //Navigator.of(context).pop();
                                                  },
                                                child: Text('Sim',
                                                    style: TextStyle(
                                                        color: Colors
                                                            .blueGrey)),

                                              )
                                            ],
                                          )
                                  );
                                  //
                                })
                              ],
                            ),
                          ),
                          onTap: () {
                           // Navigator.of(context).pushNamed(
                             //   AppRoutes.Forms,
                               // arguments: snapshot.data[index]
                            //);
                          },
                        ),
                        decoration: BoxDecoration(
                          color: Colors.white.withOpacity(0.5),
                          border: Border(bottom: BorderSide(
                              width: 1,
                              color: Colors.black54.withOpacity(.05)
                          )),
                        ),
                      ),
                    ]);
              });
        } else if (snapshot.hasError) {
          return new Text("${snapshot.error}");
        }
        return new Container(alignment: AlignmentDirectional.center,
          child: new CircularProgressIndicator(),);
      },
    );
  }

  String capitalize(String string) {
    if (string == null) {
      throw ArgumentError("string: $string");
    }

    if (string.isEmpty) {
      return string;
    }

    return string[0].toUpperCase() + string.substring(1);
  }

 
}