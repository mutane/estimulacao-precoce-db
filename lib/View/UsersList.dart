
import 'package:epbeira/Database/database.dart';
import 'package:epbeira/View/Form.dart';
import 'package:epbeira/View/RelatarProblema.dart';
import 'package:epbeira/View/Settings.dart';
import 'package:epbeira/View/SyncGoogleSheet.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter_slidable/flutter_slidable.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:google_fonts/google_fonts.dart';
//import 'package:path/path.dart';
import 'package:provider/provider.dart';
import 'package:shimmer/shimmer.dart';
import 'package:badges/badges.dart';
import 'dart:math' as math;


import 'SearchData.dart';
part 'package:epbeira/Controller/AnimateCss.dart'; 


class HomePage extends StatelessWidget {

  const HomePage({Key key}) : super(key: key);



 String capitalize(String string) {
    if (string == null) {
      throw ArgumentError("string: $string");
    }

    if (string.isEmpty) {
      return string;
    }

    return string[0].toUpperCase() + string.substring(1);
  }
  @override
  Widget build(BuildContext context) {
    final database  = Provider.of<MyDatabase>(context);
   //int   _counter = database.countForSync as int;

    return Scaffold(
      appBar: AppBar(
       backgroundColor: Colors.deepPurple,
       // Here we take the value from the MyHomePage object that was created by
       // the App.build method, and use it to set our appbar title.
       title: Text('Registos'),
       actions: <Widget>[
         IconButton(tooltip: 'Pesquisar',icon: Icon(FontAwesomeIcons.search,size: 18,), color: Colors.white,onPressed: (){
           showSearch(context: context, delegate: SearchData());
         }),

        StreamBuilder(
          stream: database.dart(),
          initialData: database.dart(),
          builder: (BuildContext context, AsyncSnapshot snapshot) {
            return Badge(
              toAnimate: false,
              shape: BadgeShape.circle,
              position: BadgePosition.topStart(
                  top: 0, start: -8
              ),
              badgeColor: Colors.orange,
              borderRadius: BorderRadius.circular(8),
              badgeContent: Text(
                  '${snapshot.data}', style: TextStyle(color: Colors.white)),
              child: IconButton(tooltip: 'Sincronização',
                  //icon: Icon(FontAwesomeIcons.syncAlt,size: 18,),
                  icon: Icon(Icons.sync),
                  color: Colors.white,
                  onPressed: () {
                    Navigator.of(context).push(
                        _createRoute('Text', SyncGoogleSheet()));
                  }),
            );
          })/*
            Badge(
             toAnimate: false,
             shape: BadgeShape.circle,
             position: BadgePosition.topEnd(
                 top: 0, end: 0
             ),
             badgeColor: Colors.orange,
             borderRadius: BorderRadius.circular(8),
             badgeContent: Text('${snapshot.data}', style: TextStyle(color: Colors.white)),
           child:  IconButton(tooltip:'Sincronização',
               //icon: Icon(FontAwesomeIcons.syncAlt,size: 18,),
               icon: Icon(Icons.sync),
               color: Colors.white ,
               onPressed:(){
                 Navigator.of(context).push(_createRoute('Text', SyncGoogleSheet()));
               }),
         );
          },
        ),
*/

         

       ],
     ),
     drawer: Drawer(
       child: Column(
         children: <Widget>[
           Container(
               child: UserAccountsDrawerHeader(
                   accountEmail: new Text('Estimulação precoce Database',style:TextStyle(
                       color: Colors.white,
                       fontWeight: FontWeight.bold,
                       fontSize: 15
                   )),
                   decoration: BoxDecoration(

                       image: DecorationImage(
                         image: AssetImage('images/dr.png'),
                         fit: BoxFit.cover
                       )
                   ),
                  accountName: Text(''),
               ),

           ),
           ListTile(
             leading: Icon(FontAwesomeIcons.userPlus,size: 17,),
             title:new Text( "Novo registo"),
             onTap: (){
               Navigator.pop(context);
               Navigator.of(context).push(_createRoute(new User(uuid: null),CreateOrUpdateUser()));
             },
             selected: true,
           ),
           ListTile(

             leading: Icon(FontAwesomeIcons.syncAlt,size: 17,),
             title:new Text( "Sincronizar dados"),
             onTap: (){
               //MyDatabase db =Provider.of<MyDatabase>(context);
               Navigator.pop(context);
               Navigator.of(context).push(
                   _createRoute('Text', SyncGoogleSheet())
               );
             },
           ),
           Divider(),
           ListTile(
             leading: Icon(Icons.info_outline),
             title:new Text( "Relatar problema"),
             onTap: (){
               Navigator.pop(context);
               Navigator.of(context).push(_createRoute('Text',Problema()));
             },
           ),
           ListTile(
             leading: Icon(Icons.settings),
             title:new Text( "Definições"),
             onTap: (){
                Navigator.pop(context);
                Navigator.of(context).push(_createRoute('Text',Settigs()));
             },
           ),
         ],
       )
     ),
     body: new Container(

         child: _buildListUsers(context)
     ),
     floatingActionButton: FloatingActionButton.extended(
       label:Text('Novo'),
       onPressed: (){
         Navigator.of(context).push(_createRoute(new User(uuid: null),CreateOrUpdateUser()));
       },
       tooltip: 'Novo registo',
       icon: Icon(Icons.person_add,),
       backgroundColor: Colors.deepPurpleAccent,
     ),
    );
  }
  StreamBuilder<List<User>> _buildListUsers(BuildContext context){

    final database  = Provider.of<MyDatabase>(context);


    /*database.addUser(User(
      observouCriancasDe1AnoEmeioAte3Anos: 'tt',
      nome: "e'${FontAwesomeIcons.random.toString()}",
      createdAt: DateTime.now().toString(),
      updatedAt: DateTime.now().toString()
    ));*/
    return StreamBuilder(
        stream:database.watchUsers(),
        // ignore: missing_return
        builder: (context,AsyncSnapshot<List<User>> snapshot){
          if(snapshot.connectionState!= ConnectionState.active
          ){
                return  Container(
               width: double.infinity,
               padding: const EdgeInsets.symmetric(horizontal: 16.0, vertical: 16.0),
                child: Column(
                  mainAxisSize: MainAxisSize.max,
                  children: <Widget>[
                    Expanded(
                      child: Shimmer.fromColors(
                        baseColor: Colors.grey[300],
                        highlightColor: Colors.grey[100],
                        enabled: true,
                        child: ListView.builder(
                          itemBuilder: (_, __) => Padding(
                            padding: const EdgeInsets.only(bottom: 15.0,top: 15.0),
                            child: Row(
                              crossAxisAlignment: CrossAxisAlignment.start,
                              children: [
                                CircleAvatar(
                                  backgroundColor: Colors.grey,
                                ),
                                const Padding(
                                  padding: EdgeInsets.symmetric(horizontal: 8.0),
                                ),
                                Expanded(
                                  child: Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                    children: <Widget>[
                                      const Padding(
                                        padding: EdgeInsets.symmetric(vertical: 2.0),
                                      ),
                                      Container(
                                        width: double.infinity,
                                        height: 8.0,
                                        color: Colors.white,
                                      ),
                                      const Padding(
                                        padding: EdgeInsets.symmetric(vertical: 2.0),
                                      ),
                                      const Padding(
                                        padding: EdgeInsets.symmetric(vertical: 2.0),
                                      ),
                                      Container(
                                        width: double.infinity,
                                        height: 8.0,
                                        color: Colors.white,
                                      ),
                                      const Padding(
                                        padding: EdgeInsets.symmetric(vertical: 2.0),
                                      ),

                                    ],
                                  ),
                                )
                              ],
                            ),
                          ),
                          itemCount: 5,
                        ),
                      ),
                    ),
                  ],
                ) ,
           );
          }else{
              final users = snapshot.data ?? List();
              return ListView.builder(
                itemCount: users.length,
                itemBuilder: (_,index) {
                  final itemBenificiario = users[index];

                  return _builListBen(itemBenificiario,database,context);
                },
              );
          }
        }
    );
  }


  Widget _builListBen(User user,MyDatabase database,BuildContext context){
  
    //print("Nome: ${user.nome}  uuid: ${user.uuid}");
  //  database.userById(1).then((value) => print(value));
    String l = (user.nome !=null && user.nome!="")?user.nome[0].toUpperCase():'U';
  
    return Slidable(
      actionPane: SlidableDrawerActionPane(),
      actionExtentRatio: 0.10,
      child: Column(
        children: <Widget> [
          Container(
            child: ListTile(
                leading: CircleAvatar(
                  child: Text('$l',
                    style: TextStyle(
                      color: Colors.white,
                      fontSize: 20,
                    ),
                  ),
                  backgroundColor: Color((math.Random().nextDouble() * 0xFFFFFF).toInt()).withOpacity(1.0),
                ),
                title: Text(capitalize(user.nomeDoPesquisador),style: GoogleFonts.roboto(
                    fontWeight: FontWeight.w400
                )),
                subtitle: Text(user.uuid),
                trailing:Container(
                  width: 150,
                  child: Row(
                    children: <Widget>[
                      IconButton(icon: Icon(Icons.remove_red_eye),onPressed: (){
                       // Navigator.of(context).push(_viewBen(user));
                       Navigator.of(context).push(_createRoute(user,CreateOrUpdateUser()));
                      }),
                      IconButton(icon: Icon(Icons.edit),onPressed: (){
                        Navigator.of(context).push(_createRoute(user,CreateOrUpdateUser()));
                      }),
                      IconButton(icon: Icon(Icons.delete), onPressed: (){
                        showDialog(
                            context: context,
                            builder: (ctx) =>AlertDialog(
                              title: Text('Apagar registo'),
                              content: Text('Tem certeza?'),
                              actions: <Widget>[
                                FlatButton(
                                  onPressed: () { Navigator.of(ctx).pop();},
                                  child: Text('Não',style: TextStyle(color: Colors.blueGrey),),
                                ),
                                FlatButton(
                                  onPressed: (){
                                     Navigator.of(ctx).pop();
                                     database.deleteUser(user.uuid);
                                    //Navigator.of(context).pop();
                                   // database.deleteBenificiario(benificiario.id);
                                    //Provider.of<Users>(context,listen: false).deleteUser(snapshot.data[index]);
                                  },
                                  child: Text('Sim',style: TextStyle(color: Colors.blueGrey)),

                                )
                              ],
                            )
                        );
                      })
                    ],
                  ),
                )
            ),
            decoration: BoxDecoration(
              color: Colors.white.withOpacity(0.5),
              border: Border(bottom: BorderSide(
                  width: 1,
                  color: Colors.black54.withOpacity(.05)
              )),
            ),
          )
        ],
      ),
    );
  }

}