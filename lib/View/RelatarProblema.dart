import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:url_launcher/url_launcher.dart';

class Problema extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text('Relatar erro ou melhoria'),
        backgroundColor: Colors.deepPurple,
      ),
      body:new Container(
       child: new Column(
         children: <Widget>[
           Container(
            child:ListTile(
               title: new Text('Relatar Por mensagem',style: TextStyle(
                   color: Colors.deepPurple,
                   fontWeight: FontWeight.bold
               ),),
               subtitle: ListTile(leading: Icon(Icons.message),title: Text('847607095')),
               onTap: () async{
                 const url = 'sms:+258847607095?body=Relato de erro(s):\n \n \n';
                  if (await canLaunch(url) != null) {
                    await launch(url);
                  } else {
                    throw 'Could not launch $url';
                  }
               },
             ),
             decoration: BoxDecoration(
               border: Border(bottom: BorderSide(
                   width: 1,
                   color: Colors.black12
               )),
             ),
           ),
           Container(
             child: ListTile(
           title: new Text('Relatar Por chamada',style: TextStyle(
           color: Colors.deepPurple,
           fontWeight: FontWeight.bold
           ),),
           subtitle: ListTile(leading: Icon(Icons.call),title: Text('847607095'),),
            onTap: () async{
                 const url = 'tel:+258847607095';
                  if (await canLaunch(url) != null) {
                    await launch(url);
                  } else {
                    throw 'Could not launch $url';
                  }
               },
           ),
             decoration: BoxDecoration(
               border: Border(bottom: BorderSide(
                   width: 1,
                   color: Colors.black12
               )),
             ),
           ),
           Container(
             child: ListTile(
               title: new Text('Relatar por email',style: TextStyle(
                   color: Colors.deepPurple,
                   fontWeight: FontWeight.bold
               ),),
               subtitle: ListTile(leading: Icon(Icons.mail),title: Text('Nelson.mutane@uzambeze.ac.mz e Nelsonmutane@gmail.com'),),
               onTap: () async{
                 const url = 'mailto:nelson.mutane@uzambeze.ac.mz,nelsonmutane@gmail.com';
                  if (await canLaunch(url) != null) {
                    await launch(url);
                  } else {
                    throw 'Could not launch $url';
                  }
               },
             ),
             decoration: BoxDecoration(
               border: Border(bottom: BorderSide(
                   width: 1,
                   color: Colors.black12
               )),
             ),
           ),
         
         
         ],
       ),
      ) ,
    );
  }
}
