import 'package:international_phone_input/international_phone_input.dart';
import 'package:intl/intl.dart';
import 'package:flutter/cupertino.dart';
import 'package:datetime_picker_formfield/datetime_picker_formfield.dart';
import 'package:epbeira/Database/database.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:provider/provider.dart';
import 'package:uuid/uuid.dart';
import 'dart:math' as math;

class CreateOrUpdateUser extends StatefulWidget {
  CreateOrUpdateUser({Key key}) : super(key: key);

  @override
  _CreateOrUpdateUserState createState() => _CreateOrUpdateUserState();
}

class _CreateOrUpdateUserState extends State<CreateOrUpdateUser> {

  final _form = GlobalKey<FormState>();
  List<String> motivoDoObito = ['','Problema na gravidez','Problema no parto','Problema depois do parto','Destino','Outro motivo'];
  List<String> bairros = ['','Chipangara','Dondo','Inhamizua','Munhava','Muave'];
  List<String> escolinhas = ['','Não tinha vagas para eles','A Escolinha é cara','Não teriam concentração necessária na escolinha','Não saberiam se comportar','Porque trabalham','Outro motivo'];
  List<String> simNao = ['','Sim','Não'];

  List<String> umaCriancaComProblemas = [
    '','E o destino','Problema de saúde dos pais','Falta de cuidado na gravidez',' Feitiçaria'
    ,'Falta de cuidado no parto','Condições socio econômica desfavoráveis da família','È um benção','Outro'
  ];
  List<String> bairroComCriancaComProblemas = [
    '','Os pais abandonem a criança','A família esconde a criança em casa','Se procura os serviços públicos (saúde / Assistência social)',
    'Se procura dos serviços tradicionais','Os pais acabam se separando','Os pais entregam para família cuidar','Outro'
  ];

  List<String> quemAjudaCriancaConProblemas = [
    '','A família','Os serviços públicos (saúde / Assistência Social)','Associação de moradores','Serviços tradicionais','Instituição religiosa'
    ,'Os vizinhos','Outro'
  ];
  List<String> quemCuidadeCriancaAte6anos = [
    '','O pai','A mãe','Os avós','Os/as irmãos/ãs mais velhos','Os/as tios/as','Outros Familiares'
  ];

  List<String> residencia = ['',
    'Caniço','Tijolos','Zinco','Mista'
  ];

  List<String> higieneDaCasa = [
    '','Boa','Mal'
  ];









  //data for form
  final format = DateFormat("yyyy-MM-dd hh:mm:ssssss");
  String uuid ="false";
  String nomeDoPesquisador;
  String numeroDeInquerito;
  DateTime data;
  String municipio;
  String bairro;
  String pontoDeReferenciaDaCasa;
  String nome;
  String idade;
  String contacto;
  String eChefeDaFamilia;
  String caoNaoVinculoDeParantesco;
  String idadeDoChefeDaFamilia;
  String estudou;
  String eAlfabetizado;
  String completouEnsinoPrimario;
  String completouEnsinoSecundario;
  String fezCursoSuperior;
  String naoSabe;
  String chefeDeFamiliaTemSalario;
  String quantoGastaParaSustentarFamiliaPorMes;
  String quantasFamiliasResidemNacasa;
  String quantosAdultos;
  String quantosAdultosMaioresDe18anos;
  String quantosAdultosMaioresDe18anosHomens;
  String quantosAdultosMaioresDe18anosHomensTrabalham;
  String quantosAdultosMaioresDe18anosHomensAlfabetizados;
  String quantosAdultosM18Mulheres;
  String quantosAdultosM18MulheresTrabalham;
  String quantosAdultosM18MulheresAlfabetizados;
  String relatosDeDeficiencia;
  String totalcriancasAte1anoemeio;
  String totalcriancasAte1anoemeioMeninos;
  String totalcriancasAte1anoemeioMeninas;
  String algumasCriancasAte1anoemeioNaodesenvolvem;
  String algumasCriancasAte1anoemeioNaodesenvolvemQuantas;
  String algumaCriancasAte1anoemeioQueTipodeProblema;
  String algumaCriancasAte1anoemeioTemProblemaMotor;
  String algumaCriancasAte1anoemeioTemProblemaMotorQuantas;
  String algumaCriancasAte1anoemeioTemMalFormacao;
  String algumaCriancasAte1anoemeioTemMalFormacaoQuantas;
  String algumaCriancasAte1anoemeioNaoReagemAIncentivo;
  String algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas;
  String algumaCriancasAte1anoemeioTemReacaoAgressiva;
  String algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas;
  String totalcriancasAte1anoemeioA3anos;
  String totalcriancasAte1anoemeioA3anosMeninos;
  String totalcriancasAte1anoemeioA3anosMeninas;
  String algumasCriancasAte1anoemeioA3anosNaodesenvolvem;
  String algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas;
  String algumaCriancasAte1anoemeioA3anosQueTipodeProblema;
  String algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas;
  String algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas;
  String algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo;
  String
  algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas;
  String algumasCriancasAte1anoemeioA3anosAlgunsAgridem;
  String algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas;
  String algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal;
  String
  algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas;
  String algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao;
  String algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas;
  String algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas;
  String algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas;
  String algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente;
  String
  algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas;
  String algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos;
  String
  algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas;
  String algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown;
  String algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas;
  String totalcriancasde3A6anos;
  String totalcriancasde3A6anosMeninos;
  String totalcriancasde3A6anosMeninas;
  String algumasCriancasde3A6anosNaodesenvolvem;
  String algumasCriancasde3A6anosNaodesenvolvemQuantas;
  String algumaCriancasde3A6anosQueTipodeProblema;
  String algumasCriancasde3A6anosNaoInteragemComPessoas;
  String algumasCriancasde3A6anosNaoInteragemComPessoasQuantas;
  String algumasCriancasde3A6anosTemComportamentoAgressivo;
  String algumasCriancasde3A6anosTemComportamentoAgressivoQuantas;
  String algumasCriancasde3A6anosAlgunsAgridem;
  String algumasCriancasde3A6anosAlgunsAgridemQuantas;
  String algumasCriancasde3A6anosAlgumasAgitadasQueONormal;
  String algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas;
  String algumasCriancasde3A6anosAlgumasTemMalFormacao;
  String algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas;
  String algumasCriancasde3A6anosAlgumasNaoAndamSozinhas;
  String algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas;
  String algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente;
  String algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas;
  String algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos;
  String algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas;
  String algumasCriancasde3A6anosAlgumasTemSindromeDeDown;
  String algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas;
  String algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas;
  String algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas;
  String algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas;
  String
  algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas;
  String
  algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas;
  String
  algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas;
  String algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas;
  String
  algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas;
  String quantasCriancasde3A6anosEstaoNaEscolinha;
  String criancasDe3A6anosNaoEstaoNaEscolinhaPorque;
  String quantasCriancasde3A6anosTrabalham;
  String criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos;
  String totalcriancasde7A9anos;
  String totalcriancasde7A9anosMeninos;
  String totalcriancasde7A9anosMeninas;
  String quantasCriancasde7A9anosEstaoNaEscola;
  String quantasCriancasde7A9anosTrabalham;
  String nosUltimos5AnosQuantasGravidezesTiveNaFamilia;
  String partosOcoridosNoHospital;
  String partosOcoridosEmCasa;
  String partosOcoridosEmOutroLocal;
  String outroLocalDoPartoQualLocal;
  String quantasCriancasNasceramVivas;
  String quantasContinuamVivas;
  String
  dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram;
  String descreverOProblema;
  String aFamiliaProcurouAjudaParaSuperarEsseProblema;
  String seSimQueTipoDeAjudaEOnde;
  String
  osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia;
  String
  dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida;
  String sabeDizerOMotivoDosObitos;
  String seSimQualFoiPrincipalRazaodoObito;
  String voceTemMedoDeTerUmFilhoOuNetoComProblemas;
  String expliquePorQue;
  String porqueVoceAchaQueUmaCriancaPodeNascerComProblema;
  String oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema;
  String achaQueUmaCriancaQueNasceComProblemaPodeSobreviver;
  String expliquePorque;
  String quemPodeAjudarQuandoUmaCriancaNasceComProblema;
  String emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao;
  String emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene;
  String emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos;
  String aPessoaEntrevistadaPareceuSinceraNasSuasRespostas;
  String algumasPerguntasIncomodaramAPessoaEntrevistada;
  String seSimQuais3PrincipaisPerguntas;
  String todasAsPerguntasIncomodaram;
  String aResidenciaEraDe;
  String comoAvaliaHigieneDaCasa;
  String oQueEraMal;
  String tinhaCriancasNaCasaDuranteInquerito;
  String tinhaCriancasNaCasaDuranteInqueritoQuantas;
  String tentouIncentivar;
  String tentouIncentivarQuantas;
  String seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos;
  String observouCriancasDeAte1Ano;
  String observouCriancasDeAte1AnoQuantos;
  String algumasApresentavamProblemas;
  String algumasApresentavamProblemasQuantos;
  String descreveOProblemaDessasCriancas;
  String observouCriancasDe1AnoEmeioAte3Anos;
  String observouCriancasDe1AnoEmeioAte3AnosQuantas;
  String algumasCriancasDe1anoEmeioObservadasApresentavamProblemas;
  String algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas;
  String
  algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve;
  String observouCriancasDe3AnosAte6Anos;
  String observouCriancasDe3AnosAte6AnosQuantas;
  String observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas;
  String
  observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas;
  String
  observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve;
  String observouSituacoesQueLheChamaramAtencao;
  String emRelacaoAPessoaEntrevistada;
  String emRelacaoAPessoaEntrevistadaDescreve;
  String emRelacaoAOutrosAdultosPresente;
  String emRelacaoAOutrosAdultosPresenteDescreve;
  String emRelacaoAsCriancasPresentes;
  String emRelacaoAsCriancasPresentesDescreve;
  String emRelacaoAVizinhanca;
  String emRelacaoAVizinhancaDescreve;
  String outras;
  String outrasDescreve;
  DateTime createdAt;
  DateTime updatedAt;
  @override
  void initState() {
    super.initState();
  }

  @override
  void dispose() {
    super.dispose();
  }
  int cast(String value){
    int sol = 0;
    if(value.length == 1){
      for(int i =0;i<10;i++){
        if(value[0] == '$i'){
          sol = i;
        }
      }
    }else if(value.length == 2){
      for(int b =0;b<value.length;b++){
        for(int i =0;i<10;i++){
          if((b == 0) && value[0] == '$i'){
            sol = i*10;
          }else if((b == 1) && value[1] == '$i'){
            sol +=i;
          }
        }
      }
    }
    return sol;
  }
  @override
  Widget build(BuildContext context) {
    final database = Provider.of<MyDatabase>(context);
    User user = ModalRoute.of(context).settings.arguments;
    String title = "Editar benificiario";
    String tooltip = "Actualizar benificiario";
    bool edit = true;
    String phoneNumber;
    String phoneIsoCode;
    bool visible = false;
    String confirmedNumber = '';

    void onPhoneNumberChange(String number, String internationalizedPhoneNumber, String isoCode) {
      print(internationalizedPhoneNumber);
      phoneNumber = number;
      this.contacto = internationalizedPhoneNumber.substring(4);
      print(this.contacto);
      phoneIsoCode = isoCode;
    }

    onValidPhoneNumber(
        String number, String internationalizedPhoneNumber, String isoCode) {
      visible = true;
      confirmedNumber = internationalizedPhoneNumber;
    }

    if(user.uuid == null){
      setState(() {
        title = "Novo benificiario";
        tooltip = "Novo benificiario";
        edit = false;
        uuid ="false";
      });

    }else{
      //bairro = user.bairro;
      setState(() {
        print("Bairro : ${user.bairro}");
        print('contact ${this.contacto}');
        uuid = user.uuid;
        title = "Editar benificiario";
        tooltip = "Actualizar benificiario";
        edit = true;
        bairro = user.bairro;
        contacto =  (user.contacto == "" || user.contacto ==null)?'':user.contacto;
        eChefeDaFamilia = user.eChefeDaFamilia;
        estudou = user.estudou;
        eAlfabetizado = user.eAlfabetizado;
        completouEnsinoPrimario = user.completouEnsinoPrimario;
        completouEnsinoSecundario = user.completouEnsinoSecundario;
        fezCursoSuperior = user.fezCursoSuperior;
        naoSabe = user.naoSabe;
        chefeDeFamiliaTemSalario =user.chefeDeFamiliaTemSalario;
        quantoGastaParaSustentarFamiliaPorMes = user.quantoGastaParaSustentarFamiliaPorMes;
        //4
        quantasFamiliasResidemNacasa = user.quantasFamiliasResidemNacasa;
        quantosAdultos = user.quantosAdultos;
        quantosAdultosMaioresDe18anos = user.quantosAdultosMaioresDe18anos;
        quantosAdultosMaioresDe18anosHomens =user.quantosAdultosMaioresDe18anosHomens;
        quantosAdultosMaioresDe18anosHomensTrabalham = user.quantosAdultosMaioresDe18anosHomensTrabalham;
        //
        quantosAdultosMaioresDe18anosHomensAlfabetizados = user.quantosAdultosMaioresDe18anosHomensAlfabetizados;
        quantosAdultosM18Mulheres = user.quantosAdultosM18Mulheres;
        quantosAdultosM18MulheresTrabalham =  user.quantosAdultosM18MulheresTrabalham;
        quantosAdultosM18MulheresAlfabetizados = user.quantosAdultosMaioresDe18anosHomensAlfabetizados;
        relatosDeDeficiencia = user.relatosDeDeficiencia;
        //fim dos outros 5
        totalcriancasAte1anoemeio = user.totalcriancasAte1anoemeio;
        totalcriancasAte1anoemeioMeninos= user.totalcriancasAte1anoemeioMeninos;
        totalcriancasAte1anoemeioMeninas = user.totalcriancasAte1anoemeioMeninas;
        algumasCriancasAte1anoemeioNaodesenvolvem = user.algumasCriancasAte1anoemeioNaodesenvolvem;
        algumasCriancasAte1anoemeioNaodesenvolvemQuantas = user.algumasCriancasAte1anoemeioNaodesenvolvemQuantas;
        algumaCriancasAte1anoemeioQueTipodeProblema =  user.algumaCriancasAte1anoemeioQueTipodeProblema;
        algumaCriancasAte1anoemeioTemProblemaMotor = user.algumaCriancasAte1anoemeioTemProblemaMotor ;
        algumaCriancasAte1anoemeioTemProblemaMotorQuantas = user.algumaCriancasAte1anoemeioTemProblemaMotorQuantas;
        algumaCriancasAte1anoemeioTemMalFormacao = user.algumaCriancasAte1anoemeioTemMalFormacao;
        algumaCriancasAte1anoemeioTemMalFormacaoQuantas = user.algumaCriancasAte1anoemeioTemMalFormacaoQuantas;

        algumaCriancasAte1anoemeioNaoReagemAIncentivo= user.algumaCriancasAte1anoemeioNaoReagemAIncentivo ;
        algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas= user.algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas;
        algumaCriancasAte1anoemeioTemReacaoAgressiva = user.algumaCriancasAte1anoemeioTemReacaoAgressiva ;
        algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas = user.algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas;
        totalcriancasAte1anoemeio = user.totalcriancasAte1anoemeio;
        totalcriancasAte1anoemeioA3anosMeninos= user.totalcriancasAte1anoemeioA3anosMeninos;
        totalcriancasAte1anoemeioA3anosMeninas = user.totalcriancasAte1anoemeioA3anosMeninas;
        algumasCriancasAte1anoemeioA3anosNaodesenvolvem = user.algumasCriancasAte1anoemeioA3anosNaodesenvolvem;
        algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas  = user.algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas;
        algumaCriancasAte1anoemeioA3anosQueTipodeProblema = user.algumaCriancasAte1anoemeioA3anosQueTipodeProblema;
        algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas = user.algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas;
        algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas  = user.algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas;
        algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo =  user.algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo;
        algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas  = user.algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas;
        algumasCriancasAte1anoemeioA3anosAlgunsAgridem = user.algumasCriancasAte1anoemeioA3anosAlgunsAgridem;
        algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas  = user.algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas;
        algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal =   user.algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal;
        algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas  = user.algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas;
        algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao = user.algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao;
        algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas  = user.algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas;
        algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas = user.algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas;
        algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas  = user.algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas;
        algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente =  user.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente;
        algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas  = user.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas;
        algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos =  user.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos;
        algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas = user.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas;
        algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown =  user.algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown ;
        algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas  = user.algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas;


        totalcriancasde3A6anos =  user.totalcriancasde3A6anos;
        totalcriancasde3A6anosMeninos  =  user.totalcriancasde3A6anosMeninos;
        totalcriancasde3A6anosMeninas =  user.totalcriancasde3A6anosMeninas;
        algumasCriancasde3A6anosNaodesenvolvem  = user.algumasCriancasde3A6anosNaodesenvolvem;
        algumasCriancasde3A6anosNaodesenvolvemQuantas  = user.algumasCriancasde3A6anosNaodesenvolvemQuantas;
        algumaCriancasde3A6anosQueTipodeProblema = user.algumaCriancasde3A6anosQueTipodeProblema;
        algumasCriancasde3A6anosNaoInteragemComPessoas  = user.algumasCriancasde3A6anosNaoInteragemComPessoas;
        algumasCriancasde3A6anosNaoInteragemComPessoasQuantas  = user.algumasCriancasde3A6anosNaoInteragemComPessoasQuantas;
        algumasCriancasde3A6anosTemComportamentoAgressivo = user.algumasCriancasde3A6anosTemComportamentoAgressivo;
        algumasCriancasde3A6anosTemComportamentoAgressivoQuantas  = user.algumasCriancasde3A6anosTemComportamentoAgressivoQuantas;
        algumasCriancasde3A6anosAlgunsAgridem  = user.algumasCriancasde3A6anosAlgunsAgridem;
        algumasCriancasde3A6anosAlgunsAgridemQuantas  = user.algumasCriancasde3A6anosAlgunsAgridemQuantas;
        algumasCriancasde3A6anosAlgumasAgitadasQueONormal = user.algumasCriancasde3A6anosAlgumasAgitadasQueONormal;
        algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas =  user.algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas;
        algumasCriancasde3A6anosAlgumasTemMalFormacao = user.algumasCriancasde3A6anosAlgumasTemMalFormacao;
        algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas = user.algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas;
        algumasCriancasde3A6anosAlgumasNaoAndamSozinhas = user.algumasCriancasde3A6anosAlgumasNaoAndamSozinhas;
        algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas  = user.algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas;
        algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente  = user.algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente;
        algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas  = user.algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas;
        algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos = user.algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos;
        algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas = user.algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas;
        algumasCriancasde3A6anosAlgumasTemSindromeDeDown =  user.algumasCriancasde3A6anosAlgumasTemSindromeDeDown;
        algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas  = user.algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas;
        algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas = user.algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas;
        algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas = user.algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas;
        algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas  =  user.algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas;
        algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas  = user.algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas;
        algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas  = user.algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas;
        algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas  = user.algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas;
        algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas  =  user.algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas;
        algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas  = user.algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas;
        quantasCriancasde3A6anosEstaoNaEscolinha  = user.quantasCriancasde3A6anosEstaoNaEscolinha;
        criancasDe3A6anosNaoEstaoNaEscolinhaPorque  = user.criancasDe3A6anosNaoEstaoNaEscolinhaPorque;
        quantasCriancasde3A6anosTrabalham  = quantasCriancasde3A6anosTrabalham;
        criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos  = user.criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos;
        totalcriancasde7A9anos  = user.totalcriancasde7A9anos;
        totalcriancasde7A9anosMeninos  = user.totalcriancasde7A9anosMeninos;
        totalcriancasde7A9anosMeninas =  user.totalcriancasde7A9anosMeninas;
        quantasCriancasde7A9anosEstaoNaEscola  = user.quantasCriancasde7A9anosEstaoNaEscola;
        quantasCriancasde7A9anosTrabalham = user.quantasCriancasde7A9anosTrabalham;
        nosUltimos5AnosQuantasGravidezesTiveNaFamilia  = user.nosUltimos5AnosQuantasGravidezesTiveNaFamilia;
        partosOcoridosNoHospital  = user.partosOcoridosNoHospital;
        partosOcoridosEmCasa  = user.partosOcoridosEmCasa;
        partosOcoridosEmOutroLocal  = user.partosOcoridosEmOutroLocal;

        outroLocalDoPartoQualLocal = user.outroLocalDoPartoQualLocal;
        quantasCriancasNasceramVivas = user.quantasCriancasNasceramVivas;
        quantasContinuamVivas = user.quantasContinuamVivas;

        dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram = user.dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram;
        descreverOProblema  = user.descreverOProblema;

        aFamiliaProcurouAjudaParaSuperarEsseProblema  =  user.aFamiliaProcurouAjudaParaSuperarEsseProblema;
        seSimQueTipoDeAjudaEOnde  = user.seSimQueTipoDeAjudaEOnde;
        osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia  = user.osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia;
        dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida  = dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida;
        sabeDizerOMotivoDosObitos  =  user.sabeDizerOMotivoDosObitos;
        seSimQualFoiPrincipalRazaodoObito  = user.seSimQualFoiPrincipalRazaodoObito;

        voceTemMedoDeTerUmFilhoOuNetoComProblemas  =  user.voceTemMedoDeTerUmFilhoOuNetoComProblemas;
        expliquePorQue  = user.expliquePorQue;

        porqueVoceAchaQueUmaCriancaPodeNascerComProblema  = user.porqueVoceAchaQueUmaCriancaPodeNascerComProblema;
        achaQueUmaCriancaQueNasceComProblemaPodeSobreviver  =  user.achaQueUmaCriancaQueNasceComProblemaPodeSobreviver;
        expliquePorque  = user.expliquePorque;
        oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema = user.oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema;

        quemPodeAjudarQuandoUmaCriancaNasceComProblema = user.quemPodeAjudarQuandoUmaCriancaNasceComProblema;
        emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao =  user.emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao;
        emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene =  user.emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene;
        emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos = user.emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos;

        aPessoaEntrevistadaPareceuSinceraNasSuasRespostas = user.aPessoaEntrevistadaPareceuSinceraNasSuasRespostas;
        algumasPerguntasIncomodaramAPessoaEntrevistada =  user.algumasPerguntasIncomodaramAPessoaEntrevistada;
        seSimQuais3PrincipaisPerguntas = user.seSimQuais3PrincipaisPerguntas;
        todasAsPerguntasIncomodaram = user.todasAsPerguntasIncomodaram;
        aResidenciaEraDe = user.aResidenciaEraDe;
        comoAvaliaHigieneDaCasa = user.comoAvaliaHigieneDaCasa;
        oQueEraMal = user.oQueEraMal;

        tinhaCriancasNaCasaDuranteInquerito =  user.tinhaCriancasNaCasaDuranteInquerito;
        tinhaCriancasNaCasaDuranteInqueritoQuantas = tinhaCriancasNaCasaDuranteInqueritoQuantas;
        tentouIncentivar = user.tentouIncentivar;
        tentouIncentivarQuantas = tentouIncentivarQuantas;

        observouCriancasDeAte1Ano  = user.observouCriancasDeAte1Ano;
        observouCriancasDeAte1AnoQuantos = user.observouCriancasDeAte1AnoQuantos;
        algumasApresentavamProblemas = user.algumasApresentavamProblemas;
        algumasApresentavamProblemasQuantos = user.algumasApresentavamProblemasQuantos;
        descreveOProblemaDessasCriancas = user.descreveOProblemaDessasCriancas;
        //remate

        observouCriancasDe1AnoEmeioAte3Anos  = user.observouCriancasDe1AnoEmeioAte3Anos;
        observouCriancasDe1AnoEmeioAte3AnosQuantas = user.observouCriancasDe1AnoEmeioAte3AnosQuantas;
        algumasCriancasDe1anoEmeioObservadasApresentavamProblemas = user.algumasCriancasDe1anoEmeioObservadasApresentavamProblemas;
        algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas = user.algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas;
        algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve = user.algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve;

        observouCriancasDe3AnosAte6Anos  = user.observouCriancasDe3AnosAte6Anos;
        observouCriancasDe3AnosAte6AnosQuantas = user.observouCriancasDe3AnosAte6AnosQuantas;
        observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas = user.observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas;
        observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas = user.observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas;
        observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve = user.observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve;

        observouSituacoesQueLheChamaramAtencao = user.observouSituacoesQueLheChamaramAtencao;
        emRelacaoAPessoaEntrevistada = user.emRelacaoAPessoaEntrevistada;
        emRelacaoAPessoaEntrevistadaDescreve = user.emRelacaoAPessoaEntrevistadaDescreve;
        emRelacaoAOutrosAdultosPresente = user.emRelacaoAOutrosAdultosPresente;
        emRelacaoAOutrosAdultosPresenteDescreve = user.emRelacaoAOutrosAdultosPresenteDescreve;
        emRelacaoAsCriancasPresentes = user.emRelacaoAsCriancasPresentes;
        emRelacaoAsCriancasPresentesDescreve = user.emRelacaoAsCriancasPresentesDescreve;
        emRelacaoAVizinhanca = user.emRelacaoAVizinhanca;
        emRelacaoAVizinhancaDescreve = user.emRelacaoAVizinhancaDescreve;
        outras = user.outras;
        outrasDescreve =user.outrasDescreve;














      });
    }
    //opcoes de dropdown


    return Scaffold(

        appBar: AppBar(
          backgroundColor: Colors.deepPurple,
          title: Text(title,
            //style: TextStyle(
            //color: Colors.black54
            //),
          ),
          actions: [
            Padding(
                padding: EdgeInsets.all(10),
                child:FloatingActionButton.extended(
                  foregroundColor: Colors.deepPurple,
                  label:Text('Salvar'),
                  tooltip: tooltip,
                  icon: Icon(edit?Icons.update:Icons.sd_storage),
                  backgroundColor: Color(0xFFCFA8F0),
                  onPressed: (){
                    final isValid = _form.currentState.validate();
                    if(isValid){
                      _form.currentState.save();
                      User ux = new User(
                        //inicio da dase 1
                        uuid: (uuid == "false")? Uuid().v4() :uuid,
                        nomeDoPesquisador: nomeDoPesquisador,
                        numeroDeInquerito: numeroDeInquerito,
                        data: data,
                        municipio: municipio,
                        bairro: (uuid == "false")? this.bairro :this.bairro!= null?this.bairro:user.bairro,
                        pontoDeReferenciaDaCasa: pontoDeReferenciaDaCasa,


                        createdAt: (uuid == "false")? DateTime.now():user.createdAt!=null?user.createdAt:DateTime.now(),
                        updatedAt: DateTime.now(),
                        //fim da fase 1
                        //inicio da fase 2
                        nome: nome,
                        idade: idade,
                        contacto: this.contacto,
                        eChefeDaFamilia: (uuid == "false")? this.eChefeDaFamilia :this.eChefeDaFamilia!= null?this.eChefeDaFamilia:user.eChefeDaFamilia,
                        caoNaoVinculoDeParantesco: caoNaoVinculoDeParantesco,
                        idadeDoChefeDaFamilia: idadeDoChefeDaFamilia,
                        estudou: (uuid == "false")? this.estudou :this.estudou!= null?this.estudou:user.estudou,
                        eAlfabetizado: (uuid == "false")? this.eAlfabetizado :this.eAlfabetizado!= null?this.eAlfabetizado:user.eAlfabetizado,
                        completouEnsinoPrimario: (uuid == "false")? this.completouEnsinoPrimario :this.completouEnsinoPrimario!= null?this.completouEnsinoPrimario:user.completouEnsinoPrimario,
                        completouEnsinoSecundario: (uuid == "false")? this.completouEnsinoSecundario :this.completouEnsinoSecundario!= null?this.completouEnsinoSecundario:user.completouEnsinoSecundario,
                        fezCursoSuperior: (uuid == "false")? this.fezCursoSuperior :this.fezCursoSuperior!= null?this.fezCursoSuperior:user.fezCursoSuperior,
                        naoSabe: (uuid == "false")? this.naoSabe :this.naoSabe!= null?this.naoSabe:user.naoSabe,
                        chefeDeFamiliaTemSalario: (uuid == "false")? this.chefeDeFamiliaTemSalario :this.chefeDeFamiliaTemSalario!= null?this.chefeDeFamiliaTemSalario:user.chefeDeFamiliaTemSalario,
                        quantoGastaParaSustentarFamiliaPorMes: quantoGastaParaSustentarFamiliaPorMes,
                        //4
                        quantasFamiliasResidemNacasa: quantasFamiliasResidemNacasa,
                        quantosAdultos: quantosAdultos,
                        quantosAdultosMaioresDe18anos: quantosAdultosMaioresDe18anos,
                        quantosAdultosMaioresDe18anosHomens: quantosAdultosMaioresDe18anosHomens,
                        quantosAdultosMaioresDe18anosHomensTrabalham: quantosAdultosMaioresDe18anosHomensTrabalham,
                        //outros 5
                        quantosAdultosMaioresDe18anosHomensAlfabetizados: quantosAdultosMaioresDe18anosHomensAlfabetizados,
                        quantosAdultosM18Mulheres: quantosAdultosM18Mulheres,
                        quantosAdultosM18MulheresTrabalham: quantosAdultosM18MulheresTrabalham,
                        quantosAdultosM18MulheresAlfabetizados: quantosAdultosMaioresDe18anosHomensAlfabetizados,
                        //
                        relatosDeDeficiencia: (uuid == "false")? this.relatosDeDeficiencia :this.relatosDeDeficiencia != null?this.relatosDeDeficiencia:user.relatosDeDeficiencia,
                        totalcriancasAte1anoemeio: totalcriancasAte1anoemeio,
                        totalcriancasAte1anoemeioMeninos: totalcriancasAte1anoemeioMeninos,
                        totalcriancasAte1anoemeioMeninas: totalcriancasAte1anoemeioMeninas,
                        algumasCriancasAte1anoemeioNaodesenvolvem: (uuid == "false")? this.algumasCriancasAte1anoemeioNaodesenvolvem :this.algumasCriancasAte1anoemeioNaodesenvolvem!= null?this.algumasCriancasAte1anoemeioNaodesenvolvem:user.algumasCriancasAte1anoemeioNaodesenvolvem,
                        algumasCriancasAte1anoemeioNaodesenvolvemQuantas :algumasCriancasAte1anoemeioNaodesenvolvemQuantas,
                        algumaCriancasAte1anoemeioQueTipodeProblema: algumaCriancasAte1anoemeioQueTipodeProblema,
                        //nova fase

                        algumaCriancasAte1anoemeioTemProblemaMotor: (uuid == "false")? this.algumaCriancasAte1anoemeioTemProblemaMotor :this.algumaCriancasAte1anoemeioTemProblemaMotor!= null?this.algumaCriancasAte1anoemeioTemProblemaMotor:user.algumaCriancasAte1anoemeioTemProblemaMotor,
                        algumaCriancasAte1anoemeioTemProblemaMotorQuantas :algumaCriancasAte1anoemeioTemProblemaMotorQuantas,
                        algumaCriancasAte1anoemeioTemMalFormacao: (uuid == "false")? this.algumaCriancasAte1anoemeioTemMalFormacao :this.algumaCriancasAte1anoemeioTemMalFormacao!= null?this.algumaCriancasAte1anoemeioTemMalFormacao:user.algumaCriancasAte1anoemeioTemMalFormacao,
                        algumaCriancasAte1anoemeioTemMalFormacaoQuantas:algumaCriancasAte1anoemeioTemMalFormacaoQuantas,


                        algumaCriancasAte1anoemeioNaoReagemAIncentivo: (uuid == "false")? this.algumaCriancasAte1anoemeioNaoReagemAIncentivo :this.algumaCriancasAte1anoemeioNaoReagemAIncentivo!= null?this.algumaCriancasAte1anoemeioNaoReagemAIncentivo:user.algumaCriancasAte1anoemeioNaoReagemAIncentivo,
                        algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas:algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas,

                        algumaCriancasAte1anoemeioTemReacaoAgressiva: (uuid == "false")? user.algumaCriancasAte1anoemeioTemReacaoAgressiva :this.algumaCriancasAte1anoemeioTemReacaoAgressiva!= null?this.algumaCriancasAte1anoemeioTemReacaoAgressiva:user.algumaCriancasAte1anoemeioTemReacaoAgressiva,
                        algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas:algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas,

                        totalcriancasAte1anoemeioA3anos: totalcriancasAte1anoemeioA3anos,
                        totalcriancasAte1anoemeioA3anosMeninos: totalcriancasAte1anoemeioA3anosMeninos,
                        totalcriancasAte1anoemeioA3anosMeninas: totalcriancasAte1anoemeioA3anosMeninas,
                        algumasCriancasAte1anoemeioA3anosNaodesenvolvem: (uuid == "false")? this.algumasCriancasAte1anoemeioA3anosNaodesenvolvem :this.algumasCriancasAte1anoemeioA3anosNaodesenvolvem!= null?this.algumasCriancasAte1anoemeioA3anosNaodesenvolvem:user.algumasCriancasAte1anoemeioA3anosNaodesenvolvem,
                        algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas :algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas,
                        algumaCriancasAte1anoemeioA3anosQueTipodeProblema: algumaCriancasAte1anoemeioA3anosQueTipodeProblema,
                        algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas: (uuid == "false")? this.algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas :this.algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas!= null?this.algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas:user.algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas,
                        algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas :algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas,
                        algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo: (uuid == "false")? this.algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo :this.algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo!= null?this.algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo:user.algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo,
                        algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas :algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas,
                        algumasCriancasAte1anoemeioA3anosAlgunsAgridem: (uuid == "false")? this.algumasCriancasAte1anoemeioA3anosAlgunsAgridem :this.algumasCriancasAte1anoemeioA3anosAlgunsAgridem!= null?this.algumasCriancasAte1anoemeioA3anosAlgunsAgridem:user.algumasCriancasAte1anoemeioA3anosAlgunsAgridem,
                        algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas :algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas,
                        algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal: (uuid == "false")? this.algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal :this.algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal!= null?this.algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal:user.algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal,
                        algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas :algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas,
                        algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao: (uuid == "false")? this.algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao :this.algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao!= null?this.algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao:user.algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao,
                        algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas :algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas,
                        algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas: (uuid == "false")? this.algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas :this.algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas!= null?this.algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas:user.algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas,
                        algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas :algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas,
                        algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente: (uuid == "false")? this.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente :this.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente!= null?this.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente:user.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente,
                        algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas :algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas,
                        algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos: (uuid == "false")? this.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos :this.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos!= null?this.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos:user.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos,
                        algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas :algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas,
                        algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown: (uuid == "false")? this.algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown :this.algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown!= null?this.algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown:user.algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown,
                        algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas :algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas,
                        //4.6 outro form
                        totalcriancasde3A6anos: totalcriancasde3A6anos,
                        totalcriancasde3A6anosMeninos : totalcriancasde3A6anosMeninos,
                        totalcriancasde3A6anosMeninas : totalcriancasde3A6anosMeninas,
                        algumasCriancasde3A6anosNaodesenvolvem : (uuid == "false")? this.algumasCriancasde3A6anosNaodesenvolvem :this.algumasCriancasde3A6anosNaodesenvolvem!= null?this.algumasCriancasde3A6anosNaodesenvolvem:user.algumasCriancasde3A6anosNaodesenvolvem,
                        algumasCriancasde3A6anosNaodesenvolvemQuantas :algumasCriancasde3A6anosNaodesenvolvemQuantas,
                        algumaCriancasde3A6anosQueTipodeProblema: algumaCriancasde3A6anosQueTipodeProblema,
                        algumasCriancasde3A6anosNaoInteragemComPessoas : (uuid == "false")? this.algumasCriancasde3A6anosNaoInteragemComPessoas :this.algumasCriancasde3A6anosNaoInteragemComPessoas!= null?this.algumasCriancasde3A6anosNaoInteragemComPessoas:user.algumasCriancasde3A6anosNaoInteragemComPessoas,
                        algumasCriancasde3A6anosNaoInteragemComPessoasQuantas :algumasCriancasde3A6anosNaoInteragemComPessoasQuantas,
                        algumasCriancasde3A6anosTemComportamentoAgressivo: (uuid == "false")? this.algumasCriancasde3A6anosTemComportamentoAgressivo :this.algumasCriancasde3A6anosTemComportamentoAgressivo!= null?this.algumasCriancasde3A6anosTemComportamentoAgressivo:user.algumasCriancasde3A6anosTemComportamentoAgressivo,
                        algumasCriancasde3A6anosTemComportamentoAgressivoQuantas :algumasCriancasde3A6anosTemComportamentoAgressivoQuantas,
                        algumasCriancasde3A6anosAlgunsAgridem : (uuid == "false")? this.algumasCriancasde3A6anosAlgunsAgridem :this.algumasCriancasde3A6anosAlgunsAgridem!= null?this.algumasCriancasde3A6anosAlgunsAgridem:user.algumasCriancasde3A6anosAlgunsAgridem,
                        algumasCriancasde3A6anosAlgunsAgridemQuantas :algumasCriancasde3A6anosAlgunsAgridemQuantas,
                        algumasCriancasde3A6anosAlgumasAgitadasQueONormal: (uuid == "false")? this.algumasCriancasde3A6anosAlgumasAgitadasQueONormal :this.algumasCriancasde3A6anosAlgumasAgitadasQueONormal!= null?this.algumasCriancasde3A6anosAlgumasAgitadasQueONormal:user.algumasCriancasde3A6anosAlgumasAgitadasQueONormal,
                        algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas :algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas,
                        algumasCriancasde3A6anosAlgumasTemMalFormacao: (uuid == "false")? this.algumasCriancasde3A6anosAlgumasTemMalFormacao :this.algumasCriancasde3A6anosAlgumasTemMalFormacao!= null?this.algumasCriancasde3A6anosAlgumasTemMalFormacao:user.algumasCriancasde3A6anosAlgumasTemMalFormacao,
                        algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas :algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas,
                        algumasCriancasde3A6anosAlgumasNaoAndamSozinhas: (uuid == "false")? this.algumasCriancasde3A6anosAlgumasNaoAndamSozinhas :this.algumasCriancasde3A6anosAlgumasNaoAndamSozinhas!= null?this.algumasCriancasde3A6anosAlgumasNaoAndamSozinhas:user.algumasCriancasde3A6anosAlgumasNaoAndamSozinhas,
                        algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas :algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas,
                        algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente : (uuid == "false")? this.algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente :this.algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente!= null?this.algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente:user.algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente,
                        algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas :algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas,
                        algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos: (uuid == "false")? this.algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos :this.algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos!= null?this.algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos:user.algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos,
                        algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas :algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas,
                        algumasCriancasde3A6anosAlgumasTemSindromeDeDown : (uuid == "false")? this.algumasCriancasde3A6anosAlgumasTemSindromeDeDown :this.algumasCriancasde3A6anosAlgumasTemSindromeDeDown!= null?this.algumasCriancasde3A6anosAlgumasTemSindromeDeDown:user.algumasCriancasde3A6anosAlgumasTemSindromeDeDown,
                        algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas :algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas,

                        algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas : (uuid == "false")? this.algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas :this.algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas!= null?this.algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas:user.algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas,
                        algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas :algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas,

                        algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas : (uuid == "false")? this.algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas :this.algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas!= null?this.algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas:user.algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas,
                        algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas :algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas,
                        algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas : (uuid == "false")? this.algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas :this.algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas!= null?this.algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas:user.algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas,
                        algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas :algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas,
                        algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas : (uuid == "false")? this.algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas :this.algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas!= null?this.algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas:user.algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas,
                        algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas :algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas,

                        quantasCriancasde3A6anosEstaoNaEscolinha :quantasCriancasde3A6anosEstaoNaEscolinha,
                        criancasDe3A6anosNaoEstaoNaEscolinhaPorque : (uuid == "false")? this.criancasDe3A6anosNaoEstaoNaEscolinhaPorque :this.criancasDe3A6anosNaoEstaoNaEscolinhaPorque!= null?this.criancasDe3A6anosNaoEstaoNaEscolinhaPorque:user.criancasDe3A6anosNaoEstaoNaEscolinhaPorque,
                        quantasCriancasde3A6anosTrabalham :quantasCriancasde3A6anosTrabalham,
                        criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos :criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos,

                        nosUltimos5AnosQuantasGravidezesTiveNaFamilia :nosUltimos5AnosQuantasGravidezesTiveNaFamilia,
                        partosOcoridosNoHospital :partosOcoridosNoHospital,
                        partosOcoridosEmCasa :partosOcoridosEmCasa,
                        partosOcoridosEmOutroLocal :partosOcoridosEmOutroLocal,

                        outroLocalDoPartoQualLocal:outroLocalDoPartoQualLocal,
                        quantasCriancasNasceramVivas:quantasCriancasNasceramVivas,
                        quantasContinuamVivas:quantasContinuamVivas,

                        totalcriancasde7A9anos :totalcriancasde7A9anos,
                        totalcriancasde7A9anosMeninos :totalcriancasde7A9anosMeninos,
                        totalcriancasde7A9anosMeninas :totalcriancasde7A9anosMeninas,
                        quantasCriancasde7A9anosEstaoNaEscola :quantasCriancasde7A9anosEstaoNaEscola,
                        quantasCriancasde7A9anosTrabalham :quantasCriancasde7A9anosTrabalham,

                        dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram : (uuid == "false")? this.dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram :this.dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram!= null?this.dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram:user.dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram,
                        descreverOProblema :descreverOProblema,

                        aFamiliaProcurouAjudaParaSuperarEsseProblema : (uuid == "false")? this.aFamiliaProcurouAjudaParaSuperarEsseProblema :this.aFamiliaProcurouAjudaParaSuperarEsseProblema!= null?this.aFamiliaProcurouAjudaParaSuperarEsseProblema:user.aFamiliaProcurouAjudaParaSuperarEsseProblema,
                        seSimQueTipoDeAjudaEOnde :seSimQueTipoDeAjudaEOnde,

                        osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia : (uuid == "false")? this.osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia :this.osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia!= null?this.osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia:user.osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia,
                        dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida :dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida,
                        sabeDizerOMotivoDosObitos : (uuid == "false")? this.sabeDizerOMotivoDosObitos :this.sabeDizerOMotivoDosObitos!= null?this.sabeDizerOMotivoDosObitos:user.sabeDizerOMotivoDosObitos,
                        seSimQualFoiPrincipalRazaodoObito : (uuid == "false")? this.seSimQualFoiPrincipalRazaodoObito :this.seSimQualFoiPrincipalRazaodoObito!= null?this.seSimQualFoiPrincipalRazaodoObito:user.seSimQualFoiPrincipalRazaodoObito,

                        voceTemMedoDeTerUmFilhoOuNetoComProblemas : (uuid == "false")? this.voceTemMedoDeTerUmFilhoOuNetoComProblemas :this.voceTemMedoDeTerUmFilhoOuNetoComProblemas!= null?this.voceTemMedoDeTerUmFilhoOuNetoComProblemas:user.voceTemMedoDeTerUmFilhoOuNetoComProblemas,
                        expliquePorQue :expliquePorQue,

                        porqueVoceAchaQueUmaCriancaPodeNascerComProblema : (uuid == "false")? this.porqueVoceAchaQueUmaCriancaPodeNascerComProblema :this.porqueVoceAchaQueUmaCriancaPodeNascerComProblema!= null?this.porqueVoceAchaQueUmaCriancaPodeNascerComProblema:user.porqueVoceAchaQueUmaCriancaPodeNascerComProblema,
                        oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema : (uuid == "false")? this.oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema :this.oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema!= null?this.oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema:user.oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema,
                        achaQueUmaCriancaQueNasceComProblemaPodeSobreviver : (uuid == "false")? this.achaQueUmaCriancaQueNasceComProblemaPodeSobreviver :this.achaQueUmaCriancaQueNasceComProblemaPodeSobreviver!= null?this.achaQueUmaCriancaQueNasceComProblemaPodeSobreviver:user.achaQueUmaCriancaQueNasceComProblemaPodeSobreviver,
                        expliquePorque :expliquePorque,
                        quemPodeAjudarQuandoUmaCriancaNasceComProblema: (uuid == "false")? this.quemPodeAjudarQuandoUmaCriancaNasceComProblema :this.quemPodeAjudarQuandoUmaCriancaNasceComProblema!= null?this.quemPodeAjudarQuandoUmaCriancaNasceComProblema:user.quemPodeAjudarQuandoUmaCriancaNasceComProblema,
                        emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao: (uuid == "false")? this.emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao :this.emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao!= null?this.emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao:user.emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao,
                        emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene: (uuid == "false")? this.emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene :this.emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene!= null?this.emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene:user.emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene,
                        emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos: (uuid == "false")? this.emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos :this.emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos!= null?this.emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos:user.emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos,

                        aPessoaEntrevistadaPareceuSinceraNasSuasRespostas: (uuid == "false")? this.aPessoaEntrevistadaPareceuSinceraNasSuasRespostas :this.aPessoaEntrevistadaPareceuSinceraNasSuasRespostas!= null?this.aPessoaEntrevistadaPareceuSinceraNasSuasRespostas:user.aPessoaEntrevistadaPareceuSinceraNasSuasRespostas,

                        algumasPerguntasIncomodaramAPessoaEntrevistada: (uuid == "false")? this.algumasPerguntasIncomodaramAPessoaEntrevistada :this.algumasPerguntasIncomodaramAPessoaEntrevistada!= null?this.algumasPerguntasIncomodaramAPessoaEntrevistada:user.algumasPerguntasIncomodaramAPessoaEntrevistada,
                        seSimQuais3PrincipaisPerguntas: seSimQuais3PrincipaisPerguntas,
                        todasAsPerguntasIncomodaram: (uuid == "false")? this.todasAsPerguntasIncomodaram :this.todasAsPerguntasIncomodaram!= null?this.todasAsPerguntasIncomodaram:user.todasAsPerguntasIncomodaram,
                        aResidenciaEraDe: (uuid == "false")? this.aResidenciaEraDe :this.aResidenciaEraDe!= null?this.aResidenciaEraDe:user.aResidenciaEraDe,
                        comoAvaliaHigieneDaCasa: (uuid == "false")? this.comoAvaliaHigieneDaCasa :this.comoAvaliaHigieneDaCasa!= null?this.comoAvaliaHigieneDaCasa:user.comoAvaliaHigieneDaCasa,
                        oQueEraMal:oQueEraMal,

                        tinhaCriancasNaCasaDuranteInquerito: (uuid == "false")? this.tinhaCriancasNaCasaDuranteInquerito :this.tinhaCriancasNaCasaDuranteInquerito!= null?this.tinhaCriancasNaCasaDuranteInquerito:user.tinhaCriancasNaCasaDuranteInquerito,
                        tinhaCriancasNaCasaDuranteInqueritoQuantas:tinhaCriancasNaCasaDuranteInqueritoQuantas,
                        tentouIncentivar: (uuid == "false")? this.tentouIncentivar :this.tentouIncentivar!= null?this.tentouIncentivar:user.tentouIncentivar,
                        tentouIncentivarQuantas:tentouIncentivarQuantas,
                        seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos:seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos,
                        //

                        observouCriancasDeAte1Ano: (uuid == "false")? this.observouCriancasDeAte1Ano :this.observouCriancasDeAte1Ano!= null?this.observouCriancasDeAte1Ano:user.observouCriancasDeAte1Ano,
                        observouCriancasDeAte1AnoQuantos:observouCriancasDeAte1AnoQuantos,
                        algumasApresentavamProblemas: (uuid == "false")? this.algumasApresentavamProblemas :this.algumasApresentavamProblemas!= null?this.algumasApresentavamProblemas:user.algumasApresentavamProblemas,
                        algumasApresentavamProblemasQuantos:algumasApresentavamProblemasQuantos,
                        descreveOProblemaDessasCriancas:descreveOProblemaDessasCriancas,
                        //remate

                        observouCriancasDe1AnoEmeioAte3Anos: (uuid == "false")? this.observouCriancasDe1AnoEmeioAte3Anos :this.observouCriancasDe1AnoEmeioAte3Anos!= null?this.observouCriancasDe1AnoEmeioAte3Anos:user.observouCriancasDe1AnoEmeioAte3Anos,
                        observouCriancasDe1AnoEmeioAte3AnosQuantas:observouCriancasDe1AnoEmeioAte3AnosQuantas,
                        algumasCriancasDe1anoEmeioObservadasApresentavamProblemas: (uuid == "false")? this.algumasCriancasDe1anoEmeioObservadasApresentavamProblemas :this.algumasCriancasDe1anoEmeioObservadasApresentavamProblemas!= null?this.algumasCriancasDe1anoEmeioObservadasApresentavamProblemas:user.algumasCriancasDe1anoEmeioObservadasApresentavamProblemas,
                        algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas:algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas,
                        algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve:algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve,

                        observouCriancasDe3AnosAte6Anos: (uuid == "false")? this.observouCriancasDe3AnosAte6Anos :this.observouCriancasDe3AnosAte6Anos!= null?this.observouCriancasDe3AnosAte6Anos:user.observouCriancasDe3AnosAte6Anos,
                        observouCriancasDe3AnosAte6AnosQuantas:observouCriancasDe3AnosAte6AnosQuantas,
                        observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas: (uuid == "false")? this.observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas :this.observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas!= null?this.observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas:user.observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas,
                        observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas:observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas,
                        observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve:observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve,


                        observouSituacoesQueLheChamaramAtencao: (uuid == "false")? this.observouSituacoesQueLheChamaramAtencao :this.observouSituacoesQueLheChamaramAtencao!= null?this.observouSituacoesQueLheChamaramAtencao:user.observouSituacoesQueLheChamaramAtencao,
                        emRelacaoAPessoaEntrevistada: (uuid == "false")? this.emRelacaoAPessoaEntrevistada :this.emRelacaoAPessoaEntrevistada!= null?this.emRelacaoAPessoaEntrevistada:user.emRelacaoAPessoaEntrevistada,
                        emRelacaoAPessoaEntrevistadaDescreve:emRelacaoAPessoaEntrevistadaDescreve,
                        emRelacaoAOutrosAdultosPresente: (uuid == "false")? this.emRelacaoAOutrosAdultosPresente :this.emRelacaoAOutrosAdultosPresente!= null?this.emRelacaoAOutrosAdultosPresente:user.emRelacaoAOutrosAdultosPresente,
                        emRelacaoAOutrosAdultosPresenteDescreve:emRelacaoAOutrosAdultosPresenteDescreve,
                        emRelacaoAsCriancasPresentes: (uuid == "false")? this.emRelacaoAsCriancasPresentes :this.emRelacaoAsCriancasPresentes!= null?this.emRelacaoAsCriancasPresentes:user.emRelacaoAsCriancasPresentes,
                        emRelacaoAsCriancasPresentesDescreve:emRelacaoAsCriancasPresentesDescreve,
                        emRelacaoAVizinhanca: (uuid == "false")? this.emRelacaoAVizinhanca :this.emRelacaoAVizinhanca!= null?this.emRelacaoAVizinhanca:user.emRelacaoAVizinhanca,
                        emRelacaoAVizinhancaDescreve:emRelacaoAVizinhancaDescreve,
                        outras: (uuid == "false")? this.outras :this.outras!= null?this.outras:user.outras,
                        outrasDescreve:outrasDescreve,








                      );
                      if(edit){
                        print("edited: $ux");
                        print(database.updateUser(ux));
                      }else{
                        print(database.addUser(ux));
                      }

                      Navigator.pop(context);
                    }
                  },
                  elevation: 0.0,
                )

            )
          ],

          //iconTheme: IconThemeData(color: Colors.black),
          //backgroundColor: Colors.white,
        ),
        body:ListView(
            children: <Widget>[
              Container(
                height: 150,
                child: Row(
                  children: <Widget>[
                    Expanded(
                      flex: 100,
                      child: Container(
                        // color: Colors.white,
                        height: 100,
                        child: CircleAvatar(
                          backgroundColor: Color((math.Random().nextDouble() * 0xFFFFFF).toInt()).withOpacity(1.0),
                          child: (edit?Text((user.nome==null || user.nome =="")?'U':user.nome[0].toUpperCase(),style: TextStyle(fontSize: 75, color: Colors.white)):Icon(
                              FontAwesomeIcons.userPlus,
                              color: Colors.white,
                              size: 50
                          )),
                        ),
                      ),
                    ),
                  ],
                ),
              ),
              // Divider(),
              Form(
                key: _form,
                child: Column(
                  children: <Widget>[
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0,bottom: 25),
                        child: AppBar(leading: Icon(Icons.location_on),title: Text('1.  Localização',),elevation: 0,)
                    ),

                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.nomeDoPesquisador:'',
                          onSaved: (value){
                            setState(() {
                              nomeDoPesquisador = value;
                            });

                          },
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 20,
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Nome do pesquisador',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Nome do pesquisador',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.numeroDeInquerito:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              numeroDeInquerito  =  value;
                            });

                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Numero de inquerito',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Numero de inquerito',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 20),
                        child: DateTimeField(
                          autofocus: true,
                          initialValue: (user.uuid !=null)? user.data: DateTime.now(),
                          format: format,
                          onShowPicker: (context, currentValue) {
                            //print(currentValue);
                            return showDatePicker(
                                context: context,
                                firstDate: DateTime(1900),
                                initialDate: (user.uuid !=null)? user.data: (currentValue ?? DateTime.now()),
                                lastDate: DateTime(2100));

                          },
                          onChanged:(DateTime time){
                            setState(() {
                              data = time;
                              // data.millisecond = DateTime.now();
                              print(time);
                            });
                          },
                          onSaved: (sete){
                            data = sete;
                          },

                          decoration: InputDecoration(
                            filled: true,
                            hintText:'Data',
                            fillColor: Colors.white,
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Selecione a data',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.municipio:'',
                          onSaved: (value){
                            setState(() {
                              municipio = value;
                            });

                          },
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 20,
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Municipio',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Municipio',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),


                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Bairro',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: bairros
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (bairro == null) ? Colors.transparent : ( bairro == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            bairro = value;
                            //print(bairro);
                          });


                        },
                        onSaved: (String value){
                          this.bairro = value;
                        },
                        isExpanded: false,
                        value: (bairro==null || bairro =="" )? bairros.first:bairro,
                        hint: Text('Bairro'),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: user!=null?user.pontoDeReferenciaDaCasa:'',
                          onSaved: (value){
                            setState(() {
                              pontoDeReferenciaDaCasa = value;
                            });

                          },
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 192,
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Ponto de referência  da casa',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Ponto de referência  da Casa',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 20,bottom: 25),
                        child: AppBar(leading: Icon(Icons.supervised_user_circle),title: Text('2. 	Entrevistado ',),elevation: 0,)
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: user!=null?user.nome:'',
                          onSaved: (value){
                            setState(() {
                              nome = value;
                            });

                          },
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 192,
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Nome do Entrevistado/a',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Nome do Entrevistado/a',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.idade:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              idade  =  value;
                            });

                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Idade do entrevistado',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Idade do entrevistado',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(padding: const EdgeInsets.only(left: 35,right: 35,top: 20),
                      child: InternationalPhoneInput(
                        decoration: InputDecoration.collapsed(
                          hintText:'Ex. 847607095',
                          //fillColor: Colors.white
                        ),
                        onPhoneNumberChange: onPhoneNumberChange,
                        initialPhoneNumber: (user.uuid !='' || user.contacto!=null )?user.contacto:contacto,
                        initialSelection: '258',
                        enabledCountries: ['+258'],
                        showCountryCodes: true,
                        showCountryFlags: true,

                      ),),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'O entrevistado é chefe da família? ',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (eChefeDaFamilia == null) ? Colors.transparent : ( eChefeDaFamilia == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            eChefeDaFamilia = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.eChefeDaFamilia = value;
                        },
                        isExpanded: false,
                        value: (eChefeDaFamilia==null || eChefeDaFamilia =="" )? simNao.first:eChefeDaFamilia,
                        hint: Text('O entrevistado é chefe da família? '),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.caoNaoVinculoDeParantesco:'',
                          onSaved: (value){
                            setState(() {
                              caoNaoVinculoDeParantesco = value;
                            });

                          },
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 20,
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Caso não,qual vínculo tem de parentesco',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Caso não,qual vínculo tem de parentesco',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),

                    //3
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 25,bottom: 25),
                        child: AppBar(leading: Icon(Icons.person_outline),title: Text('3.	Chefe de família',),elevation: 0,)
                    ),
                    //after header
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.idadeDoChefeDaFamilia:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              idadeDoChefeDaFamilia  =  value;
                            });

                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Qual idade o chefe de família tem',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Qual idade o chefe de família tem',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Estudou? ',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (estudou == null) ? Colors.transparent : ( estudou == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            estudou = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.estudou = value;
                        },
                        isExpanded: false,
                        value: (estudou==null || estudou =="" )? simNao.first:estudou,
                        hint: Text('Estudou? '),

                      )
                      ,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Completou ensino Primário? ',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (completouEnsinoPrimario == null) ? Colors.transparent : ( completouEnsinoPrimario == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            completouEnsinoPrimario = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.completouEnsinoPrimario = value;
                        },
                        isExpanded: false,
                        value: (completouEnsinoPrimario==null || completouEnsinoPrimario =="" )? simNao.first:completouEnsinoPrimario,
                        hint: Text('Completou ensino Primário? '),

                      )
                      ,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Completou ensino Primário? ',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (completouEnsinoPrimario == null) ? Colors.transparent : ( completouEnsinoPrimario == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            completouEnsinoPrimario = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.completouEnsinoPrimario = value;
                        },
                        isExpanded: false,
                        value: (completouEnsinoPrimario==null || completouEnsinoPrimario =="" )? simNao.first:completouEnsinoPrimario,
                        hint: Text('Completou ensino Primário? '),

                      )
                      ,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'E alfabetizado? ',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (eAlfabetizado == null) ? Colors.transparent : ( eAlfabetizado == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            eAlfabetizado = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.eAlfabetizado = value;
                        },
                        isExpanded: false,
                        value: (eAlfabetizado==null || eAlfabetizado =="" )? simNao.first:eAlfabetizado,
                        hint: Text('E alfabetizado?'),

                      )
                      ,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Fez curso superior? ',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (fezCursoSuperior == null) ? Colors.transparent : ( fezCursoSuperior == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            fezCursoSuperior = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.fezCursoSuperior = value;
                        },
                        isExpanded: false,
                        value: (fezCursoSuperior==null || fezCursoSuperior =="" )? simNao.first:fezCursoSuperior,
                        hint: Text('Fez curso superior? '),

                      )
                      ,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Não sabe? ',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (naoSabe == null) ? Colors.transparent : ( naoSabe == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            naoSabe = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.naoSabe = value;
                        },
                        isExpanded: false,
                        value: (naoSabe==null || naoSabe =="" )? simNao.first:naoSabe,
                        hint: Text('Não sabe? '),

                      )
                      ,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'O chefe de família tem salário? ',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (chefeDeFamiliaTemSalario == null) ? Colors.transparent : ( chefeDeFamiliaTemSalario == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            chefeDeFamiliaTemSalario = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.chefeDeFamiliaTemSalario = value;
                        },
                        isExpanded: false,
                        value: (chefeDeFamiliaTemSalario==null || chefeDeFamiliaTemSalario =="" )? simNao.first:chefeDeFamiliaTemSalario,
                        hint: Text('O chefe de família tem salário? '),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.quantoGastaParaSustentarFamiliaPorMes:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              quantoGastaParaSustentarFamiliaPorMes  =  value;
                            });

                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Quanto gasta para sustentar a família por mês',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Quanto gasta para sustentar a família por mês',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    //4
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 25,bottom: 25),
                        child: AppBar(leading: Icon(Icons.group_outlined),title: Text('4	Família: (pessoas vivendo na mesma casa)',),elevation: 0,)
                    ),
                    //afert header

                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.quantasFamiliasResidemNacasa:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              quantasFamiliasResidemNacasa  =  value;
                            });

                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Tem quantas famílias que residem na casa',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Tem quantas famílias que residem na casa',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),

                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.quantosAdultosMaioresDe18anos:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              quantosAdultosMaioresDe18anos  =  value;
                            });

                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Quantos são maiores de 18 anos',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Quantos são maiores de 18 anos',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.quantosAdultosMaioresDe18anosHomens:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              quantosAdultosMaioresDe18anosHomens  =  value;
                            });

                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Quantos homens maiores de 18 anos',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Quantos homens maiores de 18 anos',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.quantosAdultosMaioresDe18anosHomensTrabalham:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              quantosAdultosMaioresDe18anosHomensTrabalham  =  value;
                            });

                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Quantos homens maiores de 18 anos trabalham',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Quantos homens maiores de 18 anos trabalham',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    //4  1_5 elements
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0
                        ),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.quantosAdultosMaioresDe18anosHomensAlfabetizados:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              quantosAdultosMaioresDe18anosHomensAlfabetizados  =  value;
                            });

                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Quantos homens maiores de 18 anos são alfabetizados',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Quantos homens maiores de 18 anos são alfabetizados',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.quantosAdultosM18Mulheres:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              quantosAdultosM18Mulheres =  value;
                            });

                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Quantas mulheres maiores de 18 anos',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Quantas mulheres maiores de 18 anos',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.quantosAdultosM18MulheresTrabalham:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              quantosAdultosM18MulheresTrabalham  =  value;
                            });

                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Quantas mulheres maiores de 18 anos trabalham',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Quantas mulheres maiores de 18 anos trabalham',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.quantosAdultosM18MulheresAlfabetizados:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              quantosAdultosMaioresDe18anosHomensAlfabetizados  =  value;
                            });

                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Quantos mulheres maiores de 18 anos alfabetizadas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Quantos mulheres maiores de 18 anos alfabetizadas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Houve relatos nas famílias de pessoas com problemas de deficiência ',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (relatosDeDeficiencia == null) ? Colors.transparent : ( relatosDeDeficiencia == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            relatosDeDeficiencia = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.relatosDeDeficiencia = value;
                        },
                        isExpanded: false,
                        value: (relatosDeDeficiencia==null || relatosDeDeficiencia =="" )? simNao.first:relatosDeDeficiencia,
                        hint: Text('Houve relatos de deficiencia '),

                      )
                      ,
                    ),
                    //novos 5 elementos

                    //4
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 25,bottom: 25),
                        child: AppBar(leading: Icon(Icons.child_care),title: Text(' 4.4	Crianças até 1 ano e meio',),elevation: 0,)
                    ),
                    //afert header

                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.totalcriancasAte1anoemeio:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              totalcriancasAte1anoemeio = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Total de Crianças até 1 ano e meio',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Total de Crianças até 1 ano e meio',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.totalcriancasAte1anoemeioMeninos:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              totalcriancasAte1anoemeioMeninos = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Nº de Meninos até 1 ano e meio',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Nº de Meninos até 1 ano e meio',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.totalcriancasAte1anoemeioMeninas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              totalcriancasAte1anoemeioMeninas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Nº de Meninas até 1 ano e meio',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Nº de Meninas até 1 ano e meio',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),

                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Alguns apresentam problemas de desenvolvimento ',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (algumasCriancasAte1anoemeioNaodesenvolvem == null) ? Colors.transparent : ( algumasCriancasAte1anoemeioNaodesenvolvem == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            algumasCriancasAte1anoemeioNaodesenvolvem = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.algumasCriancasAte1anoemeioNaodesenvolvem = value;
                        },
                        isExpanded: false,
                        value: (algumasCriancasAte1anoemeioNaodesenvolvem==null || algumasCriancasAte1anoemeioNaodesenvolvem =="" )? simNao.first:algumasCriancasAte1anoemeioNaodesenvolvem,
                        hint: Text('Alguns apresentam problemas de desenvolvimento'),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.algumasCriancasAte1anoemeioNaodesenvolvemQuantas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              algumasCriancasAte1anoemeioNaodesenvolvemQuantas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),

                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.algumaCriancasAte1anoemeioQueTipodeProblema:'',
                          onSaved: (value){
                            setState(() {
                              algumaCriancasAte1anoemeioQueTipodeProblema = value;
                            });

                          },
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 20,
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Que tipo de problemas essas criancas apresentam ',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Que tipo de problemas essas criancas apresentam',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    //
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Algumas tem problema de coordenação motor ',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (algumaCriancasAte1anoemeioTemProblemaMotor == null) ? Colors.transparent : ( algumaCriancasAte1anoemeioTemProblemaMotor == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            algumaCriancasAte1anoemeioTemProblemaMotor = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.algumaCriancasAte1anoemeioTemProblemaMotor = value;
                        },
                        isExpanded: false,
                        value: (algumaCriancasAte1anoemeioTemProblemaMotor==null || algumaCriancasAte1anoemeioTemProblemaMotor =="" )? simNao.first:algumaCriancasAte1anoemeioTemProblemaMotor,
                        hint: Text('Algumas tem problema de coordenação motor'),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.algumaCriancasAte1anoemeioTemProblemaMotorQuantas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              algumaCriancasAte1anoemeioTemProblemaMotorQuantas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),

                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Algumas tem problema de malformação?:  ',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (algumaCriancasAte1anoemeioTemMalFormacao == null) ? Colors.transparent : ( algumaCriancasAte1anoemeioTemMalFormacao == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            algumaCriancasAte1anoemeioTemMalFormacao = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.algumaCriancasAte1anoemeioTemMalFormacao = value;
                        },
                        isExpanded: false,
                        value: (algumaCriancasAte1anoemeioTemMalFormacao==null || algumaCriancasAte1anoemeioTemMalFormacao =="" )? simNao.first:algumaCriancasAte1anoemeioTemMalFormacao,
                        hint: Text('Algumas tem problema de malformação?'),
                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.algumaCriancasAte1anoemeioTemMalFormacaoQuantas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              algumaCriancasAte1anoemeioTemMalFormacaoQuantas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),

                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Algumas não regem aos incentivos:  ',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (algumaCriancasAte1anoemeioNaoReagemAIncentivo == null) ? Colors.transparent : ( algumaCriancasAte1anoemeioNaoReagemAIncentivo == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            algumaCriancasAte1anoemeioNaoReagemAIncentivo = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.algumaCriancasAte1anoemeioNaoReagemAIncentivo = value;
                        },
                        isExpanded: false,
                        value: (algumaCriancasAte1anoemeioNaoReagemAIncentivo==null || algumaCriancasAte1anoemeioNaoReagemAIncentivo =="" )? simNao.first:algumaCriancasAte1anoemeioNaoReagemAIncentivo,
                        hint: Text('Algumas não regem aos incentivos?'),
                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              algumaCriancasAte1anoemeioNaoReagemAIncentivoQuantas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),

                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Algumas tem reações agressivas?',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (algumaCriancasAte1anoemeioTemReacaoAgressiva == null) ? Colors.transparent : ( algumaCriancasAte1anoemeioTemReacaoAgressiva == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            algumaCriancasAte1anoemeioTemReacaoAgressiva = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.algumaCriancasAte1anoemeioTemReacaoAgressiva = value;
                        },
                        isExpanded: false,
                        value: (algumaCriancasAte1anoemeioTemReacaoAgressiva==null || algumaCriancasAte1anoemeioTemReacaoAgressiva =="" )? simNao.first:algumaCriancasAte1anoemeioTemReacaoAgressiva,
                        hint: Text('Algumas tem reações agressivas?'),
                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              algumaCriancasAte1anoemeioTemReacaoAgressivaQuantas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),


                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 25,bottom: 25),
                        child: AppBar(leading: Icon(Icons.child_care),title: Text(' 4.5	Crianças de 1ano e meio a 3 anos',),elevation: 0,)
                    ),
                    //afert header

                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.totalcriancasAte1anoemeioA3anos:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              totalcriancasAte1anoemeioA3anos = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Total de Crianças até 1 ano e meio',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Total de Crianças até 1 ano e meio',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.totalcriancasAte1anoemeioA3anosMeninos:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              totalcriancasAte1anoemeioA3anosMeninos = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Nº de Meninos até 1 ano e meio a 3 anos',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Nº de Meninos até 1 ano e meio a 3 anos',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.totalcriancasAte1anoemeioA3anosMeninas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              totalcriancasAte1anoemeioA3anosMeninas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Nº de Meninas até 1 ano e meio a 3 anos',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Nº de Meninas até 1 ano e meio a 3 anos',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Alguns apresentam problemas de desenvolvimento ',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (algumasCriancasAte1anoemeioA3anosNaodesenvolvem == null) ? Colors.transparent : ( algumasCriancasAte1anoemeioA3anosNaodesenvolvem == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            algumasCriancasAte1anoemeioA3anosNaodesenvolvem = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.algumasCriancasAte1anoemeioA3anosNaodesenvolvem = value;
                        },
                        isExpanded: false,
                        value: (algumasCriancasAte1anoemeioA3anosNaodesenvolvem==null || algumasCriancasAte1anoemeioA3anosNaodesenvolvem =="" )? simNao.first:algumasCriancasAte1anoemeioA3anosNaodesenvolvem,
                        hint: Text('Alguns apresentam problemas de desenvolvimento'),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              algumasCriancasAte1anoemeioA3anosNaodesenvolvemQuantas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),

                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.algumaCriancasAte1anoemeioA3anosQueTipodeProblema:'',
                          onSaved: (value){
                            setState(() {
                              algumaCriancasAte1anoemeioA3anosQueTipodeProblema = value;
                            });

                          },
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 20,
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Que tipo de problemas essas criancas apresentam ',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Que tipo de problemas essas criancas apresentam',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),

                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Algumas não interagem com as pessoas',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas == null) ? Colors.transparent : ( algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas = value;
                        },
                        isExpanded: false,
                        value: (algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas==null || algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoas =="" )? simNao.first:algumasCriancasAte1anoemeioA3anosNaodesenvolvem,
                        hint: Text('Algumas não interagem com as pessoas'),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              algumasCriancasAte1anoemeioA3anosNaoInteragemComPessoasQuantas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    //1
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Algumas tem comportamentos agressivos?',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo == null) ? Colors.transparent : ( algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo = value;
                        },
                        isExpanded: false,
                        value: (algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo==null || algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo =="" )? simNao.first:algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivo,
                        hint: Text('Algumas tem comportamentos agressivos?'),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              algumasCriancasAte1anoemeioA3anosTemComportamentoAgressivoQuantas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    //2
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Algumas se auto agridem?',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (algumasCriancasAte1anoemeioA3anosAlgunsAgridem == null) ? Colors.transparent : ( algumasCriancasAte1anoemeioA3anosAlgunsAgridem == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            algumasCriancasAte1anoemeioA3anosAlgunsAgridem = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.algumasCriancasAte1anoemeioA3anosAlgunsAgridem = value;
                        },
                        isExpanded: false,
                        value: (algumasCriancasAte1anoemeioA3anosAlgunsAgridem==null || algumasCriancasAte1anoemeioA3anosAlgunsAgridem =="" )? simNao.first:algumasCriancasAte1anoemeioA3anosAlgunsAgridem,
                        hint: Text('Algumas se auto agridem?'),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              algumasCriancasAte1anoemeioA3anosAlgunsAgridemQuantas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    //3
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Algumas são mais agitadas que o normal?',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal == null) ? Colors.transparent : ( algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal = value;
                        },
                        isExpanded: false,
                        value: (algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal==null || algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal =="" )? simNao.first:algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormal,
                        hint: Text('Algumas são mais agitadas que o normal?'),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              algumasCriancasAte1anoemeioA3anosAlgumasAgitadasQueONormalQuantas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    //4
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Algumas tem problema de malformação?',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao == null) ? Colors.transparent : ( algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao = value;
                        },
                        isExpanded: false,
                        value: (algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao==null || algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao =="" )? simNao.first:algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacao,
                        hint: Text('Algumas tem problema de malformação?'),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              algumasCriancasAte1anoemeioA3anosAlgumasTemMalFormacaoQuantas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    //5
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Algumas não sabem andar sozinhas?',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas == null) ? Colors.transparent : ( algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas = value;
                        },
                        isExpanded: false,
                        value: (algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas==null || algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas =="" )? simNao.first:algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhas,
                        hint: Text('Algumas não sabem andar sozinhas?'),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              algumasCriancasAte1anoemeioA3anosAlgumasNaoAndamSozinhasQuantas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    //6
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Algumas não sabem se expressar verbalmente?',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente == null) ? Colors.transparent : ( algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente = value;
                        },
                        isExpanded: false,
                        value: (algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente==null || algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente =="" )? simNao.first:algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmente,
                        hint: Text('Algumas não sabem se expressar verbalmente?'),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamVerbalmenteQuantas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    //7
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Algumas não expressem seus sentimentos ?',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos == null) ? Colors.transparent : ( algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos = value;
                        },
                        isExpanded: false,
                        value: (algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos==null || algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos =="" )? simNao.first:algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentos,
                        hint: Text('Algumas não expressem seus sentimentos?'),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              algumasCriancasAte1anoemeioA3anosAlgumasNaoExpressamSentimentosQuantas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    ///8
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Algumas tem síndrome de Down?',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown == null) ? Colors.transparent : ( algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown = value;
                        },
                        isExpanded: false,
                        value: (algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown==null || algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown =="" )? simNao.first:algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDown,
                        hint: Text('Algumas tem síndrome de Down?'),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              algumasCriancasAte1anoemeioA3anosAlgumasTemSindromeDeDownQuantas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),

                    //3 a 6 anos

                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 25,bottom: 25),
                        child: AppBar(leading: Icon(Icons.child_care),title: Text('4.6	Crianças de 3 anos a 6 anos',),elevation: 0,)
                    ),
                    //afert header

                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.totalcriancasde3A6anos:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              totalcriancasde3A6anos = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Total de Crianças de 3 anos a 6 anos',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Total de	Crianças de 3 anos a 6 anos',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.totalcriancasde3A6anosMeninos:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              totalcriancasde3A6anosMeninos = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Nº  de Meninos de 3 anos a 6 anos',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Nº  de Meninos de 3 anos a 6 anos',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.totalcriancasde3A6anosMeninas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              totalcriancasde3A6anosMeninas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Nº Meninas de 3 anos a 6 anos',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Nº Meninas de 3 anos a 6 anos',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Alguns apresentam problemas de desenvolvimento ',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (algumasCriancasde3A6anosNaodesenvolvem == null) ? Colors.transparent : ( algumasCriancasde3A6anosNaodesenvolvem == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            algumasCriancasde3A6anosNaodesenvolvem = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.algumasCriancasde3A6anosNaodesenvolvem = value;
                        },
                        isExpanded: false,
                        value: (algumasCriancasde3A6anosNaodesenvolvem==null || algumasCriancasde3A6anosNaodesenvolvem =="" )? simNao.first:algumasCriancasde3A6anosNaodesenvolvem,
                        hint: Text('Alguns apresentam problemas de desenvolvimento'),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.algumasCriancasde3A6anosNaodesenvolvemQuantas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              algumasCriancasde3A6anosNaodesenvolvemQuantas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),

                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.algumaCriancasde3A6anosQueTipodeProblema:'',
                          onSaved: (value){
                            setState(() {
                              algumaCriancasde3A6anosQueTipodeProblema = value;
                            });

                          },
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 20,
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Que tipo de problemas essas criancas apresentam ',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Que tipo de problemas essas criancas apresentam',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),

                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Algumas não interagem com as pessoas',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (algumasCriancasde3A6anosNaoInteragemComPessoas == null) ? Colors.transparent : ( algumasCriancasde3A6anosNaoInteragemComPessoas == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            algumasCriancasde3A6anosNaoInteragemComPessoas = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.algumasCriancasde3A6anosNaoInteragemComPessoas = value;
                        },
                        isExpanded: false,
                        value: (algumasCriancasde3A6anosNaoInteragemComPessoas==null || algumasCriancasde3A6anosNaoInteragemComPessoas =="" )? simNao.first:algumasCriancasde3A6anosNaoInteragemComPessoas,
                        hint: Text('Algumas não interagem com as pessoas'),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.algumasCriancasde3A6anosNaoInteragemComPessoasQuantas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              algumasCriancasde3A6anosNaoInteragemComPessoasQuantas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    //1
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Algumas tem comportamentos agressivos?',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (algumasCriancasde3A6anosTemComportamentoAgressivo == null) ? Colors.transparent : ( algumasCriancasde3A6anosTemComportamentoAgressivo == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            algumasCriancasde3A6anosTemComportamentoAgressivo = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.algumasCriancasde3A6anosTemComportamentoAgressivo = value;
                        },
                        isExpanded: false,
                        value: (algumasCriancasde3A6anosTemComportamentoAgressivo==null || algumasCriancasde3A6anosTemComportamentoAgressivo =="" )? simNao.first:algumasCriancasde3A6anosTemComportamentoAgressivo,
                        hint: Text('Algumas tem comportamentos agressivos?'),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.algumasCriancasde3A6anosTemComportamentoAgressivoQuantas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              algumasCriancasde3A6anosTemComportamentoAgressivoQuantas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    //2
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Algumas se auto agridem?',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (algumasCriancasde3A6anosAlgunsAgridem == null) ? Colors.transparent : ( algumasCriancasde3A6anosAlgunsAgridem == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            algumasCriancasde3A6anosAlgunsAgridem = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.algumasCriancasde3A6anosAlgunsAgridem = value;
                        },
                        isExpanded: false,
                        value: (algumasCriancasde3A6anosAlgunsAgridem==null || algumasCriancasde3A6anosAlgunsAgridem =="" )? simNao.first:algumasCriancasde3A6anosAlgunsAgridem,
                        hint: Text('Algumas se auto agridem?'),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.algumasCriancasde3A6anosAlgunsAgridemQuantas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              algumasCriancasde3A6anosAlgunsAgridemQuantas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    //3
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Algumas são mais agitadas que o normal?',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (algumasCriancasde3A6anosAlgumasAgitadasQueONormal == null) ? Colors.transparent : ( algumasCriancasde3A6anosAlgumasAgitadasQueONormal == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            algumasCriancasde3A6anosAlgumasAgitadasQueONormal = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.algumasCriancasde3A6anosAlgumasAgitadasQueONormal = value;
                        },
                        isExpanded: false,
                        value: (algumasCriancasde3A6anosAlgumasAgitadasQueONormal==null || algumasCriancasde3A6anosAlgumasAgitadasQueONormal =="" )? simNao.first:algumasCriancasde3A6anosAlgumasAgitadasQueONormal,
                        hint: Text('Algumas são mais agitadas que o normal?'),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              algumasCriancasde3A6anosAlgumasAgitadasQueONormalQuantas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    //4
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Algumas tem problema de malformação?',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (algumasCriancasde3A6anosAlgumasTemMalFormacao == null) ? Colors.transparent : ( algumasCriancasde3A6anosAlgumasTemMalFormacao == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            algumasCriancasde3A6anosAlgumasTemMalFormacao = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.algumasCriancasde3A6anosAlgumasTemMalFormacao = value;
                        },
                        isExpanded: false,
                        value: (algumasCriancasde3A6anosAlgumasTemMalFormacao==null || algumasCriancasde3A6anosAlgumasTemMalFormacao =="" )? simNao.first:algumasCriancasde3A6anosAlgumasTemMalFormacao,
                        hint: Text('Algumas tem problema de malformação?'),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              algumasCriancasde3A6anosAlgumasTemMalFormacaoQuantas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    //5
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Algumas não sabem andar sozinhas?',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (algumasCriancasde3A6anosAlgumasNaoAndamSozinhas == null) ? Colors.transparent : ( algumasCriancasde3A6anosAlgumasNaoAndamSozinhas == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            algumasCriancasde3A6anosAlgumasNaoAndamSozinhas = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.algumasCriancasde3A6anosAlgumasNaoAndamSozinhas = value;
                        },
                        isExpanded: false,
                        value: (algumasCriancasde3A6anosAlgumasNaoAndamSozinhas==null || algumasCriancasde3A6anosAlgumasNaoAndamSozinhas =="" )? simNao.first:algumasCriancasde3A6anosAlgumasNaoAndamSozinhas,
                        hint: Text('Algumas não sabem andar sozinhas?'),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              algumasCriancasde3A6anosAlgumasNaoAndamSozinhasQuantas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    //6
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Algumas não sabem se expressar verbalmente?',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente == null) ? Colors.transparent : ( algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente = value;
                        },
                        isExpanded: false,
                        value: (algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente==null || algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente =="" )? simNao.first:algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmente,
                        hint: Text('Algumas não sabem se expressar verbalmente?'),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              algumasCriancasde3A6anosAlgumasNaoExpressamVerbalmenteQuantas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    //7
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Algumas não expressem seus sentimentos ?',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos == null) ? Colors.transparent : ( algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos = value;
                        },
                        isExpanded: false,
                        value: (algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos==null || algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos =="" )? simNao.first:algumasCriancasde3A6anosAlgumasNaoExpressamSentimentos,
                        hint: Text('Algumas não expressem seus sentimentos?'),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              algumasCriancasde3A6anosAlgumasNaoExpressamSentimentosQuantas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    ///8
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Algumas tem síndrome de Down?',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (algumasCriancasde3A6anosAlgumasTemSindromeDeDown == null) ? Colors.transparent : ( algumasCriancasde3A6anosAlgumasTemSindromeDeDown == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            algumasCriancasde3A6anosAlgumasTemSindromeDeDown = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.algumasCriancasde3A6anosAlgumasTemSindromeDeDown = value;
                        },
                        isExpanded: false,
                        value: (algumasCriancasde3A6anosAlgumasTemSindromeDeDown==null || algumasCriancasde3A6anosAlgumasTemSindromeDeDown =="" )? simNao.first:algumasCriancasde3A6anosAlgumasTemSindromeDeDown,
                        hint: Text('Algumas tem síndrome de Down?'),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              algumasCriancasde3A6anosAlgumasTemSindromeDeDownQuantas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),

                    ///cont.
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Algumas não sabem se alimentar sozinhas?',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas == null) ? Colors.transparent : ( algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas = value;
                        },
                        isExpanded: false,
                        value: (algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas==null || algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas =="" )? simNao.first:algumasCriancasde3A6anosAlgumasNaoSabemSeAlimentarSozinhas,
                        hint: Text('Algumas não sabem se alimentar sozinhas?'),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              algumasCriancasde3A6anosAlgumasNaoSabeSeAlimentarSozinhasQuantas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),

                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Algumas não conseguem tomar banho sozinhas?',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas == null) ? Colors.transparent : ( algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas = value;
                        },
                        isExpanded: false,
                        value: (algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas==null || algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas =="" )? simNao.first:algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhas,
                        hint: Text('Algumas não conseguem tomar banho sozinhas?'),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              algumasCriancasde3A6anosAlgumasNaoConseguemTomarBanhoSozinhasQuantas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),

                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Algumas não conseguem ir para casa de banho sozinhos?',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas == null) ? Colors.transparent : ( algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas = value;
                        },
                        isExpanded: false,
                        value: (algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas==null || algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas =="" )? simNao.first:algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhas,
                        hint: Text('Algumas não conseguem ir para casa de banho sozinhos?'),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              algumasCriancasde3A6anosAlgumasNaoConseguemIrACasaDeBanhoSozinhasQuantas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),

                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Algumas não conseguem se vestir sozinhas?',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas == null) ? Colors.transparent : ( algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas = value;
                        },
                        isExpanded: false,
                        value: (algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas==null || algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas =="" )? simNao.first:algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhas,
                        hint: Text('Algumas não conseguem se vestir sozinhas?'),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              algumasCriancasde3A6anosAlgumasNaoConseguemSeVestirSozinhasQuantas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),

                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.quantasCriancasde3A6anosEstaoNaEscolinha:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              quantasCriancasde3A6anosEstaoNaEscolinha = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Quantas estão na escolinha?',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Quantas estão na escolinha?',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Se algumas crianças não estão na escolinha porque?',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: escolinhas
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (criancasDe3A6anosNaoEstaoNaEscolinhaPorque == null) ? Colors.transparent : ( criancasDe3A6anosNaoEstaoNaEscolinhaPorque == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            criancasDe3A6anosNaoEstaoNaEscolinhaPorque = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.criancasDe3A6anosNaoEstaoNaEscolinhaPorque = value;
                        },
                        isExpanded: false,
                        value: (criancasDe3A6anosNaoEstaoNaEscolinhaPorque==null || criancasDe3A6anosNaoEstaoNaEscolinhaPorque =="" )? escolinhas.first:criancasDe3A6anosNaoEstaoNaEscolinhaPorque,
                        hint: Text('Se algumas crianças não estão na escolinha porque?'),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.quantasCriancasde3A6anosTrabalham:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              quantasCriancasde3A6anosTrabalham = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Quantas Trabalham?',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Quantas Trabalham?',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              criancasDe3A6anosNaoEstaoNaEscolinhaPorOutosMotivos = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Quantas não estão por outros motivos?',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Quantas TrabalhamQuantas não estão por outros motivos?',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),

                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 25,bottom: 25),
                        child: AppBar(leading: Icon(Icons.child_care),title: Text('4.7	Crianças de 7 a 9 anos',),elevation: 0,)
                    ),

                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.totalcriancasde7A9anos:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              totalcriancasde7A9anos = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Total de	Crianças de 7 a 9 anos',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Total de Crianças de 7 a 9 anos',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.totalcriancasde7A9anosMeninos:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              totalcriancasde7A9anosMeninos = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Total	Nº de Meninos',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Total	Nº de Meninos',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.totalcriancasde7A9anosMeninas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              totalcriancasde7A9anosMeninas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Total	Nº de Meninas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Total	Nº de Meninas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.quantasCriancasde7A9anosEstaoNaEscola:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              quantasCriancasde7A9anosEstaoNaEscola = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Quantas estão na escola?',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Quantas estão na escola?',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.quantasCriancasde7A9anosTrabalham:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              quantasCriancasde7A9anosTrabalham = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Quantas trabalham',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Quantas trabalham?',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),

                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 25,bottom: 25),
                        child: AppBar(leading: Icon(Icons.child_care),title: Text('5	Gravidez',),elevation: 0,)
                    ),

                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.nosUltimos5AnosQuantasGravidezesTiveNaFamilia:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              nosUltimos5AnosQuantasGravidezesTiveNaFamilia = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Nos últimos 5 anos, quantas gravidezes tive na família?',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Nos últimos 5 anos, quantas gravidezes tive na família?',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.partosOcoridosNoHospital:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              partosOcoridosNoHospital = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Quantos ocorreram no Hospital',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Quantos ocorreram no Hospital',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.partosOcoridosEmCasa:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              partosOcoridosEmCasa = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Quantos ocorreram em casa',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Quantos ocorreram em casa?',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.partosOcoridosEmOutroLocal:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              partosOcoridosEmOutroLocal = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Quantos ocorreram em outro local',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Quantos ocorreram em outro local',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),

                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.outroLocalDoPartoQualLocal:'',
                          onSaved: (value){
                            setState(() {
                              outroLocalDoPartoQualLocal = value;
                            });

                          },
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 20,
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Qual Local do parto. (referido anteriormente).',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Qual Local do parto. (referido anteriormente).',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.quantasCriancasNasceramVivas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              quantasCriancasNasceramVivas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Quantas crianças nasceram vivas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Quantas crianças nasceram vivas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.quantasContinuamVivas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              quantasContinuamVivas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Quantas continuam vivas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Quantas continuam vivas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),

                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Das vivas, algumas apresentam problemas de desenvolvimento?',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram == null) ? Colors.transparent : ( dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram = value;
                        },
                        isExpanded: false,
                        value: (dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram==null || dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram =="" )? simNao.first:dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram,
                        //hint: Text('Das vivas, algumas apresentam problemas de desenvolvimento?'),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.descreverOProblema:'',
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 192,
                          onSaved: (String value){
                            setState(() {
                              descreverOProblema = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Descreve o problema',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Descreve o problema',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),

                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'A família procurou ajuda para superar esse problema?',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram == null) ? Colors.transparent : ( dasVivasAlgumasApresentamProblemasDeDesenvolvimentoQueOutrosNaoTiveram == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            aFamiliaProcurouAjudaParaSuperarEsseProblema = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.aFamiliaProcurouAjudaParaSuperarEsseProblema = value;
                        },
                        isExpanded: false,
                        value: (aFamiliaProcurouAjudaParaSuperarEsseProblema==null || aFamiliaProcurouAjudaParaSuperarEsseProblema =="" )? simNao.first:aFamiliaProcurouAjudaParaSuperarEsseProblema,
                        //hint: Text('Das vivas, algumas apresentam problemas de desenvolvimento?'),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.seSimQueTipoDeAjudaEOnde:'',
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 192,
                          onSaved: (String value){
                            setState(() {
                              seSimQueTipoDeAjudaEOnde = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: '•	Se Sim, que tipo de ajuda e aonde',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: '•	Se Sim, que tipo de ajuda e aonde',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),

                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'OS vizinhos se preocuparam (ajudaram)?',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia == null) ? Colors.transparent : ( osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia = value;
                        },
                        isExpanded: false,
                        value: (osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia==null || osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia =="" )? simNao.first:osVizinhosSePreocuparamQuandoNasceuUmaCriancaComProblemaNaFamilia,
                        //hint: Text('Das vivas, algumas apresentam problemas de desenvolvimento?'),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              dasCriancasQueNasceramVivasQuantasNaoSobreviveramNos2PrimeirosAnosDeVida = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Das crianças que nasceram vivas, quantas não sobreviveram nos 2 primeiros anos',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Das crianças que nasceram vivas, quantas não sobreviveram nos 2 primeiros anos',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Sabe dizer o motivo dos óbitos?',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (sabeDizerOMotivoDosObitos == null) ? Colors.transparent : ( sabeDizerOMotivoDosObitos == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            sabeDizerOMotivoDosObitos = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.sabeDizerOMotivoDosObitos = value;
                        },
                        isExpanded: false,
                        value: (sabeDizerOMotivoDosObitos==null || sabeDizerOMotivoDosObitos =="" )? simNao.first:sabeDizerOMotivoDosObitos,
                        //hint: Text('Das vivas, algumas apresentam problemas de desenvolvimento?'),

                      )
                      ,
                    ),

                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Se Sim, qual foi principal razão do(s) óbito(s)?',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: motivoDoObito
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (seSimQualFoiPrincipalRazaodoObito == null) ? Colors.transparent : ( seSimQualFoiPrincipalRazaodoObito == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            seSimQualFoiPrincipalRazaodoObito = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.seSimQualFoiPrincipalRazaodoObito = value;
                        },
                        isExpanded: false,
                        value: (seSimQualFoiPrincipalRazaodoObito==null || seSimQualFoiPrincipalRazaodoObito =="" )? motivoDoObito.first:seSimQualFoiPrincipalRazaodoObito,
                        //hint: Text('Das vivas, algumas apresentam problemas de desenvolvimento?'),

                      )
                      ,
                    ),

                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 25,bottom: 25),
                        child: AppBar(leading: Icon(Icons.assignment),title: Text('6	Geral',),elevation: 0,)
                    ),

                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Você tem medo de ter um filho ou neto com problemas ?',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (voceTemMedoDeTerUmFilhoOuNetoComProblemas == null) ? Colors.transparent : ( voceTemMedoDeTerUmFilhoOuNetoComProblemas == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            voceTemMedoDeTerUmFilhoOuNetoComProblemas = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.voceTemMedoDeTerUmFilhoOuNetoComProblemas = value;
                        },
                        isExpanded: false,
                        value: (voceTemMedoDeTerUmFilhoOuNetoComProblemas==null || voceTemMedoDeTerUmFilhoOuNetoComProblemas =="" )? simNao.first:voceTemMedoDeTerUmFilhoOuNetoComProblemas,
                        //hint: Text('Das vivas, algumas apresentam problemas de desenvolvimento?'),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.expliquePorQue:'',
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 192,
                          onSaved: (String value){
                            setState(() {
                              expliquePorQue = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Explique por quê',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Explique por quê ',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    //espaco para dois campos aqui
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Porque você acha que uma criança pode nascer com "problema"?  ',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: umaCriancaComProblemas
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (porqueVoceAchaQueUmaCriancaPodeNascerComProblema == null) ? Colors.transparent : ( porqueVoceAchaQueUmaCriancaPodeNascerComProblema == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            porqueVoceAchaQueUmaCriancaPodeNascerComProblema = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.porqueVoceAchaQueUmaCriancaPodeNascerComProblema = value;
                        },
                        isExpanded: false,
                        value: (porqueVoceAchaQueUmaCriancaPodeNascerComProblema==null || porqueVoceAchaQueUmaCriancaPodeNascerComProblema =="" )? umaCriancaComProblemas.first:porqueVoceAchaQueUmaCriancaPodeNascerComProblema,
                        //hint: Text('Das vivas, algumas apresentam problemas de desenvolvimento?'),

                      )
                      ,
                    ),


                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 15, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'O que acontece no bairro quando nasce uma criança com problema? ',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: bairroComCriancaComProblemas
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema == null) ? Colors.transparent : ( oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema = value;
                        },
                        isExpanded: true,
                        value: (oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema==null || oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema =="" )? bairroComCriancaComProblemas.first:oQueAconteceNoSeuBairroQuandoNasceUmaCriancaComProblema,
                        //hint: Text('Completou ensino Primário? '),

                      )
                      ,
                    ),


                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Acha que uma criança que nasce com problema pode sobreviver? ',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (achaQueUmaCriancaQueNasceComProblemaPodeSobreviver == null) ? Colors.transparent : ( achaQueUmaCriancaQueNasceComProblemaPodeSobreviver == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            achaQueUmaCriancaQueNasceComProblemaPodeSobreviver = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.achaQueUmaCriancaQueNasceComProblemaPodeSobreviver = value;
                        },
                        isExpanded: false,
                        value: (achaQueUmaCriancaQueNasceComProblemaPodeSobreviver==null || achaQueUmaCriancaQueNasceComProblemaPodeSobreviver =="" )? simNao.first:achaQueUmaCriancaQueNasceComProblemaPodeSobreviver,
                        //hint: Text('Das vivas, algumas apresentam problemas de desenvolvimento?'),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.expliquePorque:'',
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 192,
                          onSaved: (String value){
                            setState(() {
                              expliquePorque = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Explique por quê',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Explique por quê ',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),

                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Quem pode ajudar quando uma criança nasce com problema?',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: quemAjudaCriancaConProblemas
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (quemPodeAjudarQuandoUmaCriancaNasceComProblema == null) ? Colors.transparent : ( quemPodeAjudarQuandoUmaCriancaNasceComProblema == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            quemPodeAjudarQuandoUmaCriancaNasceComProblema = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.quemPodeAjudarQuandoUmaCriancaNasceComProblema = value;
                        },
                        isExpanded: false,
                        value: (quemPodeAjudarQuandoUmaCriancaNasceComProblema==null || quemPodeAjudarQuandoUmaCriancaNasceComProblema =="" )? quemAjudaCriancaConProblemas.first:quemPodeAjudarQuandoUmaCriancaNasceComProblema,
                        //hint: Text('Das vivas, algumas apresentam problemas de desenvolvimento?'),

                      )
                      ,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Em casa, quem cuida mais das crianças até 6 anos Para Alimentação',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: quemCuidadeCriancaAte6anos
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao == null) ? Colors.transparent : ( emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao = value;
                        },
                        isExpanded: false,
                        value: (emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao==null || emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao =="" )? quemCuidadeCriancaAte6anos.first:emCasaQuemCuidaMaisDasCriancasAte6AnosParaAlimentacao,
                        //hint: Text('Das vivas, algumas apresentam problemas de desenvolvimento?'),

                      )
                      ,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Em casa, quem cuida mais das crianças até 6 anos Para Higiene ',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: quemCuidadeCriancaAte6anos
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene == null) ? Colors.transparent : ( emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene = value;
                        },
                        isExpanded: false,
                        value: (emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene==null || emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene =="" )? quemCuidadeCriancaAte6anos.first:emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsCuidadosDeHigiene,
                        //hint: Text('Das vivas, algumas apresentam problemas de desenvolvimento?'),

                      )
                      ,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Em casa, quem cuida mais das crianças até 6 anos Para Estudos ',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: quemCuidadeCriancaAte6anos
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos == null) ? Colors.transparent : ( emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos = value;
                        },
                        isExpanded: false,
                        value: (emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos==null || emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos =="" )? quemCuidadeCriancaAte6anos.first:emCasaQuemCuidaMaisDasCriancasAte6AnosParaOsEstudos,
                        //hint: Text('Das vivas, algumas apresentam problemas de desenvolvimento?'),

                      )
                      ,
                    ),

                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 25,bottom: 25),
                        child: AppBar(leading: Icon(Icons.remove_red_eye_sharp),title: Text('7	Observações do entrevistador',),elevation: 0,)
                    ),

                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'A pessoa entrevistada pareceu sincera nas suas respostas',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (aPessoaEntrevistadaPareceuSinceraNasSuasRespostas == null) ? Colors.transparent : ( aPessoaEntrevistadaPareceuSinceraNasSuasRespostas == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            aPessoaEntrevistadaPareceuSinceraNasSuasRespostas = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.aPessoaEntrevistadaPareceuSinceraNasSuasRespostas = value;
                        },
                        isExpanded: false,
                        value: (aPessoaEntrevistadaPareceuSinceraNasSuasRespostas==null || aPessoaEntrevistadaPareceuSinceraNasSuasRespostas =="" )? simNao.first:aPessoaEntrevistadaPareceuSinceraNasSuasRespostas,
                        // hint: Text('O entrevistado é chefe da família? '),

                      )
                      ,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Algumas perguntas incomodaram a pessoa entrevistada? ',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (algumasPerguntasIncomodaramAPessoaEntrevistada == null) ? Colors.transparent : ( algumasPerguntasIncomodaramAPessoaEntrevistada == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            algumasPerguntasIncomodaramAPessoaEntrevistada = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.algumasPerguntasIncomodaramAPessoaEntrevistada = value;
                        },
                        isExpanded: false,
                        value: (algumasPerguntasIncomodaramAPessoaEntrevistada==null || algumasPerguntasIncomodaramAPessoaEntrevistada =="" )? simNao.first:algumasPerguntasIncomodaramAPessoaEntrevistada,
                        //hint: Text('O entrevistado é chefe da família? '),

                      )
                      ,
                    ),

                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.seSimQuais3PrincipaisPerguntas:'',
                          onSaved: (value){
                            setState(() {
                              seSimQuais3PrincipaisPerguntas = value;
                            });

                          },
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 20,
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quais 3 principais perguntas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quais 3 principais perguntas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),

                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Todas as perguntas incomodaram? ',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (todasAsPerguntasIncomodaram == null) ? Colors.transparent : ( todasAsPerguntasIncomodaram == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            todasAsPerguntasIncomodaram = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.todasAsPerguntasIncomodaram = value;
                        },
                        isExpanded: false,
                        value: (todasAsPerguntasIncomodaram==null || todasAsPerguntasIncomodaram =="" )? simNao.first:todasAsPerguntasIncomodaram,
                        //hint: Text('O entrevistado é chefe da família? '),

                      )
                      ,
                    ),

                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'A residência, era de ? ',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: residencia
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (aResidenciaEraDe == null) ? Colors.transparent : ( aResidenciaEraDe == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            aResidenciaEraDe = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.aResidenciaEraDe = value;
                        },
                        isExpanded: false,
                        value: (aResidenciaEraDe==null || aResidenciaEraDe =="" )? residencia.first:aResidenciaEraDe,
                        //hint: Text('O entrevistado é chefe da família? '),

                      )
                      ,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Como avalia a higiene da casa ? ',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: higieneDaCasa
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (comoAvaliaHigieneDaCasa == null) ? Colors.transparent : ( comoAvaliaHigieneDaCasa == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            comoAvaliaHigieneDaCasa = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.comoAvaliaHigieneDaCasa = value;
                        },
                        isExpanded: false,
                        value: (comoAvaliaHigieneDaCasa==null || comoAvaliaHigieneDaCasa =="" )? higieneDaCasa.first:comoAvaliaHigieneDaCasa,
                        //hint: Text('O entrevistado é chefe da família? '),

                      )
                      ,
                    ),

                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.oQueEraMal:'',
                          onSaved: (value){
                            setState(() {
                              oQueEraMal = value;
                            });

                          },
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 20,
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Oque era mal',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Oque era mal',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),

                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Tinha crianças na casa durante inquérito',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (tinhaCriancasNaCasaDuranteInquerito == null) ? Colors.transparent : ( tinhaCriancasNaCasaDuranteInquerito == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            tinhaCriancasNaCasaDuranteInquerito = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.tinhaCriancasNaCasaDuranteInquerito = value;
                        },
                        isExpanded: false,
                        value: (tinhaCriancasNaCasaDuranteInquerito==null || tinhaCriancasNaCasaDuranteInquerito =="" )? simNao.first:tinhaCriancasNaCasaDuranteInquerito,
                        hint: Text('Tinha crianças na casa durante inquérito? '),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.tinhaCriancasNaCasaDuranteInqueritoQuantas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              tinhaCriancasNaCasaDuranteInqueritoQuantas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),

                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Tentou incentivar?',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (tentouIncentivar == null) ? Colors.transparent : ( tentouIncentivar == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            tentouIncentivar = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.tentouIncentivar = value;
                        },
                        isExpanded: false,
                        value: (tentouIncentivar==null || tentouIncentivar =="" )? simNao.first:tentouIncentivar,
                        //hint: Text('Tinha crianças na casa durante inquérito? '),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.tentouIncentivarQuantas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              tentouIncentivarQuantas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),

                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              seIncentivouQuantasReagiramPositivamenteAosSeusIncentivos = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se incentivou, Quantas reagiram positivamente aos seus incentivos',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se incentivou, Quantas reagiram positivamente aos seus incentivos',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 25,bottom: 25),
                        child: AppBar(leading: Icon(Icons.person_outline),title: Text('7.5 Crianças de até 1ano e meio',),elevation: 0,)
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Observou crianças de até 1ano e meio',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (observouCriancasDeAte1Ano == null) ? Colors.transparent : ( observouCriancasDeAte1Ano == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            observouCriancasDeAte1Ano = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.observouCriancasDeAte1Ano = value;
                        },
                        isExpanded: false,
                        value: (observouCriancasDeAte1Ano==null || observouCriancasDeAte1Ano =="" )? simNao.first:observouCriancasDeAte1Ano,
                        //hint: Text('Tinha crianças na casa durante inquérito? '),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.observouCriancasDeAte1AnoQuantos:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              observouCriancasDeAte1AnoQuantos = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),

                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Algumas apresentavam problemas',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (algumasApresentavamProblemas == null) ? Colors.transparent : ( algumasApresentavamProblemas == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            algumasApresentavamProblemas = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.algumasApresentavamProblemas = value;
                        },
                        isExpanded: false,
                        value: (algumasApresentavamProblemas==null || algumasApresentavamProblemas =="" )? simNao.first:algumasApresentavamProblemas,
                        //hint: Text('Tinha crianças na casa durante inquérito? '),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.algumasApresentavamProblemasQuantos:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              algumasApresentavamProblemasQuantos = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.descreveOProblemaDessasCriancas:'',
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 192,
                          onSaved: (String value){
                            setState(() {
                              descreveOProblemaDessasCriancas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Descreve o problema dessas Criancas.',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),

                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 25,bottom: 25),
                        child: AppBar(leading: Icon(Icons.person_outline),title: Text('7.6 Crianças de 1ano e meio até 3 anos',),elevation: 0,)
                    ),
                    //vamos dar remate ate onde parar
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Observou crianças de 1ano e meio até 3 anos',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (observouCriancasDe1AnoEmeioAte3Anos == null) ? Colors.transparent : ( observouCriancasDe1AnoEmeioAte3Anos == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            observouCriancasDe1AnoEmeioAte3Anos = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.observouCriancasDe1AnoEmeioAte3Anos = value;
                        },
                        isExpanded: false,
                        value: (observouCriancasDe1AnoEmeioAte3Anos==null || observouCriancasDe1AnoEmeioAte3Anos =="" )? simNao.first:observouCriancasDe1AnoEmeioAte3Anos,
                        //hint: Text('Tinha crianças na casa durante inquérito? '),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.observouCriancasDe1AnoEmeioAte3AnosQuantas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              observouCriancasDe1AnoEmeioAte3AnosQuantas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),

                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Algumas apresentavam problemas',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (algumasCriancasDe1anoEmeioObservadasApresentavamProblemas == null) ? Colors.transparent : ( algumasCriancasDe1anoEmeioObservadasApresentavamProblemas == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            algumasCriancasDe1anoEmeioObservadasApresentavamProblemas = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.algumasCriancasDe1anoEmeioObservadasApresentavamProblemas = value;
                        },
                        isExpanded: false,
                        value: (algumasCriancasDe1anoEmeioObservadasApresentavamProblemas==null || algumasCriancasDe1anoEmeioObservadasApresentavamProblemas =="" )? simNao.first:algumasCriancasDe1anoEmeioObservadasApresentavamProblemas,
                        //hint: Text('Tinha crianças na casa durante inquérito? '),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              algumasCriancasDe1anoEmeioObservadasApresentavamProblemasQuantas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve:'',
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 192,
                          onSaved: (String value){
                            setState(() {
                              algumasCriancasDe1anoEmeioObservadasApresentavamProblemasDescreve = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Descreve o problema dessas Criancas.',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),

                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 25,bottom: 25),
                        child: AppBar(leading: Icon(Icons.person_outline),title: Text('7.7	Crianças de 3 a 6 anos',),elevation: 0,)
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Observou crianças de 3 a 6 anos',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (observouCriancasDe3AnosAte6Anos == null) ? Colors.transparent : ( observouCriancasDe3AnosAte6Anos == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            observouCriancasDe3AnosAte6Anos = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.observouCriancasDe3AnosAte6Anos = value;
                        },
                        isExpanded: false,
                        value: (observouCriancasDe3AnosAte6Anos==null || observouCriancasDe3AnosAte6Anos =="" )? simNao.first:observouCriancasDe3AnosAte6Anos,
                        //hint: Text('Tinha crianças na casa durante inquérito? '),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.observouCriancasDe3AnosAte6AnosQuantas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              observouCriancasDe3AnosAte6AnosQuantas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),

                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Algumas apresentavam problemas',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas == null) ? Colors.transparent : ( observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas = value;
                        },
                        isExpanded: false,
                        value: (observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas==null || observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas =="" )? simNao.first:observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemas,
                        //hint: Text('Tinha crianças na casa durante inquérito? '),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas:'',
                          keyboardType: TextInputType.number,
                          inputFormatters: <TextInputFormatter>[
                            FilteringTextInputFormatter.digitsOnly
                          ],
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 4,
                          onSaved: (String value){
                            setState(() {
                              observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasQuantas = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim quantas',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve:'',
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 192,
                          onSaved: (String value){
                            setState(() {
                              observouCriancasDe3AnosAte6AnosQueAlgumasApresentavamProblemasDescreve = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Descreve o problema dessas Criancas.',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim quantas',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),

                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 25,bottom: 25),
                        child: AppBar(leading: Icon(Icons.person_outline),title: Text('7.8 Observou situações que lhe chamaram atenção',),elevation: 0,)
                    ),


                    //7.8
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Observou situações que lhe chamaram atenção?',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (observouSituacoesQueLheChamaramAtencao == null) ? Colors.transparent : ( observouSituacoesQueLheChamaramAtencao == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            observouSituacoesQueLheChamaramAtencao = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.observouSituacoesQueLheChamaramAtencao = value;
                        },
                        isExpanded: false,
                        value: (observouSituacoesQueLheChamaramAtencao==null || observouSituacoesQueLheChamaramAtencao =="" )? simNao.first:observouSituacoesQueLheChamaramAtencao,
                        //hint: Text('Tinha crianças na casa durante inquérito? '),

                      )
                      ,
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Em relação a pessoa entrevistada',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (emRelacaoAPessoaEntrevistada == null) ? Colors.transparent : ( emRelacaoAPessoaEntrevistada == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            emRelacaoAPessoaEntrevistada = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.emRelacaoAPessoaEntrevistada = value;
                        },
                        isExpanded: false,
                        value: (emRelacaoAPessoaEntrevistada==null || emRelacaoAPessoaEntrevistada =="" )? simNao.first:emRelacaoAPessoaEntrevistada,
                        //hint: Text('Tinha crianças na casa durante inquérito? '),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.emRelacaoAPessoaEntrevistadaDescreve:'',
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 192,
                          onSaved: (String value){
                            setState(() {
                              emRelacaoAPessoaEntrevistadaDescreve = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim descreve.',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim descreve',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Em relação a outros adultos presente? ',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (emRelacaoAOutrosAdultosPresente == null) ? Colors.transparent : ( emRelacaoAOutrosAdultosPresente == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            emRelacaoAOutrosAdultosPresente = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.emRelacaoAOutrosAdultosPresente = value;
                        },
                        isExpanded: false,
                        value: (emRelacaoAOutrosAdultosPresente==null || emRelacaoAOutrosAdultosPresente =="" )? simNao.first:emRelacaoAOutrosAdultosPresente,
                        //hint: Text('Tinha crianças na casa durante inquérito? '),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.emRelacaoAOutrosAdultosPresenteDescreve:'',
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 192,
                          onSaved: (String value){
                            setState(() {
                              emRelacaoAOutrosAdultosPresenteDescreve = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: ' Se sim descreve',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim descreve',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Em relação as crianças presentes?',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (emRelacaoAsCriancasPresentes == null) ? Colors.transparent : ( emRelacaoAsCriancasPresentes == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            emRelacaoAsCriancasPresentes = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.emRelacaoAsCriancasPresentes = value;
                        },
                        isExpanded: false,
                        value: (emRelacaoAsCriancasPresentes==null || emRelacaoAsCriancasPresentes =="" )? simNao.first:emRelacaoAsCriancasPresentes,
                        //hint: Text('Tinha crianças na casa durante inquérito? '),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.emRelacaoAsCriancasPresentesDescreve:'',
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 192,
                          onSaved: (String value){
                            setState(() {
                              emRelacaoAsCriancasPresentesDescreve = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim descreve',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim descreve',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Em relação a vizinhança?',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (emRelacaoAVizinhanca == null) ? Colors.transparent : ( emRelacaoAVizinhanca == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            emRelacaoAVizinhanca = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.emRelacaoAVizinhanca = value;
                        },
                        isExpanded: false,
                        value: (emRelacaoAVizinhanca==null || emRelacaoAVizinhanca =="" )? simNao.first:emRelacaoAVizinhanca,
                        //hint: Text('Tinha crianças na casa durante inquérito? '),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.emRelacaoAVizinhancaDescreve:'',
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 192,
                          onSaved: (String value){
                            setState(() {
                              emRelacaoAVizinhancaDescreve = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Se sim descreve',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Se sim descreve',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),
                    Padding(
                      padding: const EdgeInsets.only(
                          left: 35, right: 35, top: 20),
                      child: DropdownButtonFormField<String>(
                        autofocus: true,
                        decoration: InputDecoration(
                          filled: true,
                          fillColor: Colors.white,
                          labelStyle: TextStyle(
                            color: Color(0xFF000000),
                          ),
                          helperText: 'Outras?',
                          enabledBorder: UnderlineInputBorder(
                            borderSide:
                            BorderSide(color: Color(0xFF000000)),
                          ),
                        ),
                        items: simNao
                            .map(
                                (ba) => DropdownMenuItem<String>(

                              child:  Row(
                                mainAxisSize: MainAxisSize.min,
                                children: <Widget>[
                                  Icon(
                                    Icons.check,
                                    color: (outras == null) ? Colors.transparent : ( outras == ba?null:Colors.transparent),
                                  ),
                                  SizedBox(width: 16),
                                  Text(ba),

                                ],
                              ),
                              value: ba,
                            ))
                            .toList(),

                        onChanged: (String value) {
                          setState(() {
                            outras = value;
                            //print(bairro);
                          });
                        },
                        onSaved: (String value){
                          this.outras = value;
                        },
                        isExpanded: false,
                        value: (outras==null || outras =="" )? simNao.first:outras,
                        //hint: Text('Tinha crianças na casa durante inquérito? '),

                      )
                      ,
                    ),
                    Padding(
                        padding: const EdgeInsets.only(
                            left: 35, right: 35, top: 0,bottom: 350),
                        child: TextFormField(
                          autofocus: true,
                          initialValue: (user.uuid!=null)?user.outrasDescreve:'',
                          cursorColor: Theme.of(context).cursorColor,
                          maxLength: 192,
                          onSaved: (String value){
                            setState(() {
                              outrasDescreve = value;
                            });
                          },
                          decoration: InputDecoration(
                            filled: true,
                            fillColor: Colors.white,
                            labelText: 'Descreve outras',
                            labelStyle: TextStyle(
                              color: Color(0xFF000000),
                            ),
                            helperText: 'Descreve outras',
                            enabledBorder: UnderlineInputBorder(
                              borderSide:
                              BorderSide(color: Color(0xFF000000)),
                            ),
                          ),
                        )
                    ),









                  ],
                ),
              ),
            ]
        )
    );
  }
}