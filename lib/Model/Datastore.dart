
import 'package:epbeira/Database/UserSheet.dart';
import 'package:epbeira/Database/database.dart';
import 'package:gsheets/gsheets.dart';
import 'dart:collection';
import 'package:synchronized/synchronized.dart';

import 'package:moor/moor.dart';

// your google auth credentials
const _credentials = r'''
{
  "type": "service_account",
  "project_id": "estmulacaoprecossebeira",
  "private_key_id": "b962904dd9f1a45a3f2178b28fa8d4c6fd5d044a",
  "private_key": "-----BEGIN PRIVATE KEY-----\nMIIEvAIBADANBgkqhkiG9w0BAQEFAASCBKYwggSiAgEAAoIBAQCXaeJFEaKs9Fa6\nlYCrcFzAFFZIrHTRc3t9UBf6fJM43G43OEA+osDmK/aFitmHFMOyr72DxXJ2xEk1\nbMHvJjAKx+n1xSCIRPcpZ4+APkMx/zPWp00Fncj2IjKVKZuKMY0hgmOZoDmEB2lf\nEqzHKNNB6IQv73Uvt1bCDKPjBQL6P8io9bdzm5Br+RlRQfHg77RwDSLMrap1p3tu\nYhVbUZVIdvE8CUYCmmNRINE0fELbhBpBC/bHTkir9SaXE3WzRReOGuN38RzBTkRC\nQwvFn7uDmetosBc6KZe6pJ0pu1TusDcBRAKxalKGGTCQ3wrZDvnqjnOJXN1M8otw\n2X81/+U7AgMBAAECggEAAWWai3oKsDFq8O6nB0Rc8s2R/GCgxK3OT5vXsR07ikLI\nFRgC7AvSdCViApwKDlAWUsL4I+XA1tTMtBrakeIeSAzpIkdMrNwVzpTEJMbEwwO5\n931anXLWv8P5tISWX2N/909dltfd0bRAH/6NwsHNzJqlm5t/fDgaVPAHWTOqOrEQ\n6cT7sv9b1BKDbnfZjoJmixY47LSNDbk8rNci/fvqbyIzYMlIFBcGw960DqKhtGjk\nJGPlue9yiQFb6E9iLn0ldJ3nqiA5mc3zR5bi5zFd/GWdc37tsYV2ewn5tycHcUGy\nLKMWMkLbO3h6pxgaOOezyH11BIRMDmnnG6jjJTMzUQKBgQDLSXRkRi5Eu7NqHPga\nmpybbTYnS9dJr8kHWRUQ1nk2q97plg7oJncFi/dGOI1ZjFJVDmHNFIwqrhqQKjDu\nVtSPCXx+G1KKv0rqIDanl5t1t1kzSrA1dWS5Lxo7FeZ0y8htG9EhigcSSHY+HrFh\ni3nH6/JkWYkXGN75ngPpOCYD2QKBgQC+rPyILhDuAI01q/42Znbb3mOJZcdFl9LX\nWyx5lFUwSnWHYoyFiXvikZsjT/q23F70l3nD5muWynXsOBxTvJ6mmZofc9m49cl+\nNt+t8dMSDDrm0QTziGhcMsxshFNBlOocviIXUR9PAGkj2ywQBvOqpA8xldOCOe4O\nAmUfbI6JMwKBgDRTSu9IlT5B5jOJE1+drnPfT5Ib1mV0JtWKW37DBd5JrQ4I/vtI\nc1htJ4/enJuRH9we4JOuF0ohGFtFUOT4Wqwar/2xwLRP+tN6yTaoMm7Ba+dk8ShB\neFSmSCUvROuJTKSxBUqRwbI2DqF8uwG1qfoIxJKJ7VyRnUs5Hh6Ywkg5AoGALARR\nE7rWq6qy7nXy7GoA/Ea9YaV+OAGRaaehf7uPYuN9eDB20gjn8Wi9kIxRC0+aGAqz\n0FZr9dZtHIfjei0rzrfczW/4DeunhRxzzHrrx9nM6yXLZ9DQjqAnctGlSplu5g26\nXvmaKamlm+JnrJd+PqE65K56sjBfVRqWb4em6+MCgYBRliwPIQUSt2jhWDqXUbdH\nrcbmh0WfHyLmMOokicL7SllzSnCquwsKiGsoS6ol1e44KPgYzuJWuyQgZ3DUmG5z\nDSHLft+QA6BhafOfqem2tQ1cEvIoGQayuhhQX8UbKWhuxx+8uAWSxJvMz19BgWDh\n6vuhSC5HXoO4nKlwPyIetA==\n-----END PRIVATE KEY-----\n",
  "client_email": "sumbureroepp@estmulacaoprecossebeira.iam.gserviceaccount.com",
  "client_id": "112324384663664611323",
  "auth_uri": "https://accounts.google.com/o/oauth2/auth",
  "token_uri": "https://oauth2.googleapis.com/token",
  "auth_provider_x509_cert_url": "https://www.googleapis.com/oauth2/v1/certs",
  "client_x509_cert_url": "https://www.googleapis.com/robot/v1/metadata/x509/sumbureroepp%40estmulacaoprecossebeira.iam.gserviceaccount.com"
}

''';
const _spreadsheetId = '1SDYHAxgbxEpTxFzyaSjLq5IivZ_Sji-KgfKqE5uI3sY';

class Datastore {
  static void gSheet(MyDatabase database) async {
    GeneratedDatabase db  = database; 
    //database.emptyUsers();
    //database.emptyInsertedUsers();
    database.emptyUpdatedUsers();
    database.emptyDeletedUsers();

  final gsheets = GSheets(_credentials);
  // fetch spreadsheet by its id
  final ss = await gsheets.spreadsheet(_spreadsheetId);
  // get worksheet by its title
  var sheet =  ss.worksheetByTitle('DB');
  // create worksheet if it does not exist yet
  sheet ??= await ss.addWorksheet('DB');



  ///print(DateTime.now());

  //sheet.values.insertRow(1, User().toJson().keys.toList());
  List<User> users = await database.allUsers;
 // List<UserSheet> users = datax;

  //print("Test update: ${users.last.createdAt}");

  final data =   List.generate(users.length, (index){
        return users.elementAt(index).toJsonSheet();
    });


   // if(data.isNotEmpty) sheet.values.map.appendRows(data);

   final List<Map<String, dynamic>> maps = await sheet.values.map.allRows();
      ListQueue<User> stack = new ListQueue();
       List<User> gSheet = List.generate(maps.length, (i) => UserSheet().fromJsonSheet(maps.elementAt(i),db));
        stack.addAll(gSheet);
        gSheet.forEach((element) { 
    //       database.addUserFromServer(element);
        });
  }

  static Future syncronization(MyDatabase _db) async {
      final gsheets = GSheets(_credentials);
      // fetch spreadsheet by its id
      final ss = await gsheets.spreadsheet(_spreadsheetId);
      // get worksheet by its title
      var sheet =  ss.worksheetByTitle('DB');
      // create worksheet if it does not exist yet
      sheet ??= await ss.addWorksheet('DB');
      var lock = new Lock();
      List<InsertedUser> inserted = await lock.synchronized(() {return _db.allInsertedUsers;});
      List<UpdatedUser> updated = await lock.synchronized(() {return _db.allUpdatedUsers;});
      List<DeletedUser> deleted = await lock.synchronized(() { return _db.allDeletedUsers;});
      List<User> dbUsers = await lock.synchronized(() {return _db.allUsers;});

      ListQueue<User> insertedUsers = await lock.synchronized(() { return _db.getInsertedUsers();});
      ListQueue<User> updatedUsers = await  lock.synchronized(() { return _db.getUpdatedUsers();});
      ListQueue<User> deletedUsers = await lock.synchronized((){ return _db.getDeletedUsers();});

      List<Map<String,dynamic>> insertedUsersServer = toAppendUsers(insertedUsers);
      List<User> sheetUsers =  await  lock.synchronized(() { return fromSheetUsers(sheet,_db);});
      List<Map<String,dynamic>> updatedUsersServer  = toUpdateUsersMap(sheetUsers,toUpdateUsers(updatedUsers));
      List<Map<String,dynamic>> deletedUsersServer = toDeleteUsersMap(sheetUsers, toDeleteUsers(deletedUsers));


      List<User> localUsers = await  lock.synchronized(() { return  _db.allUsers;});
      List<int> sync = [1,2,3,4];


     // print(" Inserted : $inserted \n");
      //print(" updated : $deleted \n");
      //print(" Deleted : $deleted \n");
     // print(" Inserted Users: $insertedUsers \n");
      //print(" Updated Users: $updatedUsers \n");
      //Local to server
      await  lock.synchronized(() {
        if(insertedUsers.isNotEmpty){
          syncAddServer(sheet, insertedUsersServer);
          _db.emptyInsertedUsers().then((value){
            print('sync server => Insertion Success');
          });
        }
      });


     //  print(" updated : $updatedUsers \n");
      // print(" Updated Users: $updatedUsersServer \n");
      await  lock.synchronized(() {
        if(updatedUsersServer.isNotEmpty){
          syncUpdateServer(sheet, updatedUsersServer);
          _db.emptyUpdatedUsers().then((value) {
             print('sync server => Update Success');
          });
        }
        //  print(" deleted : $deletedUsers \n");
        // print(" Deleted Users: $deletedUsersServer \n");

          if(deletedUsersServer.isNotEmpty){
            syncDeleteServer(sheet, deletedUsersServer,sheetUsers);
            _db.emptyDeletedUsers().then((value) {
              print('sync server => Deletion Success');
            });
          }


      });

      //Fase 2 server to Local
      if(sheetUsers.isNotEmpty){
        //inserting
        await  lock.synchronized(() { syncAddLocal(_db, toInsertUsersLocal(sheetUsers, dbUsers));});
        await  lock.synchronized(() { syncUpdateLocal(_db, toUpdateUsersLocal(sheetUsers, dbUsers));});
        await  lock.synchronized(() { syncDeleteLocal(_db, toDeleteUsersLocal(sheetUsers, dbUsers));});


      }else if(sheetUsers.isEmpty && insertedUsers.isEmpty && updatedUsers.isEmpty && deletedUsers.isEmpty){
        _db.emptyDeletedUsers();
        _db.emptyInsertedUsers();
        _db.emptyUpdatedUsers();
        _db.emptyUsers();
      }

  }


  //1 fase
  static List<Map<String,dynamic>> toAppendUsers(ListQueue<User> usersToAppend) {
    List<User> list = List.from(usersToAppend.toList());
    if(list.isNotEmpty){
      // ignore: missing_return
      return List.generate(list.length, (index){
        if(list.elementAt(index)!=null) return list.elementAt(index).toJsonSheet();
    });}else{
      return List<Map<String,dynamic>>();
    }
  }

  static void syncAddServer(Worksheet sheet,List<Map<String,dynamic>> users){
    print("${users.isEmpty}");
    if(users.isNotEmpty)  sheet.values.map.appendRows(users);
  }
  static void syncAddLocal(MyDatabase _db,List<User> users){ 
    if(users.isNotEmpty) users.forEach((element) {
      _db.addUserFromServer(element).then((value){
      print("Sync Local Insertion Success.");
      users.removeWhere((el) => (el.uuid == element.uuid));
    });});
  }
  //2 fase
  static void syncUpdateServer(Worksheet sheet,List<Map<String,dynamic>> users){

    if(users.isNotEmpty) users.forEach((element) => sheet.values.map.insertRowByKey(element['uuid'],element));
  }
  static void syncUpdateLocal(MyDatabase _db,List<User> users) {
    users.forEach((element) => _db.updateUserFromServer(element).then((value){
      print("Sync Local Updating Success.");
      users.removeWhere((el) => (el.uuid == element.uuid));
    }));
  }
  static void syncDeleteLocal(MyDatabase _db,List<User> users) {
    users.forEach((element) {
      _db.deleteUserFromServer(element.uuid).then((value) {

        print("Sync Local Deletion Success.");
      });
    });
  }

  static void syncDeleteServer(Worksheet sheet,List<Map<String,dynamic>> toDeleteUsersMap,List<User> sheetUsers) {
    if(toDeleteUsersMap.isNotEmpty) {
      toDeleteUsersMap.forEach((element) {
        sheetUsers.removeWhere((el) => el.uuid.toString() == element['uuid'].toString() );
        sheet.values.rowIndexOf(element['uuid']).then((value) =>
            sheet.deleteRow(value).then((value) => value));
      });
    }
  }
 
  static List<User> toDeleteUsers(ListQueue<User> deletedUsers) {
    if(deletedUsers.isNotEmpty) return List.from(deletedUsers.toList());
    else return new List<User>();
  }
  //retorna a lista de usuaios ja preparados para a Deletar na Sheet online
  static List<Map<String,dynamic>> toDeleteUsersMap(List<User> sheet,List<User> usersToDelete){
    if(sheet.isNotEmpty && usersToDelete.isNotEmpty){
    Queue<User> aux = Queue.from(usersToDelete);
    List<Map<String,dynamic>> forReturn  = new List<Map<String,dynamic>>();
    aux.forEach((temp){
      if(contain(sheet, temp)){
        forReturn.add(temp.toJsonSheet());
      }
    });
    return forReturn;
    }else return  new List<Map<String,dynamic>>();
  }

  //@to update users retorna todos os usuarios um List<User> para a edicao
  static List<User> toUpdateUsers(ListQueue<User> toUpdateList) {
    if(toUpdateList.isNotEmpty) return List.from(toUpdateList.toList());
    else return new List<User>();
  }
  //retorna a lista de usuaios ja preparados para a insercao na Sheet online
  static List<Map<String,dynamic>> toUpdateUsersMap(List<User> sheet,List<User> usersToUpdate){
    if(sheet.isNotEmpty && usersToUpdate.isNotEmpty){
    Queue<User> aux = Queue.from(usersToUpdate);
    List<Map<String,dynamic>> forReturn  = new List<Map<String,dynamic>>();
    aux.forEach((temp){
      if(containAfter(sheet, temp)){
          forReturn.add(temp.toJsonSheet());
      };
    });
    return forReturn;}else return new List<Map<String,dynamic>>();
  }
  static Future<List<User>> fromSheetUsers(Worksheet sheet,MyDatabase _db){
    GeneratedDatabase db  = _db;
    //sheet.values.map.allRows().then((value) => print(value==null));
    return sheet.values.map.allRows().then((maps){
      if(maps!=null){
      if(maps.isNotEmpty) return List.generate((maps).length, (i) => UserSheet().fromJsonSheet(maps.elementAt(i),db));
      else return new List<User>();
      }else{
        return new List<User>();
      }
    });
  }
  static bool contain(List<User> contains,User element){
    //contain.where((element) => false)

    if(contains!=null && contains.isNotEmpty){
    List<User> contain =  List.generate((contains).length,(i) => contains.elementAt(i));
    contain.forEach((element){
      print("initial : ${element.uuid}");
    });
     contain.retainWhere((e) => e.uuid == element.uuid);
    contain.forEach((element){
      print("\n final : ${element.uuid}");
    });
     if(contain.isNotEmpty) {return true;}else{ return false;}
    }else{
      return false;
    }

  }
  static bool containAfter(List<User> lista,User elemento){
    //print("elemnt: $elemento");
    //print("users: $lista");
    if(elemento != null && lista.isNotEmpty ){
    List<User> contain =  List.generate((lista).length,(i) => lista.elementAt(i));
    contain.retainWhere((list) => (list.uuid == elemento.uuid && elemento.updatedAt.isAfter(list.updatedAt)));
    if(contain.isNotEmpty) return true;
    else return false;
    }else{
      return false;
    }
  }
  static bool containBefore(List<User> lista,User elemento){
    //contain.where((element) => false)
    List<User> contain =  List.generate((lista).length,(i) => lista.elementAt(i));
    contain.retainWhere((list) => (list.uuid == elemento.uuid && elemento.updatedAt.isBefore(list.updatedAt)));
    if(contain.isNotEmpty) return true;
    else return false;
  }
  //fase 3
  //@to delete users retorna todos os usuarios um List<User> para a remocao


  //fase 4 processo inverso de Servidor para local
  static List<User> toInsertUsersLocal(List<User> sheet,List<User> localDbUsers){
    Queue<User> elSheet = Queue.from(sheet);
    List<User> forReturn  = new List<User>();
    List<User> aux = localDbUsers;

    elSheet.forEach((temp){
      aux = localDbUsers;
      print("local temp while : ${temp.nomeDoPesquisador}");
      if(contain(aux, temp) == false){
        print("local temp : ${temp.uuid}");
        forReturn.add(temp);
      }
    });

    return forReturn;
  }
  static List<User> toUpdateUsersLocal(List<User> sheet,List<User> localDbUsers) {
    Queue<User> elSheet = Queue.from(sheet);
    List<User> forReturn  = new List<User>();

    elSheet.forEach((temp){
      if(containAfter(localDbUsers, temp)){
        forReturn.add(temp);
      }
    });

    return forReturn;
  }
  static List<User> toDeleteUsersLocal(List<User> sheet,List<User> localDbUsers) {
    Queue<User> local = Queue.from(localDbUsers);
    List<User> forReturn  = new List<User>();

    local.forEach((temp){

      if(!contain(sheet, temp)){
        forReturn.add(temp);
      }
    });

    return forReturn;
  }

}